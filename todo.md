EXPO-BOILED DOWN LIST
• language settings
• audio screen
• formatting
• test suite

EXPO
• choose language screen
• different language settings
• hidden UIDs
• select from list of multiple audio recordings
• add formatting for subheadings and verse references
• full audio screen
• full audio player within song
• single verse horizontal view
• test suite
• telemetry metrics

DATA
• collections - data
• chandra shekhar transliterator changes
• hyphens
• move back r's and s's
• redo editor
• word-4-words
• russian, spanish, bengali + hindi translations
• copyright info for translations

COMPLETE
• ~collections screen~
• ~search index (incl content search)~
• ~collections viewer overhaul~
• ~collections viewer~


*TODO*
-------------------

+ hyphenation and spaces (use cues from 2018)
+ alphabet-list
[x] reconcile all audio
[x] reorder
[x] 'ব'/'ব্' --> 'ৱ'/'ৱ্' b/v
[x] load all songs for offline
[x] transliterate setting without restart
[x] don't show song if it's not actually available
[x] sorting order after transList change
[x] unDiac before sort
[x] md5


* alphabet-list:
  - https://www.npmjs.com/package/react-native-alphabet-sectionlist ||
  - https://github.com/i6mi6/react-native-alphabetlistview ||
  - https://github.com/yoonzm/react-native-alphabet-flat-list ||
  - https://www.npmjs.com/package/react-native-atoz-list ||
  - https://www.npmjs.com/package/react-native-atoz-listview ||
  - https://www.npmjs.com/package/alpha-scroll-flat-list ||
  - https://www.npmjs.com/package/react-native-sectionlist-contacts

* keep awake: https://docs.expo.io/versions/v36.0.0/sdk/keep-awake/
* sign-in:
  - https://docs.expo.io/versions/v36.0.0/sdk/google-sign-in/ || https://docs.expo.io/versions/v36.0.0/sdk/facebook/
  - https://docs.expo.io/versions/v36.0.0/sdk/app-auth/

trigger OTA update:
  ```
  $ expo publish
  ```

Login to appstores and expo using these:
* iOS
  https://developer.apple.com/account/
  *username*: sales@bhaktistore.com
  *password*: Guruseva1008
  ```
  expo build:ios
  ```

* Android
  https://play.google.com/apps/publish?pli=1
  *username*: gvpcompliance@gmail.com
  *password*: Guruseva108
  ```
  expo build:android -t app-bundle
  ```
  or
  ```
  expo build:android -t apk
  ```

* Expo
  https://expo.io/login
  *username*: gaudiyakirtan@gmail.com
  *password*: Gurud3va@expo


Here is the key used for android:
* Retrieve Keystore
    ```
    $ expo fetch:android:keystore
    ```
*  Keystore credentials
    ```
    .
    Keystore password: 99ea6fd702474dea814e531b78b55ef2
    Key alias:         QGdhdWRpeWFraXJ0YW4vZ2F1ZGl5YS1raXJ0YW4=
    Key password:      62ba42fc4d7f424d88faf36deef5472b

    Path to Keystore:  /Users/symbolic/+/gaudiyakirtan/ux-app/gaudiya-kirtan.jks
    ````



----

okay... so... here's what happens now:

"FIRST TIME"* YOU LAUNCH:
1) download the bundle of all songs in compressed form
2) decompress them
3) save each song for offline
4) download the list.md5 file (which we use to make sure list is up to date)
5) download the list.json file (the actual list)
_________
* if it fails to get the download, the next time still counts as "FIRST TIME"

SUBSEQUENT LAUNCHES:
1) download list.md5
2) load local list.json file -> but
  a) if the md5 of the local file matches continue to 3
  b) if the md5 mismatches the fresh lisit.md5 file, so re-download list.json
3) display the list
4) if search-text changes, go through the ENTIRE* list and check for fuzzy matches
5) if a song is selected load local song.json
  a) if the md5 of the local file matches what it says in list, display the local version
  b) if the md5 mismatches re-download the song.json
_________
* we shouldn't look through the entire list if they are ADDING a letter to the END of the search string (only if they are deleting a letter or inserting a letter in the middle). if they are only ADDING a letter to the END of the search string, then it's safe to progressively filter the list we already built. this will make it MUCH faster.


---
this is what my current task was involving:

BUILDING SEARCH-INDEX LOCALLY WOULD GO LIKE THIS:
1) first time you launch, build the search index for every song
  a) open a song, turn the contents into a fuzzy content string
2) if the list changes, rebuild ?just the different parts? (cuz it's slow)
  a) this requires knowing which things changed in list.json, which we don't with the current setup

.
.
.

after seeing all of this in practice, and meditating on it for many days, I finally see what we're doing wrong, and how to do it right. it's unnecessarily complex and doesn't give phone memory enough credit.

SO... here's how it should go:

include a compressed, full search-index + list + all-song-content in one big single file with multiple levels of fuzz as an asset in the app. in other words it's there when you first download the app, but in compressed form.

on first launch decompress it. load the entire object into phone memory,* and save a copy locally. the format is one big json object which is less than 10mb (not hard for phone RAM). the format is still one entry per song, but now all data is in one big file.
_________
* a note: by loading ALL songs into memory in the beginning (<10mb should be fine), it should be much much faster to switch between songs.

we create an md5 file on the server. when we make a change to a single song, the md5 will mismatch. we keep a record of historical md5's and all the changes since that md5 are stacked:

so let's say your md5 is this: b9658490bec9326bf88aeb3bd07b95b6

a5ac2ce7c5c149b02cacd457e01ba5f2 - delta-file0000
1870deb00bd33fab308d8402c5e754d6 - delta-file0001
b9658490bec9326bf88aeb3bd07b95b6 - delta-file0002
7e56ab9046ed0af42f40cde08c629ae1 - delta-file0003
917d1ca7a79de6f1876f61c6ba8b8d94 - delta-file0004
dafd4f2d841aeb50c9c2cfd0cf663116 - delta-file0005

now we know to download the last 3 delta files (3,4,5). each of those files just contains the changed entries (the complete song object, and multiple levels of fuzz for title and content).

this way we never rebuild the search index, we just over-write the necessary entries, and we don't need to keep track of changes between the last version of the list and the new one or anything like that. at the end of the process, if the md5's still mismatch, we can fallback to downloading the most up-to-date bundle from the server and decompressing it from scratch.

if we make a lot of changes all at once, we can also replace the compressed asset in the app, and everyone will sync up via OTA. in that case we'd delete the historical record of delta-files and force the md5's matching to fail, thereby triggering the fallback.

I imagine a lot of this doesn't make sense. but anyway, I have no one else to talk to about these things, and it helps to write it all out imagining someone else has to understand it all.

---

actually we can simplify it even more by collapsing all of the delta-files into one, even if a lot of entries are redundantly downloaded by people, since it will be so damn small. then if it gets really big, we do the asset update and everyone is forced to sync to the new baseline.

the previous set-up, when you add the search-index, was snow-balling into something way too complex, and needlessly so. the set-up I just described is something I can tackle and win at in a reasonable timeframe without giving myself brain-damage.

so this:
a5ac2ce7c5c149b02cacd457e01ba5f2 - delta-file0000
1870deb00bd33fab308d8402c5e754d6 - delta-file0001
b9658490bec9326bf88aeb3bd07b95b6 - delta-file0002
7e56ab9046ed0af42f40cde08c629ae1 - delta-file0003
917d1ca7a79de6f1876f61c6ba8b8d94 - delta-file0004
dafd4f2d841aeb50c9c2cfd0cf663116 - delta-file0005

becomes just this:
dafd4f2d841aeb50c9c2cfd0cf663116 - delta-file

but that single delta-file is all 5 delta-files combined. ya, you may cause people to re-download 10 songs just to fix a single typo, but 10 songs is on the order of tens of kilobytes, and you won't even notice it happening.
