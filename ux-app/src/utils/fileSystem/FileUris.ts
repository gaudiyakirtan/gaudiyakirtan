import * as ExpoFS from "expo-file-system";

export const ServerLocation = {
  gaudiyakirtan: "https://gaudiyakirtan.s3.amazonaws.com/"
};

export const LocalDirectory = {
  root: ExpoFS.documentDirectory,
  audio: `${ExpoFS.documentDirectory}audio`,
  artistPics: `${ExpoFS.documentDirectory}pics/artists`,
  collectionPics: `${ExpoFS.documentDirectory}pics/collections`
};

export const LocalFileUris = {
  null: `${LocalDirectory.root}null`,
  bundle: `${LocalDirectory.root}data.json`,
  recents: `${LocalDirectory.root}recents.json`,
  collections: `${LocalDirectory.root}collections.json`,
  settings: `${LocalDirectory.root}settings.json`,
  audio: (audioFileName: string) => `${LocalDirectory.audio}/${audioFileName}`,
  artistPic: (artistId: string) => `${LocalDirectory.artistPics}/${artistId}.jpg`,
  collectionPic: (imagePath: string) => `${LocalDirectory.collectionPics}/${imagePath}`
};

export const RemoteFileUris = {
  bundleMD5: `${ServerLocation.gaudiyakirtan}bundle_12.md5`,
  // collectionsMD5: `${ServerLocation.gaudiyakirtan}bundle.md5`,
  songListDeltas: `${ServerLocation.gaudiyakirtan}delta.json`,
  // collectionsDeltas: `${ServerLocation.gaudiyakirtan}collDelta.json`,
  audio: (audioFileName: string) => `${ServerLocation.gaudiyakirtan}audio/${audioFileName}`,
  artistPic: (artistId: string) => `${ServerLocation.gaudiyakirtan}artists/${artistId}.jpg`,
  collectionPic: (imagePath: string) => `${ServerLocation.gaudiyakirtan}collections/${imagePath}`
};
