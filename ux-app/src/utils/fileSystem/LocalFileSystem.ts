import * as ExpoFS from "expo-file-system";

import Debug from "../debug/Debug";

import { LocalDirectory } from "./FileUris";

interface ExistsInfo {
  exists: boolean;
  md5: string | undefined;
}

/**
 * In all the methods below, we need to await within the try{} block in order to catch any exception thrown
 * by Expo filesystem apis.
 */
export default class LocalFileSystem {
  public static async init(): Promise<void> {
    Object.keys(LocalDirectory).forEach(async key => {
      const directoryPath = LocalDirectory[key];
      try {
        await ExpoFS.makeDirectoryAsync(directoryPath, { intermediates: true });
      } catch (e) {
        Debug.log(`Exception thrown by LocalFileSystem.init: ${JSON.stringify(e)}`);
      }
    });
  }

  public static async exists(localFileUri: string): Promise<ExistsInfo> {
    const fileInfo = await this.info(localFileUri);
    return fileInfo
      ? { exists: fileInfo.exists, md5: fileInfo.md5 }
      : { exists: false, md5: undefined };
  }

  public static async info(localFileUri: string): Promise<ExpoFS.FileInfo | undefined> {
    try {
      const fileInfo = await ExpoFS.getInfoAsync(localFileUri, { md5: true });
      return fileInfo;
    } catch (e) {
      Debug.log(`Exception thrown by readAsStringAsync: ${JSON.stringify(e)}`);
      return undefined;
    }
  }

  public static async read(localFileUri: string): Promise<string | undefined> {
    try {
      const fileContent = await ExpoFS.readAsStringAsync(localFileUri);
      return fileContent;
    } catch (e) {
      Debug.log(`Exception thrown by readAsStringAsync: ${JSON.stringify(e)}`);
      return undefined;
    }
  }

  public static async write(localFileUri: string, content: string): Promise<void> {
    try {
      await ExpoFS.writeAsStringAsync(localFileUri, content);
    } catch (e) {
      Debug.log(`Exception thrown by writeAsStringAsync: ${JSON.stringify(e)}`);
    }
  }

  public static async delete(localFileUri: string): Promise<void> {
    try {
      await ExpoFS.deleteAsync(localFileUri, { idempotent: true });
    } catch (e) {
      Debug.log(`Exception thrown by deleteAsync: ${JSON.stringify(e)}`);
    }
  }

  public static async readDirectory(localDirUri: string): Promise<string[]> {
    try {
      const files = await ExpoFS.readDirectoryAsync(localDirUri);
      return files;
    } catch (e) {
      Debug.log(`Exception thrown by readDirectoryAsync: ${JSON.stringify(e)}`);
      return [];
    }
  }
}
