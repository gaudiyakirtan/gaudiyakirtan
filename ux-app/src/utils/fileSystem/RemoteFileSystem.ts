import * as ExpoFS from "expo-file-system";

import LocalFileSystem from "./LocalFileSystem";

export interface IRemoteFileSystemResponse {
  content?: string;
  error?: string;
}

export default class RemoteFileSystem {
  public static async read(
    remoteFileUri: string,
    localFileUri: string,
    md5: string,
    useCache = true
  ): Promise<IRemoteFileSystemResponse> {
    try {
      const localInfo = await LocalFileSystem.exists(localFileUri);
      const localExists = localInfo.exists;
      let backup = "";
      // console.log(remoteFileUri, md5, localInfo.md5); // eslint-disable-line
      if (localExists) {
        if (useCache && (md5 === localInfo.md5 || md5 === "ignore")) {
          // console.log("using local cache"); // eslint-disable-line
          const content = (await LocalFileSystem.read(localFileUri)) || "";
          return { content };
        } else {
          backup = (await LocalFileSystem.read(localFileUri)) || "";
          // console.log("backup:", backup.substr(0,108)); // eslint-disable-line
          // await LocalFileSystem.delete(localFileUri);
        }
      }
      // the logic around here is like this:
      // • if the md5 matches and it exists locally, show it
      // • if the md5 mismatches try to get it remotely
      // • if that fails, try again ignoring the md5 this time, to show local cache

      let content = "";
      try {
        await ExpoFS.downloadAsync(remoteFileUri, localFileUri);
        content = (await LocalFileSystem.read(localFileUri)) || "";
      } catch (e) {
        // console.log("using backup (rejected promise)"); // eslint-disable-line
        content = backup;
      }
      if (content[0] === "<" || !content) {
        // console.log("using backup (html error response or no content)"); // eslint-disable-line
        content = backup;
      }
      return { content };
    } catch (error) {
      return { error: JSON.stringify(error) };
    }
  }

  // Downloads without checking local cache
  public static downloadResumable(
    remoteFileUri: string,
    localFileUri: string,
    progressCallback: ExpoFS.DownloadProgressCallback
  ): ExpoFS.DownloadResumable {
    const downloadResumable = ExpoFS.createDownloadResumable(
      remoteFileUri,
      localFileUri,
      {},
      progressCallback
    );

    return downloadResumable;
  }
}
