enum RouteName {
  SongListNavigator = "SongListNavigator",
  SongList = "/songlist",
  SongListSearch = "/songlist/search",
  CollectionsNavigator = "CollectionsNavigator",
  Collections = "/collections",
  Song = "/song",
  AudioNavigator = "AudioNavigator",
  Audio = "/audio",
  AudioDownloads = "/audio/downloads",
  SettingsNavigator = "SettingsNavigator",
  Settings = "/settings",
  SettingsDedication = "/settings/dedication",
  SettingsAbout = "/settings/about",
  SettingsAboutKirtan = "/settings/about/kirtan",
  SettingsAboutGKProject = "/settings/about/gkproject",
  SettingsAboutGKTeam = "/settings/about/gkteam",
  SettingsAboutCopyright = "/settings/about/copyright",
  SettingsReport = "/settings/report",
  SettingsRequest = "/settings/request",
  SettingsShare = "/settings/share",
  SettingsCommunity = "/settings/community",
  SettingsContact = "/settings/contact"
}

export interface ISongRouteParams {
  songUid: string;
  songMD5: string;
}

export interface ICollectionsRouteParams {
  uidPath: string;
}

export type IRouteParams = ISongRouteParams | ICollectionsRouteParams;
export default RouteName;
