/* eslint-disable max-classes-per-file */
import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import SongList from "../../components/screens/songList/SongList";
import SongListSearch from "../../components/screens/songList/SongListSearch";
import Collections from "../../components/screens/collections/Collections";
import Audio from "../../components/screens/audio/Audio";
import { Downloads } from "../../components/screens/audio/downloads/Downloads";
import Settings from "../../components/screens/settings/Settings";
import { Song } from "../../components/screens/song/Song";
import { AppFooter } from "../../components/appFooter/AppFooter";
import SettingsDedication from "../../components/screens/settings/sections/SettingsDedication";
import SettingsAbout from "../../components/screens/settings/sections/about/SettingsAbout";
import SettingsReport from "../../components/screens/settings/sections/SettingsReport";
import SettingsRequest from "../../components/screens/settings/sections/SettingsRequest";
import SettingsShare from "../../components/screens/settings/sections/SettingsShare";
import SettingsCommunity from "../../components/screens/settings/sections/SettingsCommunity";
import SettingsContact from "../../components/screens/settings/sections/SettingsContact";
import SettingsAboutKirtan from "../../components/screens/settings/sections/about/SettingsAboutKirtan";
import SettingsAboutGKProject from "../../components/screens/settings/sections/about/SettingsAboutGKProject";
import SettingsAboutGKTeam from "../../components/screens/settings/sections/about/SettingsAboutGKTeam";
import SettingsAboutCopyright from "../../components/screens/settings/sections/about/SettingsAboutCopyright";

import RouteName from "./RouteName";

class SearchNavigator extends React.Component {
  public render(): JSX.Element {
    const Stack = createStackNavigator();

    return (
      <Stack.Navigator headerMode="none">
        <Stack.Screen name={RouteName.SongList} component={SongList} />
        <Stack.Screen name={RouteName.SongListSearch} component={SongListSearch} />
      </Stack.Navigator>
    );
  }
}

class CollectionsNavigator extends React.Component {
  public render(): JSX.Element {
    const Stack = createStackNavigator();

    return (
      <Stack.Navigator headerMode="none">
        <Stack.Screen name={RouteName.Collections} component={Collections} />
      </Stack.Navigator>
    );
  }
}

class AudioNavigator extends React.Component {
  public render(): JSX.Element {
    const Stack = createStackNavigator();

    return (
      <Stack.Navigator headerMode="none">
        <Stack.Screen name={RouteName.Audio} component={Audio} />
        <Stack.Screen name={RouteName.AudioDownloads} component={Downloads} />
      </Stack.Navigator>
    );
  }
}

class SettingsNavigator extends React.Component {
  public render(): JSX.Element {
    const Stack = createStackNavigator();

    return (
      <Stack.Navigator headerMode="none">
        <Stack.Screen name={RouteName.Settings} component={Settings} />
        <Stack.Screen name={RouteName.SettingsDedication} component={SettingsDedication} />
        <Stack.Screen name={RouteName.SettingsAbout} component={SettingsAbout} />
        <Stack.Screen name={RouteName.SettingsAboutKirtan} component={SettingsAboutKirtan} />
        <Stack.Screen name={RouteName.SettingsAboutGKProject} component={SettingsAboutGKProject} />
        <Stack.Screen name={RouteName.SettingsAboutGKTeam} component={SettingsAboutGKTeam} />
        <Stack.Screen name={RouteName.SettingsAboutCopyright} component={SettingsAboutCopyright} />
        <Stack.Screen name={RouteName.SettingsReport} component={SettingsReport} />
        <Stack.Screen name={RouteName.SettingsRequest} component={SettingsRequest} />
        <Stack.Screen name={RouteName.SettingsShare} component={SettingsShare} />
        <Stack.Screen name={RouteName.SettingsCommunity} component={SettingsCommunity} />
        <Stack.Screen name={RouteName.SettingsContact} component={SettingsContact} />
      </Stack.Navigator>
    );
  }
}

export default class BottomTabNavigator extends React.Component {
  public render(): JSX.Element {
    const Tab = createBottomTabNavigator();
    return (
      <Tab.Navigator tabBar={() => <AppFooter />} initialRouteName={RouteName.Song}>
        <Tab.Screen name={RouteName.SongListNavigator} component={SearchNavigator} />
        <Tab.Screen name={RouteName.CollectionsNavigator} component={CollectionsNavigator} />
        <Tab.Screen name={RouteName.Song} component={Song} />
        <Tab.Screen name={RouteName.AudioNavigator} component={AudioNavigator} />
        <Tab.Screen name={RouteName.SettingsNavigator} component={SettingsNavigator} />
      </Tab.Navigator>
    );
  }
}
