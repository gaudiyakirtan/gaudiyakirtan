/* eslint-disable @typescript-eslint/no-non-null-assertion */

import { NavigationContainerRef, Route, NavigationState } from "@react-navigation/native";

import RouteName, { IRouteParams } from "./RouteName";

export default class Navigation {
  private static navigation: NavigationContainerRef | null;

  /**
   * Recursive methods to parse rootNavState (Example JSON below at end of file)
   */
  private static getCurrentRoute(): Route<string> | undefined {
    if (!this.navigation) {
      return undefined;
    }
    const rootNavState = this.navigation.getRootState();
    return this.getCurrentRouteRecursive(rootNavState);
  }

  private static getCurrentRouteRecursive(navState: NavigationState): Route<string> {
    const { routes, index } = navState;
    const currentRoute = routes[index];

    const childNavState = currentRoute.state;
    if (childNavState) {
      return this.getCurrentRouteRecursive(childNavState as NavigationState);
    }

    return currentRoute;
  }

  private static addChildRouteNamesRecursive(
    navState: NavigationState | undefined,
    childRouteNames: RouteName[]
  ): void {
    if (navState && navState.routes) {
      navState.routes.forEach(route => {
        childRouteNames.push(route.name as RouteName);
        this.addChildRouteNamesRecursive(route.state as NavigationState, childRouteNames);
      });
    }
  }

  private static getAllOpenRoutes(): Route<string>[] {
    if (!this.navigation) {
      return [];
    }
    const openRoutes: Route<string>[] = [];
    const rootNavState = this.navigation.getRootState();

    this.addOpenRoutesRecursive(rootNavState, openRoutes);
    return openRoutes;
  }

  private static addOpenRoutesRecursive(
    navState: NavigationState,
    openRoutes: Route<string>[]
  ): void {
    if (navState && navState.routes) {
      navState.routes.forEach(route => {
        openRoutes.push(route);
        this.addOpenRoutesRecursive(route.state as NavigationState, openRoutes);
      });
    }
  }

  /**
   * Route and params info
   */
  public static getCurrentRouteName(): RouteName | undefined {
    const currentRoute = this.getCurrentRoute();
    // TODO : Hack to highlight Song footer button on initial load. Fix this.
    return currentRoute ? (currentRoute.name as RouteName) : RouteName.Song;
  }

  public static getRouteParams(routeName: RouteName): IRouteParams | undefined {
    if (!this.navigation) {
      return undefined;
    }

    let routeParams: IRouteParams | undefined;
    const allOpenRoutes = this.getAllOpenRoutes();
    allOpenRoutes.forEach(openRoute => {
      if (openRoute.name === routeName) {
        routeParams = openRoute.params as IRouteParams;
      }
    });
    return routeParams;
  }

  public static getChildRouteNames(routeName: RouteName): RouteName[] {
    if (!this.navigation) {
      return [];
    }
    const childRouteNames: RouteName[] = [];

    const rootNavState = this.navigation.getRootState();
    rootNavState.routes.forEach(route => {
      if (route.name === routeName) {
        this.addChildRouteNamesRecursive(route.state as NavigationState, childRouteNames);
      }
    });

    return childRouteNames;
  }

  /**
   * Actions
   */
  public static init(navigation: NavigationContainerRef | null): void {
    this.navigation = navigation;
  }

  public static navigate(routeName: RouteName, routeParams?: IRouteParams): void {
    if (this.navigation) {
      this.navigation.navigate(routeName, routeParams);
    }
  }

  public static goBack(): void {
    if (this.navigation) {
      this.navigation.goBack();
    }
  }

  public static listenForRouteChange(callback: () => void): void {
    if (this.navigation) {
      this.navigation.addListener("state", callback);
    }
  }
}

/*
Sample rootNavState with Kirtans search, collections ("BVT/SAR") and song ("N9") open :

{
    "stale": false,
    "type": "tab",
    "key": "tab-_mRhSEQdHPCy1460HuBJB",
    "routeNames": ["SongListNavigator", "CollectionsNavigator", "/song", "AudioNavigator", "/settings"],
    "index": 0,
    "history": [{
            "type": "route",
            "key": "/song-kkarvQwaZxghffeI-B_gd"
        }, {
            "type": "route",
            "key": "CollectionsNavigator-K89cRr_FuvSkjzMtOHFrr"
        }, {
            "type": "route",
            "key": "SongListNavigator-kEUiSVAr5qn4P5sG9KAYd"
        }
    ],
    "routes": [{
            "name": "SongListNavigator",
            "key": "SongListNavigator-kEUiSVAr5qn4P5sG9KAYd",
            "state": {
                "stale": false,
                "type": "stack",
                "key": "stack-igNYlL7Zw3qFSNhjYTa_N",
                "routeNames": ["/songlist", "/songlist/search"],
                "index": 1,
                "routes": [{
                        "key": "/songlist-nsIaiIDse-xXtTeCSO4a1",
                        "name": "/songlist"
                    }, {
                        "key": "/songlist/search-5ztO_Y8SwrLA402vRb16C",
                        "name": "/songlist/search"
                    }
                ]
            }
        }, {
            "name": "CollectionsNavigator",
            "key": "CollectionsNavigator-K89cRr_FuvSkjzMtOHFrr",
            "state": {
                "stale": false,
                "type": "stack",
                "key": "stack-8_lWmQeRC4gZ6lS_hTLU8",
                "index": 0,
                "routeNames": ["/collections"],
                "routes": [{
                        "key": "/collections-DBdqKVV4rvuGrPmSkAdTp",
                        "name": "/collections",
                        "params": {
                            "uidPath": "BVT/SAR"
                        }
                    }
                ]
            }
        }, {
            "name": "/song",
            "key": "/song-kkarvQwaZxghffeI-B_gd",
            "params": {
                "songUid": "N9"
            }
        }, {
            "name": "AudioNavigator",
            "key": "AudioNavigator-UPTmDFJh9mJdj-E2TWAYB"
        }, {
            "name": "/settings",
            "key": "/settings-dtmOEK0_ckdeJKPTLj6Oq"
        }
    ]
}


*/
