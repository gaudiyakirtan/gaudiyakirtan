import RouteName from "../navigation/RouteName";

type ScreenRef = { forceUpdate: () => void };
type ScreenKey = RouteName | "AppFooter";

// We had to do this work around and forceUpdate active screens because
// React Navigation isn't updating the active screens in the other tabs
// whenever settings like Theme are changed - even if we rerender the entire App.
export default class ActiveScreenRefs {
  private static activeScreenRefs: { [key in ScreenKey]?: ScreenRef } = {};

  public static add(key: ScreenKey, ref: ScreenRef): void {
    this.activeScreenRefs[key] = ref;
  }

  public static get(key: ScreenKey): ScreenRef {
    return this.activeScreenRefs[key] || { forceUpdate: () => {} };
  }
}
