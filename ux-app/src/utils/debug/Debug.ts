export default class Debug {
  // eslint-disable-next-line
  public static log(message: any): void {
    if (__DEV__) {
      // eslint-disable-next-line no-console
      console.log(message);
    }
  }
}
