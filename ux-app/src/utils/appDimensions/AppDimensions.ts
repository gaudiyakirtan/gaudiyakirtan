const AppDimensions = {
  HeaderHeight: 50,
  FooterHeight: 55,
  AudioBarHeight: 50,
  TouchableWidth: 50,
  TouchableHeight: 50,
  MoreIconWidth: 40,
  PicThumbNailSize: 44,
  UidThumbNailSize: 36,
  MinimizedSliderHeight: 2,
  ListHeaderHeight: 30,
  ListItemHeight: 60
};

export default AppDimensions;
