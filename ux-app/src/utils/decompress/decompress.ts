// @ts-nocheck
import base64 from 'base-64'
import utf8 from 'utf8'

// ==== LZW DECOMPRESSION :

function lzw_decode(data){
  const dict = {};
  let currChar = String.fromCharCode(data[0]);
  let oldPhrase = currChar;
  let out = [currChar];
  let code = 57344;
  let phrase;
  for (let i = 1; i < data.length; i++) {
    const currCode = data[i];
    if (currCode < 57344) {
      phrase = String.fromCharCode(data[i]);
    } else {
      phrase = dict[currCode] ? dict[currCode] : oldPhrase + currChar;
    }
    out += phrase;
    currChar = phrase[0];
    dict[code] = oldPhrase + currChar;
    code++;
    oldPhrase = phrase;
  }
  return out;
}

// ==== CONVERT TO ARRAY & DECOMPRESS

function arrayFromBytes(bytes) {
  let x;
  let a;
  let b;
  let c;
  const array = [];
  for (let i = 0; i < bytes.length; i += 3) {
    a = bytes[i + 0].charCodeAt();
    b = bytes[i + 1].charCodeAt();
    c = bytes[i + 2].charCodeAt();
    x = a * (1 << 16) + b * (1 << 8) + c;
    array.push(x);
  }
  // console.log(array.slice(0, 64));
  return array;
}

export default function decompress(string) {
  // console.log(string.substr(-108));
  string = base64.decode(string);
  string = utf8.decode(string);
  // console.log(string.substr(-108));
  var comp = arrayFromBytes(string);
  return lzw_decode(comp);
}
