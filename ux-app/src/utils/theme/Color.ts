enum Color {
  // Accent
  Blue = "#0000FF",
  Gold = "#FFDD00",
  BlueDark = "#305fa6",
  GoldDark = "#F08000",

  // Black & White
  Black = "#111111",
  BlackLight = "#181818",
  White = "#ffffff",
  WhiteDark = "#ffeedd",
  WhiteDarker = "#f4e7d4",
  // Grey - Higher the number, closer to white
  Grey18 = "#181818",
  Grey21 = "#212121",
  Grey30 = "#303030",
  Grey42 = "#424242",
  Grey50 = "#505050",
  Grey85 = "#858585",
  GreyB3 = "#b3b3b3",
  GreyC0 = "#c0c0c0",
  // Others
  // From MaterialDesign color palette : https://material.io/archive/guidelines/style/color.html#color-color-palette
  Red = "#F44336",
  Red200 = "#EF9A9A",
  Pink = "#E91E63",
  Pink200 = "#F48FB1",
  Pink900 = "#880E4F",
  Purple = "#9C27B0",
  Purple200 = "#CE93D8",
  Purple900 = "#4A148C",
  DeepPurple = "#673AB7",
  DeepPurple200 = "#B39DDB",
  Cyan = "#00BCD4",
  Cyan100 = "#B2EBF2",
  Cyan900 = "#006064",
  Teal = "#009688",
  Teal100 = "#B2DFDB",
  Teal900 = "#004D40",
  Green = "#4CAF50",
  Green200 = "#A5D6A7",
  Green900 = "#1B5E20",
  LightGreen = "#8BC34A",
  Lime = "#CDDC39",
  Lime900 = "#827717",
  Yellow = "#FFEB3B",
  Amber = "#FFC107",
  Amber100 = "#FFECB3",
  Orange = "#FF9800",
  Orange200 = "#FFCC80",
  Orange900 = "#E65100",
  DeepOrange = "#FF5722",
  Brown = "#795548",
  Brown300 = "#A1887F"
}

export default Color;
