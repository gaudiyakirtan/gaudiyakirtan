import * as React from "react";

import ISong from "../../data/models/server/ISong";

export interface IAudioContext {
  readonly song?: ISong;
  readonly audioBarOpen: boolean;
  recreate(song: ISong | undefined, audioBarOpen: boolean): void;
  reset(): void;
  audioExists(): boolean;
}

export const defaultAudioContext: IAudioContext = {
  song: undefined,
  audioBarOpen: false,
  recreate: () => {},
  reset: () => {},
  audioExists: () => false
};

export const AudioContextType = React.createContext<IAudioContext>(defaultAudioContext);
