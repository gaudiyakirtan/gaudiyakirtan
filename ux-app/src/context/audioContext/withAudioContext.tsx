// tslint:disable:file-name-casing
import * as React from "react";

import { IAudioContext, AudioContextType } from "./IAudioContext";

export interface IAudioContextProps {
  audioContext: IAudioContext;
}

export default function withAudioContext<IOriginalProps extends IAudioContextProps>(
  component: React.ComponentType<IOriginalProps>
): React.ComponentClass<Omit<IOriginalProps, keyof IAudioContextProps>> {
  return class ComponentWithFeatureContext extends React.Component<IOriginalProps> {
    public static contextType: React.Context<IAudioContext> = AudioContextType;
    public context!: React.ContextType<typeof AudioContextType>;

    public constructor(props: Omit<IOriginalProps, keyof IAudioContextProps>) {
      super(props as IOriginalProps);
    }

    public render(): React.ReactNode {
      return React.createElement(
        component,
        // eslint-disable-next-line prefer-object-spread
        Object.assign({ audioContext: this.context }, this.props)
      );
    }
  };
}
