import ISong from "../../data/models/server/ISong";

import { IAudioContext } from "./IAudioContext";

export default class AudioContext implements IAudioContext {
  public readonly song?: ISong;
  public readonly audioBarOpen: boolean;

  private onRecreate: (song: ISong | undefined, audioBarOpen: boolean) => void;

  public constructor(
    song: ISong | undefined,
    audioBarOpen: boolean,
    onRecreate: (song: ISong | undefined, audioBarOpen: boolean) => void
  ) {
    this.song = song;
    this.audioBarOpen = audioBarOpen;
    this.onRecreate = onRecreate;
  }

  public recreate(song: ISong | undefined, audioBarOpen: boolean): void {
    this.onRecreate(song, audioBarOpen);
  }

  public reset(): void {
    this.onRecreate(undefined, false);
  }

  public audioExists(): boolean {
    return Boolean(this.song && this.song.audio.length > 0);
  }
}
