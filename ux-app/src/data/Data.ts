/* eslint-disable dot-notation */
import * as ExpoFS from "expo-file-system";

import decompress from "../utils/decompress/decompress";
import RemoteFileSystem from "../utils/fileSystem/RemoteFileSystem";
import LocalFileSystem from "../utils/fileSystem/LocalFileSystem";
import { RemoteFileUris, LocalFileUris } from "../utils/fileSystem/FileUris";
import transliterate from "../utils/transliteration/transliterator";
import Settings from "../utils/settings/Settings";
import Debug from "../utils/debug/Debug";

import ISong, { IVerse, ILine, IWord } from "./models/server/ISong";
import ISongList from "./models/server/ISongList";
import { ICollections, ICollection } from "./models/server/ICollection";
import fetchData from "./datalz";
import fetchCollectionsData from "./collectionslz";

export default class Data {
  private static retries = 0;
  private static retryLimit = 2;
  private static SongData: {};
  private static SongList: ISongList;
  private static CollectionsData: ICollections;
  private static RecentsData: ICollections;
  public static DebugReport = {};

  public static async LoadBundle(): Promise<void> {
    // force the bundle to be re-extracted if OTA updates the asset... how do we know?
    // the easiest way is to just keep a setting with an incrementing version
    const s = Settings.get();
    const dataVersion = fetchData(true);
    const newDataVersion = dataVersion !== s.dataVersion;
    console.log("current Data version:", s.dataVersion); // eslint-disable-line
    console.log("file Data version #:", dataVersion); // eslint-disable-line
    console.log("new Data version?:", newDataVersion); // eslint-disable-line

    // if there's no bundle yet, extract it from asset
    let localInfo = await LocalFileSystem.exists(LocalFileUris.bundle);
    const localExists = localInfo.exists;
    if (!localExists || newDataVersion) {
      const data = fetchData();
      if (data) {
        await LocalFileSystem.write(LocalFileUris.bundle, `${decompress(data)}`);
        localInfo = await LocalFileSystem.exists(LocalFileUris.bundle);
        Settings.set({ ...s, dataVersion });
      }
    }

    // read delta-md5 from server
    const md5 = await RemoteFileSystem.read(
      RemoteFileUris.bundleMD5,
      LocalFileUris.null,
      "ignore",
      false // useCache
    );
    if (!md5.content) {
      md5.content = "";
    }

    this.DebugReport["Date"] = new Date().toUTCString();
    this.DebugReport["settings Data Version"] = s.dataVersion;
    this.DebugReport["datalz.ts Data Version"] = dataVersion;
    this.DebugReport["remote Data MD5"] = md5.content.trim();
    this.DebugReport["local Data MD5"] = localInfo.md5;

    // if there are NEW deltas, these should mismatch.
    // WARNING: if the deltas included a song that was later reverted, it won't ever match!
    // but I haven't figured out how to handle that yet (delete bundle after N tries?)
    if (md5.content.trim() !== localInfo.md5) {
      // console.log("bundle md5 mismatch",                // eslint-disable-line
      //             "\n  expecting:", md5.content.trim(), // eslint-disable-line
      //             "\n  got:      ", localInfo.md5);     // eslint-disable-line

      // download the deltas
      const songDataString = await LocalFileSystem.read(LocalFileUris.bundle);
      const DeltaString = await RemoteFileSystem.read(
        RemoteFileUris.songListDeltas,
        LocalFileUris.null,
        "ignore",
        false // useCache
      );
      this.SongData = songDataString ? JSON.parse(songDataString) : {};
      const Deltas = JSON.parse(`${DeltaString.content}`);

      this.DebugReport["Data MD5 mismatch"] = "true";
      if (DeltaString.content) {
        this.DebugReport["Delta Bytecount"] = DeltaString.content.length;
      } else {
        this.DebugReport["Delta is undefined"] = "true";
      }

      // now we merge the deltas and save the updated bundle
      Object.keys(Deltas).forEach(entry => {
        this.SongData[entry] = Deltas[entry];
      });
      LocalFileSystem.write(LocalFileUris.bundle, JSON.stringify(this.SongData));
      localInfo = await LocalFileSystem.exists(LocalFileUris.bundle);
      if (md5.content.trim() !== localInfo.md5) {
        this.DebugReport["Data MD5 mismatch after Delta"] = "true";
        this.DebugReport["Delete local Data file and retry"] = "true";
        await LocalFileSystem.delete(LocalFileUris.bundle);
        this.retries++;
        if (this.retries < this.retryLimit) {
          this.LoadBundle();
        }
      }
    } // else console.log("bundle up to date"); // eslint-disable-line
  }

  public static async LoadSongList(): Promise<ISongList> {
    const songDataString = await LocalFileSystem.read(LocalFileUris.bundle);
    this.SongData = songDataString ? JSON.parse(songDataString) : {};
    this.SongList = [];
    Object.keys(this.SongData).forEach(entry => {
      const song = Data.SongData[entry].song;

      // UX side hack to remove the ".Try Settings > List Language :)" entry
      // TODO : Remove it on the backend and then remove this UX hack
      if (song.uid === "A0") {
        return;
      }

      this.SongList.push({
        uid: song.uid,
        title: song.title,
        fuzzyTitle: Data.SongData[entry].titleFuzz,
        fuzzyContent: Data.SongData[entry].contentFuzz,
        author: song.author,
        language: song.language,
        md5: song.md5,
        audio: song.audio
      });
    });
    return this.SongList;
  }

  public static FetchSongData(uid: string, addToRecents: boolean): ISong {
    if (addToRecents) {
      this.SaveRecent(uid);
    }
    return this.SongData[uid].song;
  }

  public static ShareSong(uid: string): string {
    const song = this.SongData[uid].song;
    let output = "Download The Gaudiya Kirtan App from http://gaudiyakirtan.com\n\n";

    // Title
    output += `*${song.title
      .replace(/^(‘|\(){0,1}(.)/, (_m: string, a: string, b: string) => {
        return (a || "") + b.toUpperCase();
      })
      .replace(/^\./, "")}*\n`;

    // Author
    output += `*${song.author}*\n\n`;

    song.verses.map((verse: IVerse) => {
      // Original Script
      verse.lines.forEach((line: ILine) => {
        let lineStr = "";
        line.forEach((word: IWord) => {
          lineStr += `${word.w}${word.h}`;
        });
        output += `${lineStr.replace(/ৱ/g, "ব")}\n`;
      });
      output += "\n";

      // Transliteration
      verse.lines.forEach((line: ILine) => {
        let lineStr = "";
        line.forEach((word: IWord) => {
          lineStr += `${word.w}${word.h}`;
        });
        output += `${transliterate(lineStr, song.language)}\n`;
      });
      output += "\n";

      // Synonyms
      verse.lines.forEach((line: ILine, lineIndex: number) => {
        let lineStr = "";
        line.forEach((word: IWord) => {
          lineStr += `*${transliterate(word.w, song.language)}* — ${word.s.eng};  `;
        });
        output += `(${lineIndex + 1}) ${lineStr}\n`;
      });
      output += "\n";

      // Translation
      output += `${verse.translation.eng}\n_______________\n\n`;

      return "";
    });

    return output;
  }

  public static fetchRecentsData(): ICollections {
    return this.RecentsData;
  }

  public static async LoadCollections(): Promise<ICollections> {
    // force the bundle to be re-extracted if OTA updates the asset... how do we know?
    // the easiest way is to just keep a setting with an incrementing version
    const s = Settings.get();
    const collDataVersion = fetchCollectionsData(true) as string;
    const newCollDataVersion = collDataVersion !== s.collDataVersion;
    // console.log("current Collections version:", s.collDataVersion); // eslint-disable-line
    // console.log("file Collections version #:", collDataVersion); // eslint-disable-line
    // console.log("new Collections version?:", newCollDataVersion); // eslint-disable-line

    // if there's no bundle yet, extract it from asset
    let localInfo = await LocalFileSystem.exists(LocalFileUris.collections);
    let localExists = localInfo.exists;
    if (!localExists || newCollDataVersion) {
      const data = fetchCollectionsData() as ICollections;
      if (data) {
        // JSON.stringify(data) ---> `${decompress(data)}`
        await LocalFileSystem.write(LocalFileUris.collections, JSON.stringify(data));
        localInfo = await LocalFileSystem.exists(LocalFileUris.collections);
        Settings.set({ ...s, collDataVersion });
      }
    }

    // pre-pend recents collection
    localInfo = await LocalFileSystem.exists(LocalFileUris.recents);
    localExists = localInfo.exists;
    if (!localExists) {
      LocalFileSystem.write(
        LocalFileUris.recents,
        JSON.stringify({
          type: "collection",
          uid: "RECENTS",
          title: "সাম্প্রতিক ~ Recents",
          count: 0,
          children: []
        })
      );
    }

    this.DebugReport["settings Collections Version"] = s.collDataVersion;
    this.DebugReport["collectionslz.ts Version"] = collDataVersion;
    // this.DebugReport["remote Collections MD5"] = md5.content.trim();
    // this.DebugReport["local Collections MD5"] = localInfo.md5;

    // TO-DO: this can have a delta file and md5 check like SongData
    /*
    // read delta-md5 from server
    const md5 = await RemoteFileSystem.read(
      RemoteFileUris.collectionsMD5,
      LocalFileUris.null,
      "ignore",
      false // useCache
    );
    if (!md5.content) md5.content = "";

    // if there are NEW deltas, these should mismatch.
    // WARNING: if the deltas included a song that was later reverted, it won't ever match!
    // but I haven't figured out how to handle that yet (delete bundle after N tries?)
    if (md5.content.trim() !== localInfo.md5) {
      // console.log("collections md5 mismatch",           // eslint-disable-line
      //             "\n  expecting:", md5.content.trim(), // eslint-disable-line
      //             "\n  got:      ", localInfo.md5);     // eslint-disable-line

      // download the deltas
      const collectionsString = await LocalFileSystem.read(LocalFileUris.collections);
      const collDeltaString = await RemoteFileSystem.read(
        RemoteFileUris.collectionsDeltas,
        LocalFileUris.null,
        "ignore",
        false // useCache
      );
      this.CollectionsData = JSON.parse(collectionsString);
      const Deltas = JSON.parse(`${collDeltaString.content}`);

      // now we merge the deltas and save the updated bundle
      Object.keys(Deltas).forEach(entry => {
        this.CollectionsData[entry] = Deltas[entry];
      });
      LocalFileSystem.write(LocalFileUris.collections, JSON.stringify(this.CollectionsData));
    } // else console.log("collections up to date"); // eslint-disable-line
    */
    const collectionsString = await LocalFileSystem.read(LocalFileUris.collections);
    const recentsString = await LocalFileSystem.read(LocalFileUris.recents);

    this.CollectionsData = collectionsString ? JSON.parse(collectionsString) : {};
    this.RecentsData = recentsString ? JSON.parse(recentsString) : {};

    const mySongBook: ICollection[] = [];
    const userMatha = s.userMatha;
    if (this.CollectionsData["children"][0]["children"] && userMatha) {
      this.CollectionsData["children"][0]["children"].map(c => {
        if (c.title.match(userMatha) && c) {
          mySongBook.push(c);
        }
        return false;
      });
    }

    return {
      children: Array.prototype.concat(
        [this.RecentsData],
        mySongBook,
        this.CollectionsData["children"]
      )
    };
  }

  public static async LoadArtistPic(artistId: string): Promise<void> {
    const localFileUri = LocalFileUris.artistPic(artistId);
    const remoteFileUri = RemoteFileUris.artistPic(artistId);

    const localInfo = await LocalFileSystem.exists(localFileUri);
    if (!localInfo.exists) {
      let result: ExpoFS.FileSystemDownloadResult | undefined;
      try {
        result = await ExpoFS.downloadAsync(remoteFileUri, localFileUri);
      } catch (e) {
        result = undefined;
      }

      if (!result || result.status !== 200) {
        await LocalFileSystem.delete(localFileUri);
        Debug.log(
          `Failed to download artist pic for artistId '${artistId}'. ${
            result ? `Http result status = ${result.status}` : ""
          }`
        );
      }
    }
  }

  public static async LoadCollectionPic(imagePath: string): Promise<void> {
    const localFileUri = LocalFileUris.collectionPic(imagePath);
    const remoteFileUri = RemoteFileUris.collectionPic(imagePath);

    const localInfo = await LocalFileSystem.exists(localFileUri);
    if (!localInfo.exists) {
      let result: ExpoFS.FileSystemDownloadResult | undefined;
      try {
        result = await ExpoFS.downloadAsync(remoteFileUri, localFileUri);
      } catch (e) {
        result = undefined;
      }

      if (!result || result.status !== 200) {
        await LocalFileSystem.delete(localFileUri);
        Debug.log(
          `Failed to download collection pic for imagePath '${imagePath}'. ${
            result ? `Http result status = ${result.status}` : ""
          }`
        );
      }
    }
  }

  private static async SaveRecent(uid: string): Promise<void> {
    // remove duplicate entries
    if (!this.RecentsData) {
      await this.LoadCollections();
    }

    const newRecentsChildren = [...this.RecentsData["children"]];
    this.RecentsData["children"] = [];
    this.RecentsData["children"].push({ type: "song", uid, title: this.SongData[uid].song.title });

    for (let i = 0; i < Math.min(newRecentsChildren.length, 11); i++) {
      if (newRecentsChildren[i].uid !== uid) {
        this.RecentsData["children"].push(newRecentsChildren[i]);
      }
    }
    this.RecentsData["count"] = this.RecentsData["children"].length;

    setTimeout(async () => {
      LocalFileSystem.write(LocalFileUris.recents, JSON.stringify(this.RecentsData));
    }, 0);
  }
}
