/**
 * Note : Whenever authors data is updated on the server side, please update this list manually.
 */

const Authors: { [authorCode: string]: string } = {
  bt: "শ্রীল ভক্তিৱিনোদ ঠাকুর",
  ndt: "শ্রীল নরোত্তম দাস ঠাকুর",
  ldt: "শ্রীল লোচন দাস ঠাকুর",
  vct: "শ্রীল ৱিশ্ৱনাথ চক্রৱর্তী ঠাকুর",
  vdt: "শ্রীল ৱৃন্দাৱন দাস ঠাকুর",
  kkg: "শ্রীল কৃষ্ণদাস কৱিরাজ গোস্ৱামী",
  bsst: "শ্রীল ভক্তিসিদ্ধান্ত সরস্ৱতী গোস্ৱামী প্রভুপাদ",
  rg: "শ্রীল রূপ গোস্ৱামী",
  rdg: "শ্রীল রঘুনাথ দাস গোস্ৱামী",
  pdt: "শ্রীল প্রেমানন্দ দাস ঠাকুর",
  "?": "অজানা লেখক ~ Unknown Author"
};

export default Authors;
