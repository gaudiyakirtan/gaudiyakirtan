import { IAudio } from "./ISong";

export interface ISongListSong {
  uid: string;
  md5: string;
  title: string;
  fuzzyTitle: string;
  fuzzyContent: string;
  author: string;
  language: string;
  audio: Array<IAudio>;
}

type ISongList = ISongListSong[];
export default ISongList;
