/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable max-len */

import { ICollections } from "./models/server/ICollection";

export default function fetchData(versionOnly = false): string | ICollections {
  if (versionOnly) {
    return version;
  }
  return data;
}

/**
 * Below data is supposed to be :
 *  1. Auto generated on the backend from list.col and other *.col files using build-collections.js script.
 *  2. And then compressed by compress.js
 *  3. And then the compressed data copied and pasted below.
 *
 *     list.col : https://gitlab.com/gaudiyakirtan/gkcontent/-/tree/master/collections
 *     build-collections.js / compress.js : https://gitlab.com/gaudiyakirtan/gkcontent/-/tree/master/batch-processors
 *
 * But when we redesigned collections, we needed changes in the structure of the data.
 * Since the backend is going to change soon we didn't bother about changing it in the backend *.col files as it was time consuming
 * and anyways they are going to go away soon.
 * So instead we directly changed it in the UX side in the json format which is much easier to change.
 */

// TODO : Below json needs to be compressed using compress.js and then pasted back here and version increased.
//        Also when this is done, JSON.stringify(data) in Data.LoadCollections():200 needs to be changed to `${decompress(data)}`
//        Check Git history.
const version = "2";
const data: ICollections = {
  children: [
    {
      type: "collection",
      uid: "VARI@1509",
      title: "বিভিন্ন মঠ ও সংস্থা ~ Songbooks from Various Saṅgas",
      count: 1812,
      children: [
        {
          type: "collection",
          uid: "GGG@1510",
          title:
            "শ্রীগৌড়ীয়-গীতিগুচ্ছ [শ্রীগৌড়ীয় বেদান্ত সমিতি] ~ Śrī Gauḍīya Gīti-guccha [Śrī Gauḍīya Vedānta Samiti]",
          children: [
            {
              type: "heading",
              uid: "*@1511",
              title: "১ম সংস্করণ ইং ১৯৫৭ ~ published 1957"
            },
            {
              type: "song",
              uid: "M9@1512",
              title: "মঙ্গলাচরণ [GVS]"
            },
            {
              type: "song",
              uid: "M8@1513",
              title: "জয়ধ্ৱনি [GVS]"
            },
            {
              type: "song",
              uid: "M10@1514",
              title: "মঙ্গলাচরণ—অতিরিক্ত [GVS + IPBYS]"
            },
            {
              type: "collection",
              uid: "1@1515",
              title: "সংস্কৃত গীতি এবং স্তোত্র ~ Sanskrit Songs and Stotras",
              children: [
                {
                  type: "collection",
                  uid: "1a@1516",
                  title: "গুরুবর্গ ~ Guru-varga",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1517",
                      title: "স্তোত্র ~ Stotras"
                    },
                    {
                      type: "song",
                      uid: "GV8@1518",
                      title: "শ্রীগুরু-পরম্পরা—সংস্কৃত [GVS]"
                    },
                    {
                      type: "song",
                      uid: "G1@1519",
                      title: "শ্রীগুর্ৱ্ৱষ্টকম্ (সংস্কৃত) ~ Śrī Gurvāṣṭakam (Sanskrit)"
                    },
                    {
                      type: "song",
                      uid: "GV14@1520",
                      title: "শ্রীকেশৱাচার্যাষ্টকম্ (১)"
                    },
                    {
                      type: "song",
                      uid: "GV15@1521",
                      title: "শ্রীকেশৱাচার্যাষ্টকম্ (২)"
                    },
                    {
                      type: "song",
                      uid: "GV17@1522",
                      title: "শ্রীপ্রভুপাদ-পদ্ম-স্তৱকঃ"
                    },
                    {
                      type: "song",
                      uid: "GV19@1523",
                      title: "শ্রীমদ্ গৌর-কিশোরাষ্টকম্‌"
                    },
                    {
                      type: "song",
                      uid: "GV21@1524",
                      title: "শ্রীমদ্ ভক্তিৱিনোদ দশকম্"
                    },
                    {
                      type: "song",
                      uid: "GV24@1525",
                      title: "শ্রীল জগন্নাথাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "GV31@1526",
                      title: "শ্রীষড়্‌গোস্ৱাম্যাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "N1@1527",
                      title: "শ্রীনিত্যানন্দাষ্টকম্"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "1b@1528",
                  title: "শ্রীগৌরচন্দ্র ~ Śrī Gauracandra",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1529",
                      title: "গীতি ~ Songs"
                    },
                    {
                      type: "song",
                      uid: "GH13@1530",
                      title: "পশ্য শচী-সুতমনুপম-রূপম্"
                    },
                    {
                      type: "song",
                      uid: "GH14@1531",
                      title: "ৱন্দে ৱিশ্ৱম্ভর-পদ-কমলম্"
                    },
                    {
                      type: "song",
                      uid: "GH15@1532",
                      title: "সখে কলয় গৌরমুদারম্"
                    },
                    {
                      type: "song",
                      uid: "GH12@1533",
                      title: "শ্রীগোদ্রুম ভজনোপদেষ"
                    },
                    {
                      type: "heading",
                      uid: "*@1534",
                      title: "স্তোত্র ~ Stotras"
                    },
                    {
                      type: "song",
                      uid: "GH1@1535",
                      title: "শ্রীশচীতনয়াষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "GH7@1536",
                      title: "শ্রীগৌরাঙ্গস্তোত্রম্"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "1c@1537",
                  title: "শ্রীমতী-রাধিকা ~ Śrīmatī Rādhikā",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1538",
                      title: "গীতি ~ Songs"
                    },
                    {
                      type: "song",
                      uid: "R8@1539",
                      title: "রাধে জয় জয় মাধৱ-দয়িতে"
                    },
                    {
                      type: "song",
                      uid: "R9@1540",
                      title: "কলয়তি নয়নং দিশি দিশি ৱলিতম্"
                    },
                    {
                      type: "song",
                      uid: "R11@1541",
                      title: "নাকর্ণমতিসুহৃদুপদেশম্"
                    },
                    {
                      type: "song",
                      uid: "R12@1542",
                      title: "স্মরতু মনো মম নিরৱধি রাধাম্"
                    },
                    {
                      type: "heading",
                      uid: "*@1543",
                      title: "স্তোত্র ~ Stotras"
                    },
                    {
                      type: "song",
                      uid: "R3@1544",
                      title: "শ্রীরাধিকাষ্টকম্ (৩)"
                    },
                    {
                      type: "song",
                      uid: "R4@1545",
                      title: "শ্রীরাধা-কৃপা-কটাক্ষ-স্তোত্রম্"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "1d@1546",
                  title: "শ্রীকৃষ্ণচন্দ্র ~ Śrī Kṛṣṇacandra",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1547",
                      title: "গীতি ~ Songs"
                    },
                    {
                      type: "song",
                      uid: "K28@1548",
                      title: "(কৃষ্ণ) দেৱ! ভৱন্তং ৱন্দে"
                    },
                    {
                      type: "song",
                      uid: "K29@1549",
                      title: "শ্রীমঙ্গল-গীতম্"
                    },
                    {
                      type: "song",
                      uid: "K30@1550",
                      title: "জয় জয় প্রাণসখে!"
                    },
                    {
                      type: "song",
                      uid: "K31@1551",
                      title: "হরে হরে গোৱিন্দ হরে"
                    },
                    {
                      type: "song",
                      uid: "K32@1552",
                      title: "ৱসতু মনো মম মদনগোপালে"
                    },
                    {
                      type: "song",
                      uid: "K33@1553",
                      title: "ৱন্দে গিরিৱরধর-পদকমলম্"
                    },
                    {
                      type: "song",
                      uid: "K34@1554",
                      title: "মধুরিপুরদ্য-ৱসন্তে"
                    },
                    {
                      type: "song",
                      uid: "K35@1555",
                      title: "জয় জয় সুন্দর নন্দ-কুমার"
                    },
                    {
                      type: "song",
                      uid: "K36@1556",
                      title: "জয় জয় ৱল্লৱরাজ-কুমার"
                    },
                    {
                      type: "heading",
                      uid: "*@1557",
                      title: "স্তোত্র ~ Stotras"
                    },
                    {
                      type: "song",
                      uid: "K1@1558",
                      title: "শ্রীদামোদরাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "K2@1559",
                      title: "শ্রীৱ্রজরাজসুতাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "K3@1560",
                      title: "শ্রীনন্দনন্দনাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "K4@1561",
                      title: "শ্রীশ্রীমধুরাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "RK1@1562",
                      title: "শ্রীরাধাৱিনোদৱিহারী-তত্ত্ৱাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "VT6@1563",
                      title: "শ্রীজগন্নাথাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "VT4@1564",
                      title: "শ্রীদশাৱতার-স্তোত্রম্"
                    },
                    {
                      type: "song",
                      uid: "NM10@1565",
                      title: "শ্রীশ্রীগোৱিন্দ-দমোদর-স্তোত্রা"
                    }
                  ]
                }
              ]
            },
            {
              type: "collection",
              uid: "2@1566",
              title:
                "বাংলা গীতি (গুরু, বৈষ্ণব, গৌরাঙ্গ, রাধা, কৃষ্ণ...) ~ Bengali Songs [Guru, Vaiṣṇava, Gaurāṅga, Rādhā, Kṛṣṇa...]",
              children: [
                {
                  type: "collection",
                  uid: "2a@1567",
                  title: "শ্রীগুরু ~ Śrī Guru",
                  children: [
                    {
                      type: "song",
                      uid: "GV9@1568",
                      title: "শ্রীগুরু-পরম্পরা—বাংলা [GVS]"
                    },
                    {
                      type: "song",
                      uid: "G2@1569",
                      title: "শ্রীগুর্ব্বষ্টকম্ (বাংলা) ~ Śrī Gurvāṣṭakam (Bengali Rendition)"
                    },
                    {
                      type: "heading",
                      uid: "*@1570",
                      title: "শ্রীগুরু-মহিমা ~ The Glories of Śrī Guru"
                    },
                    {
                      type: "song",
                      uid: "G3@1571",
                      title: "শ্রীগুরু-চরণ-পদ্ম"
                    },
                    {
                      type: "song",
                      uid: "G4@1572",
                      title: "আশ্রয় করিয়া বন্দোঁ শ্রীগুরু-চরণ"
                    },
                    {
                      type: "song",
                      uid: "G5@1573",
                      title: "মন্ত্রগুরু আর যত শিক্ষাগুরুগণ"
                    },
                    {
                      type: "heading",
                      uid: "*@1574",
                      title: "শ্রীগুরু-কৃপাপ্রার্থনা ~ Prayers for Mercy"
                    },
                    {
                      type: "song",
                      uid: "G6@1575",
                      title: "গুরুদেব! কৃপাবিন্দু দিয়া"
                    },
                    {
                      type: "song",
                      uid: "G7@1576",
                      title: "গুরুদেব! বড় কৃপা করি’"
                    },
                    {
                      type: "song",
                      uid: "G8@1577",
                      title: "গুরুদেব! কবে মোর সেই দিন হবে?"
                    },
                    {
                      type: "song",
                      uid: "G9@1578",
                      title: "গুরুদেব! কবে তব করুণা প্রকাশে"
                    },
                    {
                      type: "song",
                      uid: "G10@1579",
                      title: "জয় জয় শ্রীগুরু প্রেম-কল্পতরু"
                    },
                    {
                      type: "song",
                      uid: "GV29@1580",
                      title: "হা হা প্রভু লোকনাথ! রাখ পদদ্বন্দ্বে"
                    },
                    {
                      type: "song",
                      uid: "GV30@1581",
                      title: "লোকনাথ প্রভু, তুমি দয়া কর মোরে"
                    },
                    {
                      type: "song",
                      uid: "G12@1582",
                      title: "কবে মোর শুভদিন হইবে উদয়"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2b@1583",
                  title: "গুরুবর্গ ~ Guru-varga",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1584",
                      title:
                        "শ্রীল কেশব-গোস্বামি বন্দনা ~ Prayers to Śrīla Bhakti Prajñāna Keśava Gosvāmī"
                    },
                    {
                      type: "song",
                      uid: "GV16@1585",
                      title: "শ্রীল-কেশব-গোস্বামী-বন্দনা"
                    },
                    {
                      type: "heading",
                      uid: "*@1586",
                      title: "শ্রীল প্রভুপাদ-বন্দনা ~ Prayers to Śrīla Sarasvatī Ṭhākura Prabhupāda"
                    },
                    {
                      type: "song",
                      uid: "GV18@1587",
                      title: "শ্রীল-প্রভুপাদ-বন্দনা"
                    },
                    {
                      type: "heading",
                      uid: "*@1588",
                      title:
                        "শ্রীল গৌরকিশোর-বন্দনা ~ Prayers to Śrīla Gaura-kiśora dāsa Bābājī Mahārāja"
                    },
                    {
                      type: "song",
                      uid: "GV20@1589",
                      title: "শ্রীল-গৌরকিশোর-বন্দনা"
                    },
                    {
                      type: "heading",
                      uid: "*@1590",
                      title:
                        "শ্রীভক্তিবিনোদ জয়-গুণগান ~ Extolling the Qualities of Śrīla Bhaktivinoda Ṭhākura"
                    },
                    {
                      type: "song",
                      uid: "GV22@1591",
                      title: "শ্রীভক্তিবিনোদ-জয়-গুণ-গান"
                    },
                    {
                      type: "song",
                      uid: "GV23@1592",
                      title: "(কোথা) ভকতিবিনোদ শ্রীগৌর-স্বজন!"
                    },
                    {
                      type: "heading",
                      uid: "*@1593",
                      title:
                        "শ্রীল জগন্নাথ-বন্দনা ~ Prayers to Śrīla Jagannātha dāsa Bābājī Mahārāja"
                    },
                    {
                      type: "song",
                      uid: "GV25@1594",
                      title: "শ্রীল-জগন্নাথ-বন্দনা"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2c@1595",
                  title: "শ্রীবৈষ্ণব ~ Śrī Vaiṣṇava",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1596",
                      title: "শ্রীবৈষ্ণব-বন্দনা ~ Prayers of Adoration"
                    },
                    {
                      type: "song",
                      uid: "V1@1597",
                      title: "শ্রীবৈষ্ণব-বন্দনা"
                    },
                    {
                      type: "song",
                      uid: "GP4@1598",
                      title: "প্রাণ গোরাচাঁদ মোর"
                    },
                    {
                      type: "song",
                      uid: "GP5@1599",
                      title: "ধন্য অবতার গোরা ন্যাসি-শিরোমণি"
                    },
                    {
                      type: "song",
                      uid: "GP6@1600",
                      title: "ভাল অবতার শ্রীগৌরাঙ্গ অবতার"
                    },
                    {
                      type: "heading",
                      uid: "*@1601",
                      title: "শ্রীবৈষ্ণব-কৃপা-প্রার্থনা ~ Prayers for Mercy"
                    },
                    {
                      type: "song",
                      uid: "V3@1602",
                      title: "ওহে! বৈষ্ণব ঠাকুর"
                    },
                    {
                      type: "song",
                      uid: "V4@1603",
                      title: "কৃপা কর বৈষ্ণব ঠাকুর !"
                    },
                    {
                      type: "song",
                      uid: "V5@1604",
                      title: "কবে মুই বৈষ্ণব চিনিব"
                    },
                    {
                      type: "song",
                      uid: "V6@1605",
                      title: "হরি হরি কবে মোর হবে হেন দিন"
                    },
                    {
                      type: "heading",
                      uid: "*@1606",
                      title: "শ্রীবৈষ্ণব-বিজ্ঞপ্তি ~ Entreaties to the Vaiṣṇavas"
                    },
                    {
                      type: "song",
                      uid: "V7@1607",
                      title: "ঠাকুর বৈষ্ণবগণ"
                    },
                    {
                      type: "song",
                      uid: "V8@1608",
                      title: "এইবার করুণা কর"
                    },
                    {
                      type: "song",
                      uid: "V9@1609",
                      title: "কিরূপে পাইব সেবা"
                    },
                    {
                      type: "song",
                      uid: "V10@1610",
                      title: "সকল বৈষ্ণব গোসাঞি দয়া কর মোরে"
                    },
                    {
                      type: "song",
                      uid: "V11@1611",
                      title: "শ্রীকৃষ্ণ-ভজন লাগি’ সংসারে আইনু"
                    },
                    {
                      type: "heading",
                      uid: "*@1612",
                      title:
                        "শ্রীবৈষ্ণবের পাদোদক-মহিমা ~ The Glories of the Vaiṣṇava’s Foot-bath Water"
                    },
                    {
                      type: "song",
                      uid: "V2@1613",
                      title: "ঠাকুর বৈষ্ণব-পদ"
                    },
                    {
                      type: "heading",
                      uid: "*@1614",
                      title:
                        "শ্রীগুরু-বৈষ্ণবে বিজ্ঞপ্তি ~ An Entreaty to Śrī Guru and the Vaiṣṇavas"
                    },
                    {
                      type: "song",
                      uid: "V12@1615",
                      title: "শ্রীগুরু-বৈষ্ণব তোমার চরণ"
                    },
                    {
                      type: "heading",
                      uid: "*@1616",
                      title:
                        "শ্রীগুরু-বৈষ্ণবে লালসাময়ী প্রার্থনা ~ Prayers full of Yearning to Śrī Guru and the Vaiṣṇavas"
                    },
                    {
                      type: "song",
                      uid: "V13@1617",
                      title: "শ্রীগুরু-বৈষ্ণব-কৃপা কত দিনে হ’বে"
                    },
                    {
                      type: "song",
                      uid: "V14@1618",
                      title: "আমার এমন ভাগ্য কত দিনে হ’বে"
                    },
                    {
                      type: "song",
                      uid: "V15@1619",
                      title: "কবে মোর মূঢ় মন ছাড়ি’ অন্য ধ্যান"
                    },
                    {
                      type: "heading",
                      uid: "*@1620",
                      title: "শ্রীরূপানুগত্য-মাহাত্ম্য ~ Śrī Rūpānugatya-māhātmya"
                    },
                    {
                      type: "song",
                      uid: "GV45@1621",
                      title: "শুনিয়াছি সাধুমুখে"
                    },
                    {
                      type: "heading",
                      uid: "*@1622",
                      title:
                        "শ্রীরূপ-সনাতনে দৈন্যময়ী প্রার্থনা ~ Prayer of Humility to Śrī Rūpa and Śrī Sanātana"
                    },
                    {
                      type: "song",
                      uid: "GV46@1623",
                      title: "বিষয়-বাসনারূপ চিত্তের বিকার"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2d@1624",
                  title: "পঞ্চতত্ত্ব-মহিমা ~ The Glories of the Pañca-tattva",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1625",
                      title: "শ্রীশ্রীবাস পণ্ডিত ~ Śrī Śrīvāsa Paṇḍita"
                    },
                    {
                      type: "song",
                      uid: "PT2@1626",
                      title: "জয় জয় শ্রীশ্রীবাস ঠাকুর পণ্ডিত"
                    },
                    {
                      type: "song",
                      uid: "PT3@1627",
                      title: "সপ্তদ্বীপ দীপ্ত করি’"
                    },
                    {
                      type: "heading",
                      uid: "*@1628",
                      title: "শ্রীগদাধর পণ্ডিত ~ Śrī Gadādhara Paṇḍita"
                    },
                    {
                      type: "song",
                      uid: "PT6@1629",
                      title: "জয় জয় গদাধর প্রেমের সাগর"
                    },
                    {
                      type: "song",
                      uid: "PT7@1630",
                      title: "জয় জয় গদাধর পণ্ডিত গোঁসাঞি"
                    },
                    {
                      type: "heading",
                      uid: "*@1631",
                      title: "শ্রীঅদ্বৈতাচর্য্য ~ Śrī Advaita Ācārya"
                    },
                    {
                      type: "song",
                      uid: "PT14@1632",
                      title: "শ্রীঅদ্বৈতাবির্ভাব-লীলা"
                    },
                    {
                      type: "song",
                      uid: "PT9@1633",
                      title: "অদ্বৈত-আচার্য্য গোসাঞি সাক্ষাৎ ঈশ্বর"
                    },
                    {
                      type: "song",
                      uid: "PT10@1634",
                      title: "জয় জয় শ্রীঅদ্বৈত পতিতপাবন"
                    },
                    {
                      type: "song",
                      uid: "PT11@1635",
                      title: "বিষয়ে সকলে মত্ত"
                    },
                    {
                      type: "song",
                      uid: "PT12@1636",
                      title: "নাস্তিকতা অপধর্ম্ম জুড়িল সংসার"
                    },
                    {
                      type: "song",
                      uid: "PT13@1637",
                      title: "জয় জয় অদ্বৈত আচার্য্য দয়াময়"
                    },
                    {
                      type: "song",
                      uid: "PT15@1638",
                      title: "জয় জয় অদ্ভুত সো পহুঁ অদ্বৈত"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2e@1639",
                  title: "শ্রীমন্নিত্যানন্দ ~ Śrīman Nityānanda",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1640",
                      title: "শ্রীনিত্যাননাবির্ভাব-লীলা ~ Śrī Nityānanda’s Appearance Pastime"
                    },
                    {
                      type: "song",
                      uid: "N21@1641",
                      title: "রাঢ়দেশ নাম, একচক্রা-গ্রাম"
                    },
                    {
                      type: "heading",
                      uid: "*@1642",
                      title: "শ্রীনিত্যানন্দ-তত্ত্ব ~ Śrī Nityānanda-tattva"
                    },
                    {
                      type: "song",
                      uid: "N2@1643",
                      title: "জয় জয় নিত্যানন্দ গোকুল-ভূষণ"
                    },
                    {
                      type: "heading",
                      uid: "*@1644",
                      title: "শ্রীনিত্যানন্দ-স্তুতি ~ Praises of Śrī Nityānanda"
                    },
                    {
                      type: "song",
                      uid: "N3@1645",
                      title: "নামরূপে তুমি নিত্যানন্দ মূর্ত্তিমন্ত"
                    },
                    {
                      type: "song",
                      uid: "N4@1646",
                      title: "তুমি নিত্যানন্দ-মূর্ত্তি নিত্যানন্দ-নাম"
                    },
                    {
                      type: "song",
                      uid: "N5@1647",
                      title: "জয় জয় জয় পদ্মাবতীর নন্দন"
                    },
                    {
                      type: "heading",
                      uid: "*@1648",
                      title: "শ্রীনিত্যানন্দ-নিষ্ঠা ~ Firm Faith in Śrī Nityānanda"
                    },
                    {
                      type: "song",
                      uid: "N6@1649",
                      title: "ইষ্টদেব বন্দোঁ মোর নিত্যানন্দ রায়"
                    },
                    {
                      type: "song",
                      uid: "N7@1650",
                      title: "নিতাই-পদ-কমল"
                    },
                    {
                      type: "song",
                      uid: "N8@1651",
                      title: "নিতাই মোর জীবন-ধন"
                    },
                    {
                      type: "song",
                      uid: "N9@1652",
                      title: "অক্রোধ পরমানন্দ"
                    },
                    {
                      type: "heading",
                      uid: "*@1653",
                      title: "রূপ-বর্ণন ~ His Beautiful Form"
                    },
                    {
                      type: "song",
                      uid: "N10@1654",
                      title: "দেখ নিতাইচাঁদের মাধুরী"
                    },
                    {
                      type: "song",
                      uid: "N11@1655",
                      title: "আরে ভাই! নিতাই আমার দয়ার অবধি"
                    },
                    {
                      type: "song",
                      uid: "N12@1656",
                      title: "জয় জয় নিত্যানন্দ, নিত্যানন্দ-রাম"
                    },
                    {
                      type: "heading",
                      uid: "*@1657",
                      title: "গুণ-বর্ণন ~ His Wonderful Qualities"
                    },
                    {
                      type: "song",
                      uid: "N13@1658",
                      title: "জয় জয় নিত্যানন্দ রোহিণী-কুমার"
                    },
                    {
                      type: "song",
                      uid: "N14@1659",
                      title: "নিতাই গুণমণি আমার"
                    },
                    {
                      type: "song",
                      uid: "N16@1660",
                      title: "গজেন্দ্রগমনে নিতাই"
                    },
                    {
                      type: "song",
                      uid: "N17@1661",
                      title: "পূরবে গোবর্দ্ধন ধরল অনুজ যাঁ’র"
                    },
                    {
                      type: "song",
                      uid: "N18@1662",
                      title: "প্রেমে মত্ত নিত্যানন্দ, সহজে আনন্দ কন্দ"
                    },
                    {
                      type: "song",
                      uid: "N19@1663",
                      title: "আনন্দ কন্দ, নিতাই-চন্দ"
                    },
                    {
                      type: "song",
                      uid: "N20@1664",
                      title: "গজেন্দ্রগমনে যায় সকরুণ দিঠে চায়"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2f@1665",
                  title: "শ্রীগৌরাঙ্গ ~ Śrī Gaurāṅga",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1666",
                      title: "শ্রীগৌর-জন্মলীলা ~ Śrī Gauracandra’s Appearance Pastime"
                    },
                    {
                      type: "song",
                      uid: "GH58@1667",
                      title: "নদীয়া-উদয়গিরি"
                    },
                    {
                      type: "song",
                      uid: "GH59@1668",
                      title: "রাহু-কবলে ইন্দু"
                    },
                    {
                      type: "song",
                      uid: "GH60@1669",
                      title: "জিনিয়া রবিকর"
                    },
                    {
                      type: "song",
                      uid: "GH61@1670",
                      title: "প্রকাশ হইলা গৌরচন্দ্র"
                    },
                    {
                      type: "song",
                      uid: "GH62@1671",
                      title: "চৈতন্য-অবতার শুনিয়া দেবগণ"
                    },
                    {
                      type: "song",
                      uid: "GH63@1672",
                      title: "দুন্দুভি-ডিণ্ডিম মঙ্গল-জয়ধ্বনি"
                    },
                    {
                      type: "heading",
                      uid: "*@1673",
                      title: "শ্রীগৌরচন্দ্র-স্তুতি ~ Praises of Śrī Gauracandra"
                    },
                    {
                      type: "song",
                      uid: "GH16@1674",
                      title: "জয় জয় কৃপাসিন্ধু শ্রীগৌরসুন্দর"
                    },
                    {
                      type: "song",
                      uid: "GH17@1675",
                      title: "জয় জয় সর্ব্ব-প্রাণনাথ বিশ্বম্ভর"
                    },
                    {
                      type: "song",
                      uid: "GH18@1676",
                      title: "বিশ্বম্ভর-চরণে আমার নমস্কার"
                    },
                    {
                      type: "song",
                      uid: "GH19@1677",
                      title: "জয় জয় শ্রীকৃষ্ণচৈতন্য প্রাণনাথ"
                    },
                    {
                      type: "heading",
                      uid: "*@1678",
                      title: "শ্রীগৌর-তত্ত্ব ~ Śrī Gaura-tattva"
                    },
                    {
                      type: "song",
                      uid: "GH20@1679",
                      title: "(প্রভু হে!) এমন দুর্ম্মতি সংসার-ভিতরে"
                    },
                    {
                      type: "song",
                      uid: "GH21@1680",
                      title: "চৈতন্য-চন্দ্রের লীলা-সমুদ্র অপার"
                    },
                    {
                      type: "song",
                      uid: "GH22@1681",
                      title: "জয় নন্দনন্দন গোপীজন-বল্লভ"
                    },
                    {
                      type: "song",
                      uid: "GH23@1682",
                      title: "জয় জয় জগন্নাথ শচীর নন্দন"
                    },
                    {
                      type: "song",
                      uid: "GH24@1683",
                      title: "শ্রীকৃষ্ণচৈতন্য গোরা শচীর দুলাল"
                    },
                    {
                      type: "heading",
                      uid: "*@1684",
                      title: "শ্রীগৌর-রূপ-গুণ-বর্ণন ~ Śrī Gaurahari’s Beautiful Form and Qualities"
                    },
                    {
                      type: "song",
                      uid: "GH25@1685",
                      title: "গৌরাঙ্গ সুন্দরবেশ মদনমোহন"
                    },
                    {
                      type: "song",
                      uid: "GH26@1686",
                      title: "নাচে বিশ্বম্ভর, জগত ঈশ্বর"
                    },
                    {
                      type: "song",
                      uid: "GH27@1687",
                      title: "বিমল-হেম-জিনি’, তনু অনুপম রে!"
                    },
                    {
                      type: "song",
                      uid: "GH28@1688",
                      title: "আরে মোর নাচত গৌরকিশোর"
                    },
                    {
                      type: "song",
                      uid: "GH29@1689",
                      title: "নবদ্বীপে উদয় করিলা দ্বিজরাজ"
                    },
                    {
                      type: "song",
                      uid: "GH30@1690",
                      title: "কলিযুগে শ্রীচৈতন্য অবনী করিলা ধন্য"
                    },
                    {
                      type: "song",
                      uid: "GH31@1691",
                      title: "দেবাদিদেব গৌরচন্দ্র গৌরীদাস-মন্দিরে"
                    },
                    {
                      type: "heading",
                      uid: "*@1692",
                      title: "শ্রীগৌর-গুণ-বর্ণন ~ Śrī Gaurahari’s Wonderful Qualities"
                    },
                    {
                      type: "song",
                      uid: "GH32@1693",
                      title: "জয় জগন্নাথ-শচীনন্দন গৌরাঙ্গ পহুঁ"
                    },
                    {
                      type: "song",
                      uid: "GH33@1694",
                      title: "কি কহিব শত শত তুয়া অবতার"
                    },
                    {
                      type: "song",
                      uid: "GH34@1695",
                      title: "শচীসুত গৌরহরি, নবদ্বীপে অবতরি"
                    },
                    {
                      type: "song",
                      uid: "GH35@1696",
                      title: "এমন গৌরাঙ্গ বিনা নাহি আর!"
                    },
                    {
                      type: "song",
                      uid: "GH36@1697",
                      title: "(যদি) গৌরাঙ্গ নহিত"
                    },
                    {
                      type: "song",
                      uid: "GH37@1698",
                      title: "এমন শচীর নন্দন বিনে"
                    },
                    {
                      type: "heading",
                      uid: "*@1699",
                      title: "শ্রীগৌর-মহিমা ~ Śrī Gaurahari’s Glories"
                    },
                    {
                      type: "song",
                      uid: "GH38@1700",
                      title: "কে যাবি কে যাবি ভাই"
                    },
                    {
                      type: "song",
                      uid: "GH39@1701",
                      title: "অবতার সার"
                    },
                    {
                      type: "song",
                      uid: "GH40@1702",
                      title: "গৌরাঙ্গের দু’টী পদ"
                    },
                    {
                      type: "heading",
                      uid: "*@1703",
                      title:
                        "বর্ণমালায় শ্রীগৌর-মহিমা ~ Śrī Gaurahari’s Glories through the Alphabet"
                    },
                    {
                      type: "song",
                      uid: "GH41@1704",
                      title: "বর্ণমালায় শ্রীগৌর-মহিমা (অ-ঔ)"
                    },
                    {
                      type: "song",
                      uid: "GH42@1705",
                      title: "বর্ণমালায় শ্রীগৌর-মহিমা (ক-ক্ষ)"
                    },
                    {
                      type: "heading",
                      uid: "*@1706",
                      title: "শ্রীগৌরসুন্দরে বিজ্ঞপ্তি ~ Entreaties to Śrī Gaurasundara"
                    },
                    {
                      type: "song",
                      uid: "GH44@1707",
                      title: "জয় জয় শ্রীকৃষ্ণচৈতন্য দয়াসিন্ধু"
                    },
                    {
                      type: "song",
                      uid: "GH45@1708",
                      title: "ওহে প্রেমের ঠাকুর গোরা"
                    },
                    {
                      type: "song",
                      uid: "GH46@1709",
                      title: "পহুঁ মোর গৌরাঙ্গ গোসাঞী"
                    },
                    {
                      type: "song",
                      uid: "GH47@1710",
                      title: "কবে শ্রীচৈতন্য মোরে করিবেন দয়া"
                    },
                    {
                      type: "song",
                      uid: "GH48@1711",
                      title: "আমি ত’ দুর্জ্জন"
                    },
                    {
                      type: "heading",
                      uid: "*@1712",
                      title:
                        "শ্রীগৌরচন্দ্রে লালসাময়ী প্রার্থনা ~ Prayers Full of Longing to Śrī Gauracandra"
                    },
                    {
                      type: "song",
                      uid: "GH50@1713",
                      title: "হা হা মোর গৌর-কিশোর"
                    },
                    {
                      type: "song",
                      uid: "GH51@1714",
                      title: "কবে আহা গৌরাঙ্গ বলিয়া"
                    },
                    {
                      type: "heading",
                      uid: "*@1715",
                      title: "শ্রীগৌরাঙ্গ-নিষ্ঠা ~ Firm Faith in Śrī Gaurāṅga"
                    },
                    {
                      type: "song",
                      uid: "GH52@1716",
                      title: "আরে ভাই! ভজ মোর গৌরাঙ্গ-চরণ"
                    },
                    {
                      type: "song",
                      uid: "GH54@1717",
                      title: "ভাইরে! ভজ গোরাচাঁদের চরণ"
                    },
                    {
                      type: "song",
                      uid: "GH55@1718",
                      title: "কলিঘোর তিমিরে"
                    },
                    {
                      type: "heading",
                      uid: "*@1719",
                      title: "আক্ষেপ ~ Regret"
                    },
                    {
                      type: "song",
                      uid: "GH56@1720",
                      title: "গোরা পঁহু না ভজিয়া মৈনু"
                    },
                    {
                      type: "song",
                      uid: "GH57@1721",
                      title: "আরে মোর আরে মোর গৌরাঙ্গ গোসাঞী"
                    },
                    {
                      type: "song",
                      uid: "GN1@1722",
                      title: "শ্রীকৃষ্ণচৈতন্য, বলরাম নিত্যানন্দ"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2g@1723",
                  title: "শ্রীগৌর-নিত্যানন্দ ~ Śrī Gaurāṅga-Nityānanda",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1724",
                      title: "শ্রীগৌর-নিত্যানন্দে বিজ্ঞপ্তি ~ Entreaties to Śrī Gaura Nityānanda"
                    },
                    {
                      type: "song",
                      uid: "GN2@1725",
                      title: "শ্রীকৃষ্ণচৈতন্য নিত্যানন্দ দুই প্রভু"
                    },
                    {
                      type: "song",
                      uid: "GN3@1726",
                      title: "পরম করুণ, পঁহু দুইজন"
                    },
                    {
                      type: "song",
                      uid: "GN4@1727",
                      title: "নিতাই চৈতন্য দোঁহে বড় অবতার"
                    },
                    {
                      type: "song",
                      uid: "GN5@1728",
                      title: "জীবের ভাগ্যে অবনী বিহরে দোন ভাই"
                    },
                    {
                      type: "song",
                      uid: "GN6@1729",
                      title: "এইবার করুণা কর চৈতন্য-নিতাই"
                    },
                    {
                      type: "song",
                      uid: "GN7@1730",
                      title: "কবে হ’বে বল"
                    },
                    {
                      type: "song",
                      uid: "GN8@1731",
                      title: "এ ঘোর-সংসারে"
                    },
                    {
                      type: "heading",
                      uid: "*@1732",
                      title:
                        "শ্রীগৌর-নিত্যানন্দে লালসাময়ী প্রার্থনা ~ Prayers Full of Yearning to Śrī Gaura-Nityānanda"
                    },
                    {
                      type: "song",
                      uid: "GN9@1733",
                      title: "‘গৌরাঙ্গ’ বলিতে হবে"
                    },
                    {
                      type: "song",
                      uid: "GN10@1734",
                      title: "কবে হ’বে হেন দশা মোর"
                    },
                    {
                      type: "song",
                      uid: "GN11@1735",
                      title: "হা হা কবে গৌর-নিতাই"
                    },
                    {
                      type: "heading",
                      uid: "*@1736",
                      title: "শ্রীগৌর-নিত্যানন্দে নিষ্ঠা ~ Firm Faith in Śrī Gaura-Nityānanda"
                    },
                    {
                      type: "song",
                      uid: "GN12@1737",
                      title: "ধন মোর নিত্যানন্দ পতি মোর গৌরচন্দ্র"
                    },
                    {
                      type: "song",
                      uid: "GN13@1738",
                      title: "নিতাই-গৌর-নাম"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2h@1739",
                  title: "সগণ শ্রীগৌরাঙ্গ ~ Śrī Gaurahari and His Associates",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1740",
                      title: "সগণ শ্রীগৌর-মহিমা ~ The Glories of Śrī Gaurahari and His Associates"
                    },
                    {
                      type: "song",
                      uid: "GP2@1741",
                      title: "এ’লো গৌর-রস-নদী"
                    },
                    {
                      type: "song",
                      uid: "GP3@1742",
                      title: "নদীয়ার ঘাটে ভাই কি অদ্ভুত তরী"
                    },
                    {
                      type: "heading",
                      uid: "*@1743",
                      title:
                        "সগণ শ্রীগৌরচরণে প্রার্থনা ~ Prayers at the Feet of Gaurahari and His Associates"
                    },
                    {
                      type: "song",
                      uid: "GP8@1744",
                      title: "জয় জয় শ্রীকৃষ্ণচৈতন্য-নিত্যানন্দ"
                    },
                    {
                      type: "song",
                      uid: "GP9@1745",
                      title: "শ্রীকৃষ্ণচৈতন্য প্রভু দয়া কর মোরে"
                    },
                    {
                      type: "song",
                      uid: "GP10@1746",
                      title: "জয়রে জয়রে মোর গৌরাঙ্গ রায়"
                    },
                    {
                      type: "song",
                      uid: "GP11@1747",
                      title: "জয় জয় শ্রীকৃষ্ণচৈতন্য সর্ব্বাশ্রয়"
                    },
                    {
                      type: "song",
                      uid: "GP12@1748",
                      title: "জয় জয় নিত্যানন্দাদ্বৈত গৌরাঙ্গ"
                    },
                    {
                      type: "heading",
                      uid: "*@1749",
                      title:
                        "সপার্ষদ শ্রীগৌর-বিরহ-বিলাপ ~ Lamenting in Separation from Śrī Gaurahari and His Associates"
                    },
                    {
                      type: "song",
                      uid: "GV47@1750",
                      title: "যে আনিল প্রেমধন"
                    },
                    {
                      type: "song",
                      uid: "GV48@1751",
                      title: "গৌরাঙ্গের সহচর"
                    },
                    {
                      type: "heading",
                      uid: "*@1752",
                      title:
                        "স্বাভীষ্ট-লালসাত্মক প্রার্থনা ~ Prayers Full of Yearning for One’s Cherished Desire"
                    },
                    {
                      type: "song",
                      uid: "GV44@1753",
                      title: "শ্রীরূপমঞ্জরী-পদ"
                    },
                    {
                      type: "song",
                      uid: "RK10@1754",
                      title: "হরি হরি, কবে মোর হইবে সুদিন"
                    },
                    {
                      type: "heading",
                      uid: "*@1755",
                      title:
                        "সগণশ্রীগৌরকৃষ্ণে দৈন্যবোধিকা প্রার্থনা ~ Prayers of Humility to Śrī Gaura-Kṛṣṇa and Their Associates"
                    },
                    {
                      type: "song",
                      uid: "GP13@1756",
                      title: "হরি হরি! বিফলে জনম গোঙাইনু"
                    },
                    {
                      type: "song",
                      uid: "GP14@1757",
                      title: "হরি হরি! বড় শেল মরমে রহিল"
                    },
                    {
                      type: "song",
                      uid: "GP15@1758",
                      title: "হরি হরি! কি মোর করমগতি মন্দ"
                    },
                    {
                      type: "song",
                      uid: "GP16@1759",
                      title: "হরি হরি! কি মোর করম অনুরত"
                    },
                    {
                      type: "heading",
                      uid: "*@1760",
                      title: "সিদ্ধি-লালসা ~ Hankering for Ultimate Perfection"
                    },
                    {
                      type: "song",
                      uid: "D2@1761",
                      title: "কবে গৌর-বনে"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2i@1762",
                  title: "শ্রীরাধা",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1763",
                      title: "শ্রীরাধাষ্টক ~ Śrī Rādhāṣṭaka"
                    },
                    {
                      type: "song",
                      uid: "R13@1764",
                      title: "রাধিকাচরণ-পদ্ম"
                    },
                    {
                      type: "song",
                      uid: "R14@1765",
                      title: "বিরজার পারে, শুদ্ধপরব্যোম-ধাম"
                    },
                    {
                      type: "song",
                      uid: "R15@1766",
                      title: "রমণী-শিরোমণি, বৃষভানু-নন্দিনী"
                    },
                    {
                      type: "song",
                      uid: "R16@1767",
                      title: "রসিক-নাগরী-, গণ-শিরোমণি"
                    },
                    {
                      type: "song",
                      uid: "R17@1768",
                      title: "মহাভাব-চিন্তামণি, উদ্ভাবিত তনুখানি"
                    },
                    {
                      type: "song",
                      uid: "R18@1769",
                      title: "বরজ-বিপিনে"
                    },
                    {
                      type: "song",
                      uid: "R19@1770",
                      title: "শতকোটী গোপী"
                    },
                    {
                      type: "song",
                      uid: "R20@1771",
                      title: "রাধা-ভজনে যদি মতি নাহি ভেলা"
                    },
                    {
                      type: "heading",
                      uid: "*@1772",
                      title: "শ্রীরাধা-নিষ্ঠা ~ Firm Faith in Śrī Rādhā"
                    },
                    {
                      type: "song",
                      uid: "R21@1773",
                      title: "বৃষভানুসুতা-চরণ-সেবনে"
                    },
                    {
                      type: "song",
                      uid: "R22@1774",
                      title: "শ্রীকৃষ্ণবিরহে রাধিকার দশা"
                    },
                    {
                      type: "song",
                      uid: "R23@1775",
                      title: "রাধিকা-চরণরেণু"
                    },
                    {
                      type: "heading",
                      uid: "*@1776",
                      title:
                        "শ্রীল রঘুনাথ দাস গোস্বামীর উদ্দেশ্যে... ~ the Life Goal of Śrī Raghunātha dāsa Gosvāmī"
                    },
                    {
                      type: "song",
                      uid: "R24@1777",
                      title: "কোথায় গো প্রেমময়ি"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2j@1778",
                  title: "শ্রীশ্রীরাধাকৃষ্ণ ~ Śrī Śrī Rādhā-Kṛṣṇa",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1779",
                      title: "সিদ্ধি-লালসা ~ Hankering for Ultimate Perfection"
                    },
                    {
                      type: "song",
                      uid: "R25@1780",
                      title: "দেখিতে দেখিতে, ভুলিব বা কবে"
                    },
                    {
                      type: "song",
                      uid: "RK9@1781",
                      title: "রাধাকৃষ্ণ প্রাণ মোর"
                    },
                    {
                      type: "heading",
                      uid: "*@1782",
                      title: "শ্রীরাধা-কৃষ্ণে বিজ্ঞপ্তি ~ Entreaties to Śrī Rādhā-Kṛṣṇa"
                    },
                    {
                      type: "song",
                      uid: "RK11@1783",
                      title: "শ্রীরাধাকৃষ্ণ-পদকমলে মন"
                    },
                    {
                      type: "song",
                      uid: "RK26@1784",
                      title: "প্রভু হে, এইবার করহ করুণা"
                    },
                    {
                      type: "heading",
                      uid: "*@1785",
                      title:
                        "শ্রীরাধাকৃষ্ণ-চরণে সংপ্রার্থনা ~ Prayers at the Feet of Śrī Rādhā-Kṛṣṇa"
                    },
                    {
                      type: "song",
                      uid: "RK12@1786",
                      title: "রাধাকৃষ্ণ! নিবেদন এই জন করে"
                    },
                    {
                      type: "song",
                      uid: "PBC4@1787",
                      title: "আন কথা আন ব্যথা"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2k@1788",
                  title: "শ্রীকৃষ্ণ ~ Śrī Kṛṣṇa",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1789",
                      title: "শ্রীকৃষ্ণ-স্তুতি ~ Praises of Śrī Kṛṣṇa"
                    },
                    {
                      type: "song",
                      uid: "K38@1790",
                      title: "জয় জয় নন্দসুত ব্রজকুলপতি"
                    },
                    {
                      type: "song",
                      uid: "K39@1791",
                      title: "স্তুতিযোগ্য তুমি প্রভু"
                    },
                    {
                      type: "heading",
                      uid: "*@1792",
                      title: "শ্রীকৃষ্ণ-রূপ-বর্ণন ~ The Beautiful Form of Śrī Kṛṣṇa"
                    },
                    {
                      type: "song",
                      uid: "K41@1793",
                      title: "জনম সফল তা’র, কৃষ্ণ দরশন যা’র"
                    },
                    {
                      type: "song",
                      uid: "K42@1794",
                      title: "বন্ধুসঙ্গে"
                    },
                    {
                      type: "heading",
                      uid: "*@1795",
                      title: "শ্রীকৃষ্ণ-গুণ-বর্ণন ~ The Wonderful Qualities of Śrī Kṛṣṇa"
                    },
                    {
                      type: "song",
                      uid: "K43@1796",
                      title: "বহির্ম্মুখ হ’য়ে, মায়ারে ভজিয়ে"
                    },
                    {
                      type: "heading",
                      uid: "*@1797",
                      title: "শ্রীকৃষ্ণ-লীলা-বর্ণন ~ The Sweet Pastimes of Śrī Kṛṣṇa"
                    },
                    {
                      type: "song",
                      uid: "K44@1798",
                      title: "জীবে কৃপা করি’, গোলোকের হরি"
                    },
                    {
                      type: "song",
                      uid: "K45@1799",
                      title: "যমুনা-পুলিনে"
                    },
                    {
                      type: "heading",
                      uid: "*@1800",
                      title: "শ্রীকৃষ্ণে বিজ্ঞপ্তি ~ Entreaties to Śrī Kṛṣṇa"
                    },
                    {
                      type: "song",
                      uid: "PBC3@1801",
                      title: "তুমি ত দয়ার সিন্ধু"
                    },
                    {
                      type: "song",
                      uid: "K47@1802",
                      title: "মোর প্রভু মদনগোপাল গোবিন্দ গোপীনাথ"
                    },
                    {
                      type: "song",
                      uid: "K48@1803",
                      title: "প্রাণনাথ! মোরে তুমি কৃপাদৃষ্টি কর"
                    },
                    {
                      type: "song",
                      uid: "K49@1804",
                      title: "গোপীনাথ, মম নিবেদন শুন"
                    },
                    {
                      type: "song",
                      uid: "K50@1805",
                      title: "গোপীনাথ, ঘুচাও সংসার-জ্বালা"
                    },
                    {
                      type: "song",
                      uid: "K51@1806",
                      title: "গোপীনাথ, আমার উপায় নাই"
                    },
                    {
                      type: "heading",
                      uid: "*@1807",
                      title: "শ্রীকৃষ্ণ-প্রীতি প্রার্থনা ~ A Prayer for Love of Śrī Kṛṣṇa"
                    },
                    {
                      type: "song",
                      uid: "K52@1808",
                      title: "ব্রহ্মাণ্ড ব্যাপিয়া, আছয়ে যে-জন"
                    },
                    {
                      type: "heading",
                      uid: "*@1809",
                      title:
                        "শ্রীকৃষ্ণ-প্রীতিসূচক নির্ব্বেদ ~ Expressing Love for Śrī Kṛṣṇa through Repentance"
                    },
                    {
                      type: "song",
                      uid: "K53@1810",
                      title: "বংশীগানামৃত-ধাম, লাবণ্যামৃত-জন্মস্থান"
                    },
                    {
                      type: "heading",
                      uid: "*@1811",
                      title: "শ্রীকৃষ্ণ-ভজন-নিষ্ঠা ~ Firm Resolve toward Śrī Kṛṣṇa-bhajana"
                    },
                    {
                      type: "song",
                      uid: "K54@1812",
                      title: "ব্রজেন্দ্রনন্দন, ভজে যেই জন"
                    },
                    {
                      type: "heading",
                      uid: "*@1813",
                      title: "শ্রীকৃষ্ণে দৈন্যবোধিকা প্রার্থনা ~ Prayers of Humility to Śrī Kṛṣṇa"
                    },
                    {
                      type: "song",
                      uid: "K55@1814",
                      title: "প্রাণেশ্বর! নিবেদন এইজন করে"
                    },
                    {
                      type: "song",
                      uid: "K56@1815",
                      title: "হরি হরি! কৃপা করি’ রাখ নিজ পদে"
                    },
                    {
                      type: "song",
                      uid: "K57@1816",
                      title: "হরি হরি! কি মোর করম অভাগ"
                    },
                    {
                      type: "heading",
                      uid: "*@1817",
                      title:
                        "শ্রীকৃষ্ণে স্বাভীষ্ট-লালসাত্মক প্রার্থনা ~ Prayers Full of Yearning for One’s Cherished Desire"
                    },
                    {
                      type: "song",
                      uid: "D14@1818",
                      title: "হরি হরি! আর কবে পালটিবে দশা"
                    },
                    {
                      type: "song",
                      uid: "D15@1819",
                      title: "হরি হরি! আর কি এমন দশা হব"
                    },
                    {
                      type: "song",
                      uid: "D16@1820",
                      title: "করঙ্গ কৌপীন লঞা"
                    },
                    {
                      type: "song",
                      uid: "D17@1821",
                      title: "হরি হরি! কবে হব বৃন্দাবনবাসী"
                    },
                    {
                      type: "heading",
                      uid: "*@1822",
                      title: "শ্রীকৃষ্ণে-আত্মনিবেদন ~ Full Self-Submission to Śrī Kṛṣṇa"
                    },
                    {
                      type: "song",
                      uid: "K58@1823",
                      title: "তাতল সৈকতে, বারিবিন্দু-সম"
                    },
                    {
                      type: "song",
                      uid: "K59@1824",
                      title: "যতনে যতেক ধন, পাপে বটারলো"
                    },
                    {
                      type: "song",
                      uid: "K60@1825",
                      title: "মাধব, বহুত মিনতি করি তোয়"
                    },
                    {
                      type: "song",
                      uid: "K61@1826",
                      title: "হরি হে দয়াল মোর জয় রাধানাথ"
                    },
                    {
                      type: "heading",
                      uid: "*@1827",
                      title: "শ্রীকৃষ্ণ-মহিমা ~ The Glories of Śrī Kṛṣṇa"
                    },
                    {
                      type: "song",
                      uid: "K40@1828",
                      title: "শুন হে রসিক জন"
                    }
                  ]
                }
              ]
            },
            {
              type: "collection",
              uid: "3@1829",
              title:
                "বাংলা গীতি (নাম, মনের শিক্ষা, শরণাগতি, শোচক, আরতি...) ~ Bengali Songs [Nāma, Instructions to the Mind, Śaraṇāgati, Śocakas, Ārati...]",
              children: [
                {
                  type: "collection",
                  uid: "3a@1830",
                  title: "শ্রীনাম-কীর্ত্তন ~ Śrī Nāma-kīrtana",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1831",
                      title: "শ্রীনাম-কীর্ত্তন ~ Śrī Nāma-kīrtana"
                    },
                    {
                      type: "song",
                      uid: "NK1@1832",
                      title: "(হরি) হরয়ে নমঃ কৃষ্ণ যাদবায় নমঃ"
                    },
                    {
                      type: "song",
                      uid: "NK2@1833",
                      title: "জয় রাধে, জয় কৃষ্ণ, জয় বৃন্দাবন"
                    },
                    {
                      type: "song",
                      uid: "NK3@1834",
                      title: "জয় জয় রাধে কৃষ্ণ গোবিন্দ"
                    },
                    {
                      type: "song",
                      uid: "NK4@1835",
                      title: "জয় জয় রাধা-মাধব রাধা-মাধব রাধে"
                    },
                    {
                      type: "song",
                      uid: "NK5@1836",
                      title: "কলিকুক্কুর-কদন যদি চাও (হে)"
                    },
                    {
                      type: "song",
                      uid: "NK6@1837",
                      title: "‘দয়াল নিতাই চৈতন্য’ ব’লে নাচ্ রে আমার মন"
                    },
                    {
                      type: "song",
                      uid: "NK7@1838",
                      title: "‘হরি’ বল, ‘হরি’ বল, ‘হরি’ বল, ভাই রে"
                    },
                    {
                      type: "song",
                      uid: "A7@1839",
                      title: "বিভাবরী-শেষ"
                    },
                    {
                      type: "song",
                      uid: "A8@1840",
                      title: "যশোমতী-নন্দন"
                    },
                    {
                      type: "song",
                      uid: "NK8@1841",
                      title: "বোল হরি বোল (৩ বার)"
                    },
                    {
                      type: "song",
                      uid: "NK14@1842",
                      title: "শ্রীকৃষ্ণ গোপাল হরে মুকুন্দ"
                    },
                    {
                      type: "song",
                      uid: "NK11@1843",
                      title: "জয় গোদ্রুমপতি গোরা"
                    },
                    {
                      type: "song",
                      uid: "NK12@1844",
                      title: "কলিযুগপাবন বিশ্বম্ভর"
                    },
                    {
                      type: "song",
                      uid: "NK13@1845",
                      title: "কৃষ্ণচৈতন্য অদ্বৈত প্রভু নিত্যানন্দ"
                    },
                    {
                      type: "song",
                      uid: "NK15@1846",
                      title: "কৃষ্ণ গোবিন্দ হরে"
                    },
                    {
                      type: "song",
                      uid: "NK16@1847",
                      title: "রাধাবল্লভ মাধব শ্রীপতি মুকুন্দ"
                    },
                    {
                      type: "song",
                      uid: "NK17@1848",
                      title: "জয় রাধা-মাধব কুঞ্জবিহারী"
                    },
                    {
                      type: "song",
                      uid: "NK18@1849",
                      title: "রাধাবল্লভ রাধাবিনোদ"
                    },
                    {
                      type: "song",
                      uid: "NK19@1850",
                      title: "জয় যশোদা-নন্দন কৃষ্ণ"
                    },
                    {
                      type: "song",
                      uid: "NK20@1851",
                      title: "জয় জয় শ্রীকৃষ্ণচৈতন্য নিত্যানন্দ"
                    },
                    {
                      type: "heading",
                      uid: "*@1852",
                      title: "শ্রীমন্মহাপ্রভুর শতনাম ~ Śrīman Mahāprabhu’s Hundred Names"
                    },
                    {
                      type: "song",
                      uid: "NK21@1853",
                      title: "শ্রীমন্মহাপ্রভুর শতনাম"
                    },
                    {
                      type: "heading",
                      uid: "*@1854",
                      title: "শ্রীকৃষ্ণের বিংশোত্তর-শতনাম ~ Śrī Kṛṣṇa’s One Hundred-Twenty Names"
                    },
                    {
                      type: "song",
                      uid: "NK22@1855",
                      title: "শ্রীকৃষ্ণের বিংশোত্তর-শতনাম"
                    },
                    {
                      type: "heading",
                      uid: "*@1856",
                      title: "শ্রীকৃষ্ণের অষ্টোত্তরশতনাম ~ Śrī Kṛṣṇa’s One Hundred-Eight Names"
                    },
                    {
                      type: "song",
                      uid: "NK23@1857",
                      title: "শ্রীকৃষ্ণের অষ্টোত্তরশতনাম"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3b@1858",
                  title:
                    "অধিবাস-, অরুণোদয়-, শ্রীনগর- এবং ভজন-কীর্ত্তন ~ Adhivāsa-, Sunrise-, Nagara- and Bhajana-kīrtana",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1859",
                      title: "অধিবাস-কীর্ত্তন ~ Adhivāsa Kīrtana"
                    },
                    {
                      type: "song",
                      uid: "NK24@1860",
                      title: "জয় জয় নবদ্বীপ-মাঝ"
                    },
                    {
                      type: "heading",
                      uid: "*@1861",
                      title: "অরুণোদয়-কীর্ত্তন ~ Sunrise Kīrtana"
                    },
                    {
                      type: "song",
                      uid: "NM11@1862",
                      title: "উদিল অরুণ পূরব ভাগে"
                    },
                    {
                      type: "song",
                      uid: "NM12@1863",
                      title: "জীব জাগ, জীব জাগ"
                    },
                    {
                      type: "heading",
                      uid: "*@1864",
                      title: "শ্রীনগর-কীর্ত্তন ~ Śrī Nagara-kīrtana"
                    },
                    {
                      type: "song",
                      uid: "NK25@1865",
                      title: "নদীয়া-গোদ্রুমে নিত্যানন্দ মহাজন"
                    },
                    {
                      type: "song",
                      uid: "NK26@1866",
                      title: "বড় সুখের খবর গাই"
                    },
                    {
                      type: "song",
                      uid: "NK27@1867",
                      title: "গায় গোরা মধুর স্বরে"
                    },
                    {
                      type: "song",
                      uid: "NK28@1868",
                      title: "একবার ভাব মনে"
                    },
                    {
                      type: "song",
                      uid: "NK29@1869",
                      title: "‘রাধাকৃষ্ণ’ বল্ বল্"
                    },
                    {
                      type: "song",
                      uid: "NK30@1870",
                      title: "গায় গোরাচাঁদ জীবের তরে"
                    },
                    {
                      type: "song",
                      uid: "NK31@1871",
                      title: "অঙ্গ-উপাঙ্গ-অস্ত্র-পার্ষদ-সঙ্গে"
                    },
                    {
                      type: "song",
                      uid: "NK32@1872",
                      title: "নিতাই কি নাম এনেছে রে"
                    },
                    {
                      type: "song",
                      uid: "NK33@1873",
                      title: "হরি ব’লে মোদের গৌর এলো"
                    },
                    {
                      type: "heading",
                      uid: "*@1874",
                      title: "ভজন-কীর্ত্তন ~ Bhajana-kīrtana"
                    },
                    {
                      type: "song",
                      uid: "GP1@1875",
                      title: "ভজ রে ভজ রে আমার মন"
                    },
                    {
                      type: "song",
                      uid: "MS34@1876",
                      title: "ভাব না ভাব না, মন, তুমি অতি দুষ্ট"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3c@1877",
                  title: "শ্রীনাম-মহিমা ~ The Glories of Śrī Nāma",
                  children: [
                    {
                      type: "song",
                      uid: "NM13@1878",
                      title: "কৃষ্ণনাম ধরে কত বল"
                    },
                    {
                      type: "song",
                      uid: "NM14@1879",
                      title: "ভোজন-লালসে, রসনে আমার"
                    },
                    {
                      type: "song",
                      uid: "NM15@1880",
                      title: "সই, কেবা শুনাইল শ্যাম-নাম"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3d@1881",
                  title: "শ্রীনামাষ্টক ~ Śrī Nāmāṣṭaka",
                  children: [
                    {
                      type: "song",
                      uid: "NM1@1882",
                      title: "শ্রীরূপ-বদনে"
                    },
                    {
                      type: "song",
                      uid: "NM2@1883",
                      title: "জয় জয় হরিনাম"
                    },
                    {
                      type: "song",
                      uid: "NM3@1884",
                      title: "বিশ্বে উদিত নাম-তপন"
                    },
                    {
                      type: "song",
                      uid: "NM4@1885",
                      title: "জ্ঞানী জ্ঞানযোগে করিয়া যতনে"
                    },
                    {
                      type: "song",
                      uid: "NM5@1886",
                      title: "হরিনাম তুয়া অনেক স্বরূপ"
                    },
                    {
                      type: "song",
                      uid: "NM6@1887",
                      title: "বাচ্য বাচক—এই দুই স্বরূপ তোমার"
                    },
                    {
                      type: "song",
                      uid: "NM7@1888",
                      title: "ওহে হরিনাম তব মহিমা অপার"
                    },
                    {
                      type: "song",
                      uid: "NM8@1889",
                      title: "নারদ মুনি বাজায় বীণা"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3e@1890",
                  title: "শ্রীশিক্ষাষ্টকম্ ~ Śrī Śikṣāṣṭakam",
                  children: [
                    {
                      type: "song",
                      uid: "SI1@1891",
                      title: "পীতবরণ কলি-পাবন গোরা"
                    },
                    {
                      type: "song",
                      uid: "SI2@1892",
                      title: "তুহুঁ দয়া-সাগর"
                    },
                    {
                      type: "song",
                      uid: "SI3@1893",
                      title: "শ্রীকৃষ্ণকীর্ত্তনে যদি মানস তোঁহার"
                    },
                    {
                      type: "song",
                      uid: "SI4@1894",
                      title: "প্রভু! তব পদযুগে মোর নিবেদন"
                    },
                    {
                      type: "song",
                      uid: "SI5@1895",
                      title: "অনাদি করম-ফলে"
                    },
                    {
                      type: "song",
                      uid: "SI6@1896",
                      title: "অপরাধ-ফলে মম"
                    },
                    {
                      type: "song",
                      uid: "SI7@1897",
                      title: "গাইতে গাইতে নাম"
                    },
                    {
                      type: "song",
                      uid: "SI8@1898",
                      title: "গাইতে গোবিন্দ নাম"
                    },
                    {
                      type: "song",
                      uid: "SI9@1899",
                      title: "বন্ধুগণ! শুনহ বচন মোর"
                    },
                    {
                      type: "song",
                      uid: "SI10@1900",
                      title: "যোগপীঠোপরি স্থিত"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3f@1901",
                  title: "শ্রীউপদেশামৃত ~ Śrī Upadeśāmṛta",
                  children: [
                    {
                      type: "song",
                      uid: "U1@1902",
                      title: "হরি হে! প্রপঞ্চে পড়িয়া"
                    },
                    {
                      type: "song",
                      uid: "U2@1903",
                      title: "হরি হে! অর্থের সঞ্চয়ে"
                    },
                    {
                      type: "song",
                      uid: "U3@1904",
                      title: "হরি হে! ভজনে উৎসাহ"
                    },
                    {
                      type: "song",
                      uid: "U4@1905",
                      title: "হরি হে! দান-প্রতিগ্রহ"
                    },
                    {
                      type: "song",
                      uid: "U5@1906",
                      title: "হরি হে! সঙ্গদোষ-শূন্য"
                    },
                    {
                      type: "song",
                      uid: "U6@1907",
                      title: "হরি হে! নীরধর্ম্ম-গত"
                    },
                    {
                      type: "song",
                      uid: "U7@1908",
                      title: "হরি হে! তোমারে ভুলিয়া"
                    },
                    {
                      type: "song",
                      uid: "U8@1909",
                      title: "হরি হে! শ্রীরূপগোসাঞি"
                    },
                    {
                      type: "song",
                      uid: "U9@1910",
                      title: "বৈকুণ্ঠ হইতে শ্রেষ্ঠা ‘মথুরা’ নগরী"
                    },
                    {
                      type: "song",
                      uid: "U10@1911",
                      title: "সত্ত্বগুণে অধিষ্ঠিত পুণ্যবান্ কর্ম্মী"
                    },
                    {
                      type: "song",
                      uid: "U11@1912",
                      title: "শ্রীমতী রাধিকা—কৃষ্ণকান্তা-শিরোমণি"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3g@1913",
                  title: "শ্রীমনঃশিক্ষা ~ Instructions to the Mind",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1914",
                      title: "শ্রীমনঃশিক্ষা ~ Songs based on Śrīla Dāsa Gosvāmī’s Manaḥ-śikṣā"
                    },
                    {
                      type: "song",
                      uid: "MS2@1915",
                      title: "ব্রজধাম নিত্যধন, রাধাকৃষ্ণ দুইজন"
                    },
                    {
                      type: "song",
                      uid: "MS3@1916",
                      title: "গুরুদেবে, ব্রজবনে, ব্রজভূমিবাসী জনে"
                    },
                    {
                      type: "song",
                      uid: "MS4@1917",
                      title: "‘ধর্ম্ম’ বলি বেদে যারে"
                    },
                    {
                      type: "song",
                      uid: "MS5@1918",
                      title: "রাগাবেশে ব্রজধাম-বাসে যদি তীব্র কাম"
                    },
                    {
                      type: "song",
                      uid: "MS6@1919",
                      title: "কৃষ্ণবার্ত্তা বিনা আন"
                    },
                    {
                      type: "song",
                      uid: "MS7@1920",
                      title: "কাম-ক্রোধ-লোভ-মোহ-মদ-মৎসরতা-সহ"
                    },
                    {
                      type: "song",
                      uid: "MS8@1921",
                      title: "কাম-ক্রোধ-আদি করি’, বাহিরে সে-সব অরি"
                    },
                    {
                      type: "song",
                      uid: "MS9@1922",
                      title: "কপটতা হৈলে দূর"
                    },
                    {
                      type: "song",
                      uid: "MS10@1923",
                      title: "ব্রজভূমি চিন্তামণি"
                    },
                    {
                      type: "song",
                      uid: "MS11@1924",
                      title: "ব্রজবন-সুধাকর"
                    },
                    {
                      type: "song",
                      uid: "MS12@1925",
                      title: "সৌন্দর্য্য-কিরণমালা"
                    },
                    {
                      type: "song",
                      uid: "MS13@1926",
                      title: "ব্রজের নিকুঞ্জ-বনে"
                    },
                    {
                      type: "heading",
                      uid: "*@1927",
                      title: "উপদেশ (মনঃশিক্ষা) ~ Upadeśa — Instructions (from Kalyāṇa Kalpa-taru)"
                    },
                    {
                      type: "song",
                      uid: "MS15@1928",
                      title: "মন রে, কেন মিছে ভজিছ অসার ?"
                    },
                    {
                      type: "song",
                      uid: "MS16@1929",
                      title: "মন, তুমি ভালবাস কামের তরঙ্গ"
                    },
                    {
                      type: "song",
                      uid: "MS17@1930",
                      title: "মন রে, তুমি বড় সন্দিগ্ধ-অন্তর"
                    },
                    {
                      type: "song",
                      uid: "MS18@1931",
                      title: "মন, তুমি বড়ই পামর"
                    },
                    {
                      type: "song",
                      uid: "MS19@1932",
                      title: "মন, তব কেন এ সংশয় ?"
                    },
                    {
                      type: "song",
                      uid: "MS20@1933",
                      title: "মন, তুমি পড়িলে কি ছার ?"
                    },
                    {
                      type: "song",
                      uid: "MS21@1934",
                      title: "মন, যোগী হ’তে তোমার বাসনা"
                    },
                    {
                      type: "song",
                      uid: "MS22@1935",
                      title: "ওহে ভাই, মন কেন ব্রহ্ম হ’তে চায়"
                    },
                    {
                      type: "song",
                      uid: "MS23@1936",
                      title: "মন রে, কেন আর বর্ণ-অভিমান"
                    },
                    {
                      type: "song",
                      uid: "MS24@1937",
                      title: "মন রে, কেন কর বিদ্যার গৌরব"
                    },
                    {
                      type: "song",
                      uid: "MS25@1938",
                      title: "রূপের গৌরব কেন ভাই ?"
                    },
                    {
                      type: "song",
                      uid: "MS26@1939",
                      title: "মন রে, ধনমদ নিতান্ত অসার"
                    },
                    {
                      type: "song",
                      uid: "MS27@1940",
                      title: "মন, তুমি সন্ন্যাসী সাজিতে কেন চাও?"
                    },
                    {
                      type: "song",
                      uid: "MS28@1941",
                      title: "মন, তুমি তীর্থে সদা রত"
                    },
                    {
                      type: "song",
                      uid: "MS29@1942",
                      title: "দেখ মন, ব্রতে যেন না হও আচ্ছন্ন"
                    },
                    {
                      type: "song",
                      uid: "MS30@1943",
                      title: "মন, তুমি বড়ই চঞ্চল"
                    },
                    {
                      type: "song",
                      uid: "MS31@1944",
                      title: "মন, তোরে বলি এ বারতা"
                    },
                    {
                      type: "song",
                      uid: "MS32@1945",
                      title: "কি আর বলিব তোরে মন"
                    },
                    {
                      type: "song",
                      uid: "MS33@1946",
                      title: "কেন মন, কামেরে নাচাও প্রেম-প্রায় ?"
                    },
                    {
                      type: "heading",
                      uid: "*@1947",
                      title: "মনঃশিক্ষা (বিবিধ লেখক) ~ Instructions to the Mind (Various Authors)"
                    },
                    {
                      type: "song",
                      uid: "MS35@1948",
                      title: "ভজ ভজ হরি, মন দৃঢ় করি’"
                    },
                    {
                      type: "song",
                      uid: "MS36@1949",
                      title: "ভজহুঁ রে মন"
                    },
                    {
                      type: "song",
                      uid: "MS37@1950",
                      title: "ভোলা মন, একবার ভাব পরিণাম"
                    },
                    {
                      type: "song",
                      uid: "MS38@1951",
                      title: "ভজ মন! নন্দকুমার"
                    },
                    {
                      type: "song",
                      uid: "MS39@1952",
                      title: "সুখের লাগিয়া এ ঘর বাঁধিনু"
                    },
                    {
                      type: "song",
                      uid: "MS40@1953",
                      title: "দুষ্ট মন, তুমি কিসের বৈষ্ণব ?"
                    },
                    {
                      type: "heading",
                      uid: "*@1954",
                      title:
                        "মনঃশিক্ষা (প্রেমানন্দ দাস ঠাকুর) ~ Instructions to the Mind (by Premānanda dāsa Ṭhākura)"
                    },
                    {
                      type: "song",
                      uid: "MS41@1955",
                      title: "এ মন! আর কি মানুষ হবে"
                    },
                    {
                      type: "song",
                      uid: "MS42@1956",
                      title: "ওরে মন! শুন শুন তু অতি বর্ব্বর"
                    },
                    {
                      type: "song",
                      uid: "MS43@1957",
                      title: "ওরে মন! কিসে কর দেহের গুমান"
                    },
                    {
                      type: "song",
                      uid: "MS44@1958",
                      title: "এ মন! তুমি বা ভুলেছ কিসে"
                    },
                    {
                      type: "song",
                      uid: "MS45@1959",
                      title: "ওরে মন! কি রসে হৈয়া রৈলি ভোর"
                    },
                    {
                      type: "song",
                      uid: "MS46@1960",
                      title: "ওরে মন! শুন শুন তু বড়ি গোঙার"
                    },
                    {
                      type: "song",
                      uid: "MS47@1961",
                      title: "ওরে মন! রুচি নহে কেন কৃষ্ণ-নাম"
                    },
                    {
                      type: "song",
                      uid: "MS48@1962",
                      title: "ওরে মন! ধন-জন-জীবন-যৌবন"
                    },
                    {
                      type: "song",
                      uid: "MS49@1963",
                      title: "ওরে মন! এবার বুঝিব ভারিভুরি"
                    },
                    {
                      type: "song",
                      uid: "MS50@1964",
                      title: "এ মন! কি লাগি’ আইলি ভবে"
                    },
                    {
                      type: "song",
                      uid: "MS51@1965",
                      title: "ওরে মন! আর কি হইবে হেন জন্ম"
                    },
                    {
                      type: "song",
                      uid: "MS52@1966",
                      title: "ওরে মন! কিবা তুমি বিচারি না চাও"
                    },
                    {
                      type: "song",
                      uid: "MS53@1967",
                      title: "ওরে মন! কেন হেন বুঝ বিপরীত"
                    },
                    {
                      type: "song",
                      uid: "MS54@1968",
                      title: "ওরে মন! নিবেদন শুনহ আমার"
                    },
                    {
                      type: "song",
                      uid: "MS55@1969",
                      title: "ওরে মন! ভাবিয়া না বুঝ আপনাকে"
                    },
                    {
                      type: "song",
                      uid: "MS56@1970",
                      title: "ওরে মন! স্বর্গ নরক বুঝ কোথা"
                    },
                    {
                      type: "song",
                      uid: "MS57@1971",
                      title: "এ মন! তুমি সে কেবল ভূত"
                    },
                    {
                      type: "song",
                      uid: "MS58@1972",
                      title: "এ মন! তুমি কি ভেবেছ সুখ"
                    },
                    {
                      type: "song",
                      uid: "MS59@1973",
                      title: "এ মন! তু বড় কলির ভূত"
                    },
                    {
                      type: "song",
                      uid: "MS60@1974",
                      title: "ওরে মন! এবে তোর এ কেমন রীত"
                    },
                    {
                      type: "song",
                      uid: "MS61@1975",
                      title: "এ মন! কি করে বরণ-কুল"
                    },
                    {
                      type: "song",
                      uid: "MS62@1976",
                      title: "ওরে মন! কি ভয় শমনে করি আর"
                    },
                    {
                      type: "song",
                      uid: "MS63@1977",
                      title: "ওরে ভাই! কৃষ্ণ সে এ তিন-লোক-বন্ধু"
                    },
                    {
                      type: "song",
                      uid: "MS64@1978",
                      title: "ওরে মন! সাধু সঙ্গে করহ বসতি"
                    },
                    {
                      type: "song",
                      uid: "MS65@1979",
                      title: "এ মন! বিচারি কেন না চাও"
                    },
                    {
                      type: "song",
                      uid: "MS66@1980",
                      title: "ওরে মন! কৃষ্ণনাম-সম নাহি আন"
                    },
                    {
                      type: "song",
                      uid: "MS67@1981",
                      title: "এ মন! ‘হরিনাম’ কর সার"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3h@1982",
                  title: "শরণাগতি ~ Śaraṇāgati",
                  children: [
                    {
                      type: "heading",
                      uid: "*@1983",
                      title: "ষড়ঙ্গ শরণাগতি ~ The Six Limbs of Surrender"
                    },
                    {
                      type: "song",
                      uid: "S1@1984",
                      title: "শ্রীকৃষ্ণচৈতন্য প্রভু জীবে দয়া করি’"
                    },
                    {
                      type: "heading",
                      uid: "*@1985",
                      title:
                        "১. দৈন্য—দুঃখাত্মক, ত্রাসাত্মক, অপরাধাত্মক ও লজ্জাত্মক ~ 1. Dainya [Humility]"
                    },
                    {
                      type: "song",
                      uid: "S2@1986",
                      title: "ভুলিয়া তোমারে"
                    },
                    {
                      type: "song",
                      uid: "S3@1987",
                      title: "(প্রভু হে!) শুন মোর দুঃখের কাহিনী"
                    },
                    {
                      type: "song",
                      uid: "S4@1988",
                      title: "বিদ্যার বিলাসে, কাটাইনু কাল"
                    },
                    {
                      type: "song",
                      uid: "S5@1989",
                      title: "যৌবনে যখন, ধন-উপার্জ্জনে"
                    },
                    {
                      type: "song",
                      uid: "S6@1990",
                      title: "(প্রভু হে!) তুয়া পদে এ মিনতি মোর"
                    },
                    {
                      type: "song",
                      uid: "S7@1991",
                      title: "আমার জীবন, সদা পাপে রত"
                    },
                    {
                      type: "song",
                      uid: "S8@1992",
                      title: "নিবেদন করি প্রভু! তোমার চরণে"
                    },
                    {
                      type: "song",
                      uid: "S9@1993",
                      title: "(প্রাণেশ্বর!) কহবুঁ কি সরম্ কি বাত্"
                    },
                    {
                      type: "heading",
                      uid: "*@1994",
                      title:
                        "২. আত্মনিবেদন—মমতাস্পদ দেহসমর্পণ, অহংতাস্পদ দেহীসমর্পণ ~ 2. Ātma Nivedana [Submission of the Self]"
                    },
                    {
                      type: "song",
                      uid: "S10@1995",
                      title: "সর্ব্বস্ব তোমার"
                    },
                    {
                      type: "song",
                      uid: "S11@1996",
                      title: "বস্তুতঃ সকলি তব"
                    },
                    {
                      type: "song",
                      uid: "S12@1997",
                      title: "‘আমার’ বলিতে প্রভু! আর কিছু নাই"
                    },
                    {
                      type: "song",
                      uid: "S13@1998",
                      title: "দারা-পুত্র-নিজ-দেহ-কুটুম্ব-পালনে"
                    },
                    {
                      type: "song",
                      uid: "S14@1999",
                      title: "মানস, দেহ, গেহ, যো কিছু মোর"
                    },
                    {
                      type: "song",
                      uid: "S15@2000",
                      title: "‘অহং’-‘মম’-শব্দ-অর্থে যাহা কিছু হয়"
                    },
                    {
                      type: "song",
                      uid: "S16@2001",
                      title: "আত্মনিবেদন, তুয়া পদে করি’"
                    },
                    {
                      type: "heading",
                      uid: "*@2002",
                      title:
                        "৩. গোপ্তৃত্বে বরণ—অবশ্য রক্ষিবে কৃষ্ণ ~ 3. Goptṛtve Varaṇa [Acceptance of the Lord as One’s Only Maintainer]"
                    },
                    {
                      type: "song",
                      uid: "S17@2003",
                      title: "তুমি সর্ব্বেশ্বরেশ্বর"
                    },
                    {
                      type: "song",
                      uid: "S18@2004",
                      title: "কি জানি কি বলে"
                    },
                    {
                      type: "song",
                      uid: "S19@2005",
                      title: "না করলুঁ করম, গেয়ান নাহি ভেল"
                    },
                    {
                      type: "heading",
                      uid: "*@2006",
                      title:
                        "৪. অবশ্য রক্ষিবে কৃষ্ণ—এইরূপ বিশ্বাস ~ 4. Avaśya Rakṣibe Kṛṣṇa – Ei-rūpe Viśvāsa [Faith in Kṛṣṇa’s Protection]"
                    },
                    {
                      type: "song",
                      uid: "S20@2007",
                      title: "এখন বুঝিনু প্রভু!"
                    },
                    {
                      type: "song",
                      uid: "S21@2008",
                      title: "তুমি ত’ মারিবে যা’রে, কে তা’রে রাখিতে পারে"
                    },
                    {
                      type: "heading",
                      uid: "*@2009",
                      title: "৫. অনুকূল-গ্রহণ ~ 5. Anukūla-grahaṇa [Acceptance of the Favorable]"
                    },
                    {
                      type: "song",
                      uid: "S22@2010",
                      title: "তুয়া ভক্তি-অনুকূল যে-যে কার্য্য হয়"
                    },
                    {
                      type: "song",
                      uid: "E1@2011",
                      title: "শুদ্ধভকত-চরণ-রেণু"
                    },
                    {
                      type: "heading",
                      uid: "*@2012",
                      title:
                        "৬. প্রতিকূল-বর্জ্জন ~ 6. Pratikūla-varjana [Rejection of the Unfavorable]"
                    },
                    {
                      type: "song",
                      uid: "S23@2013",
                      title: "কেশব! তুয়া জগত বিচিত্র"
                    },
                    {
                      type: "song",
                      uid: "S24@2014",
                      title: "তুয়া ভক্তি-প্রতিকূল ধর্ম্ম যা’তে রয়"
                    },
                    {
                      type: "song",
                      uid: "S25@2015",
                      title: "বিষয়বিমূঢ় আর মায়াবাদী জন"
                    },
                    {
                      type: "heading",
                      uid: "*@2016",
                      title: "সিদ্ধদেহে—গোপ্তৃত্বে বরণ ~ Within One’s Siddha-deha"
                    },
                    {
                      type: "heading",
                      uid: "*@2017",
                      title: "সিদ্ধদেহে—আত্মনিবেদন ~ Within One’s Siddha-deha"
                    },
                    {
                      type: "song",
                      uid: "S27@2018",
                      title: "আত্মসমর্পণে গেলা অভিমান"
                    },
                    {
                      type: "heading",
                      uid: "*@2019",
                      title: "সিদ্ধদেহে—অনুকূল-গ্রহণ ~ Within One’s Siddha-deha"
                    },
                    {
                      type: "song",
                      uid: "S28@2020",
                      title: "গোদ্রুমধামে ভজন-অনুকূলে"
                    },
                    {
                      type: "heading",
                      uid: "*@2021",
                      title: "সিদ্ধদেহে—প্রতিকূল-বর্জ্জন ~ Within One’s Siddha-deha"
                    },
                    {
                      type: "song",
                      uid: "S29@2022",
                      title: "আমি ত স্বানন্দ-সুখদবাসী"
                    },
                    {
                      type: "heading",
                      uid: "*@2023",
                      title: "সিদ্ধদেহে—কৃষ্ণভজনের উদ্দীপন ~ Stimuli for Kṛṣṇa-bhajana (5th limb)"
                    },
                    {
                      type: "song",
                      uid: "S30@2024",
                      title: "রাধাকুণ্ডতট"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3i@2025",
                  title: "শ্রেয়োনির্ণয় ~ Ascertaining the Sumum Bonum",
                  children: [
                    {
                      type: "song",
                      uid: "S31@2026",
                      title: "কৃষ্ণভক্তি বিনা কভু নাহি ফলোদয়"
                    },
                    {
                      type: "song",
                      uid: "S32@2027",
                      title: "আর কেন মায়াজালে পড়িতেছ জীব মীন"
                    },
                    {
                      type: "song",
                      uid: "S33@2028",
                      title: "পীরিতি সচ্চিদানন্দে রূপবতী নারী"
                    },
                    {
                      type: "song",
                      uid: "S34@2029",
                      title: "‘নিরাকার নিরাকার’ করিয়া চীৎকার"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3j@2030",
                  title:
                    "যামুন-ভাবাবলী (শান্ত-দাস্য-ভক্তিসাধন-লালসা) The Ecstasies of Yamunācārya [Yearning to Practice Śānta- and Dāsya-bhakti]",
                  children: [
                    {
                      type: "song",
                      uid: "Y1@2031",
                      title: "হরি হে! ওহে প্রভু দয়াময়"
                    },
                    {
                      type: "song",
                      uid: "Y2@2032",
                      title: "হরি হে! তোমার ঈক্ষণে হয়"
                    },
                    {
                      type: "song",
                      uid: "Y3@2033",
                      title: "হরি হে! পরতত্ত্ব বিচক্ষণ"
                    },
                    {
                      type: "song",
                      uid: "Y4@2034",
                      title: "হরি হে! জগতের বস্তু যত"
                    },
                    {
                      type: "song",
                      uid: "Y5@2035",
                      title: "হরি হে! তুমি সর্ব্বগুণযুত"
                    },
                    {
                      type: "song",
                      uid: "Y6@2036",
                      title: "হরি হে! তোমার গম্ভীর মন"
                    },
                    {
                      type: "song",
                      uid: "Y7@2037",
                      title: "হরি হে! মায়াবদ্ধ যতক্ষণ"
                    },
                    {
                      type: "song",
                      uid: "Y8@2038",
                      title: "হরি হে! ধর্ম্মনিষ্ঠা নাহি মোর"
                    },
                    {
                      type: "song",
                      uid: "Y9@2039",
                      title: "হরি হে! হেন দুষ্ট কর্ম্ম নাই"
                    },
                    {
                      type: "song",
                      uid: "Y10@2040",
                      title: "হরি হে! নিজকর্ম্ম-দোষ-ফলে"
                    },
                    {
                      type: "song",
                      uid: "Y11@2041",
                      title: "হরি হে! অন্য আশা নাহি যা’র"
                    },
                    {
                      type: "song",
                      uid: "Y12@2042",
                      title: "হরি হে! তব পদ-পঙ্কজিনী"
                    },
                    {
                      type: "song",
                      uid: "Y13@2043",
                      title: "হরি হে! ভ্রমিতে সংসার-বনে"
                    },
                    {
                      type: "song",
                      uid: "Y14@2044",
                      title: "হরি হে! তোমার চরণপদ্ম"
                    },
                    {
                      type: "song",
                      uid: "Y15@2045",
                      title: "হরি হে! তবাঙ্ঘ্রি কমলদ্বয়"
                    },
                    {
                      type: "song",
                      uid: "Y16@2046",
                      title: "হরি হে! আমি সেই দুষ্টমতি"
                    },
                    {
                      type: "song",
                      uid: "Y17@2047",
                      title: "হরি হে! আমি অপরাধি-জন"
                    },
                    {
                      type: "song",
                      uid: "Y18@2048",
                      title: "হরি হে! অবিবেকরূপ ঘন"
                    },
                    {
                      type: "song",
                      uid: "Y19@2049",
                      title: "হরি হে! অগ্রে এক নিবেদন"
                    },
                    {
                      type: "song",
                      uid: "Y20@2050",
                      title: "হরি হে! তোমা ছাড়ি’ আমি কভু"
                    },
                    {
                      type: "song",
                      uid: "Y21@2051",
                      title: "হরি হে! স্ত্রী-পুরুষ-দেহগত"
                    },
                    {
                      type: "song",
                      uid: "Y22@2052",
                      title: "হরি হে! বেদবিধি-অনুসারে"
                    },
                    {
                      type: "song",
                      uid: "Y23@2053",
                      title: "হরি হে! তোমার যে শুদ্ধভক্ত"
                    },
                    {
                      type: "song",
                      uid: "Y24@2054",
                      title: "হরি হে! শুন হে মধুমথন!"
                    },
                    {
                      type: "song",
                      uid: "Y25@2055",
                      title: "হরি হে! আমি নরপশুপ্রায়"
                    },
                    {
                      type: "song",
                      uid: "Y26@2056",
                      title: "হরি হে! তুমি জগতের পিতা"
                    },
                    {
                      type: "song",
                      uid: "Y27@2057",
                      title: "হরি হে! আমি ত’ চঞ্চলমতি"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3k@2058",
                  title: "উপলব্ধি ~ Upalabdhi—Realization",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2059",
                      title: "অনুতাপ-লক্ষণ-উপলব্ধি ~ Realization Characterized by Remorse (Anutāpa)"
                    },
                    {
                      type: "song",
                      uid: "UP1@2060",
                      title: "আমি অতি পামর দুর্জ্জন"
                    },
                    {
                      type: "song",
                      uid: "UP2@2061",
                      title: "সাধুসঙ্গ না হইল হায়!"
                    },
                    {
                      type: "song",
                      uid: "UP3@2062",
                      title: "ওরে মন, কর্ম্মের কুহরে গেল কাল"
                    },
                    {
                      type: "song",
                      uid: "UP4@2063",
                      title: "ওরে মন, কি বিপদ হইল আমার"
                    },
                    {
                      type: "song",
                      uid: "UP5@2064",
                      title: "ওরে মন, ক্লেশ-তাপ দেখি যে অশেষ!"
                    },
                    {
                      type: "heading",
                      uid: "*@2065",
                      title:
                        "নির্ব্বেদ-লক্ষণ-উপলব্ধি ~ Realization Characterized by Repentance (Nirbeda)"
                    },
                    {
                      type: "song",
                      uid: "UP6@2066",
                      title: "ওরে মন, ভাল নাহি লাগে এ সংসার"
                    },
                    {
                      type: "song",
                      uid: "UP7@2067",
                      title: "ওরে মন, বাড়িবার আশা কেন কর’?"
                    },
                    {
                      type: "song",
                      uid: "UP8@2068",
                      title: "ওরে মন, ভুক্তি-মুক্তি-স্পৃহা কর’ দূর"
                    },
                    {
                      type: "song",
                      uid: "UP9@2069",
                      title: "দুর্ল্লভ মানব-জন্ম লভিয়া সংসারে"
                    },
                    {
                      type: "song",
                      uid: "UP10@2070",
                      title: "শরীরের সুখে, মন, দেহ জলাঞ্জলি"
                    },
                    {
                      type: "heading",
                      uid: "*@2071",
                      title:
                        "সম্বন্ধাভিদেয়-প্রয়োজন-বিজ্ঞান-উপলব্ধি ~ Realization Characterized by Knowledge of Sambandha, Abhideya and Prayojana"
                    },
                    {
                      type: "heading",
                      uid: "*(সম্বন্ধ-বিজ্ঞান-লক্ষণ-উপলব্ধি)",
                      title: "~ Realization of Sambandha"
                    },
                    {
                      type: "song",
                      uid: "UP11@2072",
                      title: "ওরে মন, বলি, শুন তত্ত্ব-বিবরণ"
                    },
                    {
                      type: "heading",
                      uid: "*(অভিধেয়-বিজ্ঞান-উপলব্ধি)",
                      title: "~ Realization of Abhideya"
                    },
                    {
                      type: "song",
                      uid: "UP12@2073",
                      title: "ভ্রমিতে ভ্রমিতে যদি সাধুসঙ্গ হয়"
                    },
                    {
                      type: "heading",
                      uid: "*(প্রয়োজন-বিজ্ঞান-উপলব্ধি)",
                      title: "~ Realization of Prayojana"
                    },
                    {
                      type: "song",
                      uid: "UP13@2074",
                      title: "অপূর্ব্ব বৈষ্ণব-তত্ত্ব! আত্মার আনন্দ-"
                    },
                    {
                      type: "song",
                      uid: "UP14@2075",
                      title: "চিজ্জড়ের দ্বৈত যিনি করেন স্থাপন"
                    },
                    {
                      type: "song",
                      uid: "UP15@2076",
                      title: "‘জীবন-সমাপ্তি-কালে করিব ভজন"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3l@2077",
                  title: "উচ্ছ্বস-দৈন্যময়ী প্রার্থনা ~ Prayers Overflowing With Humility",
                  children: [
                    {
                      type: "song",
                      uid: "P3@2078",
                      title: "ভবার্ণবে প’ড়ে মোর আকুল পরাণ"
                    },
                    {
                      type: "song",
                      uid: "P4@2079",
                      title: "আমার সমান হীন নাহি এ সংসারে"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3m@2080",
                  title: "শ্রীষড়্‌গোস্বামি-শোচক ~ Lamenting in Separation from the Six Gosvāmīs",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2081",
                      title:
                        "শ্রীমদ্রূপ-গোস্বামি-প্রভুর শোচক ~ Lamenting in Separation from Śrīla Rūpa Gosvāmī"
                    },
                    {
                      type: "song",
                      uid: "GV41@2082",
                      title: "শ্রীমদ্‌রূপ-গোস্বামী-প্রভুর শোচক (১)"
                    },
                    {
                      type: "song",
                      uid: "GV42@2083",
                      title: "শ্রীমদ্‌রূপ-গোস্বামী-প্রভুর শোচক (২)"
                    },
                    {
                      type: "song",
                      uid: "GV43@2084",
                      title: "যঙ্ কলি রূপ শরীর ন ধরত"
                    },
                    {
                      type: "song",
                      uid: "GV44@2085",
                      title: "শ্রীরূপমঞ্জরী-পদ"
                    },
                    {
                      type: "heading",
                      uid: "*@2086",
                      title:
                        "শ্রীল-সনাতন-গোস্বামি-প্রভুর শোচক ~ Lamenting in Separation from Śrīla Sanātana Gosvāmī"
                    },
                    {
                      type: "song",
                      uid: "GV38@2087",
                      title: "শ্রীল-সনাতন-গোস্বামি-প্রভুর শোচক (১)"
                    },
                    {
                      type: "song",
                      uid: "GV39@2088",
                      title: "শ্রীল-সনাতন-গোস্বামি-প্রভুর শোচক (২)"
                    },
                    {
                      type: "song",
                      uid: "GV40@2089",
                      title: "শ্রীল-সনাতন-গোস্বামি-প্রভুর শোচক (৩)"
                    },
                    {
                      type: "heading",
                      uid: "*@2090",
                      title:
                        "শ্রীল-রঘুনাথদাস-গোস্বামি-প্রভুর শোচক ~ Lamenting in Separation from Śrīla Raghunātha dāsa Gosvāmī"
                    },
                    {
                      type: "song",
                      uid: "GV36@2091",
                      title: "শ্রীল-রঘুনাথদাস-গোস্বামি-প্রভুর শোচক (১)"
                    },
                    {
                      type: "song",
                      uid: "GV37@2092",
                      title: "শ্রীল-রঘুনাথদাস-গোস্বামি-প্রভুর শোচক (২)"
                    },
                    {
                      type: "heading",
                      uid: "*@2093",
                      title:
                        "শ্রীল-জীব-গোস্বামি-প্রভুর শোচক ~ Lamenting in Separation from Śrīla Jīva Gosvāmī"
                    },
                    {
                      type: "song",
                      uid: "GV35@2094",
                      title: "শ্রীল-জীব-গোস্বামী-প্রভুর শোচক"
                    },
                    {
                      type: "heading",
                      uid: "*@2095",
                      title:
                        "শ্রীল-গোপালভট্ট-গোস্বামি-প্রভুর শোচক ~ Lamenting in Separation from Śrīla Gopāla Bhaṭṭa Gosvāmī"
                    },
                    {
                      type: "song",
                      uid: "GV34@2096",
                      title: "শ্রীল-গোপালভট্ট-গোস্বামী-প্রভুর শোচক"
                    },
                    {
                      type: "heading",
                      uid: "*@2097",
                      title:
                        "শ্রীল-রঘুনাথভট্ট-গোস্বামি-প্রভুর শোচক ~ Lamenting in Separation from Śrīla Raghunātha Bhaṭṭa Gosvāmī"
                    },
                    {
                      type: "song",
                      uid: "GV32@2098",
                      title: "শ্রীল-রঘুনাথভট্ট-গোস্বামী-প্রভুর শোচক"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3n@2099",
                  title:
                    "শ্রীল নরোত্তম ঠাকুরের শোচক ~ Lamenting in Separation from Śrīla Narottama dāsa Ṭhākura",
                  children: [
                    {
                      type: "song",
                      uid: "GV27@2100",
                      title: "শ্রীল-নরোত্তম-ঠাকুরের শোচক"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3o@2101",
                  title:
                    "শোক-শাতন (শ্রীগৌরাঙ্গ-লীলাচরিত্র) ~ The Vanquisher of Bereavement [A Description of Śrī Gaurāṅga’s Pastimes]",
                  children: [
                    {
                      type: "song",
                      uid: "GH64@2102",
                      title: "প্রদোষ-সময়ে, শ্রীবাস-অঙ্গনে"
                    },
                    {
                      type: "song",
                      uid: "GH65@2103",
                      title: "প্রবেশিয়া অন্তঃপুরে, নারীগণে শান্ত করে"
                    },
                    {
                      type: "song",
                      uid: "GH66@2104",
                      title: "ধন, জন, দেহ, গেহ কৃষ্ণে সমর্পণ"
                    },
                    {
                      type: "song",
                      uid: "GH67@2105",
                      title: "সবু মেলি’ বালক-ভাগ বিচারি’"
                    },
                    {
                      type: "song",
                      uid: "GH68@2106",
                      title: "শ্রীবাস-বচন, শ্রবণ করিয়া"
                    },
                    {
                      type: "song",
                      uid: "GH69@2107",
                      title: "প্রভুর বচন, শুনিয়া তখন"
                    },
                    {
                      type: "song",
                      uid: "GH70@2108",
                      title: "গোরাচাঁদের আজ্ঞা পে’য়ে, গৃহবাসিগণ"
                    },
                    {
                      type: "song",
                      uid: "GH71@2109",
                      title: "“পূর্ণচিদানন্দ তুমি, তোমার চিৎকণ আমি"
                    },
                    {
                      type: "song",
                      uid: "GH72@2110",
                      title: "“বাঁধিল মায়া যেদিন হ’তে"
                    },
                    {
                      type: "song",
                      uid: "GH73@2111",
                      title: "শ্রীবাসে কহেন প্রভু—“তুহুঁ মোর দাস"
                    },
                    {
                      type: "song",
                      uid: "GH74@2112",
                      title: "শ্রীবাসের প্রতি চৈতন্য-প্রসাদ"
                    },
                    {
                      type: "song",
                      uid: "GH75@2113",
                      title: "মৃত শিশু ল’য়ে তবে ভকতবৎসল"
                    },
                    {
                      type: "song",
                      uid: "GH76@2114",
                      title: "सुन्दर लाला शचीर-दुलाला"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3p@2115",
                  title: "বাউল-সঙ্গীত ~ Songs of a Madman",
                  children: [
                    {
                      type: "song",
                      uid: "BS1@2116",
                      title: "আমি তোমার দুঃখের দুঃখী, সুখের সুখী"
                    },
                    {
                      type: "song",
                      uid: "BS2@2117",
                      title: "ধর্ম্মপথে থাকি’ কর জীবন যাপন, ভাই"
                    },
                    {
                      type: "song",
                      uid: "BS3@2118",
                      title: "আসল কথা বল্‌তে কি"
                    },
                    {
                      type: "song",
                      uid: "BS4@2119",
                      title: "‘বাউল বাউল’ বল্‌ছ সবে"
                    },
                    {
                      type: "song",
                      uid: "BS5@2120",
                      title: "মানুষ-ভজন কর্‌ছো, ও ভাই"
                    },
                    {
                      type: "song",
                      uid: "BS6@2121",
                      title: "এও ত’ এক কলির চেলা"
                    },
                    {
                      type: "song",
                      uid: "BS7@2122",
                      title: "(মন আমার) হুঁসা’র থেকো, ভুল’ নাক"
                    },
                    {
                      type: "song",
                      uid: "BS8@2123",
                      title: "মনের মালা জপ্‌বি যখন, মন"
                    },
                    {
                      type: "song",
                      uid: "BS9@2124",
                      title: "ঘরে ব’সে বাউল হও রে মন"
                    },
                    {
                      type: "song",
                      uid: "BS10@2125",
                      title: "বলান্ বৈরাগী ঠাকুর, কিন্তু গৃহীর মধ্যে বাহাদুর"
                    },
                    {
                      type: "song",
                      uid: "BS11@2126",
                      title: "কেন ভেকের প্রয়াস ?"
                    },
                    {
                      type: "song",
                      uid: "BS12@2127",
                      title: "হ’য়ে বিষয়ে আবেশ, পে’লে মন, যাতনা অশেষ"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3q@2128",
                  title: "আরতি ~ Ārati",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2129",
                      title: "শ্রীগুরুদেবের আরতি ~ Ārati of Śrī Gurudeva (Śrīla BP Keśava Gosvāmī)"
                    },
                    {
                      type: "song",
                      uid: "A1@2130",
                      title: "জয় জয় গুরুদেব শ্রীভক্তিপ্রজ্ঞান"
                    },
                    {
                      type: "heading",
                      uid: "*@2131",
                      title: "শ্রীল প্রভুপাদের আরতি ~ Ārati of Śrīla Prabhupāda"
                    },
                    {
                      type: "song",
                      uid: "A2@2132",
                      title: "শ্রী প্রভুপাদের আরতি [GVS &amp; IPBYS]"
                    },
                    {
                      type: "heading",
                      uid: "*@2133",
                      title: "মঙ্গল-আরতি ~ Maṅgala-ārati"
                    },
                    {
                      type: "song",
                      uid: "A4@2134",
                      title: "শ্রীমঙ্গল-আরতি [GVS &amp; IPBYS]"
                    },
                    {
                      type: "song",
                      uid: "A5@2135",
                      title: "মঙ্গল আরতি গৌরকিশোর"
                    },
                    {
                      type: "song",
                      uid: "A6@2136",
                      title: "মঙ্গল আরতি যুগলকিশোর"
                    },
                    {
                      type: "heading",
                      uid: "*@2137",
                      title: "ভোগ-আরতি ~ Bhoga-ārati"
                    },
                    {
                      type: "song",
                      uid: "A9@2138",
                      title: "ভজ ভকত-বৎসল শ্রীগৌরহরি"
                    },
                    {
                      type: "heading",
                      uid: "*@2139",
                      title: "সন্ধ্যা-আরতি ~ Sandhyā-ārati"
                    },
                    {
                      type: "song",
                      uid: "A10@2140",
                      title: "শ্রীগৌর-আরতি"
                    },
                    {
                      type: "song",
                      uid: "A11@2141",
                      title: "শ্রীযুগল-আরতি"
                    },
                    {
                      type: "song",
                      uid: "A3@2142",
                      title: "ভালে গোরা-গদাধরের আরতি নেহারি"
                    },
                    {
                      type: "song",
                      uid: "A12@2143",
                      title: "শ্রীরাধারাণীর আরতি"
                    },
                    {
                      type: "song",
                      uid: "A13@2144",
                      title: "শ্রীমদনগোপাল-আরতি"
                    },
                    {
                      type: "heading",
                      uid: "*@2145",
                      title: "শ্রীতুলসী-আরতি ~ Śrī Tulasī-ārati"
                    },
                    {
                      type: "song",
                      uid: "A14@2146",
                      title: "‘নমো নমঃ তুলসী কৃষ্ণ-প্রেয়সী’ (নমো নমঃ)"
                    },
                    {
                      type: "song",
                      uid: "A15@2147",
                      title: "নমো নমঃ তুলসী কৃষ্ণ-প্রেয়সী"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3r@2148",
                  title:
                    "একাদশী, ব্রতপারণ, শ্রীমহাপ্রসাদ-মাহাত্ম্য ~ Ekādaśī, Breaking the Fast, The Glories of Mahāprasāda",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2149",
                      title:
                        "শ্রীমন্মহাপ্রভুর হরিবাসর ব্রতপালন ~ Śrīman Mahāprabhu’s Observance of Hari-vāsara (Ekādaśī)"
                    },
                    {
                      type: "song",
                      uid: "E2@2150",
                      title: "শ্রীমন্মহাপ্রভুর হরিবাসর-ব্রতপালন"
                    },
                    {
                      type: "heading",
                      uid: "*@2151",
                      title:
                        "ব্রতপারণে মহাপ্রসাদ-সম্মান-বিচর ~ Honoring Mahāprasāda at the Time of Pāraṇa"
                    },
                    {
                      type: "song",
                      uid: "E3@2152",
                      title: "ব্রত-পারণে মহাপ্রসাদ-সম্মান-বিচার"
                    },
                    {
                      type: "heading",
                      uid: "*@2153",
                      title: "শ্রীমহাপ্রসাদ-মাহাত্ম্য-কীর্ত্তন ~ The Glories of Śrī Mahāprasāda"
                    },
                    {
                      type: "song",
                      uid: "PR1@2154",
                      title: "ভাইরে! শরীর অবিদ্যা-জাল"
                    },
                    {
                      type: "song",
                      uid: "PR2@2155",
                      title: "ভাইরে! একদিন শান্তিপুরে"
                    },
                    {
                      type: "song",
                      uid: "PR3@2156",
                      title: "ভাইরে! শচীর অঙ্গনে কভু"
                    },
                    {
                      type: "song",
                      uid: "PR4@2157",
                      title: "ভাইরে! শ্রীচৈতন্য-নিত্যানন্দ"
                    },
                    {
                      type: "song",
                      uid: "PR5@2158",
                      title: "ভাইরে! একদিন নীলাচলে"
                    },
                    {
                      type: "song",
                      uid: "PR6@2159",
                      title: "ভাইরে! রামকৃষ্ণ গোচারণে"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3s@2160",
                  title:
                    "শ্রীপ্রম-বিবর্ত্ত থেকে কিছু উপদেশ ~ Some Instructions from Śrī Prema-vivarta",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2161",
                      title:
                        "জীবের দুর্গতি ও সাধুসঙ্গে নিস্তার ~ The Jīva’s Affliction and Deliverance Through Sādhu-saṅga"
                    },
                    {
                      type: "song",
                      uid: "X4@2162",
                      title: "জীবের দুর্গতি ও সাধুসঙ্গে নিস্তার"
                    },
                    {
                      type: "heading",
                      uid: "*@2163",
                      title: "শ্রীনামভজন-প্রণালী ~ The Method to Perform Śrī Nāma-bhajana"
                    },
                    {
                      type: "song",
                      uid: "X5@2164",
                      title: "শ্রীনামভজন-প্রণালী"
                    },
                    {
                      type: "heading",
                      uid: "*@2165",
                      title:
                        "গৃহস্থ ও বৈরাগীর প্রতি আদেশ ~ Instructions for Householders and Renunciants"
                    },
                    {
                      type: "song",
                      uid: "X6@2166",
                      title: "গৃহস্থ ও বৈরাগীর প্রতি আদেশ"
                    },
                    {
                      type: "heading",
                      uid: "*@2167",
                      title: "বিশুদ্ধ বৈরাগী ও তাঁহার কর্ত্তব্য ~ A Pure Vairāgī and His Duty"
                    },
                    {
                      type: "song",
                      uid: "X7@2168",
                      title: "বিশুদ্ধ বৈরাগী ও তাঁহার কর্ত্তব্য"
                    },
                    {
                      type: "heading",
                      uid: "*@2169",
                      title:
                        "সরল মনে ‘গোরা’-ভজন ও কপট-ভজন ~ Pure-Hearted Gorā-bhajana and Duplicitous Bhajana"
                    },
                    {
                      type: "song",
                      uid: "X8@2170",
                      title: "সরল-মনে ‘গোরা’-ভজন ও কপট-ভজন"
                    }
                  ]
                }
              ]
            },
            {
              type: "collection",
              uid: "4@2171",
              title: "অন্যান্য ভাষা এবং পরিশিষ্ট ~ Other Languages and Addendum",
              children: [
                {
                  type: "collection",
                  uid: "4a@2172",
                  title: "হিন্দী-কীর্ত্তন ~ Hindi Kīrtana",
                  children: [
                    {
                      type: "song",
                      uid: "G13@2173",
                      title: "गुरु-चरणकमल भज मन"
                    },
                    {
                      type: "song",
                      uid: "NK9@2174",
                      title: "জয় শচীনন্দন, জয় গৌরহরি"
                    },
                    {
                      type: "song",
                      uid: "B1@2175",
                      title: "जय माधव मदनमुरारी"
                    },
                    {
                      type: "song",
                      uid: "B3@2176",
                      title: "ब्रज-जन मन सुखकारी"
                    },
                    {
                      type: "song",
                      uid: "B4@2177",
                      title: "भज गोविन्द, भज गोविन्द"
                    },
                    {
                      type: "song",
                      uid: "B2@2178",
                      title: "जय गोविन्द, जय गोपाल"
                    },
                    {
                      type: "song",
                      uid: "GH76@2179",
                      title: "सुन्दर लाला शचीर-दुलाला"
                    },
                    {
                      type: "song",
                      uid: "B5@2180",
                      title: "बसो मेरे नयनन में नन्दलाल"
                    },
                    {
                      type: "song",
                      uid: "B6@2181",
                      title: "पार करेंगे नैया रे, भज कृष्ण कन्हैया"
                    },
                    {
                      type: "song",
                      uid: "NM16@2182",
                      title: "अब तो हरिनाम लौ लागी"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "4b@2183",
                  title: "অসমীয়া-কীর্ত্তন ~ Assamese Kīrtana",
                  children: [
                    {
                      type: "song",
                      uid: "AS1@2184",
                      title: "গুরু, নেরিবা এইবার মোক"
                    },
                    {
                      type: "song",
                      uid: "AS2@2185",
                      title: "প্রভু—এ, ও দয়াময়"
                    },
                    {
                      type: "song",
                      uid: "AS3@2186",
                      title: "সবে আনন্দে দিয়া জয়ধ্বনি;"
                    },
                    {
                      type: "song",
                      uid: "AS4@2187",
                      title: "ও হরি, মই কিয় ছারিলু তোমার সেয়া"
                    },
                    {
                      type: "song",
                      uid: "AS5@2188",
                      title: "গোপাল গোবিন্দ যদুনন্দন"
                    },
                    {
                      type: "song",
                      uid: "AS6@2189",
                      title: "হরি—এ ওহরি রাম"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "4c@2190",
                  title: "উৎকল-গীতি ~ Oriya Kīrtana",
                  children: [
                    {
                      type: "song",
                      uid: "O1@2191",
                      title: "শ্রীজগন্নাথ-জনান"
                    },
                    {
                      type: "song",
                      uid: "O2@2192",
                      title: "শ্রীনীলাদ্রীনাথ-মহিমা-জনান"
                    },
                    {
                      type: "song",
                      uid: "O3@2193",
                      title: "দীনবন্ধু দয়িতারী-জনান"
                    },
                    {
                      type: "song",
                      uid: "O4@2194",
                      title: "বেড় হেলানি আম, প্রভু ত্যজি রত্নাসন"
                    },
                    {
                      type: "song",
                      uid: "O5@2195",
                      title: "পতিতপাবন-জনান"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "4d@2196",
                  title: "পরিশিষ্ট ~ Addendum",
                  children: [
                    {
                      type: "song",
                      uid: "G11@2197",
                      title: "গুরুদেব! দয়াময়!"
                    },
                    {
                      type: "song",
                      uid: "GH53@2198",
                      title: "মন রে! কহনা গৌর-কথা"
                    },
                    {
                      type: "song",
                      uid: "GH49@2199",
                      title: "গৌরাঙ্গ তুমি মোরে দয়া না ছাড়িহ"
                    },
                    {
                      type: "song",
                      uid: "RK22@2200",
                      title: "বৃন্দাবন রম্য-স্থান, দিব্য-চিন্তামণি-ধাম"
                    },
                    {
                      type: "song",
                      uid: "K65@2201",
                      title: "কবে কৃষ্ণধন পাব"
                    }
                  ]
                }
              ]
            }
          ],
          count: 532
        },
        {
          type: "collection",
          uid: "GGGA@2202",
          title:
            "শ্রীগৌড়ীয়-গীতিগুচ্ছ (সংক্ষেপিত) [IPBYS] ~ Śrī Gauḍīya Gīti-guccha (Abridged) [IPBYS]",
          children: [
            {
              type: "song",
              uid: "M11@2204",
              title: "জয়ধ্ৱনি [IPBYS]"
            },
            {
              type: "song",
              uid: "M12@2205",
              title: "মঙ্গলাচরণ [IPBYS]"
            },
            {
              type: "song",
              uid: "M10@2206",
              title: "মঙ্গলাচরণ—অতিরিক্ত [GVS + IPBYS]"
            },
            {
              type: "collection",
              uid: "1@2207",
              title:
                "বাংলা কীর্ত্তন—গুরু, গৌরঙ্গ, রাধা, কৃষ্ণ... ~ Bengali Kīrtana [Guru, Gaurāṅga, Rādhā, Kṛṣṇa...]",
              children: [
                {
                  type: "collection",
                  uid: "1a@2208",
                  title: "শ্রীগুরু ~ Śrī Guru",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2209",
                      title: "গুরু-পরমপরা ~ The Succession of Bona Fide Gurus"
                    },
                    {
                      type: "song",
                      uid: "GV9@2210",
                      title: "শ্রীগুরু-পরম্পরা—বাংলা [GVS]"
                    },
                    {
                      type: "heading",
                      uid: "*@2211",
                      title: "শ্রীগুরু-মহিমা ~ The Glories of Śrī Guru"
                    },
                    {
                      type: "song",
                      uid: "G2@2212",
                      title: "শ্রীগুর্ব্বষ্টকম্ (বাংলা) ~ Śrī Gurvāṣṭakam (Bengali Rendition)"
                    },
                    {
                      type: "song",
                      uid: "G3@2213",
                      title: "শ্রীগুরু-চরণ-পদ্ম"
                    },
                    {
                      type: "song",
                      uid: "G4@2214",
                      title: "আশ্রয় করিয়া বন্দোঁ শ্রীগুরু-চরণ"
                    },
                    {
                      type: "song",
                      uid: "G10@2215",
                      title: "জয় জয় শ্রীগুরু প্রেম-কল্পতরু"
                    },
                    {
                      type: "heading",
                      uid: "*@2216",
                      title: "শ্রীগুরু-কৃপাপ্রার্থনা ~ Prayers for Mercy"
                    },
                    {
                      type: "song",
                      uid: "G6@2217",
                      title: "গুরুদেব! কৃপাবিন্দু দিয়া"
                    },
                    {
                      type: "song",
                      uid: "G7@2218",
                      title: "গুরুদেব! বড় কৃপা করি’"
                    },
                    {
                      type: "song",
                      uid: "G8@2219",
                      title: "গুরুদেব! কবে মোর সেই দিন হবে?"
                    },
                    {
                      type: "song",
                      uid: "G9@2220",
                      title: "গুরুদেব! কবে তব করুণা প্রকাশে"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "1b@2221",
                  title: "শ্রীবৈষ্ণব ~ Śrī Vaiṣṇava",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2222",
                      title: "শ্রীবৈষ্ণব-বন্দনা ~ Prayers of Adoration"
                    },
                    {
                      type: "song",
                      uid: "V1@2223",
                      title: "শ্রীবৈষ্ণব-বন্দনা"
                    },
                    {
                      type: "heading",
                      uid: "*@2224",
                      title: "শ্রীবৈষ্ণব-মহিমা ~ The Glories of Vaiṣṇavas"
                    },
                    {
                      type: "song",
                      uid: "V2@2225",
                      title: "ঠাকুর বৈষ্ণব-পদ"
                    },
                    {
                      type: "heading",
                      uid: "*@2226",
                      title: "শ্রীবৈষ্ণব-কৃপা-প্রার্থনা ~ Prayers for Mercy"
                    },
                    {
                      type: "song",
                      uid: "V3@2227",
                      title: "ওহে! বৈষ্ণব ঠাকুর"
                    },
                    {
                      type: "song",
                      uid: "V4@2228",
                      title: "কৃপা কর বৈষ্ণব ঠাকুর !"
                    },
                    {
                      type: "song",
                      uid: "V5@2229",
                      title: "কবে মুই বৈষ্ণব চিনিব"
                    },
                    {
                      type: "song",
                      uid: "V6@2230",
                      title: "হরি হরি কবে মোর হবে হেন দিন"
                    },
                    {
                      type: "heading",
                      uid: "*@2231",
                      title: "শ্রীবৈষ্ণব-বিজ্ঞপ্তি ~ Entreaties to the Vaiṣṇavas"
                    },
                    {
                      type: "song",
                      uid: "V7@2232",
                      title: "ঠাকুর বৈষ্ণবগণ"
                    },
                    {
                      type: "song",
                      uid: "V8@2233",
                      title: "এইবার করুণা কর"
                    },
                    {
                      type: "song",
                      uid: "V9@2234",
                      title: "কিরূপে পাইব সেবা"
                    },
                    {
                      type: "song",
                      uid: "V10@2235",
                      title: "সকল বৈষ্ণব গোসাঞি দয়া কর মোরে"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "1c@2236",
                  title: "শ্রীল রূপ গোস্বামী ~ Śrīla Rūpa Gosvāmī",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2237",
                      title: "শ্রীল রূপ-গোস্বামি-মহিমা ~ The Glories of Śrīla Rūpa Gosvāmī"
                    },
                    {
                      type: "song",
                      uid: "GV43@2238",
                      title: "যঙ্ কলি রূপ শরীর ন ধরত"
                    },
                    {
                      type: "heading",
                      uid: "*@2239",
                      title:
                        "শ্রীরূপ-সনাতনে দৈন্যময়ী প্রার্থনা ~ Prayer of Humility to Śrī Rūpa and Śrī Sanātana"
                    },
                    {
                      type: "song",
                      uid: "GV46@2240",
                      title: "বিষয়-বাসনারূপ চিত্তের বিকার"
                    },
                    {
                      type: "heading",
                      uid: "*@2241",
                      title:
                        "স্বাভীষ্ট-লালসাত্মক প্রার্থনা ~ Prayer of Heartfelt Longing for One’s Cherished Desire"
                    },
                    {
                      type: "song",
                      uid: "GV44@2242",
                      title: "শ্রীরূপমঞ্জরী-পদ"
                    },
                    {
                      type: "heading",
                      uid: "*@2243",
                      title: "শ্রীরূপানুগত্য-মাহাত্ম্য ~ Śrī Rūpānugatya-māhātmya"
                    },
                    {
                      type: "song",
                      uid: "GV45@2244",
                      title: "শুনিয়াছি সাধুমুখে"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "1d@2245",
                  title: "পঞ্চতত্ত্ব-মহিমা ~ The Glories of the Pañca-tattva",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2246",
                      title: "শ্রীশ্রীবাস ঠাকুর-মহিমা ~ The Glories of Śrī Śrīvāsa Ṭhākura"
                    },
                    {
                      type: "song",
                      uid: "PT3@2247",
                      title: "সপ্তদ্বীপ দীপ্ত করি’"
                    },
                    {
                      type: "heading",
                      uid: "*@2248",
                      title: "শ্রীগদাধর পণ্ডিত-মহিমা ~ The Glories of Śrī Gadādhara Paṇḍita"
                    },
                    {
                      type: "song",
                      uid: "PT7@2249",
                      title: "জয় জয় গদাধর পণ্ডিত গোঁসাঞি"
                    },
                    {
                      type: "heading",
                      uid: "*@2250",
                      title: "শ্রীঅদ্বৈতাচর্য্য-মহিমা ~ The Glories of Śrī Advaita Ācārya"
                    },
                    {
                      type: "song",
                      uid: "PT13@2251",
                      title: "জয় জয় অদ্বৈত আচার্য্য দয়াময়"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "1e@2252",
                  title: "শ্রীমন্নিত্যানন্দ ~ Śrī Nityānanda Prabhu",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2253",
                      title: "শ্রীনিত্যানন্দ-নিষ্ঠা ~ Firm Faith in Śrī Nityānanda"
                    },
                    {
                      type: "song",
                      uid: "N7@2254",
                      title: "নিতাই-পদ-কমল"
                    },
                    {
                      type: "song",
                      uid: "N8@2255",
                      title: "নিতাই মোর জীবন-ধন"
                    },
                    {
                      type: "heading",
                      uid: "*@2256",
                      title: "গুণ-বর্ণন ~ The Wonderful Qualities of Nityānanda Prabhu"
                    },
                    {
                      type: "song",
                      uid: "N9@2257",
                      title: "অক্রোধ পরমানন্দ"
                    },
                    {
                      type: "song",
                      uid: "N13@2258",
                      title: "জয় জয় নিত্যানন্দ রোহিণী-কুমার"
                    },
                    {
                      type: "song",
                      uid: "N14@2259",
                      title: "নিতাই গুণমণি আমার"
                    },
                    {
                      type: "song",
                      uid: "N19@2260",
                      title: "আনন্দ কন্দ, নিতাই-চন্দ"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "1f@2261",
                  title: "শ্রীগৌরাঙ্গ ~ Śrī Gaurāṅga",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2262",
                      title: "শ্রীগৌর-তত্ত্ব ~ Śrī Gaura-tattva"
                    },
                    {
                      type: "song",
                      uid: "GH20@2263",
                      title: "(প্রভু হে!) এমন দুর্ম্মতি সংসার-ভিতরে"
                    },
                    {
                      type: "song",
                      uid: "GH22@2264",
                      title: "জয় নন্দনন্দন গোপীজন-বল্লভ"
                    },
                    {
                      type: "song",
                      uid: "GH23@2265",
                      title: "জয় জয় জগন্নাথ শচীর নন্দন"
                    },
                    {
                      type: "heading",
                      uid: "*@2266",
                      title: "শ্রীগৌর-গুণ-বর্ণন ~ The Wonderful Qualities of Śrī Gaurāṅga"
                    },
                    {
                      type: "song",
                      uid: "GH35@2267",
                      title: "এমন গৌরাঙ্গ বিনা নাহি আর!"
                    },
                    {
                      type: "song",
                      uid: "GH36@2268",
                      title: "(যদি) গৌরাঙ্গ নহিত"
                    },
                    {
                      type: "song",
                      uid: "GH37@2269",
                      title: "এমন শচীর নন্দন বিনে"
                    },
                    {
                      type: "song",
                      uid: "GH53@2270",
                      title: "মন রে! কহনা গৌর-কথা"
                    },
                    {
                      type: "heading",
                      uid: "*@2271",
                      title: "শ্রীগৌর-মহিমা ~ The Glories of Śrī Gaurāṅga"
                    },
                    {
                      type: "song",
                      uid: "GH38@2272",
                      title: "কে যাবি কে যাবি ভাই"
                    },
                    {
                      type: "song",
                      uid: "GH40@2273",
                      title: "গৌরাঙ্গের দু’টী পদ"
                    },
                    {
                      type: "heading",
                      uid: "*@2274",
                      title: "শ্রীগৌরসুন্দরে বিজ্ঞপ্তি ~ Entreaties"
                    },
                    {
                      type: "song",
                      uid: "GH47@2275",
                      title: "কবে শ্রীচৈতন্য মোরে করিবেন দয়া"
                    },
                    {
                      type: "song",
                      uid: "GH45@2276",
                      title: "ওহে প্রেমের ঠাকুর গোরা"
                    },
                    {
                      type: "song",
                      uid: "GH49@2277",
                      title: "গৌরাঙ্গ তুমি মোরে দয়া না ছাড়িহ"
                    },
                    {
                      type: "heading",
                      uid: "*@2278",
                      title: "শ্রীগৌরচন্দ্রে লালসাময়ী প্রার্থনা ~ Prayers Full of Longing to Śrī Gauracandra"
                    },
                    {
                      type: "song",
                      uid: "GH50@2279",
                      title: "হা হা মোর গৌর-কিশোর"
                    },
                    {
                      type: "song",
                      uid: "GH51@2280",
                      title: "কবে আহা গৌরাঙ্গ বলিয়া"
                    },
                    {
                      type: "heading",
                      uid: "*@2281",
                      title: "শ্রীগৌরাঙ্গ-নিষ্ঠা ~ Firm Faith in Śrī Gaurāṅga"
                    },
                    {
                      type: "song",
                      uid: "GH52@2282",
                      title: "আরে ভাই! ভজ মোর গৌরাঙ্গ-চরণ"
                    },
                    {
                      type: "song",
                      uid: "GH39@2283",
                      title: "অবতার সার"
                    },
                    {
                      type: "heading",
                      uid: "*@2284",
                      title: "আক্ষেপ ~ Regret"
                    },
                    {
                      type: "song",
                      uid: "GH56@2285",
                      title: "গোরা পঁহু না ভজিয়া মৈনু"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "1g@2286",
                  title: "শ্রীগৌর-নিত্যানন্দ ~ Śrī Gaura-Nityānanda",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2287",
                      title: "শ্রীগৌর-নিত্যানন্দ-মহিমা ~ The Glories of Śrī Gaura-Nityānanda"
                    },
                    {
                      type: "song",
                      uid: "GN3@2288",
                      title: "পরম করুণ, পঁহু দুইজন"
                    },
                    {
                      type: "song",
                      uid: "GH31@2289",
                      title: "দেবাদিদেব গৌরচন্দ্র গৌরীদাস-মন্দিরে"
                    },
                    {
                      type: "heading",
                      uid: "*@2290",
                      title: "শ্রীগৌরনিত্যানন্দে লালসাময়ী প্রার্থনা ~ Prayers Full Of Longing"
                    },
                    {
                      type: "song",
                      uid: "GN9@2291",
                      title: "‘গৌরাঙ্গ’ বলিতে হবে"
                    },
                    {
                      type: "song",
                      uid: "GN11@2292",
                      title: "হা হা কবে গৌর-নিতাই"
                    },
                    {
                      type: "song",
                      uid: "GN 7@2293",
                      title: "undefined"
                    },
                    {
                      type: "song",
                      uid: "GN10@2294",
                      title: "কবে হ’বে হেন দশা মোর"
                    },
                    {
                      type: "heading",
                      uid: "*@2295",
                      title: "শ্রীগৌর-নিত্যানন্দে নিষ্ঠা ~ Firm Faith in Śrī Gaura-Nityānanda"
                    },
                    {
                      type: "song",
                      uid: "GN12@2296",
                      title: "ধন মোর নিত্যানন্দ পতি মোর গৌরচন্দ্র"
                    },
                    {
                      type: "song",
                      uid: "GN13@2297",
                      title: "নিতাই-গৌর-নাম"
                    },
                    {
                      type: "song",
                      uid: "GN8@2298",
                      title: "এ ঘোর-সংসারে"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "1h@2299",
                  title: "সগণ শ্রীগৌরাঙ্গ ~ Gaurāṅga and His Associates",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2300",
                      title: "সগণ শ্রীগৌর-মহিমা ~ The Glories of Śrī Gaurāṅga and His Associates"
                    },
                    {
                      type: "song",
                      uid: "GP2@2301",
                      title: "এ’লো গৌর-রস-নদী"
                    },
                    {
                      type: "song",
                      uid: "GP3@2302",
                      title: "নদীয়ার ঘাটে ভাই কি অদ্ভুত তরী"
                    },
                    {
                      type: "heading",
                      uid: "*@2303",
                      title: "সগণ শ্রীগৌরচরণে প্রার্থনা ~ Prayers For Mercy"
                    },
                    {
                      type: "song",
                      uid: "GP9@2304",
                      title: "শ্রীকৃষ্ণচৈতন্য প্রভু দয়া কর মোরে"
                    },
                    {
                      type: "song",
                      uid: "GP12@2305",
                      title: "জয় জয় নিত্যানন্দাদ্বৈত গৌরাঙ্গ"
                    },
                    {
                      type: "heading",
                      uid: "*@2306",
                      title: "সপার্ষদ শ্রীগৌর-বিরহ-বিলাপ ~ Lamenting in Separation"
                    },
                    {
                      type: "song",
                      uid: "GV47@2307",
                      title: "যে আনিল প্রেমধন"
                    },
                    {
                      type: "heading",
                      uid: "*@2308",
                      title: "সগণশ্রীগৌরকৃষ্ণে প্রার্থনা ~ A Prayer For Mercy To Gaura-Kṛṣṇa"
                    },
                    {
                      type: "song",
                      uid: "GP13@2309",
                      title: "হরি হরি! বিফলে জনম গোঙাইনু"
                    },
                    {
                      type: "heading",
                      uid: "*@2310",
                      title: "সিদ্ধি-লালসা ~ Hankering for Ultimate Perfection"
                    },
                    {
                      type: "song",
                      uid: "D2@2311",
                      title: "কবে গৌর-বনে"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "1i@2312",
                  title: "শ্রীরাধা ~ Śrī Rādhā",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2313",
                      title: "শ্রীরাধাষ্টক ~ Selected Songs from Śrī Rādhāṣṭaka"
                    },
                    {
                      type: "song",
                      uid: "R13@2314",
                      title: "রাধিকাচরণ-পদ্ম"
                    },
                    {
                      type: "song",
                      uid: "R15@2315",
                      title: "রমণী-শিরোমণি, বৃষভানু-নন্দিনী"
                    },
                    {
                      type: "song",
                      uid: "R18@2316",
                      title: "বরজ-বিপিনে"
                    },
                    {
                      type: "song",
                      uid: "R19@2317",
                      title: "শতকোটী গোপী"
                    },
                    {
                      type: "song",
                      uid: "R20@2318",
                      title: "রাধা-ভজনে যদি মতি নাহি ভেলা"
                    },
                    {
                      type: "heading",
                      uid: "*@2319",
                      title: "শ্রীরাধা-নিষ্ঠা ~ Firm Faith in Śrī Rādhā"
                    },
                    {
                      type: "song",
                      uid: "R21@2320",
                      title: "বৃষভানুসুতা-চরণ-সেবনে"
                    },
                    {
                      type: "song",
                      uid: "R23@2321",
                      title: "রাধিকা-চরণরেণু"
                    },
                    {
                      type: "heading",
                      uid: "*@2322",
                      title: "সিদ্ধি-লালসা ~ Hankering for Ultimate Perfection"
                    },
                    {
                      type: "song",
                      uid: "R25@2323",
                      title: "দেখিতে দেখিতে, ভুলিব বা কবে"
                    },
                    {
                      type: "song",
                      uid: "R27@2324",
                      title: "পাল্যদাসী করি’"
                    },
                    {
                      type: "song",
                      uid: "R28@2325",
                      title: "চিন্তামণিময়"
                    },
                    {
                      type: "song",
                      uid: "R22@2326",
                      title: "শ্রীকৃষ্ণবিরহে রাধিকার দশা"
                    },
                    {
                      type: "heading",
                      uid: "*@2327",
                      title:
                        "শ্রীল রঘুনাথ দাস গোস্বামীর উদ্দেশ্যে... ~ the Life Goal of Śrī Raghunātha dāsa Gosvāmī"
                    },
                    {
                      type: "song",
                      uid: "R24@2328",
                      title: "কোথায় গো প্রেমময়ি"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "1j@2329",
                  title: "শ্রীশ্রীরাধা-কৃষ্ণ",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2330",
                      title: "শ্রীরাধা-কৃষ্ণে বিজ্ঞপ্তি ~ An Entreaty"
                    },
                    {
                      type: "song",
                      uid: "RK11@2331",
                      title: "শ্রীরাধাকৃষ্ণ-পদকমলে মন"
                    },
                    {
                      type: "heading",
                      uid: "*@2332",
                      title: "সিদ্ধি-লালসা ~ Prayers Full of Longing"
                    },
                    {
                      type: "song",
                      uid: "RK10@2333",
                      title: "হরি হরি, কবে মোর হইবে সুদিন"
                    },
                    {
                      type: "song",
                      uid: "RK9@2334",
                      title: "রাধাকৃষ্ণ প্রাণ মোর"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "1k@2335",
                  title: "শ্রীকৃষ্ণ",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2336",
                      title: "শ্রীকৃষ্ণ-রূপ-বর্ণন ~ The Beautiful Form of Śrī Kṛṣṇa"
                    },
                    {
                      type: "song",
                      uid: "K41@2337",
                      title: "জনম সফল তা’র, কৃষ্ণ দরশন যা’র"
                    },
                    {
                      type: "song",
                      uid: "K42@2338",
                      title: "বন্ধুসঙ্গে"
                    },
                    {
                      type: "heading",
                      uid: "*@2339",
                      title: "শ্রীকৃষ্ণ-গুণ- বর্ণন ~ The Wonderful Qualities of Śrī Kṛṣṇa"
                    },
                    {
                      type: "song",
                      uid: "K40@2340",
                      title: "শুন হে রসিক জন"
                    },
                    {
                      type: "heading",
                      uid: "*@2341",
                      title: "শ্রীকৃষ্ণ-লীলা-বর্ণন ~ The Sweet Pastimes of Śrī Kṛṣṇa"
                    },
                    {
                      type: "song",
                      uid: "K45@2342",
                      title: "যমুনা-পুলিনে"
                    },
                    {
                      type: "heading",
                      uid: "*@2343",
                      title: "শ্রীকৃষ্ণে বিজ্ঞপ্তি ~ Entreaties"
                    },
                    {
                      type: "song",
                      uid: "K49@2344",
                      title: "গোপীনাথ, মম নিবেদন শুন"
                    },
                    {
                      type: "song",
                      uid: "K50@2345",
                      title: "গোপীনাথ, ঘুচাও সংসার-জ্বালা"
                    },
                    {
                      type: "song",
                      uid: "K51@2346",
                      title: "গোপীনাথ, আমার উপায় নাই"
                    },
                    {
                      type: "heading",
                      uid: "*@2347",
                      title: "শ্রীকৃষ্ণ-ভজন-নিষ্ঠা ~ Firm Faith in the Worship of Śrī Kṛṣṇa"
                    },
                    {
                      type: "song",
                      uid: "K54@2348",
                      title: "ব্রজেন্দ্রনন্দন, ভজে যেই জন"
                    },
                    {
                      type: "song",
                      uid: "MS35@2349",
                      title: "ভজ ভজ হরি, মন দৃঢ় করি’"
                    },
                    {
                      type: "song",
                      uid: "MS36@2350",
                      title: "ভজহুঁ রে মন"
                    },
                    {
                      type: "song",
                      uid: "S32@2351",
                      title: "আর কেন মায়াজালে পড়িতেছ জীব মীন"
                    },
                    {
                      type: "heading",
                      uid: "*@2352",
                      title: "শ্রীকৃষ্ণে দৈন্যবোধিকা প্রার্থনা ~ Prayers that Express Humility"
                    },
                    {
                      type: "song",
                      uid: "K56@2353",
                      title: "হরি হরি! কৃপা করি’ রাখ নিজ পদে"
                    },
                    {
                      type: "song",
                      uid: "UP9@2354",
                      title: "দুর্ল্লভ মানব-জন্ম লভিয়া সংসারে"
                    },
                    {
                      type: "heading",
                      uid: "*@2355",
                      title: "শ্রীকৃষ্ণে-লালসাত্মক প্রার্থনা ~ Prayers Full of Longing"
                    },
                    {
                      type: "song",
                      uid: "K65@2356",
                      title: "কবে কৃষ্ণধন পাব"
                    },
                    {
                      type: "song",
                      uid: "K67@2357",
                      title: "হরি ব’লব আর মদনমোহন হেরিব গো"
                    },
                    {
                      type: "heading",
                      uid: "*@2358",
                      title: "শ্রীকৃষ্ণে-আত্মনিবেদন ~ Self-Dedication"
                    },
                    {
                      type: "song",
                      uid: "K60@2359",
                      title: "মাধব, বহুত মিনতি করি তোয়"
                    },
                    {
                      type: "song",
                      uid: "K61@2360",
                      title: "হরি হে দয়াল মোর জয় রাধানাথ"
                    },
                    {
                      type: "heading",
                      uid: "*@2361",
                      title: "বৃন্দাবন-বাস-লালসা ~ Yearning for Residence in Vṛndāvana"
                    },
                    {
                      type: "song",
                      uid: "D17@2362",
                      title: "হরি হরি! কবে হব বৃন্দাবনবাসী"
                    }
                  ]
                }
              ]
            },
            {
              type: "collection",
              uid: "2@2363",
              title:
                "বাংলা কীর্ত্তন—নাম, শরণাগতি, আরতি... ~ Bengali Kīrtana [Nāma, Śaraṇāgati, Ārati...]",
              children: [
                {
                  type: "collection",
                  uid: "2a@2364",
                  title: "শ্রীনাম-কীর্ত্তন ~ Śrī Nāma-kīrtana",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2365",
                      title: "শ্রীনাম-কীর্ত্তন ~ Śrī Nāma-kīrtana"
                    },
                    {
                      type: "song",
                      uid: "NK1@2366",
                      title: "(হরি) হরয়ে নমঃ কৃষ্ণ যাদবায় নমঃ"
                    },
                    {
                      type: "song",
                      uid: "NK2@2367",
                      title: "জয় রাধে, জয় কৃষ্ণ, জয় বৃন্দাবন"
                    },
                    {
                      type: "song",
                      uid: "NK4@2368",
                      title: "জয় জয় রাধা-মাধব রাধা-মাধব রাধে"
                    },
                    {
                      type: "song",
                      uid: "NK3@2369",
                      title: "জয় জয় রাধে কৃষ্ণ গোবিন্দ"
                    },
                    {
                      type: "heading",
                      uid: "*@2370",
                      title: "শ্রীগৌর-নাম-কীর্ত্তন ~ Śrī Gaura-nāma-kīrtanaNK9"
                    },
                    {
                      type: "song",
                      uid: "NK5@2371",
                      title: "কলিকুক্কুর-কদন যদি চাও (হে)"
                    },
                    {
                      type: "heading",
                      uid: "*@2372",
                      title: "শ্রীকৃষ্ণ-নাম-কীর্ত্তন ~ Śrī Kṛṣṇa-nāma-kīrtana"
                    },
                    {
                      type: "song",
                      uid: "NK15@2373",
                      title: "কৃষ্ণ গোবিন্দ হরে"
                    },
                    {
                      type: "song",
                      uid: "NK16@2374",
                      title: "রাধাবল্লভ মাধব শ্রীপতি মুকুন্দ"
                    },
                    {
                      type: "song",
                      uid: "NK17@2375",
                      title: "জয় রাধা-মাধব কুঞ্জবিহারী"
                    },
                    {
                      type: "song",
                      uid: "NK18@2376",
                      title: "রাধাবল্লভ রাধাবিনোদ"
                    },
                    {
                      type: "song",
                      uid: "NK19@2377",
                      title: "জয় যশোদা-নন্দন কৃষ্ণ"
                    },
                    {
                      type: "song",
                      uid: "NK14@2378",
                      title: "শ্রীকৃষ্ণ গোপাল হরে মুকুন্দ"
                    },
                    {
                      type: "heading",
                      uid: "*@2379",
                      title: "অরুণোদয়-কীর্ত্তন ~ Sunrise Kīrtana"
                    },
                    {
                      type: "song",
                      uid: "NM11@2380",
                      title: "উদিল অরুণ পূরব ভাগে"
                    },
                    {
                      type: "song",
                      uid: "NM12@2381",
                      title: "জীব জাগ, জীব জাগ"
                    },
                    {
                      type: "heading",
                      uid: "*@2382",
                      title: "শ্রীনগর-কীর্ত্তন ~ Śrī Nagara-kīrtana"
                    },
                    {
                      type: "song",
                      uid: "NK25@2383",
                      title: "নদীয়া-গোদ্রুমে নিত্যানন্দ মহাজন"
                    },
                    {
                      type: "song",
                      uid: "NK26@2384",
                      title: "বড় সুখের খবর গাই"
                    },
                    {
                      type: "song",
                      uid: "NK27@2385",
                      title: "গায় গোরা মধুর স্বরে"
                    },
                    {
                      type: "song",
                      uid: "NK29@2386",
                      title: "‘রাধাকৃষ্ণ’ বল্ বল্"
                    },
                    {
                      type: "song",
                      uid: "NK30@2387",
                      title: "গায় গোরাচাঁদ জীবের তরে"
                    },
                    {
                      type: "song",
                      uid: "NK31@2388",
                      title: "অঙ্গ-উপাঙ্গ-অস্ত্র-পার্ষদ-সঙ্গে"
                    },
                    {
                      type: "song",
                      uid: "NK32@2389",
                      title: "নিতাই কি নাম এনেছে রে"
                    },
                    {
                      type: "song",
                      uid: "NK33@2390",
                      title: "হরি ব’লে মোদের গৌর এলো"
                    },
                    {
                      type: "heading",
                      uid: "*@2391",
                      title: "অনন্য-কীর্ত্তন ~ Other Kīrtanas"
                    },
                    {
                      type: "song",
                      uid: "NK10@2392",
                      title: "ভজ গৌরাঙ্গ কহ গৌরাঙ্গ"
                    },
                    {
                      type: "song",
                      uid: "NK7@2393",
                      title: "‘হরি’ বল, ‘হরি’ বল, ‘হরি’ বল, ভাই রে"
                    },
                    {
                      type: "song",
                      uid: "NK6@2394",
                      title: "‘দয়াল নিতাই চৈতন্য’ ব’লে নাচ্ রে আমার মন"
                    },
                    {
                      type: "song",
                      uid: "NK8@2395",
                      title: "বোল হরি বোল (৩ বার)"
                    },
                    {
                      type: "heading",
                      uid: "*@2396",
                      title: "ভজন-কীর্ত্তন ~ Bhajana-kīrtana"
                    },
                    {
                      type: "song",
                      uid: "GP1@2397",
                      title: "ভজ রে ভজ রে আমার মন"
                    },
                    {
                      type: "song",
                      uid: "MS34@2398",
                      title: "ভাব না ভাব না, মন, তুমি অতি দুষ্ট"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2b@2399",
                  title: "শ্রীনাম-মহিমা ~ The Glories of Śrī Nāma",
                  children: [
                    {
                      type: "song",
                      uid: "NM2@2400",
                      title: "জয় জয় হরিনাম"
                    },
                    {
                      type: "song",
                      uid: "NM5@2401",
                      title: "হরিনাম তুয়া অনেক স্বরূপ"
                    },
                    {
                      type: "song",
                      uid: "NM8@2402",
                      title: "নারদ মুনি বাজায় বীণা"
                    },
                    {
                      type: "song",
                      uid: "NM13@2403",
                      title: "কৃষ্ণনাম ধরে কত বল"
                    },
                    {
                      type: "song",
                      uid: "NM15@2404",
                      title: "সই, কেবা শুনাইল শ্যাম-নাম"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2c@2405",
                  title: "শ্রীশিক্ষাষ্টকম্ ~ Śrī Śikṣāṣṭakam",
                  children: [
                    {
                      type: "song",
                      uid: "SI1@2406",
                      title: "পীতবরণ কলি-পাবন গোরা"
                    },
                    {
                      type: "song",
                      uid: "SI2@2407",
                      title: "তুহুঁ দয়া-সাগর"
                    },
                    {
                      type: "song",
                      uid: "SI3@2408",
                      title: "শ্রীকৃষ্ণকীর্ত্তনে যদি মানস তোঁহার"
                    },
                    {
                      type: "song",
                      uid: "SI4@2409",
                      title: "প্রভু! তব পদযুগে মোর নিবেদন"
                    },
                    {
                      type: "song",
                      uid: "SI5@2410",
                      title: "অনাদি করম-ফলে"
                    },
                    {
                      type: "song",
                      uid: "SI6@2411",
                      title: "অপরাধ-ফলে মম"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3d@2412",
                  title: "শ্রীউপদেশামৃত ~ Śrī Upadeśāmṛta",
                  children: [
                    {
                      type: "song",
                      uid: "U1@2413",
                      title: "হরি হে! প্রপঞ্চে পড়িয়া"
                    },
                    {
                      type: "song",
                      uid: "U2@2414",
                      title: "হরি হে! অর্থের সঞ্চয়ে"
                    },
                    {
                      type: "song",
                      uid: "U3@2415",
                      title: "হরি হে! ভজনে উৎসাহ"
                    },
                    {
                      type: "song",
                      uid: "U4@2416",
                      title: "হরি হে! দান-প্রতিগ্রহ"
                    },
                    {
                      type: "song",
                      uid: "U5@2417",
                      title: "হরি হে! সঙ্গদোষ-শূন্য"
                    },
                    {
                      type: "song",
                      uid: "U6@2418",
                      title: "হরি হে! নীরধর্ম্ম-গত"
                    },
                    {
                      type: "song",
                      uid: "U7@2419",
                      title: "হরি হে! তোমারে ভুলিয়া"
                    },
                    {
                      type: "song",
                      uid: "U8@2420",
                      title: "হরি হে! শ্রীরূপগোসাঞি"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2e@2421",
                  title: "শরণাগতি ~ Śaraṇāgati",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2422",
                      title: "ষড়ঙ্গ শরণাগতি ~ The Six Limbs of Surrender"
                    },
                    {
                      type: "song",
                      uid: "S1@2423",
                      title: "শ্রীকৃষ্ণচৈতন্য প্রভু জীবে দয়া করি’"
                    },
                    {
                      type: "heading",
                      uid: "*@2424",
                      title: "১. দৈন্য ~ 1. Dainya [Humility]"
                    },
                    {
                      type: "song",
                      uid: "S2@2425",
                      title: "ভুলিয়া তোমারে"
                    },
                    {
                      type: "song",
                      uid: "S7@2426",
                      title: "আমার জীবন, সদা পাপে রত"
                    },
                    {
                      type: "song",
                      uid: "S8@2427",
                      title: "নিবেদন করি প্রভু! তোমার চরণে"
                    },
                    {
                      type: "heading",
                      uid: "*@2428",
                      title: "২. আত্মনিবেদন ~ 2. Ātma Nivedana [Submission of the Self]"
                    },
                    {
                      type: "song",
                      uid: "S10@2429",
                      title: "সর্ব্বস্ব তোমার"
                    },
                    {
                      type: "song",
                      uid: "S12@2430",
                      title: "‘আমার’ বলিতে প্রভু! আর কিছু নাই"
                    },
                    {
                      type: "song",
                      uid: "S14@2431",
                      title: "মানস, দেহ, গেহ, যো কিছু মোর"
                    },
                    {
                      type: "song",
                      uid: "S16@2432",
                      title: "আত্মনিবেদন, তুয়া পদে করি’"
                    },
                    {
                      type: "heading",
                      uid: "*@2433",
                      title:
                        "৩. গোপ্তৃত্বে বরণ ~ 3. Goptṛtve Varaṇa [Acceptance of the Lord as One’s Only Maintainer]"
                    },
                    {
                      type: "song",
                      uid: "S17@2434",
                      title: "তুমি সর্ব্বেশ্বরেশ্বর"
                    },
                    {
                      type: "song",
                      uid: "S18@2435",
                      title: "কি জানি কি বলে"
                    },
                    {
                      type: "heading",
                      uid: "*@2436",
                      title:
                        "৪. অবশ্য রক্ষিবে কৃষ্ণ—এইরূপ বিশ্বাস ~ 4. Avaśya Rakṣibe Kṛṣṇa – Ei-rūpe Viśvāsa [Faith in Kṛṣṇa’s Protection]"
                    },
                    {
                      type: "song",
                      uid: "S20@2437",
                      title: "এখন বুঝিনু প্রভু!"
                    },
                    {
                      type: "heading",
                      uid: "*@2438",
                      title: "৫. অনুকূল-গ্রহণ ~ 5. Anukūla-grahaṇa [Acceptance of the Favorable]"
                    },
                    {
                      type: "song",
                      uid: "S22@2439",
                      title: "তুয়া ভক্তি-অনুকূল যে-যে কার্য্য হয়"
                    },
                    {
                      type: "song",
                      uid: "S30@2440",
                      title: "রাধাকুণ্ডতট"
                    },
                    {
                      type: "heading",
                      uid: "*@2441",
                      title:
                        "৬. প্রতিকূল-বর্জ্জন ~ 6. Pratikūla-varjana [Rejection of the Unfavorable]"
                    },
                    {
                      type: "song",
                      uid: "S24@2442",
                      title: "তুয়া ভক্তি-প্রতিকূল ধর্ম্ম যা’তে রয়"
                    },
                    {
                      type: "heading",
                      uid: "*@2443",
                      title: "জাহ্নবা-দেবী প্রার্থনা ~ Prayer to Jāhnavā-devī"
                    },
                    {
                      type: "song",
                      uid: "P3@2444",
                      title: "ভবার্ণবে প’ড়ে মোর আকুল পরাণ"
                    },
                    {
                      type: "heading",
                      uid: "*@2445",
                      title: "যোগমায়া প্রার্থনা ~ Prayer to Yogamāyā"
                    },
                    {
                      type: "song",
                      uid: "P4@2446",
                      title: "আমার সমান হীন নাহি এ সংসারে"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2f@2447",
                  title: "আরতি ~ Ārati",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2448",
                      title: "গুরু-পূজা ~ Guru-pūjā"
                    },
                    {
                      type: "song",
                      uid: "A1@2449",
                      title: "জয় জয় গুরুদেব শ্রীভক্তিপ্রজ্ঞান"
                    },
                    {
                      type: "song",
                      uid: "A2@2450",
                      title: "শ্রী প্রভুপাদের আরতি [GVS &amp; IPBYS]"
                    },
                    {
                      type: "heading",
                      uid: "*@2451",
                      title: "মঙ্গল-আরতি ~ Maṅgala Ārati"
                    },
                    {
                      type: "song",
                      uid: "A4@2452",
                      title: "শ্রীমঙ্গল-আরতি [GVS &amp; IPBYS]"
                    },
                    {
                      type: "song",
                      uid: "A7@2453",
                      title: "বিভাবরী-শেষ"
                    },
                    {
                      type: "heading",
                      uid: "*@2454",
                      title: "মধ্যাহ্ন আরতি ~ Noon Ārati"
                    },
                    {
                      type: "song",
                      uid: "A9@2455",
                      title: "ভজ ভকত-বৎসল শ্রীগৌরহরি"
                    },
                    {
                      type: "song",
                      uid: "A8@2456",
                      title: "যশোমতী-নন্দন"
                    },
                    {
                      type: "heading",
                      uid: "*@2457",
                      title: "সন্ধ্যা-আরতি ~ Evening Ārati"
                    },
                    {
                      type: "song",
                      uid: "A10@2458",
                      title: "শ্রীগৌর-আরতি"
                    },
                    {
                      type: "song",
                      uid: "A11@2459",
                      title: "শ্রীযুগল-আরতি"
                    },
                    {
                      type: "heading",
                      uid: "*@2460",
                      title: "শ্রীতুলসী-আরতি ~ Śrī Tulasī-ārati"
                    },
                    {
                      type: "song",
                      uid: "A14@2461",
                      title: "‘নমো নমঃ তুলসী কৃষ্ণ-প্রেয়সী’ (নমো নমঃ)"
                    },
                    {
                      type: "song",
                      uid: "A15@2462",
                      title: "নমো নমঃ তুলসী কৃষ্ণ-প্রেয়সী"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2g@2463",
                  title: "একাদশী ~ Ekādaśī",
                  children: [
                    {
                      type: "song",
                      uid: "E1@2464",
                      title: "শুদ্ধভকত-চরণ-রেণু"
                    },
                    {
                      type: "song",
                      uid: "E2@2465",
                      title: "শ্রীমন্মহাপ্রভুর হরিবাসর-ব্রতপালন"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2h@2466",
                  title: "শ্রীমহাপ্রসাদ-মাহাত্ম্য-কীর্ত্তন ~ The Glories of Mahāprasāda",
                  children: [
                    {
                      type: "song",
                      uid: "PR1@2467",
                      title: "ভাইরে! শরীর অবিদ্যা-জাল"
                    },
                    {
                      type: "song",
                      uid: "PR2@2468",
                      title: "ভাইরে! একদিন শান্তিপুরে"
                    },
                    {
                      type: "song",
                      uid: "PR3@2469",
                      title: "ভাইরে! শচীর অঙ্গনে কভু"
                    },
                    {
                      type: "song",
                      uid: "PR4@2470",
                      title: "ভাইরে! শ্রীচৈতন্য-নিত্যানন্দ"
                    },
                    {
                      type: "song",
                      uid: "PR5@2471",
                      title: "ভাইরে! একদিন নীলাচলে"
                    },
                    {
                      type: "song",
                      uid: "PR6@2472",
                      title: "ভাইরে! রামকৃষ্ণ গোচারণে"
                    }
                  ]
                }
              ]
            },
            {
              type: "collection",
              uid: "3@2473",
              title: "সংস্কৃত-স্তোত্র ~ Sanskrit Stotras",
              children: [
                {
                  type: "collection",
                  uid: "3a@2474",
                  title: "শ্রীশ্রীগুরু-গৌরাঙ্গ ~ Śrī Śrī Guru-Gaurāṅga",
                  children: [
                    {
                      type: "song",
                      uid: "G1@2475",
                      title: "শ্রীগুর্ৱ্ৱষ্টকম্ (সংস্কৃত) ~ Śrī Gurvāṣṭakam (Sanskrit)"
                    },
                    {
                      type: "song",
                      uid: "GV14@2476",
                      title: "শ্রীকেশৱাচার্যাষ্টকম্ (১)"
                    },
                    {
                      type: "song",
                      uid: "GV17@2477",
                      title: "শ্রীপ্রভুপাদ-পদ্ম-স্তৱকঃ"
                    },
                    {
                      type: "song",
                      uid: "GV31@2478",
                      title: "শ্রীষড়্‌গোস্ৱাম্যাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "N1@2479",
                      title: "শ্রীনিত্যানন্দাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "GH1@2480",
                      title: "শ্রীশচীতনয়াষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "RK1@2481",
                      title: "শ্রীরাধাৱিনোদৱিহারী-তত্ত্ৱাষ্টকম্"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3b@2482",
                  title: "শ্রীশ্রীরাধাকৃষ্ণ ~ Śrī Śrī Rādhā-Kṛṣṇa",
                  children: [
                    {
                      type: "song",
                      uid: "R6@2483",
                      title: "শ্রীশ্রীগান্ধর্ৱ্ৱা-সংপ্রার্থনাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "R7@2484",
                      title: "শ্রীরাধা-প্রার্থনা"
                    },
                    {
                      type: "song",
                      uid: "R3@2485",
                      title: "শ্রীরাধিকাষ্টকম্ (৩)"
                    },
                    {
                      type: "song",
                      uid: "K17@2486",
                      title: "শ্রীকৃষ্ণচন্দ্রাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "R4@2487",
                      title: "শ্রীরাধা-কৃপা-কটাক্ষ-স্তোত্রম্"
                    },
                    {
                      type: "song",
                      uid: "K3@2488",
                      title: "শ্রীনন্দনন্দনাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "K1@2489",
                      title: "শ্রীদামোদরাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "K4@2490",
                      title: "শ্রীশ্রীমধুরাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "K2@2491",
                      title: "শ্রীৱ্রজরাজসুতাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "K10@2492",
                      title: "শ্রীকুঞ্জৱিহার্য্যাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "VT6@2493",
                      title: "শ্রীজগন্নাথাষ্টকম্"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3c@2494",
                  title: "ধাম এবং পরিকার",
                  children: [
                    {
                      type: "song",
                      uid: "P2@2495",
                      title: "শ্রীললিতাষ্টকম্"
                    }
                  ]
                }
              ]
            },
            {
              type: "collection",
              uid: "4@2496",
              title: "সংস্কৃত-গীতি ~ Sanskrit Songs",
              children: [
                {
                  type: "heading",
                  uid: "*@2497",
                  title: "শ্রীগৌরাঙ্গ-গীতি ~ Śrī Gaurāṅga Songs"
                },
                {
                  type: "song",
                  uid: "GH13@2498",
                  title: "পশ্য শচী-সুতমনুপম-রূপম্"
                },
                {
                  type: "song",
                  uid: "GH14@2499",
                  title: "ৱন্দে ৱিশ্ৱম্ভর-পদ-কমলম্"
                },
                {
                  type: "song",
                  uid: "GH15@2500",
                  title: "সখে কলয় গৌরমুদারম্"
                },
                {
                  type: "heading",
                  uid: "*@2501",
                  title: "শ্রীরাধা-গীতি ~ Śrī Rādhā Songs"
                },
                {
                  type: "song",
                  uid: "R8@2502",
                  title: "রাধে জয় জয় মাধৱ-দয়িতে"
                },
                {
                  type: "song",
                  uid: "R10@2503",
                  title: "ৱন্দে শ্রীৱৃষভানু-সুতা-পদ"
                },
                {
                  type: "song",
                  uid: "R9@2504",
                  title: "কলয়তি নয়নং দিশি দিশি ৱলিতম্"
                },
                {
                  type: "song",
                  uid: "R12@2505",
                  title: "স্মরতু মনো মম নিরৱধি রাধাম্"
                },
                {
                  type: "heading",
                  uid: "*@2506",
                  title: "শ্রীকৃষ্ণ-গীতি ~ Śrī Kṛṣṇa Songs"
                },
                {
                  type: "song",
                  uid: "K28@2507",
                  title: "(কৃষ্ণ) দেৱ! ভৱন্তং ৱন্দে"
                },
                {
                  type: "song",
                  uid: "K35@2508",
                  title: "জয় জয় সুন্দর নন্দ-কুমার"
                },
                {
                  type: "song",
                  uid: "K36@2509",
                  title: "জয় জয় ৱল্লৱরাজ-কুমার"
                },
                {
                  type: "song",
                  uid: "K33@2510",
                  title: "ৱন্দে গিরিৱরধর-পদকমলম্"
                },
                {
                  type: "song",
                  uid: "K37@2511",
                  title: "ৱন্দে কৃষ্ণং নন্দকুমারম্"
                },
                {
                  type: "song",
                  uid: "K31@2512",
                  title: "হরে হরে গোৱিন্দ হরে"
                },
                {
                  type: "song",
                  uid: "K32@2513",
                  title: "ৱসতু মনো মম মদনগোপালে"
                },
                {
                  type: "heading",
                  uid: "*@2514",
                  title: "শ্রীগীতগোবিন্দ ~ From Śrī Gīta-govinda"
                },
                {
                  type: "song",
                  uid: "VT4@2515",
                  title: "শ্রীদশাৱতার-স্তোত্রম্"
                },
                {
                  type: "song",
                  uid: "K29@2516",
                  title: "শ্রীমঙ্গল-গীতম্"
                },
                {
                  type: "song",
                  uid: "GG1@2517",
                  title: "রতি-সুখ-সারে"
                }
              ]
            },
            {
              type: "collection",
              uid: "5@2518",
              title: "হিন্দী এবং ব্রজভাষা কীর্ত্তন ~ Hindi & Braj Bhasha Kīrtanas",
              children: [
                {
                  type: "heading",
                  uid: "*@2519",
                  title: "শ্রীগুরু ~ Śrī Guru"
                },
                {
                  type: "song",
                  uid: "G13@2520",
                  title: "गुरु-चरणकमल भज मन"
                },
                {
                  type: "song",
                  uid: "G14@2521",
                  title: "गुरुदेव कृपा करके"
                },
                {
                  type: "heading",
                  uid: "*@2522",
                  title: "শ্রীগৌরাঙ্গ ~ Śrī Gaurāṅga"
                },
                {
                  type: "song",
                  uid: "GH76@2523",
                  title: "सुन्दर लाला शचीर-दुलाला"
                },
                {
                  type: "heading",
                  uid: "*@2524",
                  title: "শ্রীশ্রীরাধাকৃষ্ণ ~ Śrī Śrī Rādhā-Kṛṣṇa"
                },
                {
                  type: "song",
                  uid: "B22@2525",
                  title: "राधानाम परम सुखदाइ"
                },
                {
                  type: "song",
                  uid: "B21@2526",
                  title: "राधाराणी की जय! महाराणी की जय!"
                },
                {
                  type: "song",
                  uid: "B8@2527",
                  title: "हमारे ब्रज के रखवाले"
                },
                {
                  type: "song",
                  uid: "B19@2528",
                  title: "जय जय राधारमण हरिबोल"
                },
                {
                  type: "song",
                  uid: "B7@2529",
                  title: "अनूपम माधुरी जोड़ी"
                },
                {
                  type: "song",
                  uid: "B3@2530",
                  title: "ब्रज-जन मन सुखकारी"
                },
                {
                  type: "song",
                  uid: "B1@2531",
                  title: "जय माधव मदनमुरारी"
                },
                {
                  type: "song",
                  uid: "B4@2532",
                  title: "भज गोविन्द, भज गोविन्द"
                },
                {
                  type: "song",
                  uid: "B6@2533",
                  title: "पार करेंगे नैया रे, भज कृष्ण कन्हैया"
                },
                {
                  type: "song",
                  uid: "B5@2534",
                  title: "बसो मेरे नयनन में नन्दलाल"
                },
                {
                  type: "song",
                  uid: "B20@2535",
                  title: "जियो श्याम लाला, जियो श्याम लाला"
                },
                {
                  type: "song",
                  uid: "B10@2536",
                  title: "जय मोर मुकुट पीताम्बरधारी"
                },
                {
                  type: "song",
                  uid: "B2@2537",
                  title: "जय गोविन्द, जय गोपाल"
                },
                {
                  type: "heading",
                  uid: "*@2538",
                  title: "শ্রীনাম ~ Śrī Nāma"
                },
                {
                  type: "song",
                  uid: "NM17@2539",
                  title: "हरिसे बड़ा हरिका नाम"
                },
                {
                  type: "heading",
                  uid: "*@2540",
                  title: "ঝূলন-যাত্রা ~ Jhūlana-yātrā"
                },
                {
                  type: "song",
                  uid: "B25@2541",
                  title: "राधे झूलन पधारो झुक आये बदरा"
                },
                {
                  type: "song",
                  uid: "B26@2542",
                  title: "झूला झूले राधादामोदर वृन्दावनमें"
                },
                {
                  type: "heading",
                  uid: "*@2543",
                  title: "শ্রীবৃন্দাবন ~ Śrī Vṛndāvana"
                },
                {
                  type: "song",
                  uid: "D21@2544",
                  title: "आली! म्हांने लागे वृन्दावन नीको"
                },
                {
                  type: "heading",
                  uid: "*@2545",
                  title: "শ্রীগোবর্দ্ধন ~ Śrī Govardhana"
                },
                {
                  type: "song",
                  uid: "D19@2546",
                  title: "श्रीगोवर्धन महाराज"
                }
              ]
            },
            {
              type: "collection",
              uid: "6@2547",
              title: "উৎকাল কীর্ত্তন ~ Oriya Kīrtanas",
              children: [
                {
                  type: "heading",
                  uid: "*@2548",
                  title: "Coming soon..."
                }
              ]
            }
          ],
          count: 238
        },
        {
          type: "collection",
          uid: "DKH@2549",
          title: "দৈনিক কীর্ত্তন সংগ্রহ [IPBYS] ~ Daily Kīrtana Handbook [IPBYS]",
          children: [
            {
              type: "heading",
              uid: "*@2550",
              title: "১ম সংস্করণ ইং ২০১৬ ~ published 2016"
            },
            {
              type: "collection",
              uid: "1@2551",
              title: "ভূমিকা ~ Preface",
              children: [
                {
                  type: "song",
                  uid: "@2552",
                  title: "শীঘ্রই আসছে ~ Coming soon"
                }
              ]
            },
            {
              type: "collection",
              uid: "2@2553",
              title: "জয়ধ্বনি ~ Jaya-dhvani",
              children: [
                {
                  type: "song",
                  uid: "M11@2554",
                  title: "জয়ধ্ৱনি [IPBYS]"
                }
              ]
            },
            {
              type: "collection",
              uid: "3@2555",
              title: "সূর্য্যোদয়ের আগে ~ Predawn",
              children: [
                {
                  type: "heading",
                  uid: "*@2556",
                  title: "গুরু-বন্দনা ~ Prayers to Śrī Guru"
                },
                {
                  type: "song",
                  uid: "G1@2557",
                  title: "শ্রীগুর্ৱ্ৱষ্টকম্ (সংস্কৃত) ~ Śrī Gurvāṣṭakam (Sanskrit)"
                },
                {
                  type: "song",
                  uid: "GV17@2558",
                  title: "শ্রীপ্রভুপাদ-পদ্ম-স্তৱকঃ"
                },
                {
                  type: "heading",
                  uid: "*@2559",
                  title: "মঙ্গল-আরতি ~ Maṅgala-ārati"
                },
                {
                  type: "song",
                  uid: "A4@2560",
                  title: "শ্রীমঙ্গল-আরতি [GVS & IPBYS]"
                },
                {
                  type: "song",
                  uid: "A7@2561",
                  title: "বিভাবরী-শেষ"
                },
                {
                  type: "heading",
                  uid: "*@2562",
                  title: "তুলসী-মন্দির-পরিক্রমা ~ Tulasī and Mandira Parikramā"
                },
                {
                  type: "song",
                  uid: "NK2@2563",
                  title: "জয় রাধে, জয় কৃষ্ণ, জয় বৃন্দাবন"
                }
              ]
            },
            {
              type: "collection",
              uid: "4@2564",
              title: "প্রভাত ~ Morning",
              children: [
                {
                  type: "collection",
                  uid: "4a@2565",
                  title: "মঙ্গলাচরণ ~ Maṅgalācaraṇa",
                  children: [
                    {
                      type: "song",
                      uid: "M12@2566",
                      title: "মঙ্গলাচরণ [IPBYS]"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "4b@2567",
                  title: "নির্বাচিত কীর্ত্তন ~ Selected Bhajanas",
                  children: [
                    {
                      type: "song",
                      uid: "G2@2568",
                      title: "শ্রীগুর্ব্বষ্টকম্ (বাংলা) ~ Śrī Gurvāṣṭakam (Bengali Rendition)"
                    },
                    {
                      type: "song",
                      uid: "GV10@2569",
                      title: "শ্রীগুরু-পরম্পরা—সংস্কৃত [IPBYS]"
                    },
                    {
                      type: "song",
                      uid: "V1@2570",
                      title: "শ্রীবৈষ্ণব-বন্দনা"
                    },
                    {
                      type: "song",
                      uid: "S1@2571",
                      title: "শ্রীকৃষ্ণচৈতন্য প্রভু জীবে দয়া করি’"
                    },
                    {
                      type: "song",
                      uid: "N14@2572",
                      title: "নিতাই গুণমণি আমার"
                    },
                    {
                      type: "song",
                      uid: "NM11@2573",
                      title: "উদিল অরুণ পূরব ভাগে"
                    },
                    {
                      type: "song",
                      uid: "NM12@2574",
                      title: "জীব জাগ, জীব জাগ"
                    },
                    {
                      type: "song",
                      uid: "NK5@2575",
                      title: "কলিকুক্কুর-কদন যদি চাও (হে)"
                    },
                    {
                      type: "song",
                      uid: "NM2@2576",
                      title: "জয় জয় হরিনাম"
                    },
                    {
                      type: "song",
                      uid: "R20@2577",
                      title: "রাধা-ভজনে যদি মতি নাহি ভেলা"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "4c@2578",
                  title: "শ্রীগুরু-পূজা ~ Śrī Guru-pūjā",
                  children: [
                    {
                      type: "song",
                      uid: "G3@2579",
                      title: "শ্রীগুরু-চরণ-পদ্ম"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "4d@2580",
                  title: "শ্রীতুলসী-পূজা ~ Śrī Tulasī-pūjā",
                  children: [
                    {
                      type: "song",
                      uid: "P1@2581",
                      title: "শ্রীৱৃন্দাদেৱ্যষ্টকম্"
                    }
                  ]
                }
              ]
            },
            {
              type: "collection",
              uid: "5@2582",
              title: "মধ্যাহ্ন ~ Noon",
              children: [
                {
                  type: "collection",
                  uid: "5a@2583",
                  title: "গুরু-বন্দনা ~ Prayers to Śrī Guru",
                  children: [
                    {
                      type: "song",
                      uid: "A9@2584",
                      title: "ভজ ভকত-বৎসল শ্রীগৌরহরি"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "5b@2585",
                  title: "মঙ্গল-আরতি ~ Maṅgala-ārati",
                  children: [
                    {
                      type: "song",
                      uid: "A8@2586",
                      title: "যশোমতী-নন্দন"
                    },
                    {
                      type: "song",
                      uid: "NK17@2587",
                      title: "জয় রাধা-মাধব কুঞ্জবিহারী"
                    }
                  ]
                }
              ]
            },
            {
              type: "collection",
              uid: "6@2588",
              title: "সন্ধ্যা ~ Evening",
              children: [
                {
                  type: "collection",
                  uid: "6a@2589",
                  title: "নির্বাচিত কীর্তন ~ Selected Bhajanas",
                  children: [
                    {
                      type: "song",
                      uid: "G6@2590",
                      title: "গুরুদেব! কৃপাবিন্দু দিয়া"
                    },
                    {
                      type: "song",
                      uid: "V3@2591",
                      title: "ওহে! বৈষ্ণব ঠাকুর"
                    },
                    {
                      type: "song",
                      uid: "GP9@2592",
                      title: "শ্রীকৃষ্ণচৈতন্য প্রভু দয়া কর মোরে"
                    },
                    {
                      type: "song",
                      uid: "GN9@2593",
                      title: "‘গৌরাঙ্গ’ বলিতে হবে"
                    },
                    {
                      type: "song",
                      uid: "RK10@2594",
                      title: "হরি হরি, কবে মোর হইবে সুদিন"
                    },
                    {
                      type: "song",
                      uid: "RK9@2595",
                      title: "রাধাকৃষ্ণ প্রাণ মোর"
                    },
                    {
                      type: "song",
                      uid: "B3@2596",
                      title: "ब्रज-जन मन सुखकारी"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "6b@2597",
                  title: "সন্ধ্যা-আরতি ~ Evening Ārati",
                  children: [
                    {
                      type: "song",
                      uid: "A10@2598",
                      title: "শ্রীগৌর-আরতি"
                    },
                    {
                      type: "song",
                      uid: "A11@2599",
                      title: "শ্রীযুগল-আরতি"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "6c@2600",
                  title: "শ্রীতুলসী পরিক্রমা এবং আরতি ~ Śrī Tulasī Parikramā and Ārati",
                  children: [
                    {
                      type: "song",
                      uid: "A15@2601",
                      title: "‘নমো নমঃ তুলসী কৃষ্ণ-প্রেয়সী’ (নমো নমঃ)"
                    },
                    {
                      type: "song",
                      uid: "NK1@2602",
                      title: "(হরি) হরয়ে নমঃ কৃষ্ণ যাদবায় নমঃ"
                    }
                  ]
                }
              ]
            },
            {
              type: "collection",
              uid: "7@2603",
              title: "বিশেষ উপলক্ষ ~ Special Kīrtanas",
              children: [
                {
                  type: "collection",
                  uid: "7a@2604",
                  title: "কর্ত্তিক কীর্ত্তন ~ Kartika Kīrtanas",
                  children: [
                    {
                      type: "song",
                      uid: "K28@2605",
                      title: "(কৃষ্ণ) দেৱ! ভৱন্তং ৱন্দে"
                    },
                    {
                      type: "song",
                      uid: "R8@2606",
                      title: "রাধে জয় জয় মাধৱ-দয়িতে"
                    },
                    {
                      type: "song",
                      uid: "K3@2607",
                      title: "শ্রীনন্দনন্দনাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "R4@2608",
                      title: "শ্রীরাধা-কৃপা-কটাক্ষ-স্তোত্রম্"
                    },
                    {
                      type: "song",
                      uid: "K1@2609",
                      title: "শ্রীদামোদরাষ্টকম্"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "7b@2610",
                  title: "একাদশী ~ Ekādaśī",
                  children: [
                    {
                      type: "song",
                      uid: "E1@2611",
                      title: "শুদ্ধভকত-চরণ-রেণু"
                    },
                    {
                      type: "song",
                      uid: "E2@2612",
                      title: "শ্রীমন্মহাপ্রভুর হরিবাসর-ব্রতপালন"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "7c@2613",
                  title: "বিরহ কীর্ত্তন ~ Songs of Separation",
                  children: [
                    {
                      type: "song",
                      uid: "GV47@2614",
                      title: "যে আনিল প্রেমধন"
                    },
                    {
                      type: "song",
                      uid: "GV44@2615",
                      title: "শ্রীরূপমঞ্জরী-পদ"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "7d@2616",
                  title: "মহাপ্রসাদ মহিমা ~ The Glories of Mahāprasāda",
                  children: [
                    {
                      type: "song",
                      uid: "PR1@2617",
                      title: "ভাইরে! শরীর অবিদ্যা-জাল"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "7e@2618",
                  title: "নগর-কীর্ত্তন থেকে ফিরে আসা ~ Returning from Nagara-saṅkīrtana",
                  children: [
                    {
                      type: "song",
                      uid: "NK34@2619",
                      title: "নগর ভ্রমিয়া আমার গৌর এল ঘরে"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "7f@2620",
                  title: "নৃসিংহ-স্তুতি ~ Nṛsiṁha Prayers",
                  children: [
                    {
                      type: "song",
                      uid: "VT7@2621",
                      title: "নমস্তে নরসিংহায়"
                    }
                  ]
                }
              ]
            },
            {
              type: "collection",
              uid: "8@2622",
              title: "আবৃত্তি জন্য আয়াত ~ Verses for Recitation",
              children: [
                {
                  type: "song",
                  uid: "U12@2623",
                  title: "শ্রীউপদেশামৃতম্"
                },
                {
                  type: "song",
                  uid: "MS1@2624",
                  title: "শ্রীমনঃ-শিক্ষা"
                },
                {
                  type: "song",
                  uid: "SI11@2625",
                  title: "শ্রীশিক্ষাষ্টকম্"
                }
              ]
            }
          ],
          count: 49
        },
        {
          type: "collection",
          uid: "SGG@2626",
          title: "শ্রীগৌড়ীয়-গীতি [শ্রীচৈতন্য মঠ] ~ Śrī Gauḍīya-gīti [Śrī Caitanya Maṭha]",
          children: [
            {
              type: "song",
              uid: "M14@2627",
              title: "প্রেমধ্বনি (শ্রীচৈতন্য মঠ)"
            },
            {
              type: "song",
              uid: "M3@2628",
              title: "মঙ্গলাচরণ (শ্রীচৈতন্য মঠ)"
            },
            {
              type: "collection",
              uid: "1@2629",
              title: "সাধক কণ্ঠমালা",
              children: [
                {
                  type: "collection",
                  uid: "1a@2630",
                  title: "সংস্কৃত-স্তব",
                  children: [
                    {
                      type: "song",
                      uid: "GV3@2631",
                      title: "শ্রীগুরু-পরম্পরা—সংস্কৃত (শ্রীচৈতন্য মঠ)"
                    },
                    {
                      type: "song",
                      uid: "GV4@2632",
                      title: "শ্রীগুরু-পরম্পরা—বাংলা (শ্রীচৈতন্য মঠ)"
                    },
                    {
                      type: "song",
                      uid: "G1@2633",
                      title: "শ্রীগুর্ৱ্ৱষ্টকম্ (সংস্কৃত) ~ Śrī Gurvāṣṭakam (Sanskrit)"
                    },
                    {
                      type: "song",
                      uid: "GV49@2634",
                      title: "শ্রীশ্রীভক্তিবিলাস তীর্থ বন্দনাষ্টকম্‌"
                    },
                    {
                      type: "song",
                      uid: "GV18@2635",
                      title: "শ্রীল-প্রভুপাদ-বন্দনা"
                    },
                    {
                      type: "song",
                      uid: "GV19@2636",
                      title: "শ্রীমদ্ গৌর-কিশোরাষ্টকম্‌"
                    },
                    {
                      type: "song",
                      uid: "GH3@2637",
                      title: "শ্রীচৈতন্যাষ্টকম্ (১)"
                    },
                    {
                      type: "song",
                      uid: "K1@2638",
                      title: "শ্রীদামোদরাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "K17@2639",
                      title: "শ্রীকৃষ্ণচন্দ্রাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "R3@2640",
                      title: "শ্রীরাধিকাষ্টকম্ (৩)"
                    },
                    {
                      type: "song",
                      uid: "P1@2641",
                      title: "শ্রীৱৃন্দাদেৱ্যষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "NM9@2642",
                      title: "শ্রীকৃষ্ণনামাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "VT4@2643",
                      title: "শ্রীদশাৱতার-স্তোত্রম্"
                    },
                    {
                      type: "song",
                      uid: "GV31@2644",
                      title: "শ্রীষড়্‌গোস্ৱাম্যাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "SI11@2645",
                      title: "শ্রীশিক্ষাষ্টকম্"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "1b@2646",
                  title: "সংস্কৃত-গীত",
                  children: [
                    {
                      type: "song",
                      uid: "K28@2647",
                      title: "(কৃষ্ণ) দেৱ! ভৱন্তং ৱন্দে"
                    },
                    {
                      type: "song",
                      uid: "K37@2648",
                      title: "ৱন্দে কৃষ্ণং নন্দকুমারম্"
                    },
                    {
                      type: "song",
                      uid: "R8@2649",
                      title: "রাধে জয় জয় মাধৱ-দয়িতে"
                    },
                    {
                      type: "song",
                      uid: "VT10@2650",
                      title: "শ্রীজগন্নাথদেৱ-স্তৱঃ"
                    }
                  ]
                }
              ]
            },
            {
              type: "collection",
              uid: "2@2651",
              title: "বাংলা কীর্তন",
              children: [
                {
                  type: "collection",
                  uid: "2a@2652",
                  title: "তুলসী-আরতি",
                  children: [
                    {
                      type: "song",
                      uid: "A16@2653",
                      title: "শ্রীতুলসী-আরতি"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2b@2654",
                  title: "বৈষ্ণব-বন্দন",
                  children: [
                    {
                      type: "song",
                      uid: "GP4@2655",
                      title: "প্রাণ গোরাচাঁদ মোর"
                    },
                    {
                      type: "song",
                      uid: "GP5@2656",
                      title: "ধন্য অবতার গোরা ন্যাসি-শিরোমণি"
                    },
                    {
                      type: "song",
                      uid: "GP6@2657",
                      title: "ভাল অবতার শ্রীগৌরাঙ্গ অবতার"
                    },
                    {
                      type: "song",
                      uid: "GP7@2658",
                      title: "গোরা গোসাঞি পতিতপাবন অবতার"
                    },
                    {
                      type: "song",
                      uid: "V1@2659",
                      title: "শ্রীবৈষ্ণব-বন্দনা"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2c@2660",
                  title: "শ্রীনাম",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2661",
                      title: "শ্রীনাম-হট্ট"
                    },
                    {
                      type: "song",
                      uid: "NK25@2662",
                      title: "নদীয়া-গোদ্রুমে নিত্যানন্দ মহাজন"
                    },
                    {
                      type: "heading",
                      uid: "*@2663",
                      title: "দালালের গীত"
                    },
                    {
                      type: "song",
                      uid: "NK26@2664",
                      title: "বড় সুখের খবর গাই"
                    },
                    {
                      type: "heading",
                      uid: "*@2665",
                      title: "শ্রীশ্রীমহাপ্রভুর শতনাম"
                    },
                    {
                      type: "song",
                      uid: "NK21@2666",
                      title: "শ্রীমন্মহাপ্রভুর শতনাম"
                    },
                    {
                      type: "heading",
                      uid: "*@2667",
                      title: "শ্রীশ্রীকৃষ্ণের অষ্টোত্তরশতনাম"
                    },
                    {
                      type: "song",
                      uid: "NK23@2668",
                      title: "শ্রীকৃষ্ণের অষ্টোত্তরশতনাম"
                    },
                    {
                      type: "heading",
                      uid: "*@2669",
                      title: "শ্রীশ্রীনাম সংকীর্তন"
                    },
                    {
                      type: "song",
                      uid: "NK20@2670",
                      title: "জয় জয় শ্রীকৃষ্ণচৈতন্য নিত্যানন্দ"
                    },
                    {
                      type: "song",
                      uid: "NK2@2671",
                      title: "জয় রাধে, জয় কৃষ্ণ, জয় বৃন্দাবন"
                    },
                    {
                      type: "song",
                      uid: "NK1@2672",
                      title: "(হরি) হরয়ে নমঃ কৃষ্ণ যাদবায় নমঃ"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2d@2673",
                  title: "দৈন্য ও প্রপত্তি",
                  children: [
                    {
                      type: "song",
                      uid: "K61@2674",
                      title: "হরি হে দয়াল মোর জয় রাধানাথ"
                    },
                    {
                      type: "song",
                      uid: "MS36@2675",
                      title: "ভজহুঁ রে মন"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2e@2676",
                  title: "বাউল-সঙ্গীত ~ Songs of a Madman",
                  children: [
                    {
                      type: "song",
                      uid: "BS1@2677",
                      title: "আমি তোমার দুঃখের দুঃখী, সুখের সুখী"
                    },
                    {
                      type: "song",
                      uid: "BS2@2678",
                      title: "ধর্ম্মপথে থাকি’ কর জীবন যাপন, ভাই"
                    },
                    {
                      type: "song",
                      uid: "BS3@2679",
                      title: "আসল কথা বল্‌তে কি"
                    },
                    {
                      type: "song",
                      uid: "BS4@2680",
                      title: "‘বাউল বাউল’ বল্‌ছ সবে"
                    },
                    {
                      type: "song",
                      uid: "BS5@2681",
                      title: "মানুষ-ভজন কর্‌ছো, ও ভাই"
                    },
                    {
                      type: "song",
                      uid: "BS6@2682",
                      title: "এও ত’ এক কলির চেলা"
                    },
                    {
                      type: "song",
                      uid: "BS7@2683",
                      title: "(মন আমার) হুঁসা’র থেকো, ভুল’ নাক"
                    },
                    {
                      type: "song",
                      uid: "BS8@2684",
                      title: "মনের মালা জপ্‌বি যখন, মন"
                    },
                    {
                      type: "song",
                      uid: "BS9@2685",
                      title: "ঘরে ব’সে বাউল হও রে মন"
                    },
                    {
                      type: "song",
                      uid: "BS10@2686",
                      title: "বলান্ বৈরাগী ঠাকুর, কিন্তু গৃহীর মধ্যে বাহাদুর"
                    },
                    {
                      type: "song",
                      uid: "BS11@2687",
                      title: "কেন ভেকের প্রয়াস ?"
                    },
                    {
                      type: "song",
                      uid: "BS12@2688",
                      title: "হ’য়ে বিষয়ে আবেশ, পে’লে মন, যাতনা অশেষ"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2f@2689",
                  title:
                    "শ্রীল নরোত্তম ঠাকুর মহাশয়ের প্রার্থনা ~ Śrīla Narottama dāsa Ṭhākura’s Prārthanā",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2690",
                      title: "লালসাময়ী ~ Filled with Longing"
                    },
                    {
                      type: "song",
                      uid: "GN9@2691",
                      title: "‘গৌরাঙ্গ’ বলিতে হবে"
                    },
                    {
                      type: "heading",
                      uid: "*@2692",
                      title: "সংপ্রার্থনাত্মিকা ~ Supplication"
                    },
                    {
                      type: "song",
                      uid: "RK12@2693",
                      title: "রাধাকৃষ্ণ! নিবেদন এই জন করে"
                    },
                    {
                      type: "heading",
                      uid: "*@2694",
                      title: "দৈন্যবোধিকা ~ Expressions of Humility"
                    },
                    {
                      type: "song",
                      uid: "GP15@2695",
                      title: "হরি হরি! কি মোর করমগতি মন্দ"
                    },
                    {
                      type: "song",
                      uid: "GP13@2696",
                      title: "হরি হরি! বিফলে জনম গোঙাইনু"
                    },
                    {
                      type: "song",
                      uid: "K55@2697",
                      title: "প্রাণেশ্বর! নিবেদন এইজন করে"
                    },
                    {
                      type: "song",
                      uid: "K56@2698",
                      title: "হরি হরি! কৃপা করি’ রাখ নিজ পদে"
                    },
                    {
                      type: "song",
                      uid: "GP14@2699",
                      title: "হরি হরি! বড় শেল মরমে রহিল"
                    },
                    {
                      type: "heading",
                      uid: "*@2700",
                      title: "আক্ষেপাত্মিকা ~ A Prayer full of Regret"
                    },
                    {
                      type: "song",
                      uid: "K57@2701",
                      title: "হরি হরি! কি মোর করম অভাগ"
                    },
                    {
                      type: "heading",
                      uid: "*@2702",
                      title: "স্বাভীষ্ট লালসা ~ Longing for One’s Cherished Desire"
                    },
                    {
                      type: "song",
                      uid: "RK13@2703",
                      title: "হরি হরি! হেন দিন হইবে আমার"
                    },
                    {
                      type: "song",
                      uid: "RK14@2704",
                      title: "হরি হরি! কবে মোর হইবে সুদিনে"
                    },
                    {
                      type: "song",
                      uid: "RK15@2705",
                      title: "গোবর্ধন গিরিবর, কেবল নির্জ্জন স্থল"
                    },
                    {
                      type: "song",
                      uid: "RK29@2706",
                      title: "হরি হরি! আর কি এমন দশা হব, (কবে বৃষভানুপুরে...)"
                    },
                    {
                      type: "song",
                      uid: "D18@2707",
                      title: "হরি হরি! আর কি এমন দশা হব"
                    },
                    {
                      type: "song",
                      uid: "RK16@2708",
                      title: "কুসুমিত বৃন্দাবনে, নাচত শিখিগণে"
                    },
                    {
                      type: "heading",
                      uid: "*@2709",
                      title: "পুনঃ স্বাভীষ্ট-লালসা ~ Further Longing for One’s Cherished Desire"
                    },
                    {
                      type: "song",
                      uid: "RK17@2710",
                      title: "হরি হরি! কবে মোর হইবে সুদিন"
                    },
                    {
                      type: "heading",
                      uid: "*@2711",
                      title: "লালসা ~ Yearning"
                    },
                    {
                      type: "song",
                      uid: "GV44@2712",
                      title: "শ্রীরূপমঞ্জরী-পদ"
                    },
                    {
                      type: "song",
                      uid: "GV45@2713",
                      title: "শুনিয়াছি সাধুমুখে"
                    },
                    {
                      type: "song",
                      uid: "RK18@2714",
                      title: "এই নব-দাসী বলি শ্রীরূপ চাহিবে"
                    },
                    {
                      type: "song",
                      uid: "RK19@2715",
                      title: "শ্রীরূপ-পশ্চাতে আমি রহিব ভীত হঞা"
                    },
                    {
                      type: "song",
                      uid: "GV29@2716",
                      title: "হা হা প্রভু লোকনাথ! রাখ পদদ্বন্দ্বে"
                    },
                    {
                      type: "song",
                      uid: "GV30@2717",
                      title: "লোকনাথ প্রভু, তুমি দয়া কর মোরে"
                    },
                    {
                      type: "song",
                      uid: "RK20@2718",
                      title: "হা হা প্রভু! কর দয়া করুণা তোমার"
                    },
                    {
                      type: "song",
                      uid: "RK21@2719",
                      title: "হরি হরি! কবে হেন দশা হবে মোর"
                    },
                    {
                      type: "song",
                      uid: "GP8@2720",
                      title: "জয় জয় শ্রীকৃষ্ণচৈতন্য-নিত্যানন্দ"
                    },
                    {
                      type: "heading",
                      uid: "*@2721",
                      title: "সাধকদেহোচিত লালসা ~ Longing as a Sādhaka"
                    },
                    {
                      type: "song",
                      uid: "RK10@2722",
                      title: "হরি হরি, কবে মোর হইবে সুদিন"
                    },
                    {
                      type: "heading",
                      uid: "*@2723",
                      title: "সাধকদেহোচিত শ্রীবৃন্দাবনলালসা ~ Longing for Vṛndāvana as a Sādhaka"
                    },
                    {
                      type: "song",
                      uid: "D15@2724",
                      title: "হরি হরি! আর কি এমন দশা হব"
                    },
                    {
                      type: "song",
                      uid: "D14@2725",
                      title: "হরি হরি! আর কবে পালটিবে দশা"
                    },
                    {
                      type: "song",
                      uid: "D16@2726",
                      title: "করঙ্গ কৌপীন লঞা"
                    },
                    {
                      type: "song",
                      uid: "D17@2727",
                      title: "হরি হরি! কবে হব বৃন্দাবনবাসী"
                    },
                    {
                      type: "heading",
                      uid: "*@2728",
                      title:
                        "সবিলাপ শ্রীবৃন্দাবনবাস-লালসা ~ Filled with Lamentation, Yearning for Residence in Śrī Vṛndāvana"
                    },
                    {
                      type: "song",
                      uid: "D20@2729",
                      title: "আর কি এমন দশা হব"
                    },
                    {
                      type: "heading",
                      uid: "*@2730",
                      title:
                        "মাথুরবিরহোচিত দর্শন-লালসা ~ Longing in Separation to See Kṛṣṇa Who Has Gone to Mathurā"
                    },
                    {
                      type: "song",
                      uid: "K65@2731",
                      title: "কবে কৃষ্ণধন পাব"
                    },
                    {
                      type: "song",
                      uid: "K66@2732",
                      title: "এইবার পাইলে দেখা চরণ দুখানি"
                    },
                    {
                      type: "song",
                      uid: "RK22@2733",
                      title: "বৃন্দাবন রম্য-স্থান, দিব্য-চিন্তামণি-ধাম"
                    },
                    {
                      type: "song",
                      uid: "RK23@2734",
                      title: "কদম্ব তরুর ডাল, নামিয়াছে ভূমে ভাল"
                    },
                    {
                      type: "song",
                      uid: "RK24@2735",
                      title: "মৃগমদ-চন্দন"
                    },
                    {
                      type: "heading",
                      uid: "*@2736",
                      title: "স্বনিষ্ঠা ~ My Deep Resolve"
                    },
                    {
                      type: "song",
                      uid: "GN12@2737",
                      title: "ধন মোর নিত্যানন্দ পতি মোর গৌরচন্দ্র"
                    },
                    {
                      type: "heading",
                      uid: "*@2738",
                      title: "নিত্যানন্দ-নিষ্ঠা ~ Firm Faith in Nityānanda"
                    },
                    {
                      type: "song",
                      uid: "N7@2739",
                      title: "নিতাই-পদ-কমল"
                    },
                    {
                      type: "heading",
                      uid: "*@2740",
                      title: "গৌরাঙ্গ-নিষ্ঠা ~ Firm Faith in Gaurāṅga"
                    },
                    {
                      type: "song",
                      uid: "GH52@2741",
                      title: "আরে ভাই! ভজ মোর গৌরাঙ্গ-চরণ"
                    },
                    {
                      type: "heading",
                      uid: "*@2742",
                      title: "সাবরণ-গৌরমহিমা ~ The Glories of Śrī Gaurāṅga"
                    },
                    {
                      type: "song",
                      uid: "GH40@2743",
                      title: "গৌরাঙ্গের দু’টী পদ"
                    },
                    {
                      type: "heading",
                      uid: "*@2744",
                      title: "পুনঃপ্রার্থনা ~ Praying Further"
                    },
                    {
                      type: "song",
                      uid: "GP9@2745",
                      title: "শ্রীকৃষ্ণচৈতন্য প্রভু দয়া কর মোরে"
                    },
                    {
                      type: "heading",
                      uid: "*@2746",
                      title:
                        "সপার্ষদ-ভগবদ্বিরহজনিত-বিলাপ ~ Lamenting in Separation from the Lord and His Associates"
                    },
                    {
                      type: "song",
                      uid: "GV47@2747",
                      title: "যে আনিল প্রেমধন"
                    },
                    {
                      type: "heading",
                      uid: "*@2748",
                      title: "আক্ষেপ ~ Regret"
                    },
                    {
                      type: "song",
                      uid: "GH56@2749",
                      title: "গোরা পঁহু না ভজিয়া মৈনু"
                    },
                    {
                      type: "song",
                      uid: "GP16@2750",
                      title: "হরি হরি! কি মোর করম অনুরত"
                    },
                    {
                      type: "heading",
                      uid: "*@2751",
                      title: "বৈষ্ণব-মহিমা ~ The Glories of the Vaiṣṇavas"
                    },
                    {
                      type: "song",
                      uid: "V2@2752",
                      title: "ঠাকুর বৈষ্ণব-পদ"
                    },
                    {
                      type: "heading",
                      uid: "*@2753",
                      title: "বৈষ্ণব-বিজ্ঞপ্তি ~ Entreaties to the Vaiṣṇavas"
                    },
                    {
                      type: "song",
                      uid: "V7@2754",
                      title: "ঠাকুর বৈষ্ণবগণ"
                    },
                    {
                      type: "song",
                      uid: "V8@2755",
                      title: "এইবার করুণা কর"
                    },
                    {
                      type: "song",
                      uid: "V9@2756",
                      title: "কিরূপে পাইব সেবা"
                    },
                    {
                      type: "song",
                      uid: "RK25@2757",
                      title: "রাধাকৃষ্ণ সেবোঁ মুঞি জীবনে মরণে"
                    },
                    {
                      type: "heading",
                      uid: "*@2758",
                      title: "সখীবৃন্দে বিজ্ঞপ্তি ~ An Entreaty to the Sakhīs"
                    },
                    {
                      type: "song",
                      uid: "RK9@2759",
                      title: "রাধাকৃষ্ণ প্রাণ মোর"
                    },
                    {
                      type: "song",
                      uid: "R32@2760",
                      title: "প্রাণেশ্বরি! এইবার করুণা কর মোরে"
                    },
                    {
                      type: "song",
                      uid: "R33@2761",
                      title: "অরুণ-কমল-দলে"
                    },
                    {
                      type: "heading",
                      uid: "*@2762",
                      title: "শ্রীকৃষ্ণে বিজ্ঞপ্তিঃ ~ An Entreaty to Śrī Kṛṣṇa"
                    },
                    {
                      type: "song",
                      uid: "RK26@2763",
                      title: "প্রভু হে, এইবার করহ করুণা"
                    },
                    {
                      type: "song",
                      uid: "RK27@2764",
                      title: "আজি রসের বাদর নিশি"
                    },
                    {
                      type: "heading",
                      uid: "*@2765",
                      title: "অতিরিক্ত পদ ~ Extra Song"
                    },
                    {
                      type: "song",
                      uid: "K73@2766",
                      title: "হেদে হে নাগরবর, শুন হে মুরলীধর"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2g@2767",
                  title:
                    "শ্রীল নরোত্তম ঠাকুর মহাশয়ের প্রেমভক্তি-চন্দ্রিকা ~ Śrīla Narottama dāsa Ṭhākura’s Prema-bhakti-candrikā",
                  children: [
                    {
                      type: "song",
                      uid: "PBC1@2768",
                      title: "শ্রীগুরু-চরণ-পদ্ম + বৈষ্ণব-চরণরেণু"
                    },
                    {
                      type: "song",
                      uid: "PBC2@2769",
                      title: "অন্য অভিলাষ ছাড়ি’"
                    },
                    {
                      type: "song",
                      uid: "PBC3@2770",
                      title: "তুমি ত দয়ার সিন্ধু"
                    },
                    {
                      type: "song",
                      uid: "PBC4@2771",
                      title: "আন কথা আন ব্যথা"
                    },
                    {
                      type: "song",
                      uid: "PBC5@2772",
                      title: "রাগের ভজন-পথ"
                    },
                    {
                      type: "song",
                      uid: "PBC6@2773",
                      title: "যুগল-চরণ-প্রতি"
                    },
                    {
                      type: "song",
                      uid: "PBC7@2774",
                      title: "রাধাকৃষ্ণ করোঁ ধ্যান"
                    },
                    {
                      type: "song",
                      uid: "PBC8@2775",
                      title: "বচনের অগোচর, বৃন্দাবন ধামময়"
                    },
                    {
                      type: "song",
                      uid: "PBC9@2776",
                      title: "আন কথা না শুনিব"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2h@2777",
                  title: "শ্রীমনঃশিক্ষা ~ Śrī Manaḥ-śikṣā",
                  children: [
                    {
                      type: "song",
                      uid: "MS2@2778",
                      title: "ব্রজধাম নিত্যধন, রাধাকৃষ্ণ দুইজন"
                    },
                    {
                      type: "song",
                      uid: "MS3@2779",
                      title: "গুরুদেবে, ব্রজবনে, ব্রজভূমিবাসী জনে"
                    },
                    {
                      type: "song",
                      uid: "MS4@2780",
                      title: "‘ধর্ম্ম’ বলি বেদে যারে"
                    },
                    {
                      type: "song",
                      uid: "MS5@2781",
                      title: "রাগাবেশে ব্রজধাম-বাসে যদি তীব্র কাম"
                    },
                    {
                      type: "song",
                      uid: "MS6@2782",
                      title: "কৃষ্ণবার্ত্তা বিনা আন"
                    },
                    {
                      type: "song",
                      uid: "MS7@2783",
                      title: "কাম-ক্রোধ-লোভ-মোহ-মদ-মৎসরতা-সহ"
                    },
                    {
                      type: "song",
                      uid: "MS8@2784",
                      title: "কাম-ক্রোধ-আদি করি’, বাহিরে সে-সব অরি"
                    },
                    {
                      type: "song",
                      uid: "MS9@2785",
                      title: "কপটতা হৈলে দূর"
                    },
                    {
                      type: "song",
                      uid: "MS10@2786",
                      title: "ব্রজভূমি চিন্তামণি"
                    },
                    {
                      type: "song",
                      uid: "MS11@2787",
                      title: "ব্রজবন-সুধাকর"
                    },
                    {
                      type: "song",
                      uid: "MS12@2788",
                      title: "সৌন্দর্য্য-কিরণমালা"
                    },
                    {
                      type: "song",
                      uid: "MS13@2789",
                      title: "ব্রজের নিকুঞ্জ-বনে"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2j@2790",
                  title: "শ্রীষড়্‌গোস্বামি-শোচক ~ Lamenting in Separation from the Six Gosvāmīs",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2791",
                      title: ""
                    },
                    {
                      type: "song",
                      uid: "GV41@2792",
                      title: "শ্রীমদ্‌রূপ-গোস্বামী-প্রভুর শোচক (১)"
                    },
                    {
                      type: "song",
                      uid: "GV42@2793",
                      title: "শ্রীমদ্‌রূপ-গোস্বামী-প্রভুর শোচক (২)"
                    },
                    {
                      type: "song",
                      uid: "GV43@2794",
                      title: "যঙ্ কলি রূপ শরীর ন ধরত"
                    },
                    {
                      type: "song",
                      uid: "GV44@2795",
                      title: "শ্রীরূপমঞ্জরী-পদ"
                    },
                    {
                      type: "heading",
                      uid: "*@2796",
                      title: ""
                    },
                    {
                      type: "song",
                      uid: "GV38@2797",
                      title: "শ্রীল-সনাতন-গোস্বামি-প্রভুর শোচক (১)"
                    },
                    {
                      type: "song",
                      uid: "GV39@2798",
                      title: "শ্রীল-সনাতন-গোস্বামি-প্রভুর শোচক (২)"
                    },
                    {
                      type: "song",
                      uid: "GV40@2799",
                      title: "শ্রীল-সনাতন-গোস্বামি-প্রভুর শোচক (৩)"
                    },
                    {
                      type: "heading",
                      uid: "*@2800",
                      title: ""
                    },
                    {
                      type: "song",
                      uid: "GV36@2801",
                      title: "শ্রীল-রঘুনাথদাস-গোস্বামি-প্রভুর শোচক (১)"
                    },
                    {
                      type: "song",
                      uid: "GV37@2802",
                      title: "শ্রীল-রঘুনাথদাস-গোস্বামি-প্রভুর শোচক (২)"
                    },
                    {
                      type: "heading",
                      uid: "*@2803",
                      title: ""
                    },
                    {
                      type: "song",
                      uid: "GV35@2804",
                      title: "শ্রীল-জীব-গোস্বামী-প্রভুর শোচক"
                    },
                    {
                      type: "heading",
                      uid: "*@2805",
                      title: ""
                    },
                    {
                      type: "song",
                      uid: "GV34@2806",
                      title: "শ্রীল-গোপালভট্ট-গোস্বামী-প্রভুর শোচক"
                    },
                    {
                      type: "heading",
                      uid: "*@2807",
                      title: ""
                    },
                    {
                      type: "song",
                      uid: "GV32@2808",
                      title: "শ্রীল-রঘুনাথভট্ট-গোস্বামী-প্রভুর শোচক"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2k@2809",
                  title:
                    "শ্রীল নরোত্তম ঠাকুরের শোচক ~ Lamenting in Separation from Śrīla Narottama dāsa Ṭhākura",
                  children: [
                    {
                      type: "song",
                      uid: "GV27@2810",
                      title: "শ্রীল-নরোত্তম-ঠাকুরের শোচক"
                    },
                    {
                      type: "song",
                      uid: "GV50@2811",
                      title: "জয় রে জয় রে জয় ঠাকুর শ্রীনরোত্তম"
                    },
                    {
                      type: "song",
                      uid: "GV51@2812",
                      title: "নরে নরোত্তম ধন্য"
                    }
                  ]
                }
              ]
            },
            {
              type: "collection",
              uid: "3@2813",
              title: "শরণাগতি",
              children: [
                {
                  type: "heading",
                  uid: "*@2814",
                  title: "ষড়ঙ্গ শরণাগতি ~ The Six Limbs of Surrender"
                },
                {
                  type: "song",
                  uid: "S1@2815",
                  title: "শ্রীকৃষ্ণচৈতন্য প্রভু জীবে দয়া করি’"
                },
                {
                  type: "collection",
                  uid: "3a@2816",
                  title: "১. দৈন্যাত্মিকা ~ 1. Dainyātmikā [Humility]",
                  children: [
                    {
                      type: "song",
                      uid: "S2@2817",
                      title: "ভুলিয়া তোমারে"
                    },
                    {
                      type: "song",
                      uid: "S4@2818",
                      title: "বিদ্যার বিলাসে, কাটাইনু কাল"
                    },
                    {
                      type: "song",
                      uid: "S5@2819",
                      title: "যৌবনে যখন, ধন-উপার্জ্জনে"
                    },
                    {
                      type: "song",
                      uid: "S7@2820",
                      title: "আমার জীবন, সদা পাপে রত"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3b@2821",
                  title: "২. আত্মনিবেদনাত্মিকা ~ 2. Ātma-nivedanātmikā [Submission of the Self]",
                  children: [
                    {
                      type: "song",
                      uid: "S3@2822",
                      title: "(প্রভু হে!) শুন মোর দুঃখের কাহিনী"
                    },
                    {
                      type: "song",
                      uid: "S6@2823",
                      title: "(প্রভু হে!) তুয়া পদে এ মিনতি মোর"
                    },
                    {
                      type: "song",
                      uid: "GH20@2824",
                      title: "(প্রভু হে!) এমন দুর্ম্মতি সংসার-ভিতরে"
                    },
                    {
                      type: "song",
                      uid: "S19@2825",
                      title: "না করলুঁ করম, গেয়ান নাহি ভেল"
                    },
                    {
                      type: "song",
                      uid: "S9@2826",
                      title: "(প্রাণেশ্বর!) কহবুঁ কি সরম্ কি বাত্"
                    },
                    {
                      type: "song",
                      uid: "S14@2827",
                      title: "মানস, দেহ, গেহ, যো কিছু মোর"
                    },
                    {
                      type: "song",
                      uid: "S15@2828",
                      title: "‘অহং’-‘মম’-শব্দ-অর্থে যাহা কিছু হয়"
                    },
                    {
                      type: "song",
                      uid: "S12@2829",
                      title: "‘আমার’ বলিতে প্রভু! আর কিছু নাই"
                    },
                    {
                      type: "song",
                      uid: "S11@2830",
                      title: "বস্তুতঃ সকলি তব"
                    },
                    {
                      type: "song",
                      uid: "S8@2831",
                      title: "নিবেদন করি প্রভু! তোমার চরণে"
                    },
                    {
                      type: "song",
                      uid: "S16@2832",
                      title: "আত্মনিবেদন, তুয়া পদে করি’"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3c@2833",
                  title:
                    "৩. গোপ্তৃত্বে বরণ ~ 3. Goptṛtve Varaṇa [Acceptance of Lord as One’s Only Maintainer]",
                  children: [
                    {
                      type: "song",
                      uid: "S18@2834",
                      title: "কি জানি কি বলে"
                    },
                    {
                      type: "song",
                      uid: "S13@2835",
                      title: "দারা-পুত্র-নিজ-দেহ-কুটুম্ব-পালনে"
                    },
                    {
                      type: "song",
                      uid: "S10@2836",
                      title: "সর্ব্বস্ব তোমার"
                    },
                    {
                      type: "song",
                      uid: "S17@2837",
                      title: "তুমি সর্ব্বেশ্বরেশ্বর"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3d@2838",
                  title:
                    "৪. বিশ্রম্ভাত্মিকা ~ 4. Viśrambhātmikā [Confidence in Śrī Kṛṣṇa’s Protection]",
                  children: [
                    {
                      type: "song",
                      uid: "S20@2839",
                      title: "এখন বুঝিনু প্রভু!"
                    },
                    {
                      type: "song",
                      uid: "S21@2840",
                      title: "তুমি ত’ মারিবে যা’রে, কে তা’রে রাখিতে পারে"
                    },
                    {
                      type: "song",
                      uid: "S27@2841",
                      title: "আত্মসমর্পণে গেলা অভিমান"
                    },
                    {
                      type: "song",
                      uid: "S26@2842",
                      title: "ছোড়ত পুরুষ-অভিমান"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3e@2843",
                  title: "৫. বর্জ্জনাত্মিকা ~ 5. Varjanātmikā [Rejection of the Unfavorable]",
                  children: [
                    {
                      type: "song",
                      uid: "S23@2844",
                      title: "কেশব! তুয়া জগত বিচিত্র"
                    },
                    {
                      type: "song",
                      uid: "S24@2845",
                      title: "তুয়া ভক্তি-প্রতিকূল ধর্ম্ম যা’তে রয়"
                    },
                    {
                      type: "song",
                      uid: "S25@2846",
                      title: "বিষয়বিমূঢ় আর মায়াবাদী জন"
                    },
                    {
                      type: "song",
                      uid: "S29@2847",
                      title: "আমি ত স্বানন্দ-সুখদবাসী"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3f@2848",
                  title: "৬. অনুকূল্যাত্মিকা ~ 6. Anukūlyātmika [Acceptance of the Favorable]",
                  children: [
                    {
                      type: "song",
                      uid: "S22@2849",
                      title: "তুয়া ভক্তি-অনুকূল যে-যে কার্য্য হয়"
                    },
                    {
                      type: "song",
                      uid: "S28@2850",
                      title: "গোদ্রুমধামে ভজন-অনুকূলে"
                    },
                    {
                      type: "song",
                      uid: "E1@2851",
                      title: "শুদ্ধভকত-চরণ-রেণু"
                    },
                    {
                      type: "song",
                      uid: "S30@2852",
                      title: "রাধাকুণ্ডতট"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3g@2853",
                  title: "ভজন-লালসা ~ Bhajana-lāslasā [Longing for Loving Service]",
                  children: [
                    {
                      type: "song",
                      uid: "U1@2854",
                      title: "হরি হে! প্রপঞ্চে পড়িয়া"
                    },
                    {
                      type: "song",
                      uid: "U2@2855",
                      title: "হরি হে! অর্থের সঞ্চয়ে"
                    },
                    {
                      type: "song",
                      uid: "U3@2856",
                      title: "হরি হে! ভজনে উৎসাহ"
                    },
                    {
                      type: "song",
                      uid: "U4@2857",
                      title: "হরি হে! দান-প্রতিগ্রহ"
                    },
                    {
                      type: "song",
                      uid: "U5@2858",
                      title: "হরি হে! সঙ্গদোষ-শূন্য"
                    },
                    {
                      type: "song",
                      uid: "U6@2859",
                      title: "হরি হে! নীরধর্ম্ম-গত"
                    },
                    {
                      type: "song",
                      uid: "V3@2860",
                      title: "ওহে! বৈষ্ণব ঠাকুর"
                    },
                    {
                      type: "song",
                      uid: "U7@2861",
                      title: "হরি হে! তোমারে ভুলিয়া"
                    },
                    {
                      type: "song",
                      uid: "U8@2862",
                      title: "হরি হে! শ্রীরূপগোসাঞি"
                    },
                    {
                      type: "song",
                      uid: "G7@2863",
                      title: "গুরুদেব! বড় কৃপা করি’"
                    },
                    {
                      type: "song",
                      uid: "G6@2864",
                      title: "গুরুদেব! কৃপাবিন্দু দিয়া"
                    },
                    {
                      type: "song",
                      uid: "G8@2865",
                      title: "গুরুদেব! কবে মোর সেই দিন হবে?"
                    },
                    {
                      type: "song",
                      uid: "G9@2866",
                      title: "গুরুদেব! কবে তব করুণা প্রকাশে"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3h@2867",
                  title: "সিদ্ধি-লালসা ~ Siddhi-lālasā (Hankering for Ultimate Perfection)",
                  children: [
                    {
                      type: "song",
                      uid: "D2@2868",
                      title: "কবে গৌর-বনে"
                    },
                    {
                      type: "song",
                      uid: "R25@2869",
                      title: "দেখিতে দেখিতে, ভুলিব বা কবে"
                    },
                    {
                      type: "song",
                      uid: "R21@2870",
                      title: "বৃষভানুসুতা-চরণ-সেবনে"
                    }
                  ]
                },
                {
                  type: "heading",
                  uid: "*@2871",
                  title: "বিজ্ঞপ্তি ~ Vijñapti (An Entreaty)"
                },
                {
                  type: "song",
                  uid: "GN7@2872",
                  title: "কবে হ’বে বল"
                },
                {
                  type: "heading",
                  uid: "*@2873",
                  title: "শ্রীনাম-মাহাত্ম্য ~ Śrī Nāma-māhātmya (The Glories of Śrī Nāma)"
                },
                {
                  type: "song",
                  uid: "NM13@2874",
                  title: "কৃষ্ণনাম ধরে কত বল"
                },
                {
                  type: "heading",
                  uid: "*@2875",
                  title: "শরণাগতের প্রার্থনা"
                },
                {
                  type: "song",
                  uid: "S35@2876",
                  title: "শরণাগতের প্রার্থনা"
                }
              ]
            },
            {
              type: "collection",
              uid: "4@2877",
              title: "কল্যাণকল্পতরু",
              children: [
                {
                  type: "collection",
                  uid: "4a@2878",
                  title: "উপদেশ ~ Upadeśa",
                  children: [
                    {
                      type: "song",
                      uid: "MS15@2879",
                      title: "মন রে, কেন মিছে ভজিছ অসার ?"
                    },
                    {
                      type: "song",
                      uid: "MS16@2880",
                      title: "মন, তুমি ভালবাস কামের তরঙ্গ"
                    },
                    {
                      type: "song",
                      uid: "MS17@2881",
                      title: "মন রে, তুমি বড় সন্দিগ্ধ-অন্তর"
                    },
                    {
                      type: "song",
                      uid: "MS18@2882",
                      title: "মন, তুমি বড়ই পামর"
                    },
                    {
                      type: "song",
                      uid: "MS19@2883",
                      title: "মন, তব কেন এ সংশয় ?"
                    },
                    {
                      type: "song",
                      uid: "MS20@2884",
                      title: "মন, তুমি পড়িলে কি ছার ?"
                    },
                    {
                      type: "song",
                      uid: "MS21@2885",
                      title: "মন, যোগী হ’তে তোমার বাসনা"
                    },
                    {
                      type: "song",
                      uid: "MS22@2886",
                      title: "ওহে ভাই, মন কেন ব্রহ্ম হ’তে চায়"
                    },
                    {
                      type: "song",
                      uid: "MS23@2887",
                      title: "মন রে, কেন আর বর্ণ-অভিমান"
                    },
                    {
                      type: "song",
                      uid: "MS24@2888",
                      title: "মন রে, কেন কর বিদ্যার গৌরব"
                    },
                    {
                      type: "song",
                      uid: "MS25@2889",
                      title: "রূপের গৌরব কেন ভাই ?"
                    },
                    {
                      type: "song",
                      uid: "MS26@2890",
                      title: "মন রে, ধনমদ নিতান্ত অসার"
                    },
                    {
                      type: "song",
                      uid: "MS27@2891",
                      title: "মন, তুমি সন্ন্যাসী সাজিতে কেন চাও?"
                    },
                    {
                      type: "song",
                      uid: "MS28@2892",
                      title: "মন, তুমি তীর্থে সদা রত"
                    },
                    {
                      type: "song",
                      uid: "MS29@2893",
                      title: "দেখ মন, ব্রতে যেন না হও আচ্ছন্ন"
                    },
                    {
                      type: "song",
                      uid: "MS30@2894",
                      title: "মন, তুমি বড়ই চঞ্চল"
                    },
                    {
                      type: "song",
                      uid: "MS31@2895",
                      title: "মন, তোরে বলি এ বারতা"
                    },
                    {
                      type: "song",
                      uid: "MS32@2896",
                      title: "কি আর বলিব তোরে মন"
                    },
                    {
                      type: "song",
                      uid: "MS33@2897",
                      title: "কেন মন, কামেরে নাচাও প্রেম-প্রায় ?"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "4b@2898",
                  title: "উপলব্ধি ~ Upalabdhi",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2899",
                      title: "অনুতাপ ~ Anutāpa"
                    },
                    {
                      type: "song",
                      uid: "UP1@2900",
                      title: "আমি অতি পামর দুর্জ্জন"
                    },
                    {
                      type: "song",
                      uid: "UP2@2901",
                      title: "সাধুসঙ্গ না হইল হায়!"
                    },
                    {
                      type: "song",
                      uid: "UP3@2902",
                      title: "ওরে মন, কর্ম্মের কুহরে গেল কাল"
                    },
                    {
                      type: "song",
                      uid: "UP4@2903",
                      title: "ওরে মন, কি বিপদ হইল আমার"
                    },
                    {
                      type: "song",
                      uid: "UP5@2904",
                      title: "ওরে মন, ক্লেশ-তাপ দেখি যে অশেষ!"
                    },
                    {
                      type: "heading",
                      uid: "*@2905",
                      title: "নির্ব্বেদ ~ Nirbeda"
                    },
                    {
                      type: "song",
                      uid: "UP6@2906",
                      title: "ওরে মন, ভাল নাহি লাগে এ সংসার"
                    },
                    {
                      type: "song",
                      uid: "UP7@2907",
                      title: "ওরে মন, বাড়িবার আশা কেন কর’?"
                    },
                    {
                      type: "song",
                      uid: "UP8@2908",
                      title: "ওরে মন, ভুক্তি-মুক্তি-স্পৃহা কর’ দূর"
                    },
                    {
                      type: "song",
                      uid: "UP9@2909",
                      title: "দুর্ল্লভ মানব-জন্ম লভিয়া সংসারে"
                    },
                    {
                      type: "song",
                      uid: "UP10@2910",
                      title: "শরীরের সুখে, মন, দেহ জলাঞ্জলি"
                    },
                    {
                      type: "heading",
                      uid: "*@2911",
                      title: "সম্বন্ধ-অভিদেয়-প্রয়োজন-বিজ্ঞান ~ Sambandha-abhideya-prayojana-vijñāna"
                    },
                    {
                      type: "heading",
                      uid: "*@2912",
                      title: "সম্বন্ধ-বিজ্ঞান-লক্ষণ-উপলব্ধি"
                    },
                    {
                      type: "song",
                      uid: "UP11@2913",
                      title: "ওরে মন, বলি, শুন তত্ত্ব-বিবরণ"
                    },
                    {
                      type: "heading",
                      uid: "*@2914",
                      title: "অভিধেয়-বিজ্ঞান-উপলব্ধি"
                    },
                    {
                      type: "song",
                      uid: "UP12@2915",
                      title: "ভ্রমিতে ভ্রমিতে যদি সাধুসঙ্গ হয়"
                    },
                    {
                      type: "heading",
                      uid: "*@2916",
                      title: "প্রয়োজন-বিজ্ঞান-উপলব্ধি"
                    },
                    {
                      type: "song",
                      uid: "UP13@2917",
                      title: "অপূর্ব্ব বৈষ্ণব-তত্ত্ব! আত্মার আনন্দ-"
                    },
                    {
                      type: "song",
                      uid: "UP14@2918",
                      title: "চিজ্জড়ের দ্বৈত যিনি করেন স্থাপন"
                    },
                    {
                      type: "song",
                      uid: "UP15@2919",
                      title: "‘জীবন-সমাপ্তি-কালে করিব ভজন"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "4c@2920",
                  title: "উচ্ছ্বাস ~ 3 Ucchvāsa",
                  children: [
                    {
                      type: "heading",
                      uid: "*@2921",
                      title: "প্রার্থনা দৈন্যময়ী ~ Prārthanā Dainyamayī"
                    },
                    {
                      type: "song",
                      uid: "GH47@2922",
                      title: "কবে শ্রীচৈতন্য মোরে করিবেন দয়া"
                    },
                    {
                      type: "song",
                      uid: "GH48@2923",
                      title: "আমি ত’ দুর্জ্জন"
                    },
                    {
                      type: "song",
                      uid: "P3@2924",
                      title: "ভবার্ণবে প’ড়ে মোর আকুল পরাণ"
                    },
                    {
                      type: "song",
                      uid: "GV46@2925",
                      title: "বিষয়-বাসনারূপ চিত্তের বিকার"
                    },
                    {
                      type: "song",
                      uid: "P4@2926",
                      title: "আমার সমান হীন নাহি এ সংসারে"
                    },
                    {
                      type: "heading",
                      uid: "*@2927",
                      title: "প্রার্থনা লালসাময়ী ~ Prārthanā Lālasāmayī"
                    },
                    {
                      type: "song",
                      uid: "G12@2928",
                      title: "কবে মোর শুভদিন হইবে উদয়"
                    },
                    {
                      type: "song",
                      uid: "V13@2929",
                      title: "শ্রীগুরু-বৈষ্ণব-কৃপা কত দিনে হ’বে"
                    },
                    {
                      type: "song",
                      uid: "V14@2930",
                      title: "আমার এমন ভাগ্য কত দিনে হ’বে"
                    },
                    {
                      type: "song",
                      uid: "GH21@2931",
                      title: "চৈতন্য-চন্দ্রের লীলা-সমুদ্র অপার"
                    },
                    {
                      type: "song",
                      uid: "V6@2932",
                      title: "হরি হরি কবে মোর হবে হেন দিন"
                    },
                    {
                      type: "song",
                      uid: "V5@2933",
                      title: "কবে মুই বৈষ্ণব চিনিব"
                    },
                    {
                      type: "song",
                      uid: "V4@2934",
                      title: "কৃপা কর বৈষ্ণব ঠাকুর !"
                    },
                    {
                      type: "song",
                      uid: "GN10@2935",
                      title: "কবে হ’বে হেন দশা মোর"
                    },
                    {
                      type: "song",
                      uid: "GH50@2936",
                      title: "হা হা মোর গৌর-কিশোর"
                    },
                    {
                      type: "song",
                      uid: "GN11@2937",
                      title: "হা হা কবে গৌর-নিতাই"
                    },
                    {
                      type: "song",
                      uid: "GH51@2938",
                      title: "কবে আহা গৌরাঙ্গ বলিয়া"
                    },
                    {
                      type: "heading",
                      uid: "*@2939",
                      title: "বিজ্ঞপ্তি ~ Vijñapti"
                    },
                    {
                      type: "song",
                      uid: "K49@2940",
                      title: "গোপীনাথ, মম নিবেদন শুন"
                    },
                    {
                      type: "song",
                      uid: "K50@2941",
                      title: "গোপীনাথ, ঘুচাও সংসার-জ্বালা"
                    },
                    {
                      type: "song",
                      uid: "K51@2942",
                      title: "গোপীনাথ, আমার উপায় নাই"
                    },
                    {
                      type: "song",
                      uid: "RK11@2943",
                      title: "শ্রীরাধাকৃষ্ণ-পদকমলে মন"
                    },
                    {
                      type: "heading",
                      uid: "*@2944",
                      title: "উচ্ছ্বাস কীর্ত্তন ~ Ucchvāsa Kīrtana"
                    },
                    {
                      type: "heading",
                      uid: "*@2945",
                      title: "নাম-কীর্ত্তন ~ Nāma-kīrtana"
                    },
                    {
                      type: "song",
                      uid: "NK5@2946",
                      title: "কলিকুক্কুর-কদন যদি চাও (হে)"
                    },
                    {
                      type: "song",
                      uid: "A7@2947",
                      title: "বিভাবরী-শেষ"
                    },
                    {
                      type: "heading",
                      uid: "*@2948",
                      title: "রূপ-কীর্ত্তন ~ Rūpa-kīrtana"
                    },
                    {
                      type: "song",
                      uid: "K41@2949",
                      title: "জনম সফল তা’র, কৃষ্ণ দরশন যা’র"
                    },
                    {
                      type: "heading",
                      uid: "*@2950",
                      title: "গুণ-কীর্ত্তন ~ Guṇa-kīrtana"
                    },
                    {
                      type: "song",
                      uid: "K43@2951",
                      title: "বহির্ম্মুখ হ’য়ে, মায়ারে ভজিয়ে"
                    },
                    {
                      type: "song",
                      uid: "K40@2952",
                      title: "শুন হে রসিক জন"
                    },
                    {
                      type: "heading",
                      uid: "*@2953",
                      title: "লীল-কীর্ত্তন ~ Līlā-kīrtana"
                    },
                    {
                      type: "song",
                      uid: "K44@2954",
                      title: "জীবে কৃপা করি’, গোলোকের হরি"
                    },
                    {
                      type: "song",
                      uid: "K45@2955",
                      title: "যমুনা-পুলিনে"
                    },
                    {
                      type: "heading",
                      uid: "*@2956",
                      title: "রস-কীর্ত্তন্ ~ Rasa-kīrtan"
                    },
                    {
                      type: "song",
                      uid: "K70@2957",
                      title: "কৃষ্ণবংশীগীত শুনি’"
                    }
                  ]
                }
              ]
            },
            {
              type: "collection",
              uid: "5@2958",
              title: "গীতাবলী",
              children: [
                {
                  type: "collection",
                  uid: "5a@2959",
                  title: "অরুণোদয় কীর্ত্তন ~ Aruṇodaya-kīrtana",
                  children: [
                    {
                      type: "song",
                      uid: "NM11@2960",
                      title: "উদিল অরুণ পূরব ভাগে"
                    },
                    {
                      type: "song",
                      uid: "NM12@2961",
                      title: "জীব জাগ, জীব জাগ"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "5b@2962",
                  title: "আরতি ~ Ārati",
                  children: [
                    {
                      type: "song",
                      uid: "A3@2963",
                      title: "ভালে গোরা-গদাধরের আরতি নেহারি"
                    },
                    {
                      type: "song",
                      uid: "A10@2964",
                      title: "শ্রীগৌর-আরতি"
                    },
                    {
                      type: "song",
                      uid: "A11@2965",
                      title: "শ্রীযুগল-আরতি"
                    },
                    {
                      type: "song",
                      uid: "A9@2966",
                      title: "ভজ ভকত-বৎসল শ্রীগৌরহরি"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "5c@2967",
                  title: "প্রসাদ-সেবায় ~ Prasāda-sevāya",
                  children: [
                    {
                      type: "song",
                      uid: "PR1@2968",
                      title: "ভাইরে! শরীর অবিদ্যা-জাল"
                    },
                    {
                      type: "song",
                      uid: "PR2@2969",
                      title: "ভাইরে! একদিন শান্তিপুরে"
                    },
                    {
                      type: "song",
                      uid: "PR3@2970",
                      title: "ভাইরে! শচীর অঙ্গনে কভু"
                    },
                    {
                      type: "song",
                      uid: "PR4@2971",
                      title: "ভাইরে! শ্রীচৈতন্য-নিত্যানন্দ"
                    },
                    {
                      type: "song",
                      uid: "PR5@2972",
                      title: "ভাইরে! একদিন নীলাচলে"
                    },
                    {
                      type: "song",
                      uid: "PR6@2973",
                      title: "ভাইরে! রামকৃষ্ণ গোচারণে"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "5d@2974",
                  title: "শ্রীনগর-কীর্ত্তন ~ Śrī Nagara-kīrtana",
                  children: [
                    {
                      type: "song",
                      uid: "NK27@2975",
                      title: "গায় গোরা মধুর স্বরে"
                    },
                    {
                      type: "song",
                      uid: "NK28@2976",
                      title: "একবার ভাব মনে"
                    },
                    {
                      type: "song",
                      uid: "NK29@2977",
                      title: "‘রাধাকৃষ্ণ’ বল্ বল্"
                    },
                    {
                      type: "song",
                      uid: "NK30@2978",
                      title: "গায় গোরাচাঁদ জীবের তরে"
                    },
                    {
                      type: "song",
                      uid: "NK31@2979",
                      title: "অঙ্গ-উপাঙ্গ-অস্ত্র-পার্ষদ-সঙ্গে"
                    },
                    {
                      type: "song",
                      uid: "NK32@2980",
                      title: "নিতাই কি নাম এনেছে রে"
                    },
                    {
                      type: "song",
                      uid: "NK33@2981",
                      title: "হরি ব’লে মোদের গৌর এলো"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "5e@2982",
                  title: "শ্রীনাম-কীর্ত্তন ~ Śrī Nāma-kīrtana",
                  children: [
                    {
                      type: "song",
                      uid: "A8@2983",
                      title: "যশোমতী-নন্দন"
                    },
                    {
                      type: "song",
                      uid: "NK6@2984",
                      title: "‘দয়াল নিতাই চৈতন্য’ ব’লে নাচ্ রে আমার মন"
                    },
                    {
                      type: "song",
                      uid: "NK7@2985",
                      title: "‘হরি’ বল, ‘হরি’ বল, ‘হরি’ বল, ভাই রে"
                    },
                    {
                      type: "song",
                      uid: "NK8@2986",
                      title: "বোল হরি বোল (৩ বার)"
                    },
                    {
                      type: "song",
                      uid: "NK35@2987",
                      title: "হরি হরয়ে নমঃ কৃষ্ণ যাদবায় নমঃ"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "5f@2988",
                  title: "শ্রেয়নির্ণয় ~ Śreya-nirṇaya",
                  children: [
                    {
                      type: "song",
                      uid: "S31@2989",
                      title: "কৃষ্ণভক্তি বিনা কভু নাহি ফলোদয়"
                    },
                    {
                      type: "song",
                      uid: "S32@2990",
                      title: "আর কেন মায়াজালে পড়িতেছ জীব মীন"
                    },
                    {
                      type: "song",
                      uid: "S33@2991",
                      title: "পীরিতি সচ্চিদানন্দে রূপবতী নারী"
                    },
                    {
                      type: "song",
                      uid: "S34@2992",
                      title: "‘নিরাকার নিরাকার’ করিয়া চীৎকার"
                    },
                    {
                      type: "song",
                      uid: "S36@2993",
                      title: "কেন আর কর দ্বেষ, বিদেশি-জন-ভজনে"
                    },
                    {
                      type: "song",
                      uid: "GP1@2994",
                      title: "ভজ রে ভজ রে আমার মন"
                    },
                    {
                      type: "song",
                      uid: "MS34@2995",
                      title: "ভাব না ভাব না, মন, তুমি অতি দুষ্ট"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "5g@2996",
                  title: "শ্রীনামাষ্টক ~ Śrī Nāmāṣṭaka",
                  children: [
                    {
                      type: "song",
                      uid: "NM1@2997",
                      title: "শ্রীরূপ-বদনে"
                    },
                    {
                      type: "song",
                      uid: "NM2@2998",
                      title: "জয় জয় হরিনাম"
                    },
                    {
                      type: "song",
                      uid: "NM3@2999",
                      title: "বিশ্বে উদিত নাম-তপন"
                    },
                    {
                      type: "song",
                      uid: "NM4@3000",
                      title: "জ্ঞানী জ্ঞানযোগে করিয়া যতনে"
                    },
                    {
                      type: "song",
                      uid: "NM5@3001",
                      title: "হরিনাম তুয়া অনেক স্বরূপ"
                    },
                    {
                      type: "song",
                      uid: "NM6@3002",
                      title: "বাচ্য বাচক—এই দুই স্বরূপ তোমার"
                    },
                    {
                      type: "song",
                      uid: "NM7@3003",
                      title: "ওহে হরিনাম তব মহিমা অপার"
                    },
                    {
                      type: "song",
                      uid: "NM8@3004",
                      title: "নারদ মুনি বাজায় বীণা"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "5h@3005",
                  title: "শ্রীরাধাষ্টক ~ Śrī Rādhāṣṭaka",
                  children: [
                    {
                      type: "song",
                      uid: "R13@3006",
                      title: "রাধিকাচরণ-পদ্ম"
                    },
                    {
                      type: "song",
                      uid: "R14@3007",
                      title: "বিরজার পারে, শুদ্ধপরব্যোম-ধাম"
                    },
                    {
                      type: "song",
                      uid: "R15@3008",
                      title: "রমণী-শিরোমণি, বৃষভানু-নন্দিনী"
                    },
                    {
                      type: "song",
                      uid: "R16@3009",
                      title: "রসিক-নাগরী-, গণ-শিরোমণি"
                    },
                    {
                      type: "song",
                      uid: "R17@3010",
                      title: "মহাভাব-চিন্তামণি, উদ্ভাবিত তনুখানি"
                    },
                    {
                      type: "song",
                      uid: "R18@3011",
                      title: "বরজ-বিপিনে"
                    },
                    {
                      type: "song",
                      uid: "R19@3012",
                      title: "শতকোটী গোপী"
                    },
                    {
                      type: "song",
                      uid: "R20@3013",
                      title: "রাধা-ভজনে যদি মতি নাহি ভেলা"
                    },
                    {
                      type: "heading",
                      uid: "*@3014",
                      title: "পরিশিষ্ট ~ Pariśiṣṭa"
                    },
                    {
                      type: "song",
                      uid: "NM14@3015",
                      title: "ভোজন-লালসে, রসনে আমার"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "5i@3016",
                  title: "শ্রীকৃষ্ণের বিংশোত্তরশতনাম ~ Śrī Kṛṣṇera Viṁśottara-śata-nāma",
                  children: [
                    {
                      type: "song",
                      uid: "NK22@3017",
                      title: "শ্রীকৃষ্ণের বিংশোত্তর-শতনাম"
                    },
                    {
                      type: "song",
                      uid: "NK15@3018",
                      title: "কৃষ্ণ গোবিন্দ হরে"
                    },
                    {
                      type: "song",
                      uid: "NK16@3019",
                      title: "রাধাবল্লভ মাধব শ্রীপতি মুকুন্দ"
                    },
                    {
                      type: "song",
                      uid: "NK17@3020",
                      title: "জয় রাধা-মাধব কুঞ্জবিহারী"
                    },
                    {
                      type: "song",
                      uid: "NK18@3021",
                      title: "রাধাবল্লভ রাধাবিনোদ"
                    },
                    {
                      type: "song",
                      uid: "NK19@3022",
                      title: "জয় যশোদা-নন্দন কৃষ্ণ"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "5j@3023",
                  title: "শ্রীশিক্ষাষ্টক ~ Śrī Śikṣāṣṭaka",
                  children: [
                    {
                      type: "song",
                      uid: "SI1@3024",
                      title: "পীতবরণ কলি-পাবন গোরা"
                    },
                    {
                      type: "song",
                      uid: "SI2@3025",
                      title: "তুহুঁ দয়া-সাগর"
                    },
                    {
                      type: "song",
                      uid: "SI3@3026",
                      title: "শ্রীকৃষ্ণকীর্ত্তনে যদি মানস তোঁহার"
                    },
                    {
                      type: "song",
                      uid: "SI4@3027",
                      title: "প্রভু! তব পদযুগে মোর নিবেদন"
                    },
                    {
                      type: "song",
                      uid: "SI5@3028",
                      title: "অনাদি করম-ফলে"
                    },
                    {
                      type: "song",
                      uid: "SI6@3029",
                      title: "অপরাধ-ফলে মম"
                    },
                    {
                      type: "song",
                      uid: "SI7@3030",
                      title: "গাইতে গাইতে নাম"
                    },
                    {
                      type: "song",
                      uid: "SI8@3031",
                      title: "গাইতে গোবিন্দ নাম"
                    },
                    {
                      type: "song",
                      uid: "SI9@3032",
                      title: "বন্ধুগণ! শুনহ বচন মোর"
                    },
                    {
                      type: "song",
                      uid: "SI10@3033",
                      title: "যোগপীঠোপরি স্থিত"
                    }
                  ]
                }
              ]
            },
            {
              type: "collection",
              uid: "6@3034",
              title: "গীতমালা",
              children: [
                {
                  type: "collection",
                  uid: "6a@3035",
                  title:
                    "যামুন-ভাবাবলী (শান্ত-দাস্য-ভক্তিসাধন-লালসা) ~ The Ecstasies of Yamunācārya [Yearning to Practice Śānta- and Dāsya-bhakti]",
                  children: [
                    {
                      type: "song",
                      uid: "Y1@3036",
                      title: "হরি হে! ওহে প্রভু দয়াময়"
                    },
                    {
                      type: "song",
                      uid: "Y2@3037",
                      title: "হরি হে! তোমার ঈক্ষণে হয়"
                    },
                    {
                      type: "song",
                      uid: "Y3@3038",
                      title: "হরি হে! পরতত্ত্ব বিচক্ষণ"
                    },
                    {
                      type: "song",
                      uid: "Y4@3039",
                      title: "হরি হে! জগতের বস্তু যত"
                    },
                    {
                      type: "song",
                      uid: "Y5@3040",
                      title: "হরি হে! তুমি সর্ব্বগুণযুত"
                    },
                    {
                      type: "song",
                      uid: "Y6@3041",
                      title: "হরি হে! তোমার গম্ভীর মন"
                    },
                    {
                      type: "song",
                      uid: "Y7@3042",
                      title: "হরি হে! মায়াবদ্ধ যতক্ষণ"
                    },
                    {
                      type: "song",
                      uid: "Y8@3043",
                      title: "হরি হে! ধর্ম্মনিষ্ঠা নাহি মোর"
                    },
                    {
                      type: "song",
                      uid: "Y9@3044",
                      title: "হরি হে! হেন দুষ্ট কর্ম্ম নাই"
                    },
                    {
                      type: "song",
                      uid: "Y10@3045",
                      title: "হরি হে! নিজকর্ম্ম-দোষ-ফলে"
                    },
                    {
                      type: "song",
                      uid: "Y11@3046",
                      title: "হরি হে! অন্য আশা নাহি যা’র"
                    },
                    {
                      type: "song",
                      uid: "Y12@3047",
                      title: "হরি হে! তব পদ-পঙ্কজিনী"
                    },
                    {
                      type: "song",
                      uid: "Y13@3048",
                      title: "হরি হে! ভ্রমিতে সংসার-বনে"
                    },
                    {
                      type: "song",
                      uid: "Y14@3049",
                      title: "হরি হে! তোমার চরণপদ্ম"
                    },
                    {
                      type: "song",
                      uid: "Y15@3050",
                      title: "হরি হে! তবাঙ্ঘ্রি কমলদ্বয়"
                    },
                    {
                      type: "song",
                      uid: "Y16@3051",
                      title: "হরি হে! আমি সেই দুষ্টমতি"
                    },
                    {
                      type: "song",
                      uid: "Y17@3052",
                      title: "হরি হে! আমি অপরাধি-জন"
                    },
                    {
                      type: "song",
                      uid: "Y18@3053",
                      title: "হরি হে! অবিবেকরূপ ঘন"
                    },
                    {
                      type: "song",
                      uid: "Y19@3054",
                      title: "হরি হে! অগ্রে এক নিবেদন"
                    },
                    {
                      type: "song",
                      uid: "Y20@3055",
                      title: "হরি হে! তোমা ছাড়ি’ আমি কভু"
                    },
                    {
                      type: "song",
                      uid: "Y21@3056",
                      title: "হরি হে! স্ত্রী-পুরুষ-দেহগত"
                    },
                    {
                      type: "song",
                      uid: "Y22@3057",
                      title: "হরি হে! বেদবিধি-অনুসারে"
                    },
                    {
                      type: "song",
                      uid: "Y23@3058",
                      title: "হরি হে! তোমার যে শুদ্ধভক্ত"
                    },
                    {
                      type: "song",
                      uid: "Y24@3059",
                      title: "হরি হে! শুন হে মধুমথন!"
                    },
                    {
                      type: "song",
                      uid: "Y25@3060",
                      title: "হরি হে! আমি নরপশুপ্রায়"
                    },
                    {
                      type: "song",
                      uid: "Y26@3061",
                      title: "হরি হে! তুমি জগতের পিতা"
                    },
                    {
                      type: "song",
                      uid: "Y27@3062",
                      title: "হরি হে! আমি ত’ চঞ্চলমতি"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "6b@3063",
                  title: "কার্পণ্য পঞ্জিকা",
                  children: [
                    {
                      type: "song",
                      uid: "RK30@3064",
                      title: "কার্পণ্য পঞ্জিকা"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "6c@3065",
                  title: "শোক-শাতন ~ The Vanquisher of Bereavement",
                  children: [
                    {
                      type: "song",
                      uid: "GH64@3066",
                      title: "প্রদোষ-সময়ে, শ্রীবাস-অঙ্গনে"
                    },
                    {
                      type: "song",
                      uid: "GH65@3067",
                      title: "প্রবেশিয়া অন্তঃপুরে, নারীগণে শান্ত করে"
                    },
                    {
                      type: "song",
                      uid: "GH66@3068",
                      title: "ধন, জন, দেহ, গেহ কৃষ্ণে সমর্পণ"
                    },
                    {
                      type: "song",
                      uid: "GH67@3069",
                      title: "সবু মেলি’ বালক-ভাগ বিচারি’"
                    },
                    {
                      type: "song",
                      uid: "GH68@3070",
                      title: "শ্রীবাস-বচন, শ্রবণ করিয়া"
                    },
                    {
                      type: "song",
                      uid: "GH69@3071",
                      title: "প্রভুর বচন, শুনিয়া তখন"
                    },
                    {
                      type: "song",
                      uid: "GH70@3072",
                      title: "গোরাচাঁদের আজ্ঞা পে’য়ে, গৃহবাসিগণ"
                    },
                    {
                      type: "song",
                      uid: "GH71@3073",
                      title: "“পূর্ণচিদানন্দ তুমি, তোমার চিৎকণ আমি"
                    },
                    {
                      type: "song",
                      uid: "GH72@3074",
                      title: "“বাঁধিল মায়া যেদিন হ’তে"
                    },
                    {
                      type: "song",
                      uid: "GH73@3075",
                      title: "শ্রীবাসে কহেন প্রভু—“তুহুঁ মোর দাস"
                    },
                    {
                      type: "song",
                      uid: "GH74@3076",
                      title: "শ্রীবাসের প্রতি চৈতন্য-প্রসাদ"
                    },
                    {
                      type: "song",
                      uid: "GH75@3077",
                      title: "মৃত শিশু ল’য়ে তবে ভকতবৎসল"
                    },
                    {
                      type: "song",
                      uid: "GH76@3078",
                      title: "सुन्दर लाला शचीर-दुलाला"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "6d@3079",
                  title: "শ্রীশ্রীরূপানুগ ভজন-দর্পণ",
                  children: [
                    {
                      type: "song",
                      uid: "RBD1@3080",
                      title: "শ্রীশ্রীরূপানুগ ভজন-দর্পণ ভূমিকা"
                    },
                    {
                      type: "song",
                      uid: "RBD2@3081",
                      title: "শ্রদ্ধাই ভক্তিলতার বীজ"
                    },
                    {
                      type: "song",
                      uid: "RBD3@3082",
                      title: "কৃষ্ণতক্তি"
                    },
                    {
                      type: "song",
                      uid: "RBD4@3083",
                      title: "শ্রদ্ধা দ্বিবিধা, অতএব সাধনভক্তিও দ্বিবিধা"
                    },
                    {
                      type: "song",
                      uid: "RBD5@3084",
                      title: "প্রাকৃতাপ্রাকৃত রসতত্ত্ব-জ্ঞানের আবশ্যকতা"
                    },
                    {
                      type: "song",
                      uid: "RBD6@3085",
                      title: "স্থায়িভাবই রসের মূল"
                    },
                    {
                      type: "song",
                      uid: "RBD7@3086",
                      title: "মধুর রসই সর্ব্বশ্রেষ্ঠ রস"
                    },
                    {
                      type: "song",
                      uid: "RBD8@3087",
                      title: "মধুরা রতির আবির্ভাব-হেতু"
                    },
                    {
                      type: "song",
                      uid: "RBD9@3088",
                      title: "মধুর-রতিরূপ স্থায়িভাবের উন্নতিক্রম"
                    },
                    {
                      type: "song",
                      uid: "RBD10@3089",
                      title: "বিভাব"
                    },
                    {
                      type: "song",
                      uid: "RBD11@3090",
                      title: "মধুর-রসে আলম্বনরূপ বিভাব"
                    },
                    {
                      type: "song",
                      uid: "RBD12@3091",
                      title: "নায়ক-শিরোমণি শ্রীকৃষ্ণের গুণ"
                    },
                    {
                      type: "song",
                      uid: "RBD13@3092",
                      title: "তদীয় বল্লভাগণ"
                    },
                    {
                      type: "song",
                      uid: "RBD14@3093",
                      title: "নায়িকাগণের অষ্ট অবস্থা-সেবা"
                    },
                    {
                      type: "song",
                      uid: "RBD15@3094",
                      title: "প্রধান-নায়িকা শ্রীমতী রাধিকার সখী-বর্ণন"
                    },
                    {
                      type: "song",
                      uid: "RBD16@3095",
                      title: "সখীর সাধারণ-সেবা"
                    },
                    {
                      type: "song",
                      uid: "RBD17@3096",
                      title: "নিত্যসিদ্ধা নহেন, এরূপ ‘সখী’র বিচার"
                    },
                    {
                      type: "song",
                      uid: "RBD18@3097",
                      title: "সর্ব্বসখীর পরস্পর ভাব"
                    },
                    {
                      type: "song",
                      uid: "RBD19@3098",
                      title: "ব্রজগত-মধুর-রতির উদ্দীপন"
                    },
                    {
                      type: "song",
                      uid: "RBD20@3099",
                      title: "অনুভাব"
                    },
                    {
                      type: "song",
                      uid: "RBD21@3100",
                      title: "সাত্ত্বিকভাব"
                    },
                    {
                      type: "song",
                      uid: "RBD22@3101",
                      title: "ব্যভিচারী বা সঞ্চারী ভাব"
                    },
                    {
                      type: "song",
                      uid: "RBD23@3102",
                      title: "ভাবাবস্থা-প্রাপ্ত স্থায়ী ভাবের উত্তর দশা"
                    },
                    {
                      type: "song",
                      uid: "RBD24@3103",
                      title: "সম্ভোগ-বিপ্রলম্ভ-ভেদে উজ্জ্বল-রস দ্বিবিধ; তন্মধ্যে বিপ্রলম্ভ"
                    },
                    {
                      type: "song",
                      uid: "RBD25@3104",
                      title: "সম্ভোগ"
                    },
                    {
                      type: "song",
                      uid: "RBD26@3105",
                      title: "সম্তোগের প্রকার"
                    },
                    {
                      type: "song",
                      uid: "RBD27@3106",
                      title: "উজ্জ্বলরসাশ্রিত-লীলা"
                    },
                    {
                      type: "song",
                      uid: "RBD28@3107",
                      title: "ব্রজলীলার সর্ব্বশ্রেষ্ঠতা"
                    },
                    {
                      type: "song",
                      uid: "RBD29@3108",
                      title: "অষ্টকাল বর্ণন"
                    },
                    {
                      type: "song",
                      uid: "RBD30@3109",
                      title: "অথ নিশান্ত লীলা"
                    },
                    {
                      type: "song",
                      uid: "RBD31@3110",
                      title: "অথ প্রাতলীলা"
                    },
                    {
                      type: "song",
                      uid: "RBD32@3111",
                      title: "অথ পূর্ব্বাহ্ণলীলা"
                    },
                    {
                      type: "song",
                      uid: "RBD33@3112",
                      title: "অথ মধ্যাহ্নলীলা"
                    },
                    {
                      type: "song",
                      uid: "RBD34@3113",
                      title: "অথ অপরাহ্ণলীলা"
                    },
                    {
                      type: "song",
                      uid: "RBD35@3114",
                      title: "অথ সায়ংলীলা"
                    },
                    {
                      type: "song",
                      uid: "RBD36@3115",
                      title: "অথ প্রদোষ লীলা"
                    },
                    {
                      type: "song",
                      uid: "RBD37@3116",
                      title: "অথ নক্তলীলা"
                    },
                    {
                      type: "song",
                      uid: "RBD38@3117",
                      title: "নিত্যলীলার ভেদ, পীঠ ও উপকরণ"
                    },
                    {
                      type: "song",
                      uid: "RBD39@3118",
                      title: "নিত্যলীলার ভেদ, পীঠ ও উপকরণ"
                    },
                    {
                      type: "song",
                      uid: "RBD40@3119",
                      title: "নিত্যলীলার ভেদ, পীঠ ও উপকরণ"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "6e@3120",
                  title: "সিদ্ধি-লালসা",
                  children: [
                    {
                      type: "song",
                      uid: "D2@3121",
                      title: "কবে গৌর-বনে"
                    },
                    {
                      type: "song",
                      uid: "R25@3122",
                      title: "দেখিতে দেখিতে, ভুলিব বা কবে"
                    },
                    {
                      type: "song",
                      uid: "R26@3123",
                      title: "হেন কালে কবে, বিলাস মঞ্জরী"
                    },
                    {
                      type: "song",
                      uid: "R27@3124",
                      title: "পাল্যদাসী করি’"
                    },
                    {
                      type: "song",
                      uid: "R28@3125",
                      title: "চিন্তামণিময়"
                    },
                    {
                      type: "song",
                      uid: "R29@3126",
                      title: "নির্জ্জন কুটীরে, শ্রীরাধা-চরণ-"
                    },
                    {
                      type: "song",
                      uid: "R30@3127",
                      title: "শ্রীরূপ মঞ্জরী কবে মধুর বচনে"
                    },
                    {
                      type: "song",
                      uid: "R31@3128",
                      title: "বরণে তড়িৎ, বাস তারাবলী"
                    },
                    {
                      type: "song",
                      uid: "R21@3129",
                      title: "বৃষভানুসুতা-চরণ-সেবনে"
                    },
                    {
                      type: "song",
                      uid: "R22@3130",
                      title: "শ্রীকৃষ্ণবিরহে রাধিকার দশা"
                    }
                  ]
                }
              ]
            },
            {
              type: "collection",
              uid: "7@3131",
              title: "বিবিধ",
              children: [
                {
                  type: "collection",
                  uid: "7a@3132",
                  title: "সংস্কৃত-স্তব",
                  children: [
                    {
                      type: "song",
                      uid: "GH12@3133",
                      title: "শ্রীগোদ্রুম ভজনোপদেষ"
                    },
                    {
                      type: "song",
                      uid: "GH1@3134",
                      title: "শ্রীশচীতনয়াষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "GH4@3135",
                      title: "শ্রীচৈতন্যাষ্টকম্ (২)"
                    },
                    {
                      type: "song",
                      uid: "GH5@3136",
                      title: "শ্রীচৈতন্যাষ্টকম্ (৩)"
                    },
                    {
                      type: "song",
                      uid: "GH6@3137",
                      title: "শ্রীশচীসূন্ৱষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "GH14@3138",
                      title: "ৱন্দে ৱিশ্ৱম্ভর-পদ-কমলম্"
                    },
                    {
                      type: "song",
                      uid: "GH15@3139",
                      title: "সখে কলয় গৌরমুদারম্"
                    },
                    {
                      type: "song",
                      uid: "VT6@3140",
                      title: "শ্রীজগন্নাথাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "R34@3141",
                      title: "শ্রীশ্রীরাধাষ্টক"
                    },
                    {
                      type: "song",
                      uid: "R2@3142",
                      title: "শ্রীরাধিকাষ্টকম্ (২)"
                    },
                    {
                      type: "song",
                      uid: "D4@3143",
                      title: "শ্রীরাধাকুণ্ডাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "D8@3144",
                      title: "শ্রীগোৱর্ধন-ৱাস-প্রার্থনা-দশকম্"
                    },
                    {
                      type: "song",
                      uid: "K29@3145",
                      title: "শ্রীমঙ্গল-গীতম্"
                    },
                    {
                      type: "song",
                      uid: "K10@3146",
                      title: "শ্রীকুঞ্জৱিহার্য্যাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "K2@3147",
                      title: "শ্রীৱ্রজরাজসুতাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "K4@3148",
                      title: "শ্রীশ্রীমধুরাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "K24@3149",
                      title: "শ্রীচৌরাগ্রগণ্য-পুরুষাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "D13@3150",
                      title: "শ্রীগঙ্গা-স্তোত্রম্"
                    },
                    {
                      type: "song",
                      uid: "S38@3151",
                      title: "গুরৌ মন্ত্রে নাম্নি প্রভুবর-শচীগর্ভজ-পদে"
                    },
                    {
                      type: "song",
                      uid: "PT8@3152",
                      title: "শ্রীশ্রীঅদ্ৱৈতাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "VT7@3153",
                      title: "নমস্তে নরসিংহায়"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "7b@3154",
                  title: "বাংলা কীর্তন",
                  children: [
                    {
                      type: "song",
                      uid: "E4@3155",
                      title: "শ্রীহরি-বাসরে হরি-কীর্ত্তন বিধান"
                    },
                    {
                      type: "song",
                      uid: "N9@3156",
                      title: "অক্রোধ পরমানন্দ"
                    },
                    {
                      type: "song",
                      uid: "N22@3157",
                      title: "দয়া কর মোরে নিতাই"
                    },
                    {
                      type: "song",
                      uid: "N8@3158",
                      title: "নিতাই মোর জীবন-ধন"
                    },
                    {
                      type: "song",
                      uid: "GN14@3159",
                      title: "নাচেরে নাচেরে নিতাই গৌর দ্বিজমণিয়া"
                    },
                    {
                      type: "song",
                      uid: "N15@3160",
                      title: "নিতাই আমার পরম দয়াল"
                    },
                    {
                      type: "song",
                      uid: "GH13@3161",
                      title: "পশ্য শচী-সুতমনুপম-রূপম্"
                    },
                    {
                      type: "song",
                      uid: "N11@3162",
                      title: "আরে ভাই! নিতাই আমার দয়ার অবধি"
                    },
                    {
                      type: "song",
                      uid: "N13@3163",
                      title: "জয় জয় নিত্যানন্দ রোহিণী-কুমার"
                    },
                    {
                      type: "song",
                      uid: "GN6@3164",
                      title: "এইবার করুণা কর চৈতন্য-নিতাই"
                    },
                    {
                      type: "song",
                      uid: "GH49@3165",
                      title: "গৌরাঙ্গ তুমি মোরে দয়া না ছাড়িহ"
                    },
                    {
                      type: "song",
                      uid: "GH45@3166",
                      title: "ওহে প্রেমের ঠাকুর গোরা"
                    },
                    {
                      type: "song",
                      uid: "GP17@3167",
                      title: "নাচে শচীনন্দন, ভকতজীবন ধন"
                    },
                    {
                      type: "song",
                      uid: "GH79@3168",
                      title: "নাচে গোরা, প্রেমে ভোরা"
                    },
                    {
                      type: "song",
                      uid: "GH80@3169",
                      title: "কি ভাব উঠিল মনে"
                    },
                    {
                      type: "song",
                      uid: "GH81@3170",
                      title: "গৌরাঙ্গ বিহরই পরম আনন্দে"
                    },
                    {
                      type: "song",
                      uid: "MS67@3171",
                      title: "এ মন! ‘হরিনাম’ কর সার"
                    },
                    {
                      type: "song",
                      uid: "MS50@3172",
                      title: "এ মন! কি লাগি’ আইলি ভবে"
                    },
                    {
                      type: "song",
                      uid: "GH35@3173",
                      title: "এমন গৌরাঙ্গ বিনা নাহি আর!"
                    },
                    {
                      type: "song",
                      uid: "GH53@3174",
                      title: "মন রে! কহনা গৌর-কথা"
                    },
                    {
                      type: "song",
                      uid: "GH23@3175",
                      title: "জয় জয় জগন্নাথ শচীর নন্দন"
                    },
                    {
                      type: "heading",
                      uid: "*@3176",
                      title: "শ্রীগৌরহরির জন্মলীলা"
                    },
                    {
                      type: "song",
                      uid: "GH58@3177",
                      title: "নদীয়া-উদয়গিরি"
                    },
                    {
                      type: "heading",
                      uid: "*@3178",
                      title: "***"
                    },
                    {
                      type: "song",
                      uid: "K54@3179",
                      title: "ব্রজেন্দ্রনন্দন, ভজে যেই জন"
                    },
                    {
                      type: "song",
                      uid: "MS35@3180",
                      title: "ভজ ভজ হরি, মন দৃঢ় করি’"
                    },
                    {
                      type: "song",
                      uid: "GH27@3181",
                      title: "বিমল-হেম-জিনি’, তনু অনুপম রে!"
                    },
                    {
                      type: "song",
                      uid: "K58@3182",
                      title: "তাতল সৈকতে, বারিবিন্দু-সম"
                    },
                    {
                      type: "song",
                      uid: "K67@3183",
                      title: "হরি ব’লব আর মদনমোহন হেরিব গো"
                    },
                    {
                      type: "song",
                      uid: "GN8@3184",
                      title: "এ ঘোর-সংসারে"
                    },
                    {
                      type: "song",
                      uid: "A15@3185",
                      title: "নমো নমঃ তুলসী কৃষ্ণ-প্রেয়সী"
                    },
                    {
                      type: "song",
                      uid: "NK3@3186",
                      title: "জয় জয় রাধে কৃষ্ণ গোবিন্দ"
                    },
                    {
                      type: "song",
                      uid: "NK4@3187",
                      title: "জয় জয় রাধা-মাধব রাধা-মাধব রাধে"
                    },
                    {
                      type: "heading",
                      uid: "*@3188",
                      title: "শ্রীঅদ্বৈত বন্দনা"
                    },
                    {
                      type: "song",
                      uid: "PT13@3189",
                      title: "জয় জয় অদ্বৈত আচার্য্য দয়াময়"
                    },
                    {
                      type: "heading",
                      uid: "*@3190",
                      title: "***"
                    },
                    {
                      type: "heading",
                      uid: "*@3191",
                      title: "শ্রীনিত্যানন্দ জন্মলীলা"
                    },
                    {
                      type: "song",
                      uid: "N21@3192",
                      title: "রাঢ়দেশ নাম, একচক্রা-গ্রাম"
                    },
                    {
                      type: "heading",
                      uid: "*@3193",
                      title: "***"
                    },
                    {
                      type: "song",
                      uid: "N22@3194",
                      title: "দয়া কর মোরে নিতাই"
                    },
                    {
                      type: "song",
                      uid: "N10@3195",
                      title: "দেখ নিতাইচাঁদের মাধুরী"
                    },
                    {
                      type: "heading",
                      uid: "*@3196",
                      title: "শ্রীনিত্যানন্দ-অভিষেক"
                    },
                    {
                      type: "song",
                      uid: "N23@3197",
                      title: "শ্রীনিত্যানন্দ-অভিষেক"
                    },
                    {
                      type: "heading",
                      uid: "*@3198",
                      title: "***"
                    },
                    {
                      type: "heading",
                      uid: "*@3199",
                      title: "শ্রীনিত্যানন্দ-গুণবর্ণন"
                    },
                    {
                      type: "song",
                      uid: "N24@3200",
                      title: "চলে নিতাই প্রেমভরে"
                    },
                    {
                      type: "heading",
                      uid: "*@3201",
                      title: "***"
                    },
                    {
                      type: "song",
                      uid: "N25@3202",
                      title: "বন্দো প্রভু নিত্যানন্দ"
                    },
                    {
                      type: "song",
                      uid: "N26@3203",
                      title: "গোরাপ্রেমে গরগর নিতাই আমার"
                    },
                    {
                      type: "song",
                      uid: "N27@3204",
                      title: "প্রভু নিত্যানন্দ, আনন্দের কন্দ"
                    },
                    {
                      type: "heading",
                      uid: "*@3205",
                      title: "শ্রীগৌরাঙ্গের-অভিষেক"
                    },
                    {
                      type: "song",
                      uid: "GH82@3206",
                      title: "জয় জয় ধ্বনি উঠে নদীয়া নগরে"
                    },
                    {
                      type: "heading",
                      uid: "*@3207",
                      title: "শ্রীগৌরাঙ্গের নৃত্যাদি লীলা"
                    },
                    {
                      type: "song",
                      uid: "GH83@3208",
                      title: "গোরা নাচে নব রঙ্গিয়া"
                    },
                    {
                      type: "song",
                      uid: "GH84@3209",
                      title: "গোরা নাচে শচীর দুলালিয়া"
                    },
                    {
                      type: "heading",
                      uid: "*@3210",
                      title: "***"
                    },
                    {
                      type: "song",
                      uid: "GH38@3211",
                      title: "কে যাবি কে যাবি ভাই"
                    },
                    {
                      type: "heading",
                      uid: "*@3212",
                      title: "শ্রীগৌরাঙ্গের সন্ন্যাস"
                    },
                    {
                      type: "song",
                      uid: "GH85@3213",
                      title: "শয়ন-মন্দিরে, গৌরাঙ্গে সুন্দর"
                    },
                    {
                      type: "heading",
                      uid: "*@3214",
                      title: "***"
                    },
                    {
                      type: "song",
                      uid: "GH93@3215",
                      title: "চিরদিনে গোরচাঁদের আনন্দ অপার"
                    },
                    {
                      type: "song",
                      uid: "GN17@3216",
                      title: "নবদ্বীপে শুনি সিংহনাদ"
                    },
                    {
                      type: "song",
                      uid: "GH34@3217",
                      title: "শচীসুত গৌরহরি, নবদ্বীপে অবতরি"
                    },
                    {
                      type: "song",
                      uid: "GH86@3218",
                      title: "শচীর আঙ্গিনায় নাচে বিশ্বম্ভর রায়"
                    },
                    {
                      type: "song",
                      uid: "K74@3219",
                      title: "বদ বদ হরি, ছদ্ম না করিহ"
                    },
                    {
                      type: "song",
                      uid: "GN16@3220",
                      title: "জীবের ভাগ্যে অবনী বিহরে দুই ভাই"
                    },
                    {
                      type: "song",
                      uid: "V11@3221",
                      title: "শ্রীকৃষ্ণ-ভজন লাগি’ সংসারে আইনু"
                    },
                    {
                      type: "song",
                      uid: "RK37@3222",
                      title: "বৃষভানু নন্দিনী, নব অনুরাগিনী"
                    },
                    {
                      type: "song",
                      uid: "RK38@3223",
                      title: "নব ঘন কানন শোহন কুঞ্জ"
                    },
                    {
                      type: "heading",
                      uid: "*@3224",
                      title: "নৌকাবিহার"
                    },
                    {
                      type: "song",
                      uid: "GH87@3225",
                      title: "না জানিয়ে গোরাচাঁদের কোন ভাব মনে"
                    },
                    {
                      type: "heading",
                      uid: "*@3226",
                      title: "***"
                    },
                    {
                      type: "song",
                      uid: "GN2@3227",
                      title: "শ্রীকৃষ্ণচৈতন্য নিত্যানন্দ দুই প্রভু"
                    },
                    {
                      type: "song",
                      uid: "GH88@3228",
                      title: "যাঁর নাম শ্রবণে সংসার-বন্ধ ঘুচে"
                    },
                    {
                      type: "song",
                      uid: "GH89@3229",
                      title: "নদীয়ার অতি, পুণ্যবতী পতি-"
                    },
                    {
                      type: "song",
                      uid: "R13@3230",
                      title: "রাধিকাচরণ-পদ্ম"
                    },
                    {
                      type: "song",
                      uid: "GH90@3231",
                      title: "এস মম হৃদি মন্দিরে শ্রীগৌরহরি"
                    },
                    {
                      type: "song",
                      uid: "GP18@3232",
                      title: "জয় জয় মহাপ্রভু জয় গৌরচন্দ্র"
                    },
                    {
                      type: "song",
                      uid: "GH36@3233",
                      title: "(যদি) গৌরাঙ্গ নহিত"
                    },
                    {
                      type: "song",
                      uid: "GH30@3234",
                      title: "কলিযুগে শ্রীচৈতন্য অবনী করিলা ধন্য"
                    },
                    {
                      type: "song",
                      uid: "GH91@3235",
                      title: "কলিযুগে শ্রীচৈতন্য, অবনী করিলা ধন্য"
                    },
                    {
                      type: "song",
                      uid: "GH92@3236",
                      title: "ভজ রাধা কৃষ্ণ, গোপাল কৃষ্ণ"
                    },
                    {
                      type: "song",
                      uid: "K75@3237",
                      title: "পরম দয়াল হে রাধেশ্যাম!"
                    },
                    {
                      type: "song",
                      uid: "K76@3238",
                      title: "নন্দদুলাল ঘনশ্যাম"
                    },
                    {
                      type: "song",
                      uid: "K35@3239",
                      title: "জয় জয় সুন্দর নন্দ-কুমার"
                    },
                    {
                      type: "song",
                      uid: "K77@3240",
                      title: "দেব দেব দেব কৃষ্ণ"
                    },
                    {
                      type: "song",
                      uid: "K78@3241",
                      title: "দেহি পদম্‌ (শঙ্খচক্রগদাধর)"
                    },
                    {
                      type: "song",
                      uid: "R24@3242",
                      title: "কোথায় গো প্রেমময়ি"
                    },
                    {
                      type: "song",
                      uid: "K32@3243",
                      title: "ৱসতু মনো মম মদনগোপালে"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "7c@3244",
                  title: "হিন্দী কীর্ত্তন",
                  children: [
                    {
                      type: "song",
                      uid: "GH76@3245",
                      title: "सुन्दर लाला शचीर-दुलाला"
                    }
                  ]
                }
              ]
            }
          ],
          count: 500
        },
        {
          type: "collection",
          uid: "MJG@3246",
          title:
            "শ্রীমহাজন-গীতাবলী [শ্রীচৈতন্য গৌড়ীয় মঠ] ~ Śrī Mahājana Gītāvalī [Śrī Caitanya Gauḍīya Maṭha]",
          children: [
            {
              type: "heading",
              uid: "*@3247",
              title: "১ম সংস্করণ ইং ১৯৬৯ ~ published 1969"
            },
            {
              type: "song",
              uid: "M7@3248",
              title: "মঙ্গলাচরণ (শ্রীচৈতন্য গৌড়ীয় মঠ)"
            },
            {
              type: "collection",
              uid: "1@3249",
              title: "শ্রীগুরু এবং গুরুবর্গ ~ Śrī Guru and Guru-varga",
              children: [
                {
                  type: "collection",
                  uid: "1a@3250",
                  title: "গুরুবর্গ ~ Guru-varga",
                  children: [
                    {
                      type: "song",
                      uid: "GV6@3251",
                      title: "শ্রীগুরু-পরম্পরা—সংস্কৃত (শ্রীচৈতন্য গৌড়ীয় মঠ)"
                    },
                    {
                      type: "song",
                      uid: "GV7@3252",
                      title: "শ্রীগুরু-পরম্পরা—বাংলা (শ্রীচৈতন্য গৌড়ীয় মঠ)"
                    },
                    {
                      type: "song",
                      uid: "G1@3253",
                      title: "শ্রীগুর্ৱ্ৱষ্টকম্ (সংস্কৃত) ~ Śrī Gurvāṣṭakam (Sanskrit)"
                    },
                    {
                      type: "song",
                      uid: "GV13@3254",
                      title: "শ্রীল মাধৱ-গোস্ৱামী-মহারাজ-পাদপদ্মেস্তৱকৈকাদশম্‌"
                    },
                    {
                      type: "song",
                      uid: "GV17@3255",
                      title: "শ্রীপ্রভুপাদ-পদ্ম-স্তৱকঃ"
                    },
                    {
                      type: "song",
                      uid: "G2@3256",
                      title: "শ্রীগুর্ব্বষ্টকম্ (বাংলা) ~ Śrī Gurvāṣṭakam (Bengali Rendition)"
                    },
                    {
                      type: "heading",
                      uid: "*@3257",
                      title: "শ্রীল প্রভুপাদ-বন্দনা ~ Prayers to Śrīla Sarasvatī Ṭhākura Prabhupāda"
                    },
                    {
                      type: "song",
                      uid: "GV19@3258",
                      title: "শ্রীমদ্ গৌর-কিশোরাষ্টকম্‌"
                    },
                    {
                      type: "song",
                      uid: "GV21@3259",
                      title: "শ্রীমদ্ ভক্তিৱিনোদ দশকম্"
                    },
                    {
                      type: "song",
                      uid: "GV24@3260",
                      title: "শ্রীল জগন্নাথাষ্টকম্"
                    },
                    {
                      type: "song",
                      uid: "GV31@3261",
                      title: "শ্রীষড়্‌গোস্ৱাম্যাষ্টকম্"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "1b@3262",
                  title: "শ্রীষড়্‌গোস্বামি-শোচক ~ Lamenting in Separation from the Six Gosvāmīs",
                  children: [
                    {
                      type: "heading",
                      uid: "*@3263",
                      title:
                        "শ্রীমদ্রূপ-গোস্বামি-প্রভুর শোচক ~ Lamenting in Separation from Śrīla Rūpa Gosvāmī"
                    },
                    {
                      type: "song",
                      uid: "GV41@3264",
                      title: "শ্রীমদ্‌রূপ-গোস্বামী-প্রভুর শোচক (১)"
                    },
                    {
                      type: "song",
                      uid: "GV42@3265",
                      title: "শ্রীমদ্‌রূপ-গোস্বামী-প্রভুর শোচক (২)"
                    },
                    {
                      type: "song",
                      uid: "GV43@3266",
                      title: "যঙ্ কলি রূপ শরীর ন ধরত"
                    },
                    {
                      type: "song",
                      uid: "GV44@3267",
                      title: "শ্রীরূপমঞ্জরী-পদ"
                    },
                    {
                      type: "heading",
                      uid: "*@3268",
                      title:
                        "শ্রীল-সনাতন-গোস্বামি-প্রভুর শোচক ~ Lamenting in Separation from Śrīla Sanātana Gosvāmī"
                    },
                    {
                      type: "song",
                      uid: "GV38@3269",
                      title: "শ্রীল-সনাতন-গোস্বামি-প্রভুর শোচক (১)"
                    },
                    {
                      type: "song",
                      uid: "GV39@3270",
                      title: "শ্রীল-সনাতন-গোস্বামি-প্রভুর শোচক (২)"
                    },
                    {
                      type: "song",
                      uid: "GV40@3271",
                      title: "শ্রীল-সনাতন-গোস্বামি-প্রভুর শোচক (৩)"
                    },
                    {
                      type: "heading",
                      uid: "*@3272",
                      title:
                        "শ্রীল-রঘুনাথদাস-গোস্বামি-প্রভুর শোচক ~ Lamenting in Separation from Śrīla Raghunātha dāsa Gosvāmī"
                    },
                    {
                      type: "song",
                      uid: "GV36@3273",
                      title: "শ্রীল-রঘুনাথদাস-গোস্বামি-প্রভুর শোচক (১)"
                    },
                    {
                      type: "song",
                      uid: "GV37@3274",
                      title: "শ্রীল-রঘুনাথদাস-গোস্বামি-প্রভুর শোচক (২)"
                    },
                    {
                      type: "heading",
                      uid: "*@3275",
                      title:
                        "শ্রীল-জীব-গোস্বামি-প্রভুর শোচক ~ Lamenting in Separation from Śrīla Jīva Gosvāmī"
                    },
                    {
                      type: "song",
                      uid: "GV35@3276",
                      title: "শ্রীল-জীব-গোস্বামী-প্রভুর শোচক"
                    },
                    {
                      type: "heading",
                      uid: "*@3277",
                      title:
                        "শ্রীল-গোপালভট্ট-গোস্বামি-প্রভুর শোচক ~ Lamenting in Separation from Śrīla Gopāla Bhaṭṭa Gosvāmī"
                    },
                    {
                      type: "song",
                      uid: "GV34@3278",
                      title: "শ্রীল-গোপালভট্ট-গোস্বামী-প্রভুর শোচক"
                    },
                    {
                      type: "heading",
                      uid: "*@3279",
                      title:
                        "শ্রীল-রঘুনাথভট্ট-গোস্বামি-প্রভুর শোচক ~ Lamenting in Separation from Śrīla Raghunātha Bhaṭṭa Gosvāmī"
                    },
                    {
                      type: "song",
                      uid: "GV32@3280",
                      title: "শ্রীল-রঘুনাথভট্ট-গোস্বামী-প্রভুর শোচক"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "1c@3281",
                  title: "শ্রীগুরু",
                  children: [
                    {
                      type: "song",
                      uid: "G3@3282",
                      title: "শ্রীগুরু-চরণ-পদ্ম"
                    },
                    {
                      type: "song",
                      uid: "G6@3283",
                      title: "গুরুদেব! কৃপাবিন্দু দিয়া"
                    },
                    {
                      type: "song",
                      uid: "G7@3284",
                      title: "গুরুদেব! বড় কৃপা করি’"
                    },
                    {
                      type: "song",
                      uid: "G8@3285",
                      title: "গুরুদেব! কবে মোর সেই দিন হবে?"
                    },
                    {
                      type: "song",
                      uid: "G9@3286",
                      title: "গুরুদেব! কবে তব করুণা প্রকাশে"
                    },
                    {
                      type: "song",
                      uid: "MS3@3287",
                      title: "গুরুদেবে, ব্রজবনে, ব্রজভূমিবাসী জনে"
                    },
                    {
                      type: "heading",
                      uid: "*@3288",
                      title: "শ্রীমদ্‌রূপ-গোস্বামী"
                    },
                    {
                      type: "song",
                      uid: "GV44@3289",
                      title: "শ্রীরূপমঞ্জরী-পদ"
                    },
                    {
                      type: "song",
                      uid: "GV45@3290",
                      title: "শুনিয়াছি সাধুমুখে"
                    },
                    {
                      type: "song",
                      uid: "RK18@3291",
                      title: "এই নব-দাসী বলি শ্রীরূপ চাহিবে"
                    },
                    {
                      type: "song",
                      uid: "RK19@3292",
                      title: "শ্রীরূপ-পশ্চাতে আমি রহিব ভীত হঞা"
                    }
                  ]
                }
              ]
            },
            {
              type: "collection",
              uid: "2@3293",
              title: "শ্রীবৈষ্ণব ~ Śrī Vaiṣṇava",
              children: [
                {
                  type: "song",
                  uid: "MS40@3294",
                  title: "দুষ্ট মন, তুমি কিসের বৈষ্ণব ?"
                },
                {
                  type: "heading",
                  uid: "*@3295",
                  title: "শ্রীবৈষ্ণব-বন্দনা ~ Prayers of Adoration"
                },
                {
                  type: "song",
                  uid: "V1@3296",
                  title: "শ্রীবৈষ্ণব-বন্দনা"
                },
                {
                  type: "song",
                  uid: "GP4@3297",
                  title: "প্রাণ গোরাচাঁদ মোর"
                },
                {
                  type: "song",
                  uid: "GP5@3298",
                  title: "ধন্য অবতার গোরা ন্যাসি-শিরোমণি"
                },
                {
                  type: "song",
                  uid: "GP6@3299",
                  title: "ভাল অবতার শ্রীগৌরাঙ্গ অবতার"
                },
                {
                  type: "song",
                  uid: "GP9@3300",
                  title: "শ্রীকৃষ্ণচৈতন্য প্রভু দয়া কর মোরে"
                },
                {
                  type: "heading",
                  uid: "*@3301",
                  title: "শ্রীবৈষ্ণব-কৃপা-প্রার্থনা ~ Prayers for Mercy"
                },
                {
                  type: "song",
                  uid: "U6@3302",
                  title: "হরি হে! নীরধর্ম্ম-গত"
                },
                {
                  type: "song",
                  uid: "V3@3303",
                  title: "ওহে! বৈষ্ণব ঠাকুর"
                },
                {
                  type: "song",
                  uid: "V6@3304",
                  title: "হরি হরি কবে মোর হবে হেন দিন"
                },
                {
                  type: "song",
                  uid: "V5@3305",
                  title: "কবে মুই বৈষ্ণব চিনিব"
                },
                {
                  type: "song",
                  uid: "GV46@3306",
                  title: "বিষয়-বাসনারূপ চিত্তের বিকার"
                },
                {
                  type: "song",
                  uid: "P4@3307",
                  title: "আমার সমান হীন নাহি এ সংসারে"
                },
                {
                  type: "song",
                  uid: "G12@3308",
                  title: "কবে মোর শুভদিন হইবে উদয়"
                },
                {
                  type: "song",
                  uid: "V13@3309",
                  title: "শ্রীগুরু-বৈষ্ণব-কৃপা কত দিনে হ’বে"
                },
                {
                  type: "song",
                  uid: "V14@3310",
                  title: "আমার এমন ভাগ্য কত দিনে হ’বে"
                },
                {
                  type: "song",
                  uid: "V4@3311",
                  title: "কৃপা কর বৈষ্ণব ঠাকুর !"
                },
                {
                  type: "song",
                  uid: "V2@3312",
                  title: "ঠাকুর বৈষ্ণব-পদ"
                },
                {
                  type: "song",
                  uid: "V7@3313",
                  title: "ঠাকুর বৈষ্ণবগণ"
                },
                {
                  type: "song",
                  uid: "V8@3314",
                  title: "এইবার করুণা কর"
                },
                {
                  type: "song",
                  uid: "V9@3315",
                  title: "কিরূপে পাইব সেবা"
                },
                {
                  type: "song",
                  uid: "GV47@3316",
                  title: "যে আনিল প্রেমধন"
                },
                {
                  type: "song",
                  uid: "GP16@3317",
                  title: "হরি হরি! কি মোর করম অনুরত"
                },
                {
                  type: "song",
                  uid: "UP2@3318",
                  title: "সাধুসঙ্গ না হইল হায়!"
                },
                {
                  type: "song",
                  uid: "Y23@3319",
                  title: "হরি হে! তোমার যে শুদ্ধভক্ত"
                },
                {
                  type: "song",
                  uid: "E1@3320",
                  title: "শুদ্ধভকত-চরণ-রেণু"
                }
              ]
            },
            {
              type: "collection",
              uid: "3@3321",
              title: "শ্রীমন্নিত্যানন্দ ~ Śrīman Nityānanda",
              children: [
                {
                  type: "song",
                  uid: "N21@3322",
                  title: "রাঢ়দেশ নাম, একচক্রা-গ্রাম"
                },
                {
                  type: "song",
                  uid: "N8@3323",
                  title: "নিতাই মোর জীবন-ধন"
                },
                {
                  type: "song",
                  uid: "N10@3324",
                  title: "দেখ নিতাইচাঁদের মাধুরী"
                },
                {
                  type: "song",
                  uid: "N11@3325",
                  title: "আরে ভাই! নিতাই আমার দয়ার অবধি"
                },
                {
                  type: "song",
                  uid: "N13@3326",
                  title: "জয় জয় নিত্যানন্দ রোহিণী-কুমার"
                },
                {
                  type: "song",
                  uid: "N16@3327",
                  title: "গজেন্দ্রগমনে নিতাই"
                },
                {
                  type: "song",
                  uid: "N18@3328",
                  title: "প্রেমে মত্ত নিত্যানন্দ, সহজে আনন্দ কন্দ"
                },
                {
                  type: "song",
                  uid: "N14@3329",
                  title: "নিতাই গুণমণি আমার"
                },
                {
                  type: "song",
                  uid: "N9@3330",
                  title: "অক্রোধ পরমানন্দ"
                },
                {
                  type: "song",
                  uid: "GN12@3331",
                  title: "ধন মোর নিত্যানন্দ পতি মোর গৌরচন্দ্র"
                },
                {
                  type: "song",
                  uid: "N7@3332",
                  title: "নিতাই-পদ-কমল"
                },
                {
                  type: "song",
                  uid: "NK32@3333",
                  title: "নিতাই কি নাম এনেছে রে"
                },
                {
                  type: "song",
                  uid: "NK25@3334",
                  title: "নদীয়া-গোদ্রুমে নিত্যানন্দ মহাজন"
                },
                {
                  type: "song",
                  uid: "NK6@3335",
                  title: "‘দয়াল নিতাই চৈতন্য’ ব’লে নাচ্ রে আমার মন"
                },
                {
                  type: "song",
                  uid: "NK7@3336",
                  title: "‘হরি’ বল, ‘হরি’ বল, ‘হরি’ বল, ভাই রে"
                }
              ]
            },
            {
              type: "collection",
              uid: "4@3337",
              title: "শ্রীগৌরাঙ্গ ~ Śrī Gaurāṅga",
              children: [
                {
                  type: "song",
                  uid: "GH3@3338",
                  title: "শ্রীচৈতন্যাষ্টকম্ (১)"
                },
                {
                  type: "song",
                  uid: "GH1@3339",
                  title: "শ্রীশচীতনয়াষ্টকম্"
                },
                {
                  type: "heading",
                  uid: "*@3340",
                  title: "শ্রীগৌর-জন্মলীলা ~ Śrī Gauracandra’s Appearance Pastime"
                },
                {
                  type: "song",
                  uid: "GH58@3341",
                  title: "নদীয়া-উদয়গিরি"
                },
                {
                  type: "song",
                  uid: "GH59@3342",
                  title: "রাহু-কবলে ইন্দু"
                },
                {
                  type: "song",
                  uid: "GH60@3343",
                  title: "জিনিয়া রবিকর"
                },
                {
                  type: "song",
                  uid: "GH61@3344",
                  title: "প্রকাশ হইলা গৌরচন্দ্র"
                },
                {
                  type: "heading",
                  uid: "*@3345",
                  title: "শ্রীগৌর-মহিমা ~ The Glories of Śrī Gaurahari"
                },
                {
                  type: "song",
                  uid: "GH27@3346",
                  title: "বিমল-হেম-জিনি’, তনু অনুপম রে!"
                },
                {
                  type: "song",
                  uid: "GH32@3347",
                  title: "জয় জগন্নাথ-শচীনন্দন গৌরাঙ্গ পহুঁ"
                },
                {
                  type: "song",
                  uid: "GH23@3348",
                  title: "জয় জয় জগন্নাথ শচীর নন্দন"
                },
                {
                  type: "song",
                  uid: "GH34@3349",
                  title: "শচীসুত গৌরহরি, নবদ্বীপে অবতরি"
                },
                {
                  type: "song",
                  uid: "GH38@3350",
                  title: "কে যাবি কে যাবি ভাই"
                },
                {
                  type: "song",
                  uid: "GH47@3351",
                  title: "কবে শ্রীচৈতন্য মোরে করিবেন দয়া"
                },
                {
                  type: "song",
                  uid: "S1@3352",
                  title: "শ্রীকৃষ্ণচৈতন্য প্রভু জীবে দয়া করি’"
                },
                {
                  type: "song",
                  uid: "GH48@3353",
                  title: "আমি ত’ দুর্জ্জন"
                },
                {
                  type: "song",
                  uid: "GN10@3354",
                  title: "কবে হ’বে হেন দশা মোর"
                },
                {
                  type: "song",
                  uid: "GH51@3355",
                  title: "কবে আহা গৌরাঙ্গ বলিয়া"
                },
                {
                  type: "song",
                  uid: "NK21@3356",
                  title: "শ্রীমন্মহাপ্রভুর শতনাম"
                },
                {
                  type: "song",
                  uid: "NK11@3357",
                  title: "জয় গোদ্রুমপতি গোরা"
                },
                {
                  type: "song",
                  uid: "NK12@3358",
                  title: "কলিযুগপাবন বিশ্বম্ভর"
                },
                {
                  type: "song",
                  uid: "NK13@3359",
                  title: "কৃষ্ণচৈতন্য অদ্বৈত প্রভু নিত্যানন্দ"
                },
                {
                  type: "song",
                  uid: "D2@3360",
                  title: "কবে গৌর-বনে"
                },
                {
                  type: "song",
                  uid: "GN9@3361",
                  title: "‘গৌরাঙ্গ’ বলিতে হবে"
                },
                {
                  type: "song",
                  uid: "NK20@3362",
                  title: "জয় জয় শ্রীকৃষ্ণচৈতন্য নিত্যানন্দ"
                },
                {
                  type: "song",
                  uid: "GH52@3363",
                  title: "আরে ভাই! ভজ মোর গৌরাঙ্গ-চরণ"
                },
                {
                  type: "song",
                  uid: "GH40@3364",
                  title: "গৌরাঙ্গের দু’টী পদ"
                },
                {
                  type: "song",
                  uid: "GP9@3365",
                  title: "শ্রীকৃষ্ণচৈতন্য প্রভু দয়া কর মোরে"
                },
                {
                  type: "song",
                  uid: "GH56@3366",
                  title: "গোরা পঁহু না ভজিয়া মৈনু"
                },
                {
                  type: "song",
                  uid: "GH39@3367",
                  title: "অবতার সার"
                },
                {
                  type: "song",
                  uid: "GH36@3368",
                  title: "(যদি) গৌরাঙ্গ নহিত"
                },
                {
                  type: "song",
                  uid: "GN3@3369",
                  title: "পরম করুণ, পঁহু দুইজন"
                },
                {
                  type: "song",
                  uid: "NK30@3370",
                  title: "গায় গোরাচাঁদ জীবের তরে"
                },
                {
                  type: "song",
                  uid: "NK31@3371",
                  title: "অঙ্গ-উপাঙ্গ-অস্ত্র-পার্ষদ-সঙ্গে"
                },
                {
                  type: "song",
                  uid: "NK33@3372",
                  title: "হরি ব’লে মোদের গৌর এলো"
                },
                {
                  type: "song",
                  uid: "NK27@3373",
                  title: "গায় গোরা মধুর স্বরে"
                },
                {
                  type: "song",
                  uid: "GH78@3374",
                  title: "সুন্দর সুন্দর গৌরাঙ্গ সুন্দর সুন্দর সুন্দর রূপ"
                },
                {
                  type: "song",
                  uid: "GH45@3375",
                  title: "ওহে প্রেমের ঠাকুর গোরা"
                },
                {
                  type: "song",
                  uid: "GN15@3376",
                  title: "ভজ ভাই! চৈতন্য নিত্যানন্দ"
                },
                {
                  type: "song",
                  uid: "V17@3377",
                  title: "অপার করুণাসিদ্ধু বৈষ্ণব ঠাকুর"
                },
                {
                  type: "song",
                  uid: "GN6@3378",
                  title: "এইবার করুণা কর চৈতন্য-নিতাই"
                },
                {
                  type: "song",
                  uid: "GN13@3379",
                  title: "নিতাই-গৌর-নাম"
                },
                {
                  type: "song",
                  uid: "GN11@3380",
                  title: "হা হা কবে গৌর-নিতাই"
                },
                {
                  type: "song",
                  uid: "GH20@3381",
                  title: "(প্রভু হে!) এমন দুর্ম্মতি সংসার-ভিতরে"
                },
                {
                  type: "song",
                  uid: "GH54@3382",
                  title: "ভাইরে! ভজ গোরাচাঁদের চরণ"
                },
                {
                  type: "song",
                  uid: "GH55@3383",
                  title: "কলিঘোর তিমিরে"
                },
                {
                  type: "song",
                  uid: "GN1@3384",
                  title: "শ্রীকৃষ্ণচৈতন্য, বলরাম নিত্যানন্দ"
                },
                {
                  type: "song",
                  uid: "GN5@3385",
                  title: "জীবের ভাগ্যে অবনী বিহরে দোন ভাই"
                },
                {
                  type: "song",
                  uid: "GN4@3386",
                  title: "নিতাই চৈতন্য দোঁহে বড় অবতার"
                }
              ]
            },
            {
              type: "collection",
              uid: "5@3387",
              title: "শ্রীরাধা এবং শ্রীশ্রীরাধাকৃষ্ণ ~ Śrī Rādhā and Śrī Śrī Rādhā-Kṛṣṇa",
              children: [
                {
                  type: "song",
                  uid: "R1@3388",
                  title: "শ্রীরাধিকাষ্টকম্ (১)"
                },
                {
                  type: "song",
                  uid: "R2@3389",
                  title: "শ্রীরাধিকাষ্টকম্ (২)"
                },
                {
                  type: "song",
                  uid: "R3@3390",
                  title: "শ্রীরাধিকাষ্টকম্ (৩)"
                },
                {
                  type: "song",
                  uid: "R8@3391",
                  title: "রাধে জয় জয় মাধৱ-দয়িতে"
                },
                {
                  type: "song",
                  uid: "RK11@3392",
                  title: "শ্রীরাধাকৃষ্ণ-পদকমলে মন"
                },
                {
                  type: "song",
                  uid: "R13@3393",
                  title: "রাধিকাচরণ-পদ্ম"
                },
                {
                  type: "song",
                  uid: "R14@3394",
                  title: "বিরজার পারে, শুদ্ধপরব্যোম-ধাম"
                },
                {
                  type: "song",
                  uid: "R15@3395",
                  title: "রমণী-শিরোমণি, বৃষভানু-নন্দিনী"
                },
                {
                  type: "song",
                  uid: "R16@3396",
                  title: "রসিক-নাগরী-, গণ-শিরোমণি"
                },
                {
                  type: "song",
                  uid: "R17@3397",
                  title: "মহাভাব-চিন্তামণি, উদ্ভাবিত তনুখানি"
                },
                {
                  type: "song",
                  uid: "R18@3398",
                  title: "বরজ-বিপিনে"
                },
                {
                  type: "song",
                  uid: "R19@3399",
                  title: "শতকোটী গোপী"
                },
                {
                  type: "song",
                  uid: "R20@3400",
                  title: "রাধা-ভজনে যদি মতি নাহি ভেলা"
                },
                {
                  type: "song",
                  uid: "NM14@3401",
                  title: "ভোজন-লালসে, রসনে আমার"
                },
                {
                  type: "song",
                  uid: "R25@3402",
                  title: "দেখিতে দেখিতে, ভুলিব বা কবে"
                },
                {
                  type: "song",
                  uid: "RK29@3403",
                  title: "হরি হরি! আর কি এমন দশা হব, (কবে বৃষভানুপুরে...)"
                },
                {
                  type: "song",
                  uid: "R26@3404",
                  title: "হেন কালে কবে, বিলাস মঞ্জরী"
                },
                {
                  type: "song",
                  uid: "R27@3405",
                  title: "পাল্যদাসী করি’"
                },
                {
                  type: "song",
                  uid: "R28@3406",
                  title: "চিন্তামণিময়"
                },
                {
                  type: "song",
                  uid: "R29@3407",
                  title: "নির্জ্জন কুটীরে, শ্রীরাধা-চরণ-"
                },
                {
                  type: "song",
                  uid: "R30@3408",
                  title: "শ্রীরূপ মঞ্জরী কবে মধুর বচনে"
                },
                {
                  type: "song",
                  uid: "R31@3409",
                  title: "বরণে তড়িৎ, বাস তারাবলী"
                },
                {
                  type: "song",
                  uid: "R21@3410",
                  title: "বৃষভানুসুতা-চরণ-সেবনে"
                },
                {
                  type: "song",
                  uid: "R22@3411",
                  title: "শ্রীকৃষ্ণবিরহে রাধিকার দশা"
                },
                {
                  type: "song",
                  uid: "RK12@3412",
                  title: "রাধাকৃষ্ণ! নিবেদন এই জন করে"
                },
                {
                  type: "song",
                  uid: "GP15@3413",
                  title: "হরি হরি! কি মোর করমগতি মন্দ"
                },
                {
                  type: "song",
                  uid: "GP13@3414",
                  title: "হরি হরি! বিফলে জনম গোঙাইনু"
                },
                {
                  type: "song",
                  uid: "K55@3415",
                  title: "প্রাণেশ্বর! নিবেদন এইজন করে"
                },
                {
                  type: "song",
                  uid: "RK25@3416",
                  title: "রাধাকৃষ্ণ সেবোঁ মুঞি জীবনে মরণে"
                },
                {
                  type: "song",
                  uid: "RK9@3417",
                  title: "রাধাকৃষ্ণ প্রাণ মোর"
                },
                {
                  type: "song",
                  uid: "R32@3418",
                  title: "প্রাণেশ্বরি! এইবার করুণা কর মোরে"
                },
                {
                  type: "song",
                  uid: "R22@3419",
                  title: "শ্রীকৃষ্ণবিরহে রাধিকার দশা"
                },
                {
                  type: "song",
                  uid: "R33@3420",
                  title: "অরুণ-কমল-দলে"
                },
                {
                  type: "song",
                  uid: "R24@3421",
                  title: "কোথায় গো প্রেমময়ি"
                },
                {
                  type: "song",
                  uid: "S30@3422",
                  title: "রাধাকুণ্ডতট"
                },
                {
                  type: "song",
                  uid: "D4@3423",
                  title: "শ্রীরাধাকুণ্ডাষ্টকম্"
                },
                {
                  type: "song",
                  uid: "RK30@3424",
                  title: "কার্পণ্য পঞ্জিকা"
                },
                {
                  type: "song",
                  uid: "K57@3425",
                  title: "হরি হরি! কি মোর করম অভাগ"
                },
                {
                  type: "song",
                  uid: "RK13@3426",
                  title: "হরি হরি! হেন দিন হইবে আমার"
                },
                {
                  type: "song",
                  uid: "RK14@3427",
                  title: "হরি হরি! কবে মোর হইবে সুদিনে"
                },
                {
                  type: "song",
                  uid: "RK15@3428",
                  title: "গোবর্ধন গিরিবর, কেবল নির্জ্জন স্থল"
                },
                {
                  type: "song",
                  uid: "D20@3429",
                  title: "আর কি এমন দশা হব"
                },
                {
                  type: "song",
                  uid: "RK16@3430",
                  title: "কুসুমিত বৃন্দাবনে, নাচত শিখিগণে"
                },
                {
                  type: "song",
                  uid: "RK17@3431",
                  title: "হরি হরি! কবে মোর হইবে সুদিন"
                },
                {
                  type: "song",
                  uid: "GV29@3432",
                  title: "হা হা প্রভু লোকনাথ! রাখ পদদ্বন্দ্বে"
                },
                {
                  type: "song",
                  uid: "GV30@3433",
                  title: "লোকনাথ প্রভু, তুমি দয়া কর মোরে"
                },
                {
                  type: "song",
                  uid: "RK20@3434",
                  title: "হা হা প্রভু! কর দয়া করুণা তোমার"
                },
                {
                  type: "song",
                  uid: "RK21@3435",
                  title: "হরি হরি! কবে হেন দশা হবে মোর"
                },
                {
                  type: "song",
                  uid: "RK10@3436",
                  title: "হরি হরি, কবে মোর হইবে সুদিন"
                },
                {
                  type: "song",
                  uid: "RK22@3437",
                  title: "বৃন্দাবন রম্য-স্থান, দিব্য-চিন্তামণি-ধাম"
                },
                {
                  type: "song",
                  uid: "RK23@3438",
                  title: "কদম্ব তরুর ডাল, নামিয়াছে ভূমে ভাল"
                },
                {
                  type: "song",
                  uid: "RK24@3439",
                  title: "মৃগমদ-চন্দন"
                },
                {
                  type: "song",
                  uid: "NK4@3440",
                  title: "জয় জয় রাধা-মাধব রাধা-মাধব রাধে"
                },
                {
                  type: "song",
                  uid: "NK29@3441",
                  title: "‘রাধাকৃষ্ণ’ বল্ বল্"
                }
              ]
            },
            {
              type: "collection",
              uid: "6@3442",
              title: "শ্রীকৃষ্ণ ~ Śrī Kṛṣṇa and Śrī Nāma",
              children: [
                {
                  type: "song",
                  uid: "K61@3443",
                  title: "হরি হে দয়াল মোর জয় রাধানাথ"
                },
                {
                  type: "song",
                  uid: "K17@3444",
                  title: "শ্রীকৃষ্ণচন্দ্রাষ্টকম্"
                },
                {
                  type: "song",
                  uid: "D5@3445",
                  title: "শ্রীশ্যামকুণ্ডাষ্টকম্"
                },
                {
                  type: "song",
                  uid: "K49@3446",
                  title: "গোপীনাথ, মম নিবেদন শুন"
                },
                {
                  type: "song",
                  uid: "K50@3447",
                  title: "গোপীনাথ, ঘুচাও সংসার-জ্বালা"
                },
                {
                  type: "song",
                  uid: "K51@3448",
                  title: "গোপীনাথ, আমার উপায় নাই"
                },
                {
                  type: "song",
                  uid: "NK5@3449",
                  title: "কলিকুক্কুর-কদন যদি চাও (হে)"
                },
                {
                  type: "song",
                  uid: "A7@3450",
                  title: "বিভাবরী-শেষ"
                },
                {
                  type: "song",
                  uid: "A8@3451",
                  title: "যশোমতী-নন্দন"
                },
                {
                  type: "song",
                  uid: "K41@3452",
                  title: "জনম সফল তা’র, কৃষ্ণ দরশন যা’র"
                },
                {
                  type: "song",
                  uid: "K43@3453",
                  title: "বহির্ম্মুখ হ’য়ে, মায়ারে ভজিয়ে"
                },
                {
                  type: "song",
                  uid: "K40@3454",
                  title: "শুন হে রসিক জন"
                },
                {
                  type: "song",
                  uid: "K44@3455",
                  title: "জীবে কৃপা করি’, গোলোকের হরি"
                },
                {
                  type: "song",
                  uid: "K45@3456",
                  title: "যমুনা-পুলিনে"
                },
                {
                  type: "song",
                  uid: "K70@3457",
                  title: "কৃষ্ণবংশীগীত শুনি’"
                },
                {
                  type: "song",
                  uid: "NK22@3458",
                  title: "শ্রীকৃষ্ণের বিংশোত্তর-শতনাম"
                },
                {
                  type: "song",
                  uid: "NK15@3459",
                  title: "কৃষ্ণ গোবিন্দ হরে"
                },
                {
                  type: "song",
                  uid: "NK16@3460",
                  title: "রাধাবল্লভ মাধব শ্রীপতি মুকুন্দ"
                },
                {
                  type: "song",
                  uid: "NK17@3461",
                  title: "জয় রাধা-মাধব কুঞ্জবিহারী"
                },
                {
                  type: "song",
                  uid: "NK18@3462",
                  title: "রাধাবল্লভ রাধাবিনোদ"
                },
                {
                  type: "song",
                  uid: "NK19@3463",
                  title: "জয় যশোদা-নন্দন কৃষ্ণ"
                },
                {
                  type: "song",
                  uid: "NK23@3464",
                  title: "শ্রীকৃষ্ণের অষ্টোত্তরশতনাম"
                },
                {
                  type: "song",
                  uid: "K28@3465",
                  title: "(কৃষ্ণ) দেৱ! ভৱন্তং ৱন্দে"
                },
                {
                  type: "song",
                  uid: "K29@3466",
                  title: "শ্রীমঙ্গল-গীতম্"
                },
                {
                  type: "song",
                  uid: "K4@3467",
                  title: "শ্রীশ্রীমধুরাষ্টকম্"
                },
                {
                  type: "song",
                  uid: "RK26@3468",
                  title: "প্রভু হে, এইবার করহ করুণা"
                },
                {
                  type: "song",
                  uid: "RK27@3469",
                  title: "আজি রসের বাদর নিশি"
                },
                {
                  type: "song",
                  uid: "NM9@3470",
                  title: "শ্রীকৃষ্ণনামাষ্টকম্"
                },
                {
                  type: "song",
                  uid: "S17@3471",
                  title: "তুমি সর্ব্বেশ্বরেশ্বর"
                },
                {
                  type: "song",
                  uid: "S20@3472",
                  title: "এখন বুঝিনু প্রভু!"
                },
                {
                  type: "song",
                  uid: "K69@3473",
                  title: "কৃষ্ণের যতেক খেলা, তার মধ্যে নরলীলা"
                },
                {
                  type: "song",
                  uid: "K65@3474",
                  title: "কবে কৃষ্ণধন পাব"
                },
                {
                  type: "song",
                  uid: "MS35@3475",
                  title: "ভজ ভজ হরি, মন দৃঢ় করি’"
                },
                {
                  type: "song",
                  uid: "K66@3476",
                  title: "এইবার পাইলে দেখা চরণ দুখানি"
                },
                {
                  type: "song",
                  uid: "S21@3477",
                  title: "তুমি ত’ মারিবে যা’রে, কে তা’রে রাখিতে পারে"
                },
                {
                  type: "song",
                  uid: "NM13@3478",
                  title: "কৃষ্ণনাম ধরে কত বল"
                },
                {
                  type: "song",
                  uid: "Y26@3479",
                  title: "হরি হে! তুমি জগতের পিতা"
                },
                {
                  type: "song",
                  uid: "MS36@3480",
                  title: "ভজহুঁ রে মন"
                },
                {
                  type: "song",
                  uid: "NK2@3481",
                  title: "জয় রাধে, জয় কৃষ্ণ, জয় বৃন্দাবন"
                },
                {
                  type: "song",
                  uid: "K42@3482",
                  title: "বন্ধুসঙ্গে"
                },
                {
                  type: "song",
                  uid: "K27@3483",
                  title: "শ্রীশ্রীমধুসূদন স্তোত্রং"
                },
                {
                  type: "song",
                  uid: "VT6@3484",
                  title: "শ্রীজগন্নাথাষ্টকম্"
                },
                {
                  type: "song",
                  uid: "K24@3485",
                  title: "শ্রীচৌরাগ্রগণ্য-পুরুষাষ্টকম্"
                },
                {
                  type: "song",
                  uid: "VT4@3486",
                  title: "শ্রীদশাৱতার-স্তোত্রম্"
                },
                {
                  type: "song",
                  uid: "K1@3487",
                  title: "শ্রীদামোদরাষ্টকম্"
                },
                {
                  type: "heading",
                  uid: "*@3488",
                  title: "Dhāma"
                },
                {
                  type: "song",
                  uid: "D8@3489",
                  title: "শ্রীগোৱর্ধন-ৱাস-প্রার্থনা-দশকম্"
                },
                {
                  type: "song",
                  uid: "D6@3490",
                  title: "শ্রীগোৱর্ধনাষ্টকম্ (১)"
                },
                {
                  type: "song",
                  uid: "P1@3491",
                  title: "শ্রীৱৃন্দাদেৱ্যষ্টকম্"
                }
              ]
            },
            {
              type: "collection",
              uid: "7@3492",
              title: "শ্রীমনঃশিক্ষা ~ Śrī Manaḥ-śikṣā",
              children: [
                {
                  type: "song",
                  uid: "MS2@3493",
                  title: "ব্রজধাম নিত্যধন, রাধাকৃষ্ণ দুইজন"
                },
                {
                  type: "song",
                  uid: "MS3@3494",
                  title: "গুরুদেবে, ব্রজবনে, ব্রজভূমিবাসী জনে"
                },
                {
                  type: "song",
                  uid: "MS4@3495",
                  title: "‘ধর্ম্ম’ বলি বেদে যারে"
                },
                {
                  type: "song",
                  uid: "MS5@3496",
                  title: "রাগাবেশে ব্রজধাম-বাসে যদি তীব্র কাম"
                },
                {
                  type: "song",
                  uid: "MS6@3497",
                  title: "কৃষ্ণবার্ত্তা বিনা আন"
                },
                {
                  type: "song",
                  uid: "MS7@3498",
                  title: "কাম-ক্রোধ-লোভ-মোহ-মদ-মৎসরতা-সহ"
                },
                {
                  type: "song",
                  uid: "MS8@3499",
                  title: "কাম-ক্রোধ-আদি করি’, বাহিরে সে-সব অরি"
                },
                {
                  type: "song",
                  uid: "MS9@3500",
                  title: "কপটতা হৈলে দূর"
                },
                {
                  type: "song",
                  uid: "MS10@3501",
                  title: "ব্রজভূমি চিন্তামণি"
                },
                {
                  type: "song",
                  uid: "MS11@3502",
                  title: "ব্রজবন-সুধাকর"
                },
                {
                  type: "song",
                  uid: "MS12@3503",
                  title: "সৌন্দর্য্য-কিরণমালা"
                },
                {
                  type: "song",
                  uid: "MS13@3504",
                  title: "ব্রজের নিকুঞ্জ-বনে"
                }
              ]
            },
            {
              type: "collection",
              uid: "8@3505",
              title: "শরণাগতি ~ Śaraṇāgati",
              children: [
                {
                  type: "heading",
                  uid: "*@3506",
                  title: "১. দৈন্যাত্মিকা ~ 1. Dainyātmikā [Humility]"
                },
                {
                  type: "song",
                  uid: "S2@3507",
                  title: "ভুলিয়া তোমারে"
                },
                {
                  type: "song",
                  uid: "S4@3508",
                  title: "বিদ্যার বিলাসে, কাটাইনু কাল"
                },
                {
                  type: "song",
                  uid: "S5@3509",
                  title: "যৌবনে যখন, ধন-উপার্জ্জনে"
                },
                {
                  type: "song",
                  uid: "S7@3510",
                  title: "আমার জীবন, সদা পাপে রত"
                },
                {
                  type: "heading",
                  uid: "*@3511",
                  title: "২. আত্মনিবেদনাত্মিকা ~ 2. Ātma-nivedanātmikā [Submission of the Self]"
                },
                {
                  type: "song",
                  uid: "S3@3512",
                  title: "(প্রভু হে!) শুন মোর দুঃখের কাহিনী"
                },
                {
                  type: "song",
                  uid: "S6@3513",
                  title: "(প্রভু হে!) তুয়া পদে এ মিনতি মোর"
                },
                {
                  type: "song",
                  uid: "S19@3514",
                  title: "না করলুঁ করম, গেয়ান নাহি ভেল"
                },
                {
                  type: "song",
                  uid: "S9@3515",
                  title: "(প্রাণেশ্বর!) কহবুঁ কি সরম্ কি বাত্"
                },
                {
                  type: "song",
                  uid: "S14@3516",
                  title: "মানস, দেহ, গেহ, যো কিছু মোর"
                },
                {
                  type: "song",
                  uid: "S15@3517",
                  title: "‘অহং’-‘মম’-শব্দ-অর্থে যাহা কিছু হয়"
                },
                {
                  type: "song",
                  uid: "S12@3518",
                  title: "‘আমার’ বলিতে প্রভু! আর কিছু নাই"
                },
                {
                  type: "song",
                  uid: "S11@3519",
                  title: "বস্তুতঃ সকলি তব"
                },
                {
                  type: "song",
                  uid: "S8@3520",
                  title: "নিবেদন করি প্রভু! তোমার চরণে"
                },
                {
                  type: "song",
                  uid: "S16@3521",
                  title: "আত্মনিবেদন, তুয়া পদে করি’"
                },
                {
                  type: "heading",
                  uid: "*@3522",
                  title:
                    "৩. গোপ্তৃত্বে বরণ ~ 3. Goptṛtve Varaṇa [Acceptance of the Lord as One’s Only Maintainer]"
                },
                {
                  type: "song",
                  uid: "S18@3523",
                  title: "কি জানি কি বলে"
                },
                {
                  type: "song",
                  uid: "S13@3524",
                  title: "দারা-পুত্র-নিজ-দেহ-কুটুম্ব-পালনে"
                },
                {
                  type: "song",
                  uid: "S10@3525",
                  title: "সর্ব্বস্ব তোমার"
                },
                {
                  type: "heading",
                  uid: "*@3526",
                  title:
                    "৪. বিশ্রম্ভাত্মিকা ~ 4. Viśrambhātmikā [Confidence in Śrī Kṛṣṇa’s Protection]"
                },
                {
                  type: "song",
                  uid: "S27@3527",
                  title: "আত্মসমর্পণে গেলা অভিমান"
                },
                {
                  type: "song",
                  uid: "S26@3528",
                  title: "ছোড়ত পুরুষ-অভিমান"
                },
                {
                  type: "heading",
                  uid: "*@3529",
                  title: "৫. বর্জ্জনাত্মিকা ~ 5. Varjanātmikā [Rejection of the Unfavorable]"
                },
                {
                  type: "song",
                  uid: "S23@3530",
                  title: "কেশব! তুয়া জগত বিচিত্র"
                },
                {
                  type: "song",
                  uid: "S24@3531",
                  title: "তুয়া ভক্তি-প্রতিকূল ধর্ম্ম যা’তে রয়"
                },
                {
                  type: "song",
                  uid: "S25@3532",
                  title: "বিষয়বিমূঢ় আর মায়াবাদী জন"
                },
                {
                  type: "song",
                  uid: "S29@3533",
                  title: "আমি ত স্বানন্দ-সুখদবাসী"
                },
                {
                  type: "heading",
                  uid: "*@3534",
                  title: "৬. অনুকূল্যাত্মিকা ~ 6. Anukūlyātmika [Acceptance of the Favorable]"
                },
                {
                  type: "song",
                  uid: "S22@3535",
                  title: "তুয়া ভক্তি-অনুকূল যে-যে কার্য্য হয়"
                },
                {
                  type: "song",
                  uid: "S28@3536",
                  title: "গোদ্রুমধামে ভজন-অনুকূলে"
                }
              ]
            },
            {
              type: "collection",
              uid: "9@3537",
              title: "উপদেশামৃত ~ Upadeśāmṛta",
              children: [
                {
                  type: "song",
                  uid: "U1@3538",
                  title: "হরি হে! প্রপঞ্চে পড়িয়া"
                },
                {
                  type: "song",
                  uid: "U2@3539",
                  title: "হরি হে! অর্থের সঞ্চয়ে"
                },
                {
                  type: "song",
                  uid: "U3@3540",
                  title: "হরি হে! ভজনে উৎসাহ"
                },
                {
                  type: "song",
                  uid: "U4@3541",
                  title: "হরি হে! দান-প্রতিগ্রহ"
                },
                {
                  type: "song",
                  uid: "U5@3542",
                  title: "হরি হে! সঙ্গদোষ-শূন্য"
                },
                {
                  type: "song",
                  uid: "U6@3543",
                  title: "হরি হে! নীরধর্ম্ম-গত"
                },
                {
                  type: "song",
                  uid: "U7@3544",
                  title: "হরি হে! তোমারে ভুলিয়া"
                },
                {
                  type: "song",
                  uid: "U8@3545",
                  title: "হরি হে! শ্রীরূপগোসাঞি"
                },
                {
                  type: "heading",
                  uid: "*@3546",
                  title: "বিবিধ ~ Miscellaneous"
                },
                {
                  type: "song",
                  uid: "GN7@3547",
                  title: "কবে হ’বে বল"
                },
                {
                  type: "song",
                  uid: "S35@3548",
                  title: "শরণাগতের প্রার্থনা"
                }
              ]
            },
            {
              type: "collection",
              uid: "10@3549",
              title: "উপদেশ ~ Upadeśa",
              children: [
                {
                  type: "song",
                  uid: "MS15@3550",
                  title: "মন রে, কেন মিছে ভজিছ অসার ?"
                },
                {
                  type: "song",
                  uid: "MS16@3551",
                  title: "মন, তুমি ভালবাস কামের তরঙ্গ"
                },
                {
                  type: "song",
                  uid: "MS17@3552",
                  title: "মন রে, তুমি বড় সন্দিগ্ধ-অন্তর"
                },
                {
                  type: "song",
                  uid: "MS18@3553",
                  title: "মন, তুমি বড়ই পামর"
                },
                {
                  type: "song",
                  uid: "MS19@3554",
                  title: "মন, তব কেন এ সংশয় ?"
                },
                {
                  type: "song",
                  uid: "MS20@3555",
                  title: "মন, তুমি পড়িলে কি ছার ?"
                },
                {
                  type: "song",
                  uid: "MS21@3556",
                  title: "মন, যোগী হ’তে তোমার বাসনা"
                },
                {
                  type: "song",
                  uid: "MS22@3557",
                  title: "ওহে ভাই, মন কেন ব্রহ্ম হ’তে চায়"
                },
                {
                  type: "song",
                  uid: "MS23@3558",
                  title: "মন রে, কেন আর বর্ণ-অভিমান"
                },
                {
                  type: "song",
                  uid: "MS24@3559",
                  title: "মন রে, কেন কর বিদ্যার গৌরব"
                },
                {
                  type: "song",
                  uid: "MS25@3560",
                  title: "রূপের গৌরব কেন ভাই ?"
                },
                {
                  type: "song",
                  uid: "MS26@3561",
                  title: "মন রে, ধনমদ নিতান্ত অসার"
                },
                {
                  type: "song",
                  uid: "MS27@3562",
                  title: "মন, তুমি সন্ন্যাসী সাজিতে কেন চাও?"
                },
                {
                  type: "song",
                  uid: "MS28@3563",
                  title: "মন, তুমি তীর্থে সদা রত"
                },
                {
                  type: "song",
                  uid: "MS29@3564",
                  title: "দেখ মন, ব্রতে যেন না হও আচ্ছন্ন"
                },
                {
                  type: "song",
                  uid: "MS30@3565",
                  title: "মন, তুমি বড়ই চঞ্চল"
                },
                {
                  type: "song",
                  uid: "MS31@3566",
                  title: "মন, তোরে বলি এ বারতা"
                },
                {
                  type: "song",
                  uid: "MS32@3567",
                  title: "কি আর বলিব তোরে মন"
                },
                {
                  type: "song",
                  uid: "MS33@3568",
                  title: "কেন মন, কামেরে নাচাও প্রেম-প্রায় ?"
                }
              ]
            },
            {
              type: "collection",
              uid: "11@3569",
              title: "উপলব্ধি ~ Upalabdhi",
              children: [
                {
                  type: "song",
                  uid: "UP1@3570",
                  title: "আমি অতি পামর দুর্জ্জন"
                },
                {
                  type: "song",
                  uid: "UP2@3571",
                  title: "সাধুসঙ্গ না হইল হায়!"
                },
                {
                  type: "song",
                  uid: "UP3@3572",
                  title: "ওরে মন, কর্ম্মের কুহরে গেল কাল"
                },
                {
                  type: "song",
                  uid: "UP4@3573",
                  title: "ওরে মন, কি বিপদ হইল আমার"
                },
                {
                  type: "song",
                  uid: "UP5@3574",
                  title: "ওরে মন, ক্লেশ-তাপ দেখি যে অশেষ!"
                },
                {
                  type: "song",
                  uid: "UP6@3575",
                  title: "ওরে মন, ভাল নাহি লাগে এ সংসার"
                },
                {
                  type: "song",
                  uid: "UP7@3576",
                  title: "ওরে মন, বাড়িবার আশা কেন কর’?"
                },
                {
                  type: "song",
                  uid: "UP8@3577",
                  title: "ওরে মন, ভুক্তি-মুক্তি-স্পৃহা কর’ দূর"
                },
                {
                  type: "song",
                  uid: "UP9@3578",
                  title: "দুর্ল্লভ মানব-জন্ম লভিয়া সংসারে"
                },
                {
                  type: "song",
                  uid: "UP10@3579",
                  title: "শরীরের সুখে, মন, দেহ জলাঞ্জলি"
                },
                {
                  type: "song",
                  uid: "UP11@3580",
                  title: "ওরে মন, বলি, শুন তত্ত্ব-বিবরণ"
                },
                {
                  type: "song",
                  uid: "UP12@3581",
                  title: "ভ্রমিতে ভ্রমিতে যদি সাধুসঙ্গ হয়"
                },
                {
                  type: "song",
                  uid: "UP13@3582",
                  title: "অপূর্ব্ব বৈষ্ণব-তত্ত্ব! আত্মার আনন্দ-"
                },
                {
                  type: "song",
                  uid: "UP14@3583",
                  title: "চিজ্জড়ের দ্বৈত যিনি করেন স্থাপন"
                },
                {
                  type: "song",
                  uid: "UP15@3584",
                  title: "‘জীবন-সমাপ্তি-কালে করিব ভজন"
                },
                {
                  type: "song",
                  uid: "P3@3585",
                  title: "ভবার্ণবে প’ড়ে মোর আকুল পরাণ"
                },
                {
                  type: "heading",
                  uid: "*@3586",
                  title: "বিবিধ ~ Miscellaneous"
                },
                {
                  type: "song",
                  uid: "GH21@3587",
                  title: "চৈতন্য-চন্দ্রের লীলা-সমুদ্র অপার"
                },
                {
                  type: "song",
                  uid: "V15@3588",
                  title: "কবে মোর মূঢ় মন ছাড়ি’ অন্য ধ্যান"
                },
                {
                  type: "song",
                  uid: "NM11@3589",
                  title: "উদিল অরুণ পূরব ভাগে"
                },
                {
                  type: "song",
                  uid: "NM12@3590",
                  title: "জীব জাগ, জীব জাগ"
                }
              ]
            },
            {
              type: "collection",
              uid: "12@3591",
              title: "আরতি এবং প্রসাদ-সেবায় ~ Ārati and Prasāda-sevāya",
              children: [
                {
                  type: "heading",
                  uid: "*@3592",
                  title: "আরতি"
                },
                {
                  type: "song",
                  uid: "A3@3593",
                  title: "ভালে গোরা-গদাধরের আরতি নেহারি"
                },
                {
                  type: "song",
                  uid: "A10@3594",
                  title: "শ্রীগৌর-আরতি"
                },
                {
                  type: "song",
                  uid: "A11@3595",
                  title: "শ্রীযুগল-আরতি"
                },
                {
                  type: "song",
                  uid: "A9@3596",
                  title: "ভজ ভকত-বৎসল শ্রীগৌরহরি"
                },
                {
                  type: "heading",
                  uid: "*@3597",
                  title: "প্রসাদ-সেবায়"
                },
                {
                  type: "song",
                  uid: "PR1@3598",
                  title: "ভাইরে! শরীর অবিদ্যা-জাল"
                },
                {
                  type: "song",
                  uid: "PR2@3599",
                  title: "ভাইরে! একদিন শান্তিপুরে"
                },
                {
                  type: "song",
                  uid: "PR3@3600",
                  title: "ভাইরে! শচীর অঙ্গনে কভু"
                },
                {
                  type: "song",
                  uid: "PR4@3601",
                  title: "ভাইরে! শ্রীচৈতন্য-নিত্যানন্দ"
                },
                {
                  type: "song",
                  uid: "PR5@3602",
                  title: "ভাইরে! একদিন নীলাচলে"
                },
                {
                  type: "song",
                  uid: "PR6@3603",
                  title: "ভাইরে! রামকৃষ্ণ গোচারণে"
                },
                {
                  type: "heading",
                  uid: "*@3604",
                  title: "তুলসী-আরতি"
                },
                {
                  type: "song",
                  uid: "A16@3605",
                  title: "শ্রীতুলসী-আরতি"
                },
                {
                  type: "song",
                  uid: "A15@3606",
                  title: "নমো নমঃ তুলসী কৃষ্ণ-প্রেয়সী"
                },
                {
                  type: "heading",
                  uid: "*@3607",
                  title: "নাম-কীর্ত্তন"
                },
                {
                  type: "song",
                  uid: "NK28@3608",
                  title: "একবার ভাব মনে"
                },
                {
                  type: "song",
                  uid: "NK8@3609",
                  title: "বোল হরি বোল (৩ বার)"
                },
                {
                  type: "song",
                  uid: "NK36@3610",
                  title: "রাধাগোবিন্দ বল"
                }
              ]
            },
            {
              type: "collection",
              uid: "13@3611",
              title: "শ্রেয়োনির্ণয় ~ Ascertaining the Sumum Bonum",
              children: [
                {
                  type: "song",
                  uid: "S31@3612",
                  title: "কৃষ্ণভক্তি বিনা কভু নাহি ফলোদয়"
                },
                {
                  type: "song",
                  uid: "S32@3613",
                  title: "আর কেন মায়াজালে পড়িতেছ জীব মীন"
                },
                {
                  type: "song",
                  uid: "S33@3614",
                  title: "পীরিতি সচ্চিদানন্দে রূপবতী নারী"
                },
                {
                  type: "song",
                  uid: "S34@3615",
                  title: "‘নিরাকার নিরাকার’ করিয়া চীৎকার"
                },
                {
                  type: "song",
                  uid: "S36@3616",
                  title: "কেন আর কর দ্বেষ, বিদেশি-জন-ভজনে"
                },
                {
                  type: "song",
                  uid: "GP1@3617",
                  title: "ভজ রে ভজ রে আমার মন"
                },
                {
                  type: "song",
                  uid: "MS34@3618",
                  title: "ভাব না ভাব না, মন, তুমি অতি দুষ্ট"
                }
              ]
            },
            {
              type: "collection",
              uid: "14@3619",
              title: "শ্রীনামাষ্টক ~ Śrī Nāmāṣṭaka",
              children: [
                {
                  type: "song",
                  uid: "NM1@3620",
                  title: "শ্রীরূপ-বদনে"
                },
                {
                  type: "song",
                  uid: "NM2@3621",
                  title: "জয় জয় হরিনাম"
                },
                {
                  type: "song",
                  uid: "NM3@3622",
                  title: "বিশ্বে উদিত নাম-তপন"
                },
                {
                  type: "song",
                  uid: "NM4@3623",
                  title: "জ্ঞানী জ্ঞানযোগে করিয়া যতনে"
                },
                {
                  type: "song",
                  uid: "NM5@3624",
                  title: "হরিনাম তুয়া অনেক স্বরূপ"
                },
                {
                  type: "song",
                  uid: "NM6@3625",
                  title: "বাচ্য বাচক—এই দুই স্বরূপ তোমার"
                },
                {
                  type: "song",
                  uid: "NM7@3626",
                  title: "ওহে হরিনাম তব মহিমা অপার"
                },
                {
                  type: "song",
                  uid: "NM8@3627",
                  title: "নারদ মুনি বাজায় বীণা"
                }
              ]
            },
            {
              type: "collection",
              uid: "15@3628",
              title: "যামুন-ভাবাবলী ~ The Ecstasies of Yamunācārya",
              children: [
                {
                  type: "song",
                  uid: "Y1@3629",
                  title: "হরি হে! ওহে প্রভু দয়াময়"
                },
                {
                  type: "song",
                  uid: "Y2@3630",
                  title: "হরি হে! তোমার ঈক্ষণে হয়"
                },
                {
                  type: "song",
                  uid: "Y3@3631",
                  title: "হরি হে! পরতত্ত্ব বিচক্ষণ"
                },
                {
                  type: "song",
                  uid: "Y4@3632",
                  title: "হরি হে! জগতের বস্তু যত"
                },
                {
                  type: "song",
                  uid: "Y5@3633",
                  title: "হরি হে! তুমি সর্ব্বগুণযুত"
                },
                {
                  type: "song",
                  uid: "Y6@3634",
                  title: "হরি হে! তোমার গম্ভীর মন"
                },
                {
                  type: "song",
                  uid: "Y7@3635",
                  title: "হরি হে! মায়াবদ্ধ যতক্ষণ"
                },
                {
                  type: "song",
                  uid: "Y8@3636",
                  title: "হরি হে! ধর্ম্মনিষ্ঠা নাহি মোর"
                },
                {
                  type: "song",
                  uid: "Y9@3637",
                  title: "হরি হে! হেন দুষ্ট কর্ম্ম নাই"
                },
                {
                  type: "song",
                  uid: "Y10@3638",
                  title: "হরি হে! নিজকর্ম্ম-দোষ-ফলে"
                },
                {
                  type: "song",
                  uid: "Y11@3639",
                  title: "হরি হে! অন্য আশা নাহি যা’র"
                },
                {
                  type: "song",
                  uid: "Y12@3640",
                  title: "হরি হে! তব পদ-পঙ্কজিনী"
                },
                {
                  type: "song",
                  uid: "Y13@3641",
                  title: "হরি হে! ভ্রমিতে সংসার-বনে"
                },
                {
                  type: "song",
                  uid: "Y14@3642",
                  title: "হরি হে! তোমার চরণপদ্ম"
                },
                {
                  type: "song",
                  uid: "Y15@3643",
                  title: "হরি হে! তবাঙ্ঘ্রি কমলদ্বয়"
                },
                {
                  type: "song",
                  uid: "Y16@3644",
                  title: "হরি হে! আমি সেই দুষ্টমতি"
                },
                {
                  type: "song",
                  uid: "Y17@3645",
                  title: "হরি হে! আমি অপরাধি-জন"
                },
                {
                  type: "song",
                  uid: "Y18@3646",
                  title: "হরি হে! অবিবেকরূপ ঘন"
                },
                {
                  type: "song",
                  uid: "Y19@3647",
                  title: "হরি হে! অগ্রে এক নিবেদন"
                },
                {
                  type: "song",
                  uid: "Y20@3648",
                  title: "হরি হে! তোমা ছাড়ি’ আমি কভু"
                },
                {
                  type: "song",
                  uid: "Y21@3649",
                  title: "হরি হে! স্ত্রী-পুরুষ-দেহগত"
                },
                {
                  type: "song",
                  uid: "Y22@3650",
                  title: "হরি হে! বেদবিধি-অনুসারে"
                },
                {
                  type: "song",
                  uid: "Y23@3651",
                  title: "হরি হে! তোমার যে শুদ্ধভক্ত"
                },
                {
                  type: "song",
                  uid: "Y24@3652",
                  title: "হরি হে! শুন হে মধুমথন!"
                },
                {
                  type: "song",
                  uid: "Y25@3653",
                  title: "হরি হে! আমি নরপশুপ্রায়"
                },
                {
                  type: "song",
                  uid: "Y26@3654",
                  title: "হরি হে! তুমি জগতের পিতা"
                },
                {
                  type: "song",
                  uid: "Y27@3655",
                  title: "হরি হে! আমি ত’ চঞ্চলমতি"
                }
              ]
            },
            {
              type: "collection",
              uid: "16@3656",
              title: "শোক-শাতন ~ Śoka-śātana",
              children: [
                {
                  type: "song",
                  uid: "GH64@3657",
                  title: "প্রদোষ-সময়ে, শ্রীবাস-অঙ্গনে"
                },
                {
                  type: "song",
                  uid: "GH65@3658",
                  title: "প্রবেশিয়া অন্তঃপুরে, নারীগণে শান্ত করে"
                },
                {
                  type: "song",
                  uid: "GH66@3659",
                  title: "ধন, জন, দেহ, গেহ কৃষ্ণে সমর্পণ"
                },
                {
                  type: "song",
                  uid: "GH67@3660",
                  title: "সবু মেলি’ বালক-ভাগ বিচারি’"
                },
                {
                  type: "song",
                  uid: "GH68@3661",
                  title: "শ্রীবাস-বচন, শ্রবণ করিয়া"
                },
                {
                  type: "song",
                  uid: "GH69@3662",
                  title: "প্রভুর বচন, শুনিয়া তখন"
                },
                {
                  type: "song",
                  uid: "GH70@3663",
                  title: "গোরাচাঁদের আজ্ঞা পে’য়ে, গৃহবাসিগণ"
                },
                {
                  type: "song",
                  uid: "GH71@3664",
                  title: "“পূর্ণচিদানন্দ তুমি, তোমার চিৎকণ আমি"
                },
                {
                  type: "song",
                  uid: "GH72@3665",
                  title: "“বাঁধিল মায়া যেদিন হ’তে"
                },
                {
                  type: "song",
                  uid: "GH73@3666",
                  title: "শ্রীবাসে কহেন প্রভু—“তুহুঁ মোর দাস"
                },
                {
                  type: "song",
                  uid: "GH74@3667",
                  title: "শ্রীবাসের প্রতি চৈতন্য-প্রসাদ"
                },
                {
                  type: "song",
                  uid: "GH75@3668",
                  title: "মৃত শিশু ল’য়ে তবে ভকতবৎসল"
                },
                {
                  type: "song",
                  uid: "GH77@3669",
                  title: "(শ্রোতৃগণের প্রতি নিবেদন)"
                }
              ]
            },
            {
              type: "collection",
              uid: "17@3670",
              title: "শ্রীশ্রীরূপানুগ-ভজন-দর্পণ ~ Śrī Śrī Rūpānuga-bhajana-darpaṇa",
              children: [
                {
                  type: "song",
                  uid: "RBD1@3671",
                  title: "শ্রীশ্রীরূপানুগ ভজন-দর্পণ ভূমিকা"
                },
                {
                  type: "song",
                  uid: "RBD2@3672",
                  title: "শ্রদ্ধাই ভক্তিলতার বীজ"
                },
                {
                  type: "song",
                  uid: "RBD3@3673",
                  title: "কৃষ্ণতক্তি"
                },
                {
                  type: "song",
                  uid: "RBD4@3674",
                  title: "শ্রদ্ধা দ্বিবিধা, অতএব সাধনভক্তিও দ্বিবিধা"
                },
                {
                  type: "song",
                  uid: "RBD5@3675",
                  title: "প্রাকৃতাপ্রাকৃত রসতত্ত্ব-জ্ঞানের আবশ্যকতা"
                },
                {
                  type: "song",
                  uid: "RBD6@3676",
                  title: "স্থায়িভাবই রসের মূল"
                },
                {
                  type: "song",
                  uid: "RBD7@3677",
                  title: "মধুর রসই সর্ব্বশ্রেষ্ঠ রস"
                },
                {
                  type: "song",
                  uid: "RBD8@3678",
                  title: "মধুরা রতির আবির্ভাব-হেতু"
                },
                {
                  type: "song",
                  uid: "RBD9@3679",
                  title: "মধুর-রতিরূপ স্থায়িভাবের উন্নতিক্রম"
                },
                {
                  type: "song",
                  uid: "RBD10@3680",
                  title: "বিভাব"
                },
                {
                  type: "song",
                  uid: "RBD11@3681",
                  title: "মধুর-রসে আলম্বনরূপ বিভাব"
                },
                {
                  type: "song",
                  uid: "RBD12@3682",
                  title: "নায়ক-শিরোমণি শ্রীকৃষ্ণের গুণ"
                },
                {
                  type: "song",
                  uid: "RBD13@3683",
                  title: "তদীয় বল্লভাগণ"
                },
                {
                  type: "song",
                  uid: "RBD14@3684",
                  title: "নায়িকাগণের অষ্ট অবস্থা-সেবা"
                },
                {
                  type: "song",
                  uid: "RBD15@3685",
                  title: "প্রধান-নায়িকা শ্রীমতী রাধিকার সখী-বর্ণন"
                },
                {
                  type: "song",
                  uid: "RBD16@3686",
                  title: "সখীর সাধারণ-সেবা"
                },
                {
                  type: "song",
                  uid: "RBD17@3687",
                  title: "নিত্যসিদ্ধা নহেন, এরূপ ‘সখী’র বিচার"
                },
                {
                  type: "song",
                  uid: "RBD18@3688",
                  title: "সর্ব্বসখীর পরস্পর ভাব"
                },
                {
                  type: "song",
                  uid: "RBD19@3689",
                  title: "ব্রজগত-মধুর-রতির উদ্দীপন"
                },
                {
                  type: "song",
                  uid: "RBD20@3690",
                  title: "অনুভাব"
                },
                {
                  type: "song",
                  uid: "RBD21@3691",
                  title: "সাত্ত্বিকভাব"
                },
                {
                  type: "song",
                  uid: "RBD22@3692",
                  title: "ব্যভিচারী বা সঞ্চারী ভাব"
                },
                {
                  type: "song",
                  uid: "RBD23@3693",
                  title: "ভাবাবস্থা-প্রাপ্ত স্থায়ী ভাবের উত্তর দশা"
                },
                {
                  type: "song",
                  uid: "RBD24@3694",
                  title: "সম্ভোগ-বিপ্রলম্ভ-ভেদে উজ্জ্বল-রস দ্বিবিধ; তন্মধ্যে বিপ্রলম্ভ"
                },
                {
                  type: "song",
                  uid: "RBD25@3695",
                  title: "সম্ভোগ"
                },
                {
                  type: "song",
                  uid: "RBD26@3696",
                  title: "সম্তোগের প্রকার"
                },
                {
                  type: "song",
                  uid: "RBD27@3697",
                  title: "উজ্জ্বলরসাশ্রিত-লীলা"
                },
                {
                  type: "song",
                  uid: "RBD28@3698",
                  title: "ব্রজলীলার সর্ব্বশ্রেষ্ঠতা"
                },
                {
                  type: "song",
                  uid: "RBD29@3699",
                  title: "অষ্টকাল বর্ণন"
                },
                {
                  type: "song",
                  uid: "RBD30@3700",
                  title: "অথ নিশান্ত লীলা"
                },
                {
                  type: "song",
                  uid: "RBD31@3701",
                  title: "অথ প্রাতলীলা"
                },
                {
                  type: "song",
                  uid: "RBD32@3702",
                  title: "অথ পূর্ব্বাহ্ণলীলা"
                },
                {
                  type: "song",
                  uid: "RBD33@3703",
                  title: "অথ মধ্যাহ্নলীলা"
                },
                {
                  type: "song",
                  uid: "RBD34@3704",
                  title: "অথ অপরাহ্ণলীলা"
                },
                {
                  type: "song",
                  uid: "RBD35@3705",
                  title: "অথ সায়ংলীলা"
                },
                {
                  type: "song",
                  uid: "RBD36@3706",
                  title: "অথ প্রদোষ লীলা"
                },
                {
                  type: "song",
                  uid: "RBD37@3707",
                  title: "অথ নক্তলীলা"
                },
                {
                  type: "song",
                  uid: "RBD38@3708",
                  title: "নিত্যলীলার ভেদ, পীঠ ও উপকরণ"
                },
                {
                  type: "song",
                  uid: "RBD39@3709",
                  title: "নিত্যলীলার ভেদ, পীঠ ও উপকরণ"
                },
                {
                  type: "song",
                  uid: "RBD40@3710",
                  title: "নিত্যলীলার ভেদ, পীঠ ও উপকরণ"
                }
              ]
            },
            {
              type: "collection",
              uid: "18@3711",
              title: "বিবিধ নরোত্তম দাস ঠাকুরের গীত",
              children: [
                {
                  type: "song",
                  uid: "K56@3712",
                  title: "হরি হরি! কৃপা করি’ রাখ নিজ পদে"
                },
                {
                  type: "song",
                  uid: "GP14@3713",
                  title: "হরি হরি! বড় শেল মরমে রহিল"
                },
                {
                  type: "song",
                  uid: "D15@3714",
                  title: "হরি হরি! আর কি এমন দশা হব"
                },
                {
                  type: "song",
                  uid: "D14@3715",
                  title: "হরি হরি! আর কবে পালটিবে দশা"
                },
                {
                  type: "song",
                  uid: "D16@3716",
                  title: "করঙ্গ কৌপীন লঞা"
                },
                {
                  type: "song",
                  uid: "D17@3717",
                  title: "হরি হরি! কবে হব বৃন্দাবনবাসী"
                },
                {
                  type: "heading",
                  uid: "*@3718",
                  title: "প্রেমভক্তিচন্দ্রিকা"
                },
                {
                  type: "song",
                  uid: "PBC1@3719",
                  title: "শ্রীগুরু-চরণ-পদ্ম + বৈষ্ণব-চরণরেণু"
                },
                {
                  type: "song",
                  uid: "PBC2@3720",
                  title: "অন্য অভিলাষ ছাড়ি’"
                },
                {
                  type: "song",
                  uid: "PBC3@3721",
                  title: "তুমি ত দয়ার সিন্ধু"
                },
                {
                  type: "song",
                  uid: "PBC4@3722",
                  title: "আন কথা আন ব্যথা"
                },
                {
                  type: "song",
                  uid: "PBC5@3723",
                  title: "রাগের ভজন-পথ"
                },
                {
                  type: "song",
                  uid: "PBC6@3724",
                  title: "যুগল-চরণ-প্রতি"
                },
                {
                  type: "song",
                  uid: "PBC7@3725",
                  title: "রাধাকৃষ্ণ করোঁ ধ্যান"
                },
                {
                  type: "song",
                  uid: "PBC8@3726",
                  title: "বচনের অগোচর, বৃন্দাবন ধামময়"
                },
                {
                  type: "song",
                  uid: "PBC9@3727",
                  title: "আন কথা না শুনিব"
                },
                {
                  type: "heading",
                  uid: "*@3728",
                  title: "ভক্তিবিনোদ ঠাকুরের দালালের গীত"
                },
                {
                  type: "song",
                  uid: "NK26@3729",
                  title: "বড় সুখের খবর গাই"
                },
                {
                  type: "song",
                  uid: "NK37@3730",
                  title: "চতুর্যু‌গের মহামন্ত্র"
                }
              ]
            },
            {
              type: "collection",
              uid: "19@3731",
              title:
                "মনঃশিক্ষা (প্রেমানন্দ দাস ঠাকুর) ~ Instructions to the Mind (by Premānanda dāsa Ṭhākura)",
              children: [
                {
                  type: "song",
                  uid: "MS68@3732",
                  title: "এ মন! বলরে গোবিন্দ নাম"
                },
                {
                  type: "song",
                  uid: "MS63@3733",
                  title: "ওরে ভাই! কৃষ্ণ সে এ তিন-লোক-বন্ধু"
                },
                {
                  type: "song",
                  uid: "MS66@3734",
                  title: "ওরে মন! কৃষ্ণনাম-সম নাহি আন"
                },
                {
                  type: "song",
                  uid: "MS69@3735",
                  title: "ওরে মন! হরি হরি বল ভাই"
                },
                {
                  type: "song",
                  uid: "MS70@3736",
                  title: "ওরে মন ধিক্‌রে তোমায়"
                },
                {
                  type: "song",
                  uid: "MS67@3737",
                  title: "এ মন! ‘হরিনাম’ কর সার"
                },
                {
                  type: "song",
                  uid: "MS58@3738",
                  title: "এ মন! তুমি কি ভেবেছ সুখ"
                },
                {
                  type: "song",
                  uid: "MS41@3739",
                  title: "এ মন! আর কি মানুষ হবে"
                },
                {
                  type: "song",
                  uid: "MS42@3740",
                  title: "ওরে মন! শুন শুন তু অতি বর্ব্বর"
                },
                {
                  type: "song",
                  uid: "MS43@3741",
                  title: "ওরে মন! কিসে কর দেহের গুমান"
                },
                {
                  type: "song",
                  uid: "MS44@3742",
                  title: "এ মন! তুমি বা ভুলেছ কিসে"
                },
                {
                  type: "song",
                  uid: "MS45@3743",
                  title: "ওরে মন! কি রসে হৈয়া রৈলি ভোর"
                },
                {
                  type: "song",
                  uid: "MS46@3744",
                  title: "ওরে মন! শুন শুন তু বড়ি গোঙার"
                },
                {
                  type: "song",
                  uid: "MS47@3745",
                  title: "ওরে মন! রুচি নহে কেন কৃষ্ণ-নাম"
                },
                {
                  type: "song",
                  uid: "MS48@3746",
                  title: "ওরে মন! ধন-জন-জীবন-যৌবন"
                },
                {
                  type: "song",
                  uid: "MS49@3747",
                  title: "ওরে মন! এবার বুঝিব ভারিভুরি"
                },
                {
                  type: "song",
                  uid: "MS50@3748",
                  title: "এ মন! কি লাগি’ আইলি ভবে"
                },
                {
                  type: "song",
                  uid: "MS51@3749",
                  title: "ওরে মন! আর কি হইবে হেন জন্ম"
                },
                {
                  type: "song",
                  uid: "MS52@3750",
                  title: "ওরে মন! কিবা তুমি বিচারি না চাও"
                },
                {
                  type: "song",
                  uid: "MS53@3751",
                  title: "ওরে মন! কেন হেন বুঝ বিপরীত"
                },
                {
                  type: "song",
                  uid: "MS54@3752",
                  title: "ওরে মন! নিবেদন শুনহ আমার"
                },
                {
                  type: "song",
                  uid: "MS55@3753",
                  title: "ওরে মন! ভাবিয়া না বুঝ আপনাকে"
                },
                {
                  type: "song",
                  uid: "MS56@3754",
                  title: "ওরে মন! স্বর্গ নরক বুঝ কোথা"
                },
                {
                  type: "song",
                  uid: "MS60@3755",
                  title: "ওরে মন! এবে তোর এ কেমন রীত"
                }
              ]
            },
            {
              type: "collection",
              uid: "20@3756",
              title: "বিবিধ ২ ~ Miscellaneous 2",
              children: [
                {
                  type: "song",
                  uid: "NK1@3757",
                  title: "(হরি) হরয়ে নমঃ কৃষ্ণ যাদবায় নমঃ"
                },
                {
                  type: "song",
                  uid: "GN8@3758",
                  title: "এ ঘোর-সংসারে"
                },
                {
                  type: "song",
                  uid: "V16@3759",
                  title: "মিছে মায়া-বশে, সংসার-সাগরে"
                },
                {
                  type: "song",
                  uid: "MS39@3760",
                  title: "সুখের লাগিয়া এ ঘর বাঁধিনু"
                },
                {
                  type: "song",
                  uid: "K71@3761",
                  title: "কি কহব রে সখি আজুক আনন্দ ওর ৷"
                },
                {
                  type: "song",
                  uid: "NM20@3762",
                  title: "মুখে লৈতে কৃষ্ণ নাম"
                },
                {
                  type: "song",
                  uid: "NM21@3763",
                  title: "মুখে লৈতে কৃষ্ণ নাম, নাচে তুণ্ড অবিরাম"
                },
                {
                  type: "song",
                  uid: "NM15@3764",
                  title: "সই, কেবা শুনাইল শ্যাম-নাম"
                },
                {
                  type: "song",
                  uid: "K72@3765",
                  title: "বঁধু হে নয়নে লুকায়ে থোব"
                },
                {
                  type: "song",
                  uid: "K58@3766",
                  title: "তাতল সৈকতে, বারিবিন্দু-সম"
                },
                {
                  type: "song",
                  uid: "K60@3767",
                  title: "মাধব, বহুত মিনতি করি তোয়"
                }
              ]
            },
            {
              type: "collection",
              uid: "21@3768",
              title: "কার্ত্তিকব্রতে অষ্টকালীয় কীর্ত্তন",
              children: [
                {
                  type: "heading",
                  uid: "*@3769",
                  title: "প্রথমযম-সাধন ~ First Yama Sādhana"
                },
                {
                  type: "heading",
                  uid: "*@3770",
                  title: "নিশান্ত-ভজন—শ্রদ্ধা ~ The close of night (niśānta) bhajana—śraddhā"
                },
                {
                  type: "heading",
                  uid: "*@3771",
                  title: "(রাত্রের শেষ ছয়দণ্ড) ~ (the last six daṇḍas of the night"
                },
                {
                  type: "song",
                  uid: "ASTA1@3772",
                  title: "প্রথমযম-সাধন ~ First Yama Sādhana"
                },
                {
                  type: "heading",
                  uid: "*@3773",
                  title: "দ্বিতীয়যাম-সাধন ~ Second Yama Sādhana"
                },
                {
                  type: "heading",
                  uid: "*@3774",
                  title: "প্রাতঃকালীয়ভজন ~ Early morning (prātaḥ) bhajana"
                },
                {
                  type: "heading",
                  uid: "*@3775",
                  title: "(প্রাতে প্রথম ছয় দণ্ড) ~ (the first 6 daṇḍas of the morning"
                },
                {
                  type: "song",
                  uid: "ASTA2@3776",
                  title: "দ্বিতীয়যাম-সাধন ~ Second Yama Sādhana"
                },
                {
                  type: "heading",
                  uid: "*@3777",
                  title: "তৃতীয়যম-সাধন ~ Third Yama Sādhana"
                },
                {
                  type: "heading",
                  uid: "*@3778",
                  title:
                    "পূর্ব্বাহ্নকালীয়ভজন—নিষ্ঠাভজন ~ Late morning (pūrvāhna) bhajana—niṣṭhā-bhajana"
                },
                {
                  type: "heading",
                  uid: "*@3779",
                  title:
                    "(ছয়দণ্ড বেলা হইতে দ্বিপ্রহর দিবাস পর্য্যন্ত) ~ (from 6 daṇḍas until 2 praharas"
                },
                {
                  type: "song",
                  uid: "ASTA3@3780",
                  title: "তৃতীয়যম-সাধন ~ Third Yama Sādhana"
                },
                {
                  type: "heading",
                  uid: "*@3781",
                  title: "চতুর্যাম-সাধন ~ Fourth Yama Sādhana"
                },
                {
                  type: "heading",
                  uid: "*@3782",
                  title: "মধ্যাহ্নকালীয়ভজন—রুচিভজন ~ Midday (madhyāhna) bhajana—ruci-bhajana"
                },
                {
                  type: "heading",
                  uid: "*@3783",
                  title:
                    "(দ্বিপ্রহর দিবস হইতে সাড়ে তিন্ প্রহর পর্য্যন্ত) ~ (from the 2nd prahara until 3 1/2 praharas"
                },
                {
                  type: "song",
                  uid: "ASTA4@3784",
                  title: "চতুর্যাম-সাধন ~ Fourth Yama Sādhana"
                },
                {
                  type: "heading",
                  uid: "*@3785",
                  title: "পঞ্চমযাম-সাধন ~ Fifth Yama Sādhana"
                },
                {
                  type: "heading",
                  uid: "*@3786",
                  title: "অপরাহ্নকালীয়ভজন—কৃষ্ণাসক্তি ~ Afternoon (aparāhna) bhajana—kṛṣṇa-āsakti"
                },
                {
                  type: "heading",
                  uid: "*@3787",
                  title:
                    "(সাড়ে তিনপ্রহর দিবস হইতে সন্ধ্যা পর্য্যন্ত) ~ (from 3 1/2 praharas of the day until dusk"
                },
                {
                  type: "song",
                  uid: "ASTA5@3788",
                  title: "পঞ্চমযাম-সাধন ~ Fifth Yama Sādhana"
                },
                {
                  type: "heading",
                  uid: "*@3789",
                  title: "ষষ্ঠযাম-সাধন ~ Sixth Yama Sādhana"
                },
                {
                  type: "heading",
                  uid: "*@3790",
                  title: "সায়ংকালীন-ভজন—ভাব ~ Evening (sāyaṁ) bhajana—bhāva"
                },
                {
                  type: "heading",
                  uid: "*@3791",
                  title: "(সন্ধ্যার পর ছয়দণ্ড) ~ (6 daṇḍas after dusk"
                },
                {
                  type: "song",
                  uid: "ASTA6@3792",
                  title: "ষষ্ঠযাম-সাধন ~ Sixth Yama Sādhana"
                },
                {
                  type: "heading",
                  uid: "*@3793",
                  title: "সপ্তমযাম-সাধন ~ Seventh Yama Sādhana"
                },
                {
                  type: "heading",
                  uid: "*@3794",
                  title:
                    "প্রদোষকালীন-ভজন—প্রেম-বিপ্রলম্ভ ~ Nightfall (pradoṣa) bhajana—vipralambha-prema"
                },
                {
                  type: "heading",
                  uid: "*@3795",
                  title:
                    "(ছয়দণ্ড রাত্র হইতে মধ্যরাত্র পর্য্যন্ত) ~ (from 6 daṇḍas of the night until midnight"
                },
                {
                  type: "song",
                  uid: "ASTA7@3796",
                  title: "সপ্তমযাম-সাধন ~ Seventh Yama Sādhana"
                },
                {
                  type: "heading",
                  uid: "*@3797",
                  title: "অষ্টমযাম-সাধন ~ Eighth Yama Sādhana"
                },
                {
                  type: "heading",
                  uid: "*@3798",
                  title:
                    "রাত্রলীলা—প্রেমভজন-সম্ভোগ ~ Late night (rātri) pastimes—prema-bhajana sambhoga"
                },
                {
                  type: "heading",
                  uid: "*@3799",
                  title:
                    "(মধারাত্র হইতে সাড়ে তিনপ্রহররাত্র পর্য্যন্ত) ~ (from midnight to 3 1/2 praharas of the night"
                },
                {
                  type: "song",
                  uid: "ASTA8@3800",
                  title: "অষ্টমযাম-সাধন ~ Eighth Yama Sādhana"
                }
              ]
            },
            {
              type: "collection",
              uid: "22@3801",
              title: "বাউল-সঙ্গীত ~ Songs of a Madman",
              children: [
                {
                  type: "song",
                  uid: "BS1@3802",
                  title: "আমি তোমার দুঃখের দুঃখী, সুখের সুখী"
                },
                {
                  type: "song",
                  uid: "BS2@3803",
                  title: "ধর্ম্মপথে থাকি’ কর জীবন যাপন, ভাই"
                },
                {
                  type: "song",
                  uid: "BS3@3804",
                  title: "আসল কথা বল্‌তে কি"
                },
                {
                  type: "song",
                  uid: "BS4@3805",
                  title: "‘বাউল বাউল’ বল্‌ছ সবে"
                },
                {
                  type: "song",
                  uid: "BS5@3806",
                  title: "মানুষ-ভজন কর্‌ছো, ও ভাই"
                },
                {
                  type: "song",
                  uid: "BS6@3807",
                  title: "এও ত’ এক কলির চেলা"
                },
                {
                  type: "song",
                  uid: "BS7@3808",
                  title: "(মন আমার) হুঁসা’র থেকো, ভুল’ নাক"
                },
                {
                  type: "song",
                  uid: "BS8@3809",
                  title: "মনের মালা জপ্‌বি যখন, মন"
                },
                {
                  type: "song",
                  uid: "BS9@3810",
                  title: "ঘরে ব’সে বাউল হও রে মন"
                },
                {
                  type: "song",
                  uid: "BS10@3811",
                  title: "বলান্ বৈরাগী ঠাকুর, কিন্তু গৃহীর মধ্যে বাহাদুর"
                },
                {
                  type: "song",
                  uid: "BS11@3812",
                  title: "কেন ভেকের প্রয়াস ?"
                },
                {
                  type: "song",
                  uid: "BS12@3813",
                  title: "হ’য়ে বিষয়ে আবেশ, পে’লে মন, যাতনা অশেষ"
                }
              ]
            },
            {
              type: "collection",
              uid: "23@3814",
              title: "Hindi Songs",
              children: [
                {
                  type: "song",
                  uid: "G13@3815",
                  title: "गुरु-चरणकमल भज मन"
                },
                {
                  type: "song",
                  uid: "B1@3816",
                  title: "जय माधव मदनमुरारी"
                },
                {
                  type: "song",
                  uid: "B4@3817",
                  title: "भज गोविन्द, भज गोविन्द"
                }
              ]
            }
          ],
          count: 493
        }
      ]
    },
    {
      type: "collection",
      uid: "BVT@3818",
      title: "শ্রীল ভক্তিবিনোদ ঠাকুর ~ Śrīla Bhaktivinoda Ṭhākura",
      count: 272,
      children: [
        {
          type: "collection",
          uid: "KKT@3819",
          title:
            "কল্যাণ-কল্পতরু [হরিভক্তিপ্রদায়িনী সভা] ~ Kalyāṇa-kalpataru [Hari-bhakti-pradāyinī Sabhā]",
          children: [
            {
              type: "heading",
              uid: "*@3820",
              title: "১ম সংস্করণ ইং ১৮৮০ ~ published in 1880"
            },
            {
              type: "collection",
              uid: "1@3821",
              title: "উপদেশ ~ Upadeśa",
              children: [
                {
                  type: "song",
                  uid: "MS15@3822",
                  title: "মন রে, কেন মিছে ভজিছ অসার ?"
                },
                {
                  type: "song",
                  uid: "MS16@3823",
                  title: "মন, তুমি ভালবাস কামের তরঙ্গ"
                },
                {
                  type: "song",
                  uid: "MS17@3824",
                  title: "মন রে, তুমি বড় সন্দিগ্ধ-অন্তর"
                },
                {
                  type: "song",
                  uid: "MS18@3825",
                  title: "মন, তুমি বড়ই পামর"
                },
                {
                  type: "song",
                  uid: "MS19@3826",
                  title: "মন, তব কেন এ সংশয় ?"
                },
                {
                  type: "song",
                  uid: "MS20@3827",
                  title: "মন, তুমি পড়িলে কি ছার ?"
                },
                {
                  type: "song",
                  uid: "MS21@3828",
                  title: "মন, যোগী হ’তে তোমার বাসনা"
                },
                {
                  type: "song",
                  uid: "MS22@3829",
                  title: "ওহে ভাই, মন কেন ব্রহ্ম হ’তে চায়"
                },
                {
                  type: "song",
                  uid: "MS23@3830",
                  title: "মন রে, কেন আর বর্ণ-অভিমান"
                },
                {
                  type: "song",
                  uid: "MS24@3831",
                  title: "মন রে, কেন কর বিদ্যার গৌরব"
                },
                {
                  type: "song",
                  uid: "MS25@3832",
                  title: "রূপের গৌরব কেন ভাই ?"
                },
                {
                  type: "song",
                  uid: "MS26@3833",
                  title: "মন রে, ধনমদ নিতান্ত অসার"
                },
                {
                  type: "song",
                  uid: "MS27@3834",
                  title: "মন, তুমি সন্ন্যাসী সাজিতে কেন চাও?"
                },
                {
                  type: "song",
                  uid: "MS28@3835",
                  title: "মন, তুমি তীর্থে সদা রত"
                },
                {
                  type: "song",
                  uid: "MS29@3836",
                  title: "দেখ মন, ব্রতে যেন না হও আচ্ছন্ন"
                },
                {
                  type: "song",
                  uid: "MS30@3837",
                  title: "মন, তুমি বড়ই চঞ্চল"
                },
                {
                  type: "song",
                  uid: "MS31@3838",
                  title: "মন, তোরে বলি এ বারতা"
                },
                {
                  type: "song",
                  uid: "MS32@3839",
                  title: "কি আর বলিব তোরে মন"
                },
                {
                  type: "song",
                  uid: "MS33@3840",
                  title: "কেন মন, কামেরে নাচাও প্রেম-প্রায় ?"
                }
              ]
            },
            {
              type: "collection",
              uid: "2@3841",
              title: "উপলব্ধি ~ Upalabdhi",
              children: [
                {
                  type: "collection",
                  uid: "2a@3842",
                  title: "অনুতাপ ~ Anutāpa",
                  children: [
                    {
                      type: "song",
                      uid: "UP1@3843",
                      title: "আমি অতি পামর দুর্জ্জন"
                    },
                    {
                      type: "song",
                      uid: "UP2@3844",
                      title: "সাধুসঙ্গ না হইল হায়!"
                    },
                    {
                      type: "song",
                      uid: "UP3@3845",
                      title: "ওরে মন, কর্ম্মের কুহরে গেল কাল"
                    },
                    {
                      type: "song",
                      uid: "UP4@3846",
                      title: "ওরে মন, কি বিপদ হইল আমার"
                    },
                    {
                      type: "song",
                      uid: "UP5@3847",
                      title: "ওরে মন, ক্লেশ-তাপ দেখি যে অশেষ!"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2b@3848",
                  title: "নির্ব্বেদ ~ Nirbeda",
                  children: [
                    {
                      type: "song",
                      uid: "UP6@3849",
                      title: "ওরে মন, ভাল নাহি লাগে এ সংসার"
                    },
                    {
                      type: "song",
                      uid: "UP7@3850",
                      title: "ওরে মন, বাড়িবার আশা কেন কর’?"
                    },
                    {
                      type: "song",
                      uid: "UP8@3851",
                      title: "ওরে মন, ভুক্তি-মুক্তি-স্পৃহা কর’ দূর"
                    },
                    {
                      type: "song",
                      uid: "UP9@3852",
                      title: "দুর্ল্লভ মানব-জন্ম লভিয়া সংসারে"
                    },
                    {
                      type: "song",
                      uid: "UP10@3853",
                      title: "শরীরের সুখে, মন, দেহ জলাঞ্জলি"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "2c@3854",
                  title: "সম্বন্ধ-অভিদেয়-প্রয়োজন-বিজ্ঞান ~ Sambandha-abhideya-prayojana-vijñāna",
                  children: [
                    {
                      type: "heading",
                      uid: "*@3855",
                      title: "সম্বন্ধ-বিজ্ঞান-লক্ষণ-উপলব্ধি"
                    },
                    {
                      type: "song",
                      uid: "UP11@3856",
                      title: "ওরে মন, বলি, শুন তত্ত্ব-বিবরণ"
                    },
                    {
                      type: "heading",
                      uid: "*@3857",
                      title: "অভিধেয়-বিজ্ঞান-উপলব্ধি"
                    },
                    {
                      type: "song",
                      uid: "UP12@3858",
                      title: "ভ্রমিতে ভ্রমিতে যদি সাধুসঙ্গ হয়"
                    },
                    {
                      type: "heading",
                      uid: "*@3859",
                      title: "প্রয়োজন-বিজ্ঞান-উপলব্ধি"
                    },
                    {
                      type: "song",
                      uid: "UP13@3860",
                      title: "অপূর্ব্ব বৈষ্ণব-তত্ত্ব! আত্মার আনন্দ-"
                    },
                    {
                      type: "song",
                      uid: "UP14@3861",
                      title: "চিজ্জড়ের দ্বৈত যিনি করেন স্থাপন"
                    },
                    {
                      type: "song",
                      uid: "UP15@3862",
                      title: "‘জীবন-সমাপ্তি-কালে করিব ভজন"
                    }
                  ]
                }
              ]
            },
            {
              type: "collection",
              uid: "3@3863",
              title: "উচ্ছ্বাস ~ Ucchvāsa",
              children: [
                {
                  type: "collection",
                  uid: "3a@3864",
                  title: "প্রার্থনা দৈন্যময়ী ~ Prārthanā Dainyamayī",
                  children: [
                    {
                      type: "song",
                      uid: "GH47@3865",
                      title: "কবে শ্রীচৈতন্য মোরে করিবেন দয়া"
                    },
                    {
                      type: "song",
                      uid: "GH48@3866",
                      title: "আমি ত’ দুর্জ্জন"
                    },
                    {
                      type: "song",
                      uid: "P3@3867",
                      title: "ভবার্ণবে প’ড়ে মোর আকুল পরাণ"
                    },
                    {
                      type: "song",
                      uid: "GV46@3868",
                      title: "বিষয়-বাসনারূপ চিত্তের বিকার"
                    },
                    {
                      type: "song",
                      uid: "P4@3869",
                      title: "আমার সমান হীন নাহি এ সংসারে"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3b@3870",
                  title: "প্রার্থনা লালসাময়ী ~ Prārthanā Lālasāmayī",
                  children: [
                    {
                      type: "song",
                      uid: "G12@3871",
                      title: "কবে মোর শুভদিন হইবে উদয়"
                    },
                    {
                      type: "song",
                      uid: "V13@3872",
                      title: "শ্রীগুরু-বৈষ্ণব-কৃপা কত দিনে হ’বে"
                    },
                    {
                      type: "song",
                      uid: "V14@3873",
                      title: "আমার এমন ভাগ্য কত দিনে হ’বে"
                    },
                    {
                      type: "song",
                      uid: "GH21@3874",
                      title: "চৈতন্য-চন্দ্রের লীলা-সমুদ্র অপার"
                    },
                    {
                      type: "song",
                      uid: "V6@3875",
                      title: "হরি হরি কবে মোর হবে হেন দিন"
                    },
                    {
                      type: "song",
                      uid: "V5@3876",
                      title: "কবে মুই বৈষ্ণব চিনিব"
                    },
                    {
                      type: "song",
                      uid: "V4@3877",
                      title: "কৃপা কর বৈষ্ণব ঠাকুর !"
                    },
                    {
                      type: "song",
                      uid: "GN10@3878",
                      title: "কবে হ’বে হেন দশা মোর"
                    },
                    {
                      type: "song",
                      uid: "GH50@3879",
                      title: "হা হা মোর গৌর-কিশোর"
                    },
                    {
                      type: "song",
                      uid: "GN11@3880",
                      title: "হা হা কবে গৌর-নিতাই"
                    },
                    {
                      type: "song",
                      uid: "GH51@3881",
                      title: "কবে আহা গৌরাঙ্গ বলিয়া"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3c@3882",
                  title: "বিজ্ঞপ্তি ~ Vijñapti",
                  children: [
                    {
                      type: "song",
                      uid: "K49@3883",
                      title: "গোপীনাথ, মম নিবেদন শুন"
                    },
                    {
                      type: "song",
                      uid: "K50@3884",
                      title: "গোপীনাথ, ঘুচাও সংসার-জ্বালা"
                    },
                    {
                      type: "song",
                      uid: "K51@3885",
                      title: "গোপীনাথ, আমার উপায় নাই"
                    },
                    {
                      type: "song",
                      uid: "RK11@3886",
                      title: "শ্রীরাধাকৃষ্ণ-পদকমলে মন"
                    }
                  ]
                },
                {
                  type: "collection",
                  uid: "3d@3887",
                  title: "উচ্ছ্বাস কীর্ত্তন ~ Ucchvāsa Kīrtana",
                  children: [
                    {
                      type: "heading",
                      uid: "*@3888",
                      title: "নাম-কীর্ত্তন ~ Nāma-kīrtana"
                    },
                    {
                      type: "song",
                      uid: "NK5@3889",
                      title: "কলিকুক্কুর-কদন যদি চাও (হে)"
                    },
                    {
                      type: "song",
                      uid: "A7@3890",
                      title: "বিভাবরী-শেষ"
                    },
                    {
                      type: "heading",
                      uid: "*@3891",
                      title: "রূপ-কীর্ত্তন ~ Rūpa-kīrtana"
                    },
                    {
                      type: "song",
                      uid: "K41@3892",
                      title: "জনম সফল তা’র, কৃষ্ণ দরশন যা’র"
                    },
                    {
                      type: "heading",
                      uid: "*@3893",
                      title: "গুণ-কীর্ত্তন ~ Guṇa-kīrtana"
                    },
                    {
                      type: "song",
                      uid: "K43@3894",
                      title: "বহির্ম্মুখ হ’য়ে, মায়ারে ভজিয়ে"
                    },
                    {
                      type: "song",
                      uid: "K40@3895",
                      title: "শুন হে রসিক জন"
                    },
                    {
                      type: "heading",
                      uid: "*@3896",
                      title: "লীল-কীর্ত্তন ~ Līlā-kīrtana"
                    },
                    {
                      type: "song",
                      uid: "K44@3897",
                      title: "জীবে কৃপা করি’, গোলোকের হরি"
                    },
                    {
                      type: "song",
                      uid: "K45@3898",
                      title: "যমুনা-পুলিনে"
                    },
                    {
                      type: "heading",
                      uid: "*@3899",
                      title: "রস-কীর্ত্তন্ ~ Rasa-kīrtan"
                    },
                    {
                      type: "song",
                      uid: "K70@3900",
                      title: "কৃষ্ণবংশীগীত শুনি’"
                    }
                  ]
                }
              ]
            }
          ],
          count: 62
        },
        {
          type: "collection",
          uid: "SAR@3901",
          title: "শরণাগতি ~ Śaraṇāgati",
          children: [
            {
              type: "heading",
              uid: "*@3903",
              title: "ষড়ঙ্গ শরণাগতি ~ The Six Limbs of Surrender"
            },
            {
              type: "song",
              uid: "S1@3904",
              title: "শ্রীকৃষ্ণচৈতন্য প্রভু জীবে দয়া করি’"
            },
            {
              type: "collection",
              uid: "1@3905",
              title: "১. দৈন্যাত্মিকা ~ 1. Dainyātmikā [Humility]",
              children: [
                {
                  type: "song",
                  uid: "S2@3906",
                  title: "ভুলিয়া তোমারে"
                },
                {
                  type: "song",
                  uid: "S4@3907",
                  title: "বিদ্যার বিলাসে, কাটাইনু কাল"
                },
                {
                  type: "song",
                  uid: "S5@3908",
                  title: "যৌবনে যখন, ধন-উপার্জ্জনে"
                },
                {
                  type: "song",
                  uid: "S7@3909",
                  title: "আমার জীবন, সদা পাপে রত"
                }
              ]
            },
            {
              type: "collection",
              uid: "2@3910",
              title: "২. আত্মনিবেদনাত্মিকা ~ 2. Ātma-nivedanātmikā [Submission of the Self]",
              children: [
                {
                  type: "song",
                  uid: "S3@3911",
                  title: "(প্রভু হে!) শুন মোর দুঃখের কাহিনী"
                },
                {
                  type: "song",
                  uid: "S6@3912",
                  title: "(প্রভু হে!) তুয়া পদে এ মিনতি মোর"
                },
                {
                  type: "song",
                  uid: "GH20@3913",
                  title: "(প্রভু হে!) এমন দুর্ম্মতি সংসার-ভিতরে"
                },
                {
                  type: "song",
                  uid: "S19@3914",
                  title: "না করলুঁ করম, গেয়ান নাহি ভেল"
                },
                {
                  type: "song",
                  uid: "S9@3915",
                  title: "(প্রাণেশ্বর!) কহবুঁ কি সরম্ কি বাত্"
                },
                {
                  type: "song",
                  uid: "S14@3916",
                  title: "মানস, দেহ, গেহ, যো কিছু মোর"
                },
                {
                  type: "song",
                  uid: "S15@3917",
                  title: "‘অহং’-‘মম’-শব্দ-অর্থে যাহা কিছু হয়"
                },
                {
                  type: "song",
                  uid: "S12@3918",
                  title: "‘আমার’ বলিতে প্রভু! আর কিছু নাই"
                },
                {
                  type: "song",
                  uid: "S11@3919",
                  title: "বস্তুতঃ সকলি তব"
                },
                {
                  type: "song",
                  uid: "S8@3920",
                  title: "নিবেদন করি প্রভু! তোমার চরণে"
                },
                {
                  type: "song",
                  uid: "S16@3921",
                  title: "আত্মনিবেদন, তুয়া পদে করি’"
                }
              ]
            },
            {
              type: "collection",
              uid: "3@3922",
              title:
                "৩. গোপ্তৃত্বে বরণ ~ 3. Goptṛtve Varaṇa [Acceptance of the Lord as One’s Only Maintainer]",
              children: [
                {
                  type: "song",
                  uid: "S18@3923",
                  title: "কি জানি কি বলে"
                },
                {
                  type: "song",
                  uid: "S13@3924",
                  title: "দারা-পুত্র-নিজ-দেহ-কুটুম্ব-পালনে"
                },
                {
                  type: "song",
                  uid: "S10@3925",
                  title: "সর্ব্বস্ব তোমার"
                },
                {
                  type: "song",
                  uid: "S17@3926",
                  title: "তুমি সর্ব্বেশ্বরেশ্বর"
                }
              ]
            },
            {
              type: "collection",
              uid: "4@3927",
              title:
                "৪. বিশ্রম্ভাত্মিকা ~ 4. Viśrambhātmikā [Confidence in Śrī Kṛṣṇa’s Protection]",
              children: [
                {
                  type: "song",
                  uid: "S20@3928",
                  title: "এখন বুঝিনু প্রভু!"
                },
                {
                  type: "song",
                  uid: "S21@3929",
                  title: "তুমি ত’ মারিবে যা’রে, কে তা’রে রাখিতে পারে"
                },
                {
                  type: "song",
                  uid: "S27@3930",
                  title: "আত্মসমর্পণে গেলা অভিমান"
                },
                {
                  type: "song",
                  uid: "S26@3931",
                  title: "ছোড়ত পুরুষ-অভিমান"
                }
              ]
            },
            {
              type: "collection",
              uid: "5@3932",
              title: "৫. বর্জ্জনাত্মিকা ~ 5. Varjanātmikā [Rejection of the Unfavorable]",
              children: [
                {
                  type: "song",
                  uid: "S23@3933",
                  title: "কেশব! তুয়া জগত বিচিত্র"
                },
                {
                  type: "song",
                  uid: "S24@3934",
                  title: "তুয়া ভক্তি-প্রতিকূল ধর্ম্ম যা’তে রয়"
                },
                {
                  type: "song",
                  uid: "S25@3935",
                  title: "বিষয়বিমূঢ় আর মায়াবাদী জন"
                },
                {
                  type: "song",
                  uid: "S29@3936",
                  title: "আমি ত স্বানন্দ-সুখদবাসী"
                }
              ]
            },
            {
              type: "collection",
              uid: "6@3937",
              title: "৬. অনুকূল্যাত্মিকা ~ 6. Anukūlyātmika [Acceptance of the Favorable]",
              children: [
                {
                  type: "song",
                  uid: "S22@3938",
                  title: "তুয়া ভক্তি-অনুকূল যে-যে কার্য্য হয়"
                },
                {
                  type: "song",
                  uid: "S28@3939",
                  title: "গোদ্রুমধামে ভজন-অনুকূলে"
                },
                {
                  type: "song",
                  uid: "E1@3940",
                  title: "শুদ্ধভকত-চরণ-রেণু"
                },
                {
                  type: "song",
                  uid: "S30@3941",
                  title: "রাধাকুণ্ডতট"
                }
              ]
            },
            {
              type: "collection",
              uid: "7@3942",
              title: "ভজন-লালসা ~ Bhajana-lāslasā [Longing for Loving Service]",
              children: [
                {
                  type: "song",
                  uid: "U1@3943",
                  title: "হরি হে! প্রপঞ্চে পড়িয়া"
                },
                {
                  type: "song",
                  uid: "U2@3944",
                  title: "হরি হে! অর্থের সঞ্চয়ে"
                },
                {
                  type: "song",
                  uid: "U3@3945",
                  title: "হরি হে! ভজনে উৎসাহ"
                },
                {
                  type: "song",
                  uid: "U4@3946",
                  title: "হরি হে! দান-প্রতিগ্রহ"
                },
                {
                  type: "song",
                  uid: "U5@3947",
                  title: "হরি হে! সঙ্গদোষ-শূন্য"
                },
                {
                  type: "song",
                  uid: "U6@3948",
                  title: "হরি হে! নীরধর্ম্ম-গত"
                },
                {
                  type: "song",
                  uid: "V3@3949",
                  title: "ওহে! বৈষ্ণব ঠাকুর"
                },
                {
                  type: "song",
                  uid: "U7@3950",
                  title: "হরি হে! তোমারে ভুলিয়া"
                },
                {
                  type: "song",
                  uid: "U8@3951",
                  title: "হরি হে! শ্রীরূপগোসাঞি"
                },
                {
                  type: "song",
                  uid: "G7@3952",
                  title: "গুরুদেব! বড় কৃপা করি’"
                },
                {
                  type: "song",
                  uid: "G6@3953",
                  title: "গুরুদেব! কৃপাবিন্দু দিয়া"
                },
                {
                  type: "song",
                  uid: "G8@3954",
                  title: "গুরুদেব! কবে মোর সেই দিন হবে?"
                },
                {
                  type: "song",
                  uid: "G9@3955",
                  title: "গুরুদেব! কবে তব করুণা প্রকাশে"
                },
                {
                  type: "heading",
                  uid: "*@3956",
                  title: "সিদ্ধি-লালসা ~ Siddhi-lālasā (Hankering for Ultimate Perfection)"
                },
                {
                  type: "song",
                  uid: "D2@3957",
                  title: "কবে গৌর-বনে"
                },
                {
                  type: "song",
                  uid: "R25@3958",
                  title: "দেখিতে দেখিতে, ভুলিব বা কবে"
                },
                {
                  type: "song",
                  uid: "R21@3959",
                  title: "বৃষভানুসুতা-চরণ-সেবনে"
                },
                {
                  type: "heading",
                  uid: "*@3960",
                  title: "বিজ্ঞপ্তি ~ Vijñapti (An Entreaty)"
                },
                {
                  type: "song",
                  uid: "GN7@3961",
                  title: "কবে হ’বে বল"
                },
                {
                  type: "heading",
                  uid: "*@3962",
                  title: "শ্রীনাম-মাহাত্ম্য ~ Śrī Nāma-māhātmya (The Glories of Śrī Nāma)"
                },
                {
                  type: "song",
                  uid: "NM13@3963",
                  title: "কৃষ্ণনাম ধরে কত বল"
                }
              ]
            }
          ],
          count: 50
        },
        {
          type: "collection",
          uid: "GV@3964",
          title: "গীতাবলী ~ Śrī Gītāvalī",
          children: [
            {
              type: "heading",
              uid: "*@3965",
              title: "১ম সংস্করণ ইং ১৮৯৩ ~ published in 1893"
            },
            {
              type: "collection",
              uid: "1@3966",
              title: "অরুণোদয় কীর্ত্তন ~ Aruṇodaya-kīrtana",
              children: [
                {
                  type: "song",
                  uid: "NM11@3967",
                  title: "উদিল অরুণ পূরব ভাগে"
                },
                {
                  type: "song",
                  uid: "NM12@3968",
                  title: "জীব জাগ, জীব জাগ"
                }
              ]
            },
            {
              type: "collection",
              uid: "2@3969",
              title: "আরতি ~ Ārati",
              children: [
                {
                  type: "song",
                  uid: "A3@3970",
                  title: "ভালে গোরা-গদাধরের আরতি নেহারি"
                },
                {
                  type: "song",
                  uid: "A10@3971",
                  title: "শ্রীগৌর-আরতি"
                },
                {
                  type: "song",
                  uid: "A11@3972",
                  title: "শ্রীযুগল-আরতি"
                },
                {
                  type: "song",
                  uid: "A9@3973",
                  title: "ভজ ভকত-বৎসল শ্রীগৌরহরি"
                }
              ]
            },
            {
              type: "collection",
              uid: "3@3974",
              title: "প্রসাদ-সেবায় ~ Prasāda-sevāya",
              children: [
                {
                  type: "song",
                  uid: "PR1@3975",
                  title: "ভাইরে! শরীর অবিদ্যা-জাল"
                },
                {
                  type: "song",
                  uid: "PR2@3976",
                  title: "ভাইরে! একদিন শান্তিপুরে"
                },
                {
                  type: "song",
                  uid: "PR3@3977",
                  title: "ভাইরে! শচীর অঙ্গনে কভু"
                },
                {
                  type: "song",
                  uid: "PR4@3978",
                  title: "ভাইরে! শ্রীচৈতন্য-নিত্যানন্দ"
                },
                {
                  type: "song",
                  uid: "PR5@3979",
                  title: "ভাইরে! একদিন নীলাচলে"
                },
                {
                  type: "song",
                  uid: "PR6@3980",
                  title: "ভাইরে! রামকৃষ্ণ গোচারণে"
                }
              ]
            },
            {
              type: "collection",
              uid: "4@3981",
              title: "শ্রীনগর-কীর্ত্তন ~ Śrī Nagara-kīrtana",
              children: [
                {
                  type: "song",
                  uid: "NK25@3982",
                  title: "নদীয়া-গোদ্রুমে নিত্যানন্দ মহাজন"
                },
                {
                  type: "song",
                  uid: "NK27@3983",
                  title: "গায় গোরা মধুর স্বরে"
                },
                {
                  type: "song",
                  uid: "NK28@3984",
                  title: "একবার ভাব মনে"
                },
                {
                  type: "song",
                  uid: "NK29@3985",
                  title: "‘রাধাকৃষ্ণ’ বল্ বল্"
                },
                {
                  type: "song",
                  uid: "NK30@3986",
                  title: "গায় গোরাচাঁদ জীবের তরে"
                },
                {
                  type: "song",
                  uid: "NK31@3987",
                  title: "অঙ্গ-উপাঙ্গ-অস্ত্র-পার্ষদ-সঙ্গে"
                },
                {
                  type: "song",
                  uid: "NK32@3988",
                  title: "নিতাই কি নাম এনেছে রে"
                },
                {
                  type: "song",
                  uid: "NK33@3989",
                  title: "হরি ব’লে মোদের গৌর এলো"
                }
              ]
            },
            {
              type: "collection",
              uid: "5@3990",
              title: "শ্রীনাম-কীর্ত্তন ~ Śrī Nāma-kīrtana",
              children: [
                {
                  type: "song",
                  uid: "A8@3991",
                  title: "যশোমতী-নন্দন"
                },
                {
                  type: "song",
                  uid: "NK6@3992",
                  title: "‘দয়াল নিতাই চৈতন্য’ ব’লে নাচ্ রে আমার মন"
                },
                {
                  type: "song",
                  uid: "NK7@3993",
                  title: "‘হরি’ বল, ‘হরি’ বল, ‘হরি’ বল, ভাই রে"
                },
                {
                  type: "song",
                  uid: "NK8@3994",
                  title: "বোল হরি বোল (৩ বার)"
                },
                {
                  type: "song",
                  uid: "NK35@3995",
                  title: "হরি হরয়ে নমঃ কৃষ্ণ যাদবায় নমঃ"
                }
              ]
            },
            {
              type: "collection",
              uid: "6@3996",
              title: "শ্রেয়নির্ণয় ~ Śreya-nirṇaya",
              children: [
                {
                  type: "song",
                  uid: "S31@3997",
                  title: "কৃষ্ণভক্তি বিনা কভু নাহি ফলোদয়"
                },
                {
                  type: "song",
                  uid: "S32@3998",
                  title: "আর কেন মায়াজালে পড়িতেছ জীব মীন"
                },
                {
                  type: "song",
                  uid: "S33@3999",
                  title: "পীরিতি সচ্চিদানন্দে রূপবতী নারী"
                },
                {
                  type: "song",
                  uid: "S34@4000",
                  title: "‘নিরাকার নিরাকার’ করিয়া চীৎকার"
                },
                {
                  type: "song",
                  uid: "S36@4001",
                  title: "কেন আর কর দ্বেষ, বিদেশি-জন-ভজনে"
                },
                {
                  type: "song",
                  uid: "GP1@4002",
                  title: "ভজ রে ভজ রে আমার মন"
                },
                {
                  type: "song",
                  uid: "MS34@4003",
                  title: "ভাব না ভাব না, মন, তুমি অতি দুষ্ট"
                }
              ]
            },
            {
              type: "collection",
              uid: "7@4004",
              title: "শ্রীনামাষ্টক ~ Śrī Nāmāṣṭaka",
              children: [
                {
                  type: "song",
                  uid: "NM1@4005",
                  title: "শ্রীরূপ-বদনে"
                },
                {
                  type: "song",
                  uid: "NM2@4006",
                  title: "জয় জয় হরিনাম"
                },
                {
                  type: "song",
                  uid: "NM3@4007",
                  title: "বিশ্বে উদিত নাম-তপন"
                },
                {
                  type: "song",
                  uid: "NM4@4008",
                  title: "জ্ঞানী জ্ঞানযোগে করিয়া যতনে"
                },
                {
                  type: "song",
                  uid: "NM5@4009",
                  title: "হরিনাম তুয়া অনেক স্বরূপ"
                },
                {
                  type: "song",
                  uid: "NM6@4010",
                  title: "বাচ্য বাচক—এই দুই স্বরূপ তোমার"
                },
                {
                  type: "song",
                  uid: "NM7@4011",
                  title: "ওহে হরিনাম তব মহিমা অপার"
                },
                {
                  type: "song",
                  uid: "NM8@4012",
                  title: "নারদ মুনি বাজায় বীণা"
                }
              ]
            },
            {
              type: "collection",
              uid: "8@4013",
              title: "শ্রীরাধাষ্টক ~ Śrī Rādhāṣṭaka",
              children: [
                {
                  type: "song",
                  uid: "R13@4014",
                  title: "রাধিকাচরণ-পদ্ম"
                },
                {
                  type: "song",
                  uid: "R14@4015",
                  title: "বিরজার পারে, শুদ্ধপরব্যোম-ধাম"
                },
                {
                  type: "song",
                  uid: "R15@4016",
                  title: "রমণী-শিরোমণি, বৃষভানু-নন্দিনী"
                },
                {
                  type: "song",
                  uid: "R16@4017",
                  title: "রসিক-নাগরী-, গণ-শিরোমণি"
                },
                {
                  type: "song",
                  uid: "R17@4018",
                  title: "মহাভাব-চিন্তামণি, উদ্ভাবিত তনুখানি"
                },
                {
                  type: "song",
                  uid: "R18@4019",
                  title: "বরজ-বিপিনে"
                },
                {
                  type: "song",
                  uid: "R19@4020",
                  title: "শতকোটী গোপী"
                },
                {
                  type: "song",
                  uid: "R20@4021",
                  title: "রাধা-ভজনে যদি মতি নাহি ভেলা"
                }
              ]
            },
            {
              type: "collection",
              uid: "9@4022",
              title: "পরিশিষ্ট ~ Pariśiṣṭa",
              children: [
                {
                  type: "song",
                  uid: "NM14@4023",
                  title: "ভোজন-লালসে, রসনে আমার"
                }
              ]
            },
            {
              type: "collection",
              uid: "10@4024",
              title: "শ্রীমন্মহাপ্রভুর শতনাম ~ Śrīman Mahāprabhur Śatanāma",
              children: [
                {
                  type: "song",
                  uid: "NK21@4025",
                  title: "শ্রীমন্মহাপ্রভুর শতনাম"
                },
                {
                  type: "song",
                  uid: "NK11@4026",
                  title: "জয় গোদ্রুমপতি গোরা"
                },
                {
                  type: "song",
                  uid: "NK12@4027",
                  title: "কলিযুগপাবন বিশ্বম্ভর"
                },
                {
                  type: "song",
                  uid: "NK13@4028",
                  title: "কৃষ্ণচৈতন্য অদ্বৈত প্রভু নিত্যানন্দ"
                }
              ]
            },
            {
              type: "collection",
              uid: "11@4029",
              title: "শ্রীকৃষ্ণের বিংশোত্তরশতনাম ~ Śrī Kṛṣṇera Viṁśottara-śata-nāma",
              children: [
                {
                  type: "song",
                  uid: "NK22@4030",
                  title: "শ্রীকৃষ্ণের বিংশোত্তর-শতনাম"
                },
                {
                  type: "song",
                  uid: "NK15@4031",
                  title: "কৃষ্ণ গোবিন্দ হরে"
                },
                {
                  type: "song",
                  uid: "NK16@4032",
                  title: "রাধাবল্লভ মাধব শ্রীপতি মুকুন্দ"
                },
                {
                  type: "song",
                  uid: "NK17@4033",
                  title: "জয় রাধা-মাধব কুঞ্জবিহারী"
                },
                {
                  type: "song",
                  uid: "NK18@4034",
                  title: "রাধাবল্লভ রাধাবিনোদ"
                },
                {
                  type: "song",
                  uid: "NK19@4035",
                  title: "জয় যশোদা-নন্দন কৃষ্ণ"
                }
              ]
            },
            {
              type: "collection",
              uid: "12@4036",
              title: "শ্রীশিক্ষাষ্টক ~ Śrī Śikṣāṣṭaka",
              children: [
                {
                  type: "song",
                  uid: "SI1@4037",
                  title: "পীতবরণ কলি-পাবন গোরা"
                },
                {
                  type: "song",
                  uid: "SI2@4038",
                  title: "তুহুঁ দয়া-সাগর"
                },
                {
                  type: "song",
                  uid: "SI3@4039",
                  title: "শ্রীকৃষ্ণকীর্ত্তনে যদি মানস তোঁহার"
                },
                {
                  type: "song",
                  uid: "SI4@4040",
                  title: "প্রভু! তব পদযুগে মোর নিবেদন"
                },
                {
                  type: "song",
                  uid: "SI5@4041",
                  title: "অনাদি করম-ফলে"
                },
                {
                  type: "song",
                  uid: "SI6@4042",
                  title: "অপরাধ-ফলে মম"
                },
                {
                  type: "song",
                  uid: "SI7@4043",
                  title: "গাইতে গাইতে নাম"
                },
                {
                  type: "song",
                  uid: "SI8@4044",
                  title: "গাইতে গোবিন্দ নাম"
                },
                {
                  type: "song",
                  uid: "SI9@4045",
                  title: "বন্ধুগণ! শুনহ বচন মোর"
                },
                {
                  type: "song",
                  uid: "SI10@4046",
                  title: "যোগপীঠোপরি স্থিত"
                }
              ]
            }
          ],
          count: 69
        },
        {
          type: "collection",
          uid: "GM@4047",
          title: "গীতমালা ~ Śrī Gītamālā",
          children: [
            {
              type: "heading",
              uid: "*@4048",
              title: "১ম সংস্করণ ইং ১৮৯৩ ~ published in 1893"
            },
            {
              type: "collection",
              uid: "1@4049",
              title:
                "যামুন-ভাবাবলী (শান্ত-দাস্য-ভক্তিসাধন-লালসা) ~ The Ecstasies of Yamunācārya [Yearning to Practice Śānta- and Dāsya-bhakti]",
              children: [
                {
                  type: "song",
                  uid: "Y1@4050",
                  title: "হরি হে! ওহে প্রভু দয়াময়"
                },
                {
                  type: "song",
                  uid: "Y2@4051",
                  title: "হরি হে! তোমার ঈক্ষণে হয়"
                },
                {
                  type: "song",
                  uid: "Y3@4052",
                  title: "হরি হে! পরতত্ত্ব বিচক্ষণ"
                },
                {
                  type: "song",
                  uid: "Y4@4053",
                  title: "হরি হে! জগতের বস্তু যত"
                },
                {
                  type: "song",
                  uid: "Y5@4054",
                  title: "হরি হে! তুমি সর্ব্বগুণযুত"
                },
                {
                  type: "song",
                  uid: "Y6@4055",
                  title: "হরি হে! তোমার গম্ভীর মন"
                },
                {
                  type: "song",
                  uid: "Y7@4056",
                  title: "হরি হে! মায়াবদ্ধ যতক্ষণ"
                },
                {
                  type: "song",
                  uid: "Y8@4057",
                  title: "হরি হে! ধর্ম্মনিষ্ঠা নাহি মোর"
                },
                {
                  type: "song",
                  uid: "Y9@4058",
                  title: "হরি হে! হেন দুষ্ট কর্ম্ম নাই"
                },
                {
                  type: "song",
                  uid: "Y10@4059",
                  title: "হরি হে! নিজকর্ম্ম-দোষ-ফলে"
                },
                {
                  type: "song",
                  uid: "Y11@4060",
                  title: "হরি হে! অন্য আশা নাহি যা’র"
                },
                {
                  type: "song",
                  uid: "Y12@4061",
                  title: "হরি হে! তব পদ-পঙ্কজিনী"
                },
                {
                  type: "song",
                  uid: "Y13@4062",
                  title: "হরি হে! ভ্রমিতে সংসার-বনে"
                },
                {
                  type: "song",
                  uid: "Y14@4063",
                  title: "হরি হে! তোমার চরণপদ্ম"
                },
                {
                  type: "song",
                  uid: "Y15@4064",
                  title: "হরি হে! তবাঙ্ঘ্রি কমলদ্বয়"
                },
                {
                  type: "song",
                  uid: "Y16@4065",
                  title: "হরি হে! আমি সেই দুষ্টমতি"
                },
                {
                  type: "song",
                  uid: "Y17@4066",
                  title: "হরি হে! আমি অপরাধি-জন"
                },
                {
                  type: "song",
                  uid: "Y18@4067",
                  title: "হরি হে! অবিবেকরূপ ঘন"
                },
                {
                  type: "song",
                  uid: "Y19@4068",
                  title: "হরি হে! অগ্রে এক নিবেদন"
                },
                {
                  type: "song",
                  uid: "Y20@4069",
                  title: "হরি হে! তোমা ছাড়ি’ আমি কভু"
                },
                {
                  type: "song",
                  uid: "Y21@4070",
                  title: "হরি হে! স্ত্রী-পুরুষ-দেহগত"
                },
                {
                  type: "song",
                  uid: "Y22@4071",
                  title: "হরি হে! বেদবিধি-অনুসারে"
                },
                {
                  type: "song",
                  uid: "Y23@4072",
                  title: "হরি হে! তোমার যে শুদ্ধভক্ত"
                },
                {
                  type: "song",
                  uid: "Y24@4073",
                  title: "হরি হে! শুন হে মধুমথন!"
                },
                {
                  type: "song",
                  uid: "Y25@4074",
                  title: "হরি হে! আমি নরপশুপ্রায়"
                },
                {
                  type: "song",
                  uid: "Y26@4075",
                  title: "হরি হে! তুমি জগতের পিতা"
                },
                {
                  type: "song",
                  uid: "Y27@4076",
                  title: "হরি হে! আমি ত’ চঞ্চলমতি"
                }
              ]
            },
            {
              type: "collection",
              uid: "2@4077",
              title: "কার্পণ্য পঞ্জিকা",
              children: [
                {
                  type: "song",
                  uid: "RK30@4078",
                  title: "কার্পণ্য পঞ্জিকা"
                }
              ]
            },
            {
              type: "collection",
              uid: "3@4079",
              title: "শোক-শাতন ~ The Vanquisher of Bereavement",
              children: [
                {
                  type: "song",
                  uid: "GH64@4080",
                  title: "প্রদোষ-সময়ে, শ্রীবাস-অঙ্গনে"
                },
                {
                  type: "song",
                  uid: "GH65@4081",
                  title: "প্রবেশিয়া অন্তঃপুরে, নারীগণে শান্ত করে"
                },
                {
                  type: "song",
                  uid: "GH66@4082",
                  title: "ধন, জন, দেহ, গেহ কৃষ্ণে সমর্পণ"
                },
                {
                  type: "song",
                  uid: "GH67@4083",
                  title: "সবু মেলি’ বালক-ভাগ বিচারি’"
                },
                {
                  type: "song",
                  uid: "GH68@4084",
                  title: "শ্রীবাস-বচন, শ্রবণ করিয়া"
                },
                {
                  type: "song",
                  uid: "GH69@4085",
                  title: "প্রভুর বচন, শুনিয়া তখন"
                },
                {
                  type: "song",
                  uid: "GH70@4086",
                  title: "গোরাচাঁদের আজ্ঞা পে’য়ে, গৃহবাসিগণ"
                },
                {
                  type: "song",
                  uid: "GH71@4087",
                  title: "“পূর্ণচিদানন্দ তুমি, তোমার চিৎকণ আমি"
                },
                {
                  type: "song",
                  uid: "GH72@4088",
                  title: "“বাঁধিল মায়া যেদিন হ’তে"
                },
                {
                  type: "song",
                  uid: "GH73@4089",
                  title: "শ্রীবাসে কহেন প্রভু—“তুহুঁ মোর দাস"
                },
                {
                  type: "song",
                  uid: "GH74@4090",
                  title: "শ্রীবাসের প্রতি চৈতন্য-প্রসাদ"
                },
                {
                  type: "song",
                  uid: "GH75@4091",
                  title: "মৃত শিশু ল’য়ে তবে ভকতবৎসল"
                },
                {
                  type: "song",
                  uid: "GH76@4092",
                  title: "सुन्दर लाला शचीर-दुलाला"
                }
              ]
            },
            {
              type: "collection",
              uid: "4@4093",
              title: "শ্রীশ্রীরূপানুগ ভজন-দর্পণ",
              children: [
                {
                  type: "song",
                  uid: "RBD1@4094",
                  title: "শ্রীশ্রীরূপানুগ ভজন-দর্পণ ভূমিকা"
                },
                {
                  type: "song",
                  uid: "RBD2@4095",
                  title: "শ্রদ্ধাই ভক্তিলতার বীজ"
                },
                {
                  type: "song",
                  uid: "RBD3@4096",
                  title: "কৃষ্ণতক্তি"
                },
                {
                  type: "song",
                  uid: "RBD4@4097",
                  title: "শ্রদ্ধা দ্বিবিধা, অতএব সাধনভক্তিও দ্বিবিধা"
                },
                {
                  type: "song",
                  uid: "RBD5@4098",
                  title: "প্রাকৃতাপ্রাকৃত রসতত্ত্ব-জ্ঞানের আবশ্যকতা"
                },
                {
                  type: "song",
                  uid: "RBD6@4099",
                  title: "স্থায়িভাবই রসের মূল"
                },
                {
                  type: "song",
                  uid: "RBD7@4100",
                  title: "মধুর রসই সর্ব্বশ্রেষ্ঠ রস"
                },
                {
                  type: "song",
                  uid: "RBD8@4101",
                  title: "মধুরা রতির আবির্ভাব-হেতু"
                },
                {
                  type: "song",
                  uid: "RBD9@4102",
                  title: "মধুর-রতিরূপ স্থায়িভাবের উন্নতিক্রম"
                },
                {
                  type: "song",
                  uid: "RBD10@4103",
                  title: "বিভাব"
                },
                {
                  type: "song",
                  uid: "RBD11@4104",
                  title: "মধুর-রসে আলম্বনরূপ বিভাব"
                },
                {
                  type: "song",
                  uid: "RBD12@4105",
                  title: "নায়ক-শিরোমণি শ্রীকৃষ্ণের গুণ"
                },
                {
                  type: "song",
                  uid: "RBD13@4106",
                  title: "তদীয় বল্লভাগণ"
                },
                {
                  type: "song",
                  uid: "RBD14@4107",
                  title: "নায়িকাগণের অষ্ট অবস্থা-সেবা"
                },
                {
                  type: "song",
                  uid: "RBD15@4108",
                  title: "প্রধান-নায়িকা শ্রীমতী রাধিকার সখী-বর্ণন"
                },
                {
                  type: "song",
                  uid: "RBD16@4109",
                  title: "সখীর সাধারণ-সেবা"
                },
                {
                  type: "song",
                  uid: "RBD17@4110",
                  title: "নিত্যসিদ্ধা নহেন, এরূপ ‘সখী’র বিচার"
                },
                {
                  type: "song",
                  uid: "RBD18@4111",
                  title: "সর্ব্বসখীর পরস্পর ভাব"
                },
                {
                  type: "song",
                  uid: "RBD19@4112",
                  title: "ব্রজগত-মধুর-রতির উদ্দীপন"
                },
                {
                  type: "song",
                  uid: "RBD20@4113",
                  title: "অনুভাব"
                },
                {
                  type: "song",
                  uid: "RBD21@4114",
                  title: "সাত্ত্বিকভাব"
                },
                {
                  type: "song",
                  uid: "RBD22@4115",
                  title: "ব্যভিচারী বা সঞ্চারী ভাব"
                },
                {
                  type: "song",
                  uid: "RBD23@4116",
                  title: "ভাবাবস্থা-প্রাপ্ত স্থায়ী ভাবের উত্তর দশা"
                },
                {
                  type: "song",
                  uid: "RBD24@4117",
                  title: "সম্ভোগ-বিপ্রলম্ভ-ভেদে উজ্জ্বল-রস দ্বিবিধ; তন্মধ্যে বিপ্রলম্ভ"
                },
                {
                  type: "song",
                  uid: "RBD25@4118",
                  title: "সম্ভোগ"
                },
                {
                  type: "song",
                  uid: "RBD26@4119",
                  title: "সম্তোগের প্রকার"
                },
                {
                  type: "song",
                  uid: "RBD27@4120",
                  title: "উজ্জ্বলরসাশ্রিত-লীলা"
                },
                {
                  type: "song",
                  uid: "RBD28@4121",
                  title: "ব্রজলীলার সর্ব্বশ্রেষ্ঠতা"
                },
                {
                  type: "song",
                  uid: "RBD29@4122",
                  title: "অষ্টকাল বর্ণন"
                },
                {
                  type: "song",
                  uid: "RBD30@4123",
                  title: "অথ নিশান্ত লীলা"
                },
                {
                  type: "song",
                  uid: "RBD31@4124",
                  title: "অথ প্রাতলীলা"
                },
                {
                  type: "song",
                  uid: "RBD32@4125",
                  title: "অথ পূর্ব্বাহ্ণলীলা"
                },
                {
                  type: "song",
                  uid: "RBD33@4126",
                  title: "অথ মধ্যাহ্নলীলা"
                },
                {
                  type: "song",
                  uid: "RBD34@4127",
                  title: "অথ অপরাহ্ণলীলা"
                },
                {
                  type: "song",
                  uid: "RBD35@4128",
                  title: "অথ সায়ংলীলা"
                },
                {
                  type: "song",
                  uid: "RBD36@4129",
                  title: "অথ প্রদোষ লীলা"
                },
                {
                  type: "song",
                  uid: "RBD37@4130",
                  title: "অথ নক্তলীলা"
                },
                {
                  type: "song",
                  uid: "RBD38@4131",
                  title: "নিত্যলীলার ভেদ, পীঠ ও উপকরণ"
                },
                {
                  type: "song",
                  uid: "RBD39@4132",
                  title: "নিত্যলীলার ভেদ, পীঠ ও উপকরণ"
                },
                {
                  type: "song",
                  uid: "RBD40@4133",
                  title: "নিত্যলীলার ভেদ, পীঠ ও উপকরণ"
                }
              ]
            },
            {
              type: "collection",
              uid: "5@4134",
              title: "সিদ্ধি-লালসা",
              children: [
                {
                  type: "song",
                  uid: "D2@4135",
                  title: "কবে গৌর-বনে"
                },
                {
                  type: "song",
                  uid: "R25@4136",
                  title: "দেখিতে দেখিতে, ভুলিব বা কবে"
                },
                {
                  type: "song",
                  uid: "R26@4137",
                  title: "হেন কালে কবে, বিলাস মঞ্জরী"
                },
                {
                  type: "song",
                  uid: "R27@4138",
                  title: "পাল্যদাসী করি’"
                },
                {
                  type: "song",
                  uid: "R28@4139",
                  title: "চিন্তামণিময়"
                },
                {
                  type: "song",
                  uid: "R29@4140",
                  title: "নির্জ্জন কুটীরে, শ্রীরাধা-চরণ-"
                },
                {
                  type: "song",
                  uid: "R30@4141",
                  title: "শ্রীরূপ মঞ্জরী কবে মধুর বচনে"
                },
                {
                  type: "song",
                  uid: "R31@4142",
                  title: "বরণে তড়িৎ, বাস তারাবলী"
                },
                {
                  type: "song",
                  uid: "R21@4143",
                  title: "বৃষভানুসুতা-চরণ-সেবনে"
                },
                {
                  type: "song",
                  uid: "R22@4144",
                  title: "শ্রীকৃষ্ণবিরহে রাধিকার দশা"
                }
              ]
            }
          ],
          count: 91
        }
      ]
    },
    {
      type: "collection",
      uid: "NDT@4145",
      title: "শ্রীল নরোত্তম দাস ঠাকুর ~ Śrīla Narottama dāsa Ṭhākura",
      count: 61,
      children: [
        {
          type: "collection",
          uid: "PBC@4146",
          title: "শ্রীপ্রেমভক্তি-চন্দ্রিকা ~ Śrī Prema-bhakti-candrikā",
          children: [
            {
              type: "song",
              uid: "PBC1@4148",
              title: "শ্রীগুরু-চরণ-পদ্ম + বৈষ্ণব-চরণরেণু"
            },
            {
              type: "song",
              uid: "PBC2@4149",
              title: "অন্য অভিলাষ ছাড়ি’"
            },
            {
              type: "song",
              uid: "PBC3@4150",
              title: "তুমি ত দয়ার সিন্ধু"
            },
            {
              type: "song",
              uid: "PBC4@4151",
              title: "আন কথা আন ব্যথা"
            },
            {
              type: "song",
              uid: "PBC5@4152",
              title: "রাগের ভজন-পথ"
            },
            {
              type: "song",
              uid: "PBC6@4153",
              title: "যুগল-চরণ-প্রতি"
            },
            {
              type: "song",
              uid: "PBC7@4154",
              title: "রাধাকৃষ্ণ করোঁ ধ্যান"
            },
            {
              type: "song",
              uid: "PBC8@4155",
              title: "বচনের অগোচর, বৃন্দাবন ধামময়"
            },
            {
              type: "song",
              uid: "PBC9@4156",
              title: "আন কথা না শুনিব"
            }
          ],
          count: 9
        },
        {
          type: "collection",
          uid: "PRAR@4157",
          title: "প্রার্থনা ~ Prārthanā",
          children: [
            {
              type: "heading",
              uid: "*@4158",
              title: "লালসাময়ী ~ Filled with Longing"
            },
            {
              type: "song",
              uid: "GN9@4159",
              title: "‘গৌরাঙ্গ’ বলিতে হবে"
            },
            {
              type: "heading",
              uid: "*@4160",
              title: "সংপ্রার্থনাত্মিকা ~ Supplication"
            },
            {
              type: "song",
              uid: "RK12@4161",
              title: "রাধাকৃষ্ণ! নিবেদন এই জন করে"
            },
            {
              type: "heading",
              uid: "*@4162",
              title: "দৈন্যবোধিকা ~ Expressions of Humility"
            },
            {
              type: "song",
              uid: "GP15@4163",
              title: "হরি হরি! কি মোর করমগতি মন্দ"
            },
            {
              type: "song",
              uid: "GP13@4164",
              title: "হরি হরি! বিফলে জনম গোঙাইনু"
            },
            {
              type: "song",
              uid: "K55@4165",
              title: "প্রাণেশ্বর! নিবেদন এইজন করে"
            },
            {
              type: "song",
              uid: "K56@4166",
              title: "হরি হরি! কৃপা করি’ রাখ নিজ পদে"
            },
            {
              type: "song",
              uid: "GP14@4167",
              title: "হরি হরি! বড় শেল মরমে রহিল"
            },
            {
              type: "heading",
              uid: "*@4168",
              title: "দৈন্যবোধিকা প্রার্থনা ~ A Prayer of Humility"
            },
            {
              type: "song",
              uid: "K57@4169",
              title: "হরি হরি! কি মোর করম অভাগ"
            },
            {
              type: "heading",
              uid: "*@4170",
              title: "স্বাভীষ্ট লালসা ~ Longing for One’s Cherished Desire"
            },
            {
              type: "song",
              uid: "RK13@4171",
              title: "হরি হরি! হেন দিন হইবে আমার"
            },
            {
              type: "song",
              uid: "RK14@4172",
              title: "হরি হরি! কবে মোর হইবে সুদিনে"
            },
            {
              type: "song",
              uid: "RK15@4173",
              title: "গোবর্ধন গিরিবর, কেবল নির্জ্জন স্থল"
            },
            {
              type: "song",
              uid: "D18@4174",
              title: "হরি হরি! আর কি এমন দশা হব"
            },
            {
              type: "song",
              uid: "RK16@4175",
              title: "কুসুমিত বৃন্দাবনে, নাচত শিখিগণে"
            },
            {
              type: "heading",
              uid: "*@4176",
              title: "পুনঃ স্বাভীষ্ট-লালসা ~ Further Longing for One’s Cherished Desire"
            },
            {
              type: "song",
              uid: "RK17@4177",
              title: "হরি হরি! কবে মোর হইবে সুদিন"
            },
            {
              type: "heading",
              uid: "*@4178",
              title: "লালসা ~ Yearning"
            },
            {
              type: "song",
              uid: "GV44@4179",
              title: "শ্রীরূপমঞ্জরী-পদ"
            },
            {
              type: "song",
              uid: "GV45@4180",
              title: "শুনিয়াছি সাধুমুখে"
            },
            {
              type: "song",
              uid: "RK18@4181",
              title: "এই নব-দাসী বলি শ্রীরূপ চাহিবে"
            },
            {
              type: "song",
              uid: "RK19@4182",
              title: "শ্রীরূপ-পশ্চাতে আমি রহিব ভীত হঞা"
            },
            {
              type: "song",
              uid: "GV29@4183",
              title: "হা হা প্রভু লোকনাথ! রাখ পদদ্বন্দ্বে"
            },
            {
              type: "song",
              uid: "GV30@4184",
              title: "লোকনাথ প্রভু, তুমি দয়া কর মোরে"
            },
            {
              type: "song",
              uid: "RK20@4185",
              title: "হা হা প্রভু! কর দয়া করুণা তোমার"
            },
            {
              type: "song",
              uid: "RK21@4186",
              title: "হরি হরি! কবে হেন দশা হবে মোর"
            },
            {
              type: "song",
              uid: "GP8@4187",
              title: "জয় জয় শ্রীকৃষ্ণচৈতন্য-নিত্যানন্দ"
            },
            {
              type: "heading",
              uid: "*@4188",
              title: "সাধকদেহোচিত লালসা ~ Longing as a Sādhaka"
            },
            {
              type: "song",
              uid: "RK10@4189",
              title: "হরি হরি, কবে মোর হইবে সুদিন"
            },
            {
              type: "heading",
              uid: "*@4190",
              title: "সাধকদেহোচিত শ্রীবৃন্দাবনলালসা ~ Longing for Vṛndāvana as a Sādhaka"
            },
            {
              type: "song",
              uid: "D15@4191",
              title: "হরি হরি! আর কি এমন দশা হব"
            },
            {
              type: "song",
              uid: "D14@4192",
              title: "হরি হরি! আর কবে পালটিবে দশা"
            },
            {
              type: "song",
              uid: "D16@4193",
              title: "করঙ্গ কৌপীন লঞা"
            },
            {
              type: "song",
              uid: "D17@4194",
              title: "হরি হরি! কবে হব বৃন্দাবনবাসী"
            },
            {
              type: "heading",
              uid: "*@4195",
              title:
                "সবিলাপ শ্রীবৃন্দাবনবাস-লালসা ~ Filled with Lamentation, Yearning for Residence in Śrī Vṛndāvana"
            },
            {
              type: "song",
              uid: "D20@4196",
              title: "আর কি এমন দশা হব"
            },
            {
              type: "heading",
              uid: "*@4197",
              title:
                "মাথুরবিরহোচিত দর্শন-লালসা ~ Longing in Separation to See Kṛṣṇa Who Has Gone to Mathurā"
            },
            {
              type: "song",
              uid: "K65@4198",
              title: "কবে কৃষ্ণধন পাব"
            },
            {
              type: "song",
              uid: "K66@4199",
              title: "এইবার পাইলে দেখা চরণ দুখানি"
            },
            {
              type: "song",
              uid: "RK22@4200",
              title: "বৃন্দাবন রম্য-স্থান, দিব্য-চিন্তামণি-ধাম"
            },
            {
              type: "song",
              uid: "RK23@4201",
              title: "কদম্ব তরুর ডাল, নামিয়াছে ভূমে ভাল"
            },
            {
              type: "song",
              uid: "RK24@4202",
              title: "মৃগমদ-চন্দন"
            },
            {
              type: "heading",
              uid: "*@4203",
              title: "স্বনিষ্ঠা ~ My Deep Resolve"
            },
            {
              type: "song",
              uid: "GN12@4204",
              title: "ধন মোর নিত্যানন্দ পতি মোর গৌরচন্দ্র"
            },
            {
              type: "heading",
              uid: "*@4205",
              title: "নিত্যানন্দ-নিষ্ঠা ~ Firm Faith in Nityānanda"
            },
            {
              type: "song",
              uid: "N7@4206",
              title: "নিতাই-পদ-কমল"
            },
            {
              type: "heading",
              uid: "*@4207",
              title: "গৌরাঙ্গ-নিষ্ঠা ~ Firm Faith in Gaurāṅga"
            },
            {
              type: "song",
              uid: "GH52@4208",
              title: "আরে ভাই! ভজ মোর গৌরাঙ্গ-চরণ"
            },
            {
              type: "heading",
              uid: "*@4209",
              title: "সাবরণ-গৌরমহিমা ~ The Glories of Śrī Gaurāṅga"
            },
            {
              type: "song",
              uid: "GH40@4210",
              title: "গৌরাঙ্গের দু’টী পদ"
            },
            {
              type: "heading",
              uid: "*@4211",
              title: "পুনঃপ্রার্থনা ~ Praying Further"
            },
            {
              type: "song",
              uid: "GP9@4212",
              title: "শ্রীকৃষ্ণচৈতন্য প্রভু দয়া কর মোরে"
            },
            {
              type: "heading",
              uid: "*@4213",
              title:
                "সপার্ষদ-ভগবদ্বিরহজনিত-বিলাপ ~ Lamenting in Separation from the Lord and His Associates"
            },
            {
              type: "song",
              uid: "GV47@4214",
              title: "যে আনিল প্রেমধন"
            },
            {
              type: "heading",
              uid: "*@4215",
              title: "আক্ষেপ ~ Regret"
            },
            {
              type: "song",
              uid: "GH56@4216",
              title: "গোরা পঁহু না ভজিয়া মৈনু"
            },
            {
              type: "song",
              uid: "GP16@4217",
              title: "হরি হরি! কি মোর করম অনুরত"
            },
            {
              type: "heading",
              uid: "*@4218",
              title: "বৈষ্ণব-মহিমা ~ The Glories of the Vaiṣṇavas"
            },
            {
              type: "song",
              uid: "V2@4219",
              title: "ঠাকুর বৈষ্ণব-পদ"
            },
            {
              type: "heading",
              uid: "*@4220",
              title: "বৈষ্ণব-বিজ্ঞপ্তি ~ Entreaties to the Vaiṣṇavas"
            },
            {
              type: "song",
              uid: "V7@4221",
              title: "ঠাকুর বৈষ্ণবগণ"
            },
            {
              type: "song",
              uid: "V8@4222",
              title: "এইবার করুণা কর"
            },
            {
              type: "song",
              uid: "V9@4223",
              title: "কিরূপে পাইব সেবা"
            },
            {
              type: "heading",
              uid: "*@4224",
              title: "শ্রীরূপরতিমঞ্জর্য্যোঃ বিজ্ঞপ্তিঃ ~ An Entreaty to Śrī Rūpa and Rati Mañjarī"
            },
            {
              type: "song",
              uid: "RK25@4225",
              title: "রাধাকৃষ্ণ সেবোঁ মুঞি জীবনে মরণে"
            },
            {
              type: "heading",
              uid: "*@4226",
              title: "সখীবৃন্দে বিজ্ঞপ্তি ~ An Entreaty to the Sakhīs"
            },
            {
              type: "song",
              uid: "RK9@4227",
              title: "রাধাকৃষ্ণ প্রাণ মোর"
            },
            {
              type: "heading",
              uid: "*@4228",
              title:
                "সিদ্ধদেহেন শ্রীবৃন্দাবনেশ্বর্য্যাং সাক্ষাদ্বিজ্ঞপ্তিঃ ~ A Direct Appeal to the Lord of Vṛndāvana Within One’s Spiritually Perfected Body"
            },
            {
              type: "song",
              uid: "R32@4229",
              title: "প্রাণেশ্বরি! এইবার করুণা কর মোরে"
            },
            {
              type: "song",
              uid: "R33@4230",
              title: "প্রাণেশ্বরি! কবে মোরে হবে কৃপাদিঠি"
            },
            {
              type: "heading",
              uid: "*@4231",
              title: "শ্রীকৃষ্ণে বিজ্ঞপ্তিঃ ~ An Entreaty to Śrī Kṛṣṇa"
            },
            {
              type: "song",
              uid: "RK26@4232",
              title: "প্রভু হে, এইবার করহ করুণা"
            },
            {
              type: "song",
              uid: "RK27@4233",
              title: "আজি রসের বাদর নিশি"
            }
          ],
          count: 52
        }
      ]
    }
  ]
};
