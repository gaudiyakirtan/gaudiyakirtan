import React from "react";
import { Image } from "react-native";
import { StyleProvider, Root } from "native-base";
import * as Font from "expo-font";
import * as SplashScreen from "expo-splash-screen";
import { NavigationContainer, NavigationContainerRef } from "@react-navigation/native";

import Navigation from "./utils/navigation/Navigation";
import BottomTabNavigator from "./utils/navigation/Navigators";
import Theme from "./utils/theme/Theme";
import Settings from "./utils/settings/Settings";
import LocalFileSystem from "./utils/fileSystem/LocalFileSystem";
import { IAudioContext, AudioContextType } from "./context/audioContext/IAudioContext";
import AudioContext from "./context/audioContext/AudioContext";
import ISong from "./data/models/server/ISong";
import AudioMetaData from "./components/screens/audio/downloads/AudioMetaData";

interface IState {
  loadingResources: boolean;
  audioContext: IAudioContext;
}

export default class App extends React.Component<{}, IState> {
  // eslint-disable-next-line react/sort-comp
  private navigationContainerRef = React.createRef<NavigationContainerRef>();

  public constructor(props: {}) {
    super(props);
    this.state = {
      loadingResources: true,
      audioContext: new AudioContext(undefined, false, this.onAudioContextRecreate)
    };

    SplashScreen.preventAutoHideAsync();
  }

  public async componentDidMount(): Promise<void> {
    await this.init();

    this.setState({ loadingResources: false }, async () => {
      await SplashScreen.hideAsync();
    });
  }

  public async componentDidUpdate(): Promise<void> {
    Navigation.init(this.navigationContainerRef.current);
  }

  public render(): JSX.Element {
    if (this.state.loadingResources) {
      return <Image source={require("./images/app/splash.png")} style={{ resizeMode: "center" }} />;
    }

    return (
      <StyleProvider style={Theme.getThemeVariables()}>
        <AudioContextType.Provider value={this.state.audioContext}>
          <Root>
            <NavigationContainer ref={this.navigationContainerRef}>
              <BottomTabNavigator />
            </NavigationContainer>
          </Root>
        </AudioContextType.Provider>
      </StyleProvider>
    );
  }

  private async init(): Promise<void> {
    await this.loadAndCacheFonts();
    await LocalFileSystem.init();
    await Settings.init();
    await AudioMetaData.init();
  }

  private async loadAndCacheFonts(): Promise<void> {
    return Font.loadAsync({
      Roboto: require("../node_modules/native-base/Fonts/Roboto.ttf"),
      // eslint-disable-next-line @typescript-eslint/camelcase
      Roboto_medium: require("../node_modules/native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("../node_modules/native-base/Fonts/Ionicons.ttf")
    });
  }

  private onAudioContextRecreate = (song: ISong | undefined, audioBarOpen: boolean): void => {
    this.setState({
      audioContext: new AudioContext(song, audioBarOpen, this.onAudioContextRecreate)
    });
  };
}
