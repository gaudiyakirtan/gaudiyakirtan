import RouteName from "../../utils/navigation/RouteName";
import { IconFamily, IconSize } from "../lib/icon/Icon";

export interface IAppFooterButton {
  readonly iconName: string;
  readonly iconFamily?: IconFamily;
  readonly iconSize?: IconSize | number;
  readonly text?: string;
  readonly routeName: RouteName;
}

export interface IAppFooterConfig {
  buttons: IAppFooterButton[];
}

const AppFooterConfig: IAppFooterConfig = {
  buttons: [
    {
      iconName: "book-open-page-variant",
      iconFamily: "MaterialCommunityIcons",
      text: "Kirtans",
      routeName: RouteName.SongListNavigator
    },
    {
      iconName: "ios-folder-open",
      text: "Collections",
      routeName: RouteName.CollectionsNavigator
    },
    {
      iconName: "dot-circle",
      iconFamily: "FontAwesome5",
      text: "Song",
      routeName: RouteName.Song
    },
    {
      iconName: "md-musical-notes",
      text: "Audio",
      routeName: RouteName.AudioNavigator
    },
    {
      iconName: "ios-settings",
      text: "Settings",
      routeName: RouteName.SettingsNavigator
    }
  ]
};

export default AppFooterConfig;
