import React from "react";
import { StyleSheet, View } from "react-native";

import AppDimensions from "../../utils/appDimensions/AppDimensions";
import Navigation from "../../utils/navigation/Navigation";
import Text from "../lib/text/Text";
import Icon, { IconSize } from "../lib/icon/Icon";
import Theme from "../../utils/theme/Theme";
import ActiveScreenRefs from "../../utils/activeScreenRefs/ActiveScreenRefs";
import Touchable from "../lib/touchable/Touchable";
import AudioBar from "../audioBar/AudioBar";
import withAudioContext, { IAudioContextProps } from "../../context/audioContext/withAudioContext";
import RouteName from "../../utils/navigation/RouteName";
import Circle from "../lib/circle/Circle";
import Logo from "../lib/logo/Logo";
import { getOpenSongUid } from "../screens/song/Song";
import StatusBar from "../lib/statusBar/StatusBar";

import AppFooterConfig, { IAppFooterButton } from "./AppFooter.config";

const styles = StyleSheet.create({
  footer: {
    display: "flex",
    flexDirection: "row",
    height: AppDimensions.FooterHeight + StatusBar.bottomFooterNotchSpace()
  }
});

interface IProps extends IAudioContextProps {}

class AppFooter extends React.Component<IProps> {
  public componentDidMount(): void {
    ActiveScreenRefs.add("AppFooter", this);
  }

  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();
    const audioCtxt = this.props.audioContext;
    const audiobarExists = audioCtxt.audioExists() && audioCtxt.audioBarOpen;

    return (
      <View>
        {audiobarExists && audioCtxt.song && (
          <AudioBar song={audioCtxt.song} onClose={this.onAudioBarClose} key={audioCtxt.song.uid} />
        )}
        <View style={{ ...styles.footer, backgroundColor: theme.backgroundSecondary }}>
          {AppFooterConfig.buttons.map(appFooterButton => this.renderButton(appFooterButton))}
        </View>
      </View>
    );
  }

  private renderButton(appFooterButton: IAppFooterButton): JSX.Element {
    const currentRouteName = Navigation.getCurrentRouteName();

    const appFooterButtonChildRouteNames = Navigation.getChildRouteNames(appFooterButton.routeName);
    const isActive = Boolean(
      appFooterButton.routeName === currentRouteName ||
        (currentRouteName && appFooterButtonChildRouteNames.includes(currentRouteName))
    );

    const isSong = appFooterButton.routeName === RouteName.Song;

    const width = `${100 / AppFooterConfig.buttons.length}%`;
    const height = AppDimensions.FooterHeight;

    return (
      <Touchable
        onPress={() => this.onButtonPress(appFooterButton)}
        width={width}
        height={height}
        key={appFooterButton.routeName}
      >
        {isSong && this.renderLogoOrUidCircle(isActive, height)}
        {!isSong && (
          <>
            <Icon
              name={appFooterButton.iconName}
              family={appFooterButton.iconFamily || "Ionicons"}
              size={appFooterButton.iconSize || IconSize.Normal}
              type={isActive ? "highlight" : "lowlight"}
            />
            {appFooterButton.text && (
              <Text type={isActive ? "highlight" : "lowlight"} fontSize={11}>
                {appFooterButton.text}
              </Text>
            )}
          </>
        )}
      </Touchable>
    );
  }

  private renderLogoOrUidCircle(isActive: boolean, height: number): JSX.Element {
    const theme = Theme.getCurrentTheme();
    const songUid = getOpenSongUid();

    return (
      <View style={{ position: "relative", top: 0 }}>
        <Circle
          height={height - 5}
          width={height - 5}
          backgroundColor={isActive ? theme.backgroundTertiary : theme.backgroundSecondary}
          border={!isActive}
        >
          {songUid ? (
            <Text type={isActive ? "highlight" : "normal"} fontSize={isActive ? 16 : 14}>
              {songUid}
            </Text>
          ) : (
            <Logo grayScale={!isActive} />
          )}
        </Circle>
      </View>
    );
  }

  private onButtonPress(appFooterButton: IAppFooterButton): void {
    Navigation.navigate(appFooterButton.routeName);
  }

  private onAudioBarClose = (): void => {
    this.props.audioContext.reset();
  };
}

const appFooterWithContext = withAudioContext(AppFooter);
// eslint-disable-next-line import/prefer-default-export
export { appFooterWithContext as AppFooter };
