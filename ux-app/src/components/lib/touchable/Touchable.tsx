import React from "react";
import { StyleSheet, TouchableOpacity, ViewStyle } from "react-native";

import AppDimensions from "../../../utils/appDimensions/AppDimensions";
import Theme from "../../../utils/theme/Theme";

interface IProps {
  children?: React.ReactNode;
  width?: number | string;
  height?: number | string;
  noHorzCenter?: boolean;
  noVerCenter?: boolean;
  flexDirectionRow?: boolean;
  backgroundColor?: string;
  disabled?: boolean;
  displayBorder?: boolean;
  styles?: ViewStyle;
  onPress?: () => void;
}

// Adds touchable area (with visual feedback) around the child item and centers it in the area.
export default class Touchable extends React.Component<IProps> {
  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();
    const props = this.props;

    return (
      <TouchableOpacity
        onPress={props.onPress}
        disabled={props.disabled}
        style={{
          width: props.width || AppDimensions.TouchableWidth,
          height: props.height || AppDimensions.TouchableHeight,
          flexDirection: props.flexDirectionRow ? "row" : "column",
          alignItems: props.noHorzCenter ? undefined : "center",
          justifyContent: props.noVerCenter ? undefined : "center",
          backgroundColor: props.backgroundColor,
          borderWidth: props.displayBorder ? StyleSheet.hairlineWidth : undefined,
          borderColor: props.displayBorder ? theme.border : undefined,
          ...props.styles
        }}
        delayPressIn={0}
      >
        {props.children}
      </TouchableOpacity>
    );
  }
}
