import React from "react";

import IconAction from "../icon/IconAction";

import LinkingUtils from "./LinkingUtils";

interface IProps {
  email?: string;
  subject: string;
  body?: string;
}

export default class EmailAction extends React.Component<IProps> {
  public render(): JSX.Element {
    return (
      <IconAction name="email-edit" family="MaterialCommunityIcons" onPress={this.openEmail} />
    );
  }

  private openEmail = (): void => {
    LinkingUtils.openEmail(this.props.email, this.props.subject, this.props.body);
  };
}
