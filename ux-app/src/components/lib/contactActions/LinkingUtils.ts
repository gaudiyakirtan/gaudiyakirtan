import { Linking, Platform } from "react-native";

import Debug from "../../../utils/debug/Debug";
import Toast, { ToastType } from "../toast/Toast";

export default class LinkingUtils {
  public static openWebsite(url: string): void {
    this.openUrl(url);
  }

  public static openEmail(email?: string, subject?: string, body?: string): void {
    let url = `mailto:${email || ""}`;
    if (subject) {
      url += `?subject=${subject}`;
      if (body) {
        url += `&body=${body}`;
      }
    }
    this.openUrl(url);
  }

  public static makePhoneCall(phoneNumber: string): void {
    const phoneNumberNoDashes = phoneNumber.replace(/-/g, "");
    const domain = Platform.OS === "android" ? "tel" : "telprompt";
    const phoneUrl = `${domain}:${phoneNumberNoDashes}`;

    this.openUrl(phoneUrl);
  }

  public static openWhatsApp(phoneNumWithPlusCountryCode?: string, message?: string): void {
    let phoneNumberNoDashes = "";
    if (phoneNumWithPlusCountryCode) {
      const phoneNumber =
        Platform.OS === "ios"
          ? phoneNumWithPlusCountryCode.replace("+", "")
          : phoneNumWithPlusCountryCode;
      phoneNumberNoDashes = phoneNumber.replace(/-/g, "");
    }

    let url = `whatsapp://send`;
    if (phoneNumberNoDashes || message) {
      url += "?";
      if (phoneNumberNoDashes) {
        url += `phone=${phoneNumberNoDashes}`;
      }
      if (message) {
        url += `${phoneNumberNoDashes ? "&" : ""}text=${message}`;
      }
    }

    this.openUrl(url);
  }

  private static openUrl(url: string): void {
    Linking.canOpenURL(url)
      .then(supported => {
        if (supported) {
          Linking.openURL(url);
        } else {
          Debug.log(`Linking cannot open the url : ${url}`);
          this.showErrorToast();
        }
      })
      .catch(err => {
        Debug.log(`Linking cannot open the url : ${url}\n Error : ${JSON.stringify(err)}`);
        this.showErrorToast();
      });
  }

  private static showErrorToast(): void {
    Toast.show(ToastType.Error, "Oops! Couldn't open the link");
  }
}
