import React from "react";

import IconAction from "../icon/IconAction";

import LinkingUtils from "./LinkingUtils";

interface IProps {
  phoneNumber?: string;
  message?: string;
}

export default class WhatsAppAction extends React.Component<IProps> {
  public render(): JSX.Element {
    return <IconAction name="whatsapp" family="FontAwesome" onPress={this.openWhatsApp} />;
  }

  private openWhatsApp = (): void => {
    LinkingUtils.openWhatsApp(this.props.phoneNumber, this.props.message);
  };
}
