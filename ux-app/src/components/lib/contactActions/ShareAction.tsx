import React from "react";
import { Share } from "react-native";

import Debug from "../../../utils/debug/Debug";
import Toast, { ToastType } from "../toast/Toast";
import IconAction from "../icon/IconAction";

interface IProps {
  message: string;
}

export default class ShareAction extends React.Component<IProps> {
  public render(): JSX.Element {
    return <IconAction name="share" family="Entypo" onPress={this.share} />;
  }

  private share = async (): Promise<void> => {
    try {
      await Share.share({
        message: this.props.message
      });
    } catch (error) {
      Debug.log(`Failed to share the message  : ${this.props.message}`);
      Toast.show(ToastType.Error, "Oops! Couldn't open share");
    }
  };
}
