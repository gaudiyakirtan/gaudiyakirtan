import React from "react";

import IconAction from "../icon/IconAction";

import LinkingUtils from "./LinkingUtils";

interface IProps {
  phoneNumber: string;
}

export default class PhoneAction extends React.Component<IProps> {
  public render(): JSX.Element {
    return <IconAction name="phone-in-talk" family="MaterialIcons" onPress={this.makePhoneCall} />;
  }

  private makePhoneCall = (): void => {
    LinkingUtils.makePhoneCall(this.props.phoneNumber);
  };
}
