import React from "react";

import IconAction from "../icon/IconAction";

import LinkingUtils from "./LinkingUtils";

interface IProps {
  url: string;
}

export default class WebsiteAction extends React.Component<IProps> {
  public render(): JSX.Element {
    return <IconAction name="open-in-new" family="MaterialIcons" onPress={this.openWebsite} />;
  }

  private openWebsite = (): void => {
    LinkingUtils.openWebsite(this.props.url);
  };
}
