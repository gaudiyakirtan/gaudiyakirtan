/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import LinkingUtils from "../contactActions/LinkingUtils";
import Text, { ITextProps } from "../text/Text";

interface IProps {
  href: string;
  children: string | JSX.Element;
  textProps?: ITextProps;
}

export default class Anchor extends React.Component<IProps> {
  public render(): JSX.Element {
    const isString = typeof this.props.children === "string";
    return (
      <>
        {isString && (
          <Text type="highlight" {...this.props.textProps} onPress={this.onPress}>
            {this.props.children}
          </Text>
        )}
        {!isString && this.props.children}
      </>
    );
  }

  private onPress = (): void => {
    LinkingUtils.openWebsite(this.props.href);
  };
}
