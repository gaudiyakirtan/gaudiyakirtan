import React from "react";

import AppDimensions from "../../../utils/appDimensions/AppDimensions";
import List, { IListItem } from "../lists/list/List";
import ScrollView from "../scrollView/ScrollView";

export interface IRadioOption {
  key: number | string;
  title: string | JSX.Element;
  description?: string;
  leftContent?: JSX.Element;
  rightContent?: JSX.Element;
  noSelectionCheckmark?: boolean;
}

interface IProps {
  options: IRadioOption[];
  selectedKey?: number | string;
  height?: number;
  onOptionSelect(selectedKey: number | string): void;
}

export default class RadioList extends React.Component<IProps> {
  public render(): JSX.Element {
    const listItems = this.getListItems();
    return (
      <ScrollView styles={{ height: this.props.height || 450 }}>
        <List items={listItems} />
      </ScrollView>
    );
  }

  private getListItems(): IListItem[] {
    const listItems: IListItem[] = this.props.options.map(option => ({
      key: option.key,
      title: option.title,
      description: option.description,
      leftContent: option.leftContent,
      leftContentWidth: AppDimensions.PicThumbNailSize + 15,
      rightContent: option.rightContent,
      isSelected: option.key === this.props.selectedKey,
      noSelectionCheckmark: option.noSelectionCheckmark,
      onClick: this.onListitemClick
    }));
    return listItems;
  }

  private onListitemClick = (item: IListItem): void => {
    this.props.onOptionSelect(item.key);
  };
}
