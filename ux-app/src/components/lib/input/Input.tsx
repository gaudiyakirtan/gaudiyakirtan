import React from "react";
import ReactNative, { View } from "react-native";
import { Input as NativeBaseInput } from "native-base";

import Theme from "../../../utils/theme/Theme";
import Icon from "../icon/Icon";
import Touchable from "../touchable/Touchable";

interface IProps {
  placeholder: string;
  iconName: string;
  autoFocus?: boolean;
  onChangeText?: (newText?: string) => void;
  onIconPress?(): void;
  getRef?: React.Ref<ReactNative.TextInput>;
}

interface IState {
  text?: string;
}

export default class Input extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      text: undefined
    };
  }

  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();
    return (
      <View
        style={{
          height: 45,
          backgroundColor: theme.backgroundSecondary,
          borderRadius: 30,
          display: "flex",
          flexDirection: "row",
          alignItems: "center"
        }}
      >
        <View style={{ marginLeft: 10 }}>
          {this.props.onIconPress ? (
            <Touchable onPress={this.props.onIconPress} width={40}>
              <Icon name={this.props.iconName} size={26} type="highlight" />
            </Touchable>
          ) : (
            <Icon name={this.props.iconName} type="highlight" />
          )}
        </View>
        <NativeBaseInput
          value={this.state.text}
          onChangeText={this.onChangeText}
          placeholder={this.props.placeholder}
          placeholderTextColor={theme.textNormal}
          autoFocus={this.props.autoFocus}
          autoCorrect={false}
          style={{ color: theme.textNormal }}
          ref={this.props.getRef}
        />
        {Boolean(this.state.text) && (
          <Touchable onPress={this.clearText}>
            <Icon name="md-close" type="normal" />
          </Touchable>
        )}
      </View>
    );
  }

  private onChangeText = (newText?: string): void => {
    this.setState({ text: newText });
    if (this.props.onChangeText) {
      this.props.onChangeText(newText);
    }
  };

  private clearText = (): void => {
    this.onChangeText(undefined);
  };
}
