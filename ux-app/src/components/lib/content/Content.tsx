import React from "react";
import { View } from "react-native";

interface IProps {
  children?: React.ReactNode;
  marginTop?: number;
  marginBottom?: number;
  paddingLeft?: number;
  paddingRight?: number;
}

export default class Content extends React.Component<IProps> {
  public render(): JSX.Element {
    return (
      <View
        style={{
          flex: 1,
          marginTop: this.props.marginTop,
          marginBottom: this.props.marginBottom,
          // Padding (and not margin) on the right to keep the right scroll bar touching the edge of phone.
          paddingLeft: this.props.paddingLeft,
          paddingRight: this.props.paddingRight
        }}
      >
        {this.props.children}
      </View>
    );
  }
}
