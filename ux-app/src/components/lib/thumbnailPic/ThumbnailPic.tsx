import { Thumbnail } from "native-base";
import React from "react";
import {
  Image,
  ImageRequireSource,
  ImageStyle,
  ImageURISource,
  StyleSheet,
  TouchableOpacity,
  View
} from "react-native";

import AppDimensions from "../../../utils/appDimensions/AppDimensions";
import Theme from "../../../utils/theme/Theme";
import Circle from "../circle/Circle";
import Logo from "../logo/Logo";
import Text from "../text/Text";
import Modal from "../modal/Modal";
import Touchable from "../touchable/Touchable";

const styles = StyleSheet.create({
  modalTitle: {
    margin: 5,
    alignItems: "center"
  }
});

export interface IImageDimensions {
  width: number | string;
  height: number | string;
}

interface IProps {
  type: "thumbnail" | "image";
  imageSource: ImageURISource | ImageRequireSource | undefined;
  dimensions?: IImageDimensions;
  square?: boolean;
  modalHeight?: number;
  modalTitle?: string;
  noLogoWhenEmpty?: boolean;
}

interface IState {
  modalOpen: boolean;
}

export default class ThumbnailPic extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      modalOpen: false
    };
  }

  public render(): JSX.Element {
    return (
      <>
        {this.renderPic()}
        {this.props.type === "thumbnail" && this.state.modalOpen && this.props.imageSource && (
          <Modal
            open={this.state.modalOpen}
            mode="coverScreen"
            hasBackdrop={true}
            content={{
              noBorderRadius: true,
              margin: 30
            }}
            animationIn="zoomIn"
            animationOut="fadeOut"
            onClose={this.onModalClose}
          >
            <TouchableOpacity onPress={this.onModalClose}>
              <Image
                style={{ width: "auto", height: this.props.modalHeight || 300 }}
                source={this.props.imageSource}
                resizeMode="cover"
              />
              {this.props.modalTitle && (
                <View style={styles.modalTitle}>
                  <Text type="highlight" fontSize={16}>
                    {this.props.modalTitle}
                  </Text>
                </View>
              )}
            </TouchableOpacity>
          </Modal>
        )}
      </>
    );
  }

  private renderPic(): JSX.Element {
    const theme = Theme.getCurrentTheme();
    const imageSource = this.props.imageSource || 0;
    const square = Boolean(this.props.square);

    let displayLogo = !this.props.imageSource;
    if (this.props.noLogoWhenEmpty) {
      displayLogo = false;
    }

    if (this.props.type === "thumbnail") {
      const defaultSize = AppDimensions.PicThumbNailSize;
      const size = this.props.dimensions || { width: defaultSize, height: defaultSize };
      const thumbNailStyle: ImageStyle = square ? { ...size, borderRadius: 7 } : size;

      return displayLogo ? (
        <Touchable>
          <Circle backgroundColor={theme.backgroundTertiary}>
            <Logo width={size.width} height={size.height} />
          </Circle>
        </Touchable>
      ) : (
        <Touchable onPress={this.onThumbnailPress}>
          <Thumbnail style={thumbNailStyle} source={imageSource} square={square} />
        </Touchable>
      );
    }

    if (this.props.type === "image") {
      const size = this.props.dimensions || { width: undefined, height: undefined };
      return displayLogo ? (
        <Logo width={size.width} height={size.height} />
      ) : (
        <Image style={size} source={imageSource} resizeMode="cover" />
      );
    }

    return <></>;
  }

  private onThumbnailPress = (): void => {
    this.setState({ modalOpen: true });
  };

  private onModalClose = (): void => {
    this.setState({ modalOpen: false });
  };
}
