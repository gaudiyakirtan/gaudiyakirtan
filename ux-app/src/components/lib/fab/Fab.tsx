import React from "react";
import { Fab as NativeBaseFab } from "native-base";

import Icon from "../icon/Icon";
import Theme from "../../../utils/theme/Theme";

interface IProps {
  iconName: string;
  position?: "bottomLeft" | "bottomRight";
  onPress: () => void;
}

export default class Fab extends React.Component<IProps> {
  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();
    const position = this.props.position || "bottomRight";
    return (
      <NativeBaseFab
        position={position}
        onPress={this.props.onPress}
        style={{ backgroundColor: theme.controlHighlight, width: 40, height: 40 }}
        containerStyle={{
          bottom: 10,
          left: position === "bottomLeft" ? 0 : undefined,
          right: position === "bottomRight" ? 0 : undefined
        }}
      >
        <Icon name={this.props.iconName} type="onAccent" />
      </NativeBaseFab>
    );
  }
}
