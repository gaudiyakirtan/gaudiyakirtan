import React from "react";

import Center from "../center/Center";
import Text, { TextType } from "../text/Text";
import Icon, { IconType, IconSize } from "../icon/Icon";
import Button from "../button/Button";

interface IProps {
  text: string;
  textLine2?: string;
  type?: "error" | "info";
  iconName?: string;
  details?: string;
  retry?: boolean;
  onRetryPress?: () => void;
}

export default class ErrorScreen extends React.Component<IProps> {
  public render(): JSX.Element {
    const { textType, iconType, iconName } = this.getTextIconInfo();
    return (
      <Center hor={true} ver={true}>
        <>
          <Icon name={iconName} type={iconType} size={IconSize.VeryLarge} />

          <Text type={textType}>{this.props.text}</Text>
          {this.props.textLine2 && <Text type={textType}>{this.props.textLine2}</Text>}

          {this.props.retry && this.renderRetry()}

          {__DEV__ && this.props.details && <Text type="normal">{this.props.details}</Text>}
        </>
      </Center>
    );
  }

  private renderRetry(): JSX.Element {
    if (!this.props.onRetryPress) {
      return <></>;
    }
    return (
      <Button
        text="Retry"
        iconName="ios-refresh"
        onPress={this.props.onRetryPress}
        transparent={true}
      />
    );
  }

  private getTextIconInfo(): { textType: TextType; iconType: IconType; iconName: string } {
    const type = this.props.type || "error";

    let textType: TextType;
    let iconType: IconType;
    let iconName: string;
    switch (type) {
      case "error":
        textType = "error";
        iconType = "error";
        iconName = "ios-bonfire";
        break;
      case "info":
      default:
        textType = "highlight";
        iconType = "highlight";
        iconName = "ios-bonfire";
        break;
    }

    if (this.props.iconName) {
      iconName = this.props.iconName;
    }

    return { textType, iconType, iconName };
  }
}
