/* eslint-disable react/no-unused-state */
/* eslint-disable react/sort-comp */
import React from "react";
import { Thumbnail } from "native-base";
import { ImageRequireSource, View } from "react-native";

import RadioList, { IRadioOption } from "../radioList/RadioList";
import Settings from "../../../utils/settings/Settings";
import ISettings from "../../../data/models/local/ISettings";
import { Mathas, UserMatha } from "../../../utils/settings/Matha";
import DataTransform from "../../../data/DataTransform";
import Icon from "../icon/Icon";

interface IState {
  lastUpdateTime: number;
}

// Settings.get() should be the single source of truth for the latest value of settings for every component.
// Otherwise we run into race conditions with multiple copies of settings.
// So, don't store settings in local state, except for non-stored temporary state.
// Instead use a dummy time state variable to rerender.
export default class MathaList extends React.Component<{}, IState> {
  public constructor(props: {}) {
    super(props);
    this.state = {
      lastUpdateTime: Date.now()
    };
  }

  public render(): JSX.Element {
    return (
      <RadioList
        options={this.getRadioOptions()}
        selectedKey={Settings.get().userMatha}
        height={400}
        onOptionSelect={this.onOptionSelect}
      />
    );
  }

  private getRadioOptions(): IRadioOption[] {
    return Object.keys(Mathas).map(mathaName => {
      const mathaInfo = Mathas[mathaName];
      return {
        key: mathaName,
        title: DataTransform.transformMatha(mathaName),
        description: DataTransform.transformMatha(mathaInfo.founder),
        leftContent: this.renderLeftContent(mathaName, mathaInfo.pic),
        noSelectionCheckmark: true
      };
    });
  }

  private renderLeftContent(mathaName: string, imageSource: ImageRequireSource): JSX.Element {
    const isSelected = mathaName === Settings.get().userMatha;
    return (
      <View style={{ width: 50, height: 50 }}>
        <Thumbnail source={imageSource} style={{ width: 50, height: 50 }} />
        {isSelected && (
          <View style={{ position: "absolute", bottom: -8, right: -10 }}>
            <Icon name="check-circle" family="MaterialIcons" type="highlight" />
          </View>
        )}
      </View>
    );
  }

  private onOptionSelect = (selectedKey: number | string): void => {
    const currentSettings = Settings.get();
    this.updateSettings({ ...currentSettings, userMatha: selectedKey as UserMatha });
  };

  private updateSettings(newSettings: ISettings): void {
    Settings.set(newSettings);
    this.setState({ lastUpdateTime: Date.now() });
  }
}
