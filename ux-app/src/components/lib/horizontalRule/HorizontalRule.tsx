import React from "react";
import { StyleSheet, View } from "react-native";

import Theme from "../../../utils/theme/Theme";

interface IProps {
  noMargin?: boolean;
  marginTop?: number;
  marginBottom?: number;
  marginLeft?: number;
  marginRight?: number;
}

export default class HorizontalRule extends React.Component<IProps> {
  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();

    let marginTop = this.props.marginTop !== undefined ? this.props.marginTop : 15;
    let marginBottom = this.props.marginBottom !== undefined ? this.props.marginBottom : 15;

    if (this.props.noMargin) {
      marginTop = 0;
      marginBottom = 0;
    }

    return (
      <View
        style={{
          borderBottomColor: theme.border,
          borderBottomWidth: StyleSheet.hairlineWidth,
          marginTop,
          marginBottom,
          marginLeft: this.props.marginLeft,
          marginRight: this.props.marginRight
        }}
      />
    );
  }
}
