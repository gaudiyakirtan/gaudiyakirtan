import React from "react";
import { ScrollView as ReactNativeScrollView, StyleProp, ViewStyle } from "react-native";

import Settings from "../../../utils/settings/Settings";

interface IProps {
  persistentScrollbar?: boolean;
  children?: React.ReactNode;
  styles?: StyleProp<ViewStyle>;
}

export default class ScrollView extends React.Component<IProps> {
  public render(): JSX.Element {
    const isDarkTheme = Settings.get().isDarkTheme;
    return (
      <ReactNativeScrollView
        persistentScrollbar={this.props.persistentScrollbar} // android only
        indicatorStyle={isDarkTheme ? "white" : "black"} // ios only
        // scrollIndicatorInsets={{ top: 0, left: 0, bottom: 0, right: 5 }} // ios only
        // contentInset={{ top: 0, left: 0, bottom: 0, right: 5 }} // ios only
        contentContainerStyle={this.props.styles}
      >
        {this.props.children}
      </ReactNativeScrollView>
    );
  }
}
