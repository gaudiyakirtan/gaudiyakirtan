import React from "react";
import { Dimensions, StyleSheet, View } from "react-native";

import List, { IListItem, RecyclerListType } from "../list/List";
import Text from "../../text/Text";
import Touchable from "../../touchable/Touchable";
import AppDimensions from "../../../../utils/appDimensions/AppDimensions";
import Settings from "../../../../utils/settings/Settings";
import Theme from "../../../../utils/theme/Theme";
import { UserLanguage } from "../../../../utils/settings/UserLanguage";

/**
 * Available open source Alphabetic list libs :
 * --------------------------------------------
 * react-native-alphabet-sectionlist      : https://github.com/CoderJWYang/react-native-alphabet-sectionlist
 * react-native-alphabetlistview          : https://github.com/i6mi6/react-native-alphabetlistview
 * react-native-selectablesectionlistview : https://github.com/johanneslumpe/react-native-selectablesectionlistview
 *
 * But all the 3 of the above have the same performance (slow initial rendering and lag in scrolling) and
 * high memory usage issues mentioned in List.tsx as they are built on top of ScrollView or FlatList.
 *
 * So, in this file we built our own AlphabeticList on top of the performant 'RecyclerListView'.
 * And it is performant.
 */

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center"
  },
  alphabetSection: {
    flex: 1,
    flexDirection: "column"
  },
  headerFiller: {
    height: AppDimensions.ListHeaderHeight
  },
  alphabets: {
    flex: 1,
    flexDirection: "column",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "center"
  }
});

interface IAlphabet {
  text: string;
  itemsIndex: number; // Index to scroll to when this alphabet is clicked
}

interface IProps {
  items: IListItem[];
}

interface IState {
  alphabets: IAlphabet[];
  activeAlphabet: IAlphabet | undefined;
}

export default class AlphabeticList extends React.Component<IProps, IState> {
  private readonly eachAlphabetHeight = 25;
  private eachAlphabetWidth = 35;

  private recyclerListInstance: RecyclerListType | undefined;
  private isAutoScrolling: boolean;

  public constructor(props: IProps) {
    super(props);
    this.isAutoScrolling = false;

    this.state = {
      alphabets: this.getAlphabets(props.items),
      activeAlphabet: undefined
    };
  }

  public UNSAFE_componentWillReceiveProps(nextProps: IProps): void {
    this.setState({
      alphabets: this.getAlphabets(nextProps.items),
      activeAlphabet: undefined
    });
  }

  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();
    const { listWidth, alphabetSectionWidth, alphabetSectionHeight } = this.getDimensions();

    // Whenever 'userLanguage' setting is changed in settings and the list is rerendered,
    // currently selected alphabet doesn't make sense for the new list language.
    // So adding a key based on 'userLanguage' setting so that whenever it changes, the key changes and the entire
    // list component is destroyed and recreated again by React.
    const listKey = Settings.get().userLanguage;

    return (
      <View style={styles.container} key={listKey}>
        <View style={{ width: listWidth }}>
          <List
            items={this.props.items}
            width={listWidth}
            setRef={this.setRef}
            onScroll={this.onScroll}
          />
        </View>
        <View
          style={{
            width: alphabetSectionWidth,
            ...styles.alphabetSection
          }}
        >
          <View style={{ ...styles.headerFiller, backgroundColor: theme.backgroundSecondary }} />
          <View style={{ maxHeight: alphabetSectionHeight, ...styles.alphabets }}>
            {this.state.alphabets.map(alphabet => this.renderAlphabet(alphabet))}
          </View>
        </View>
      </View>
    );
  }

  private renderAlphabet(alphabet: IAlphabet): JSX.Element {
    const isActive = this.state.activeAlphabet && this.state.activeAlphabet.text === alphabet.text;
    return (
      <Touchable
        width={this.eachAlphabetWidth}
        height={this.eachAlphabetHeight}
        onPress={() => this.onAlphabetClick(alphabet)}
        key={alphabet.text}
      >
        <Text fontSize={isActive ? 22 : 16} type={isActive ? "highlight" : "normal"}>
          {alphabet.text.toLocaleUpperCase()}
        </Text>
      </Touchable>
    );
  }

  private getAlphabets = (items: IListItem[]): IAlphabet[] => {
    const isListLangEnglish = Settings.get().userLanguage === UserLanguage.eng;
    const alphabets: IAlphabet[] = [];
    items.forEach((item, index) => {
      if (item.isHeader && typeof item.title === "string") {
        // Hack to be removed after bengali translations complete
        if ((isListLangEnglish && item.title.match(/[a-z]/i)) || !isListLangEnglish) {
          alphabets.push({
            text: item.title,
            itemsIndex: index
          });
        }
      }
    });
    return alphabets;
  };

  // Below calculations are to span the alphabetSection either to one or more columns without overflowing
  private getDimensions(): {
    listWidth: number;
    alphabetSectionWidth: number;
    alphabetSectionHeight: number;
  } {
    const availableHeight =
      Dimensions.get("window").height -
      AppDimensions.HeaderHeight -
      AppDimensions.FooterHeight -
      AppDimensions.ListHeaderHeight; // headerFiller
    const requiredHeight = this.eachAlphabetHeight * this.state.alphabets.length;
    const numberOfColumns = Math.ceil(requiredHeight / availableHeight);

    this.eachAlphabetWidth = numberOfColumns > 1 ? 30 : 35;

    const alphabetSectionWidth = numberOfColumns * this.eachAlphabetWidth;
    const listWidth = Dimensions.get("window").width - alphabetSectionWidth;

    return { listWidth, alphabetSectionWidth, alphabetSectionHeight: availableHeight };
  }

  private setRef = (recyclerInstance: RecyclerListType): void => {
    this.recyclerListInstance = recyclerInstance;
  };

  private onAlphabetClick = (alphabet: IAlphabet): void => {
    if (this.recyclerListInstance) {
      this.isAutoScrolling = true;
      this.recyclerListInstance.scrollToIndex(alphabet.itemsIndex, true);
      this.setState({ activeAlphabet: alphabet }, () => {
        setTimeout(() => {
          this.isAutoScrolling = false;
        }, 3000);
      });
    }
  };

  private onScroll = (): void => {
    if (this.recyclerListInstance && !this.isAutoScrolling) {
      const firstVisibleIndex = this.recyclerListInstance.findApproxFirstVisibleIndex();

      let activeAlphabetIndex =
        this.state.alphabets.findIndex(a => a.itemsIndex > firstVisibleIndex) - 1;
      activeAlphabetIndex = activeAlphabetIndex < 0 ? 0 : activeAlphabetIndex;
      // eslint-disable-next-line react/no-access-state-in-setstate
      const activeAlphabet = this.state.alphabets[activeAlphabetIndex];

      this.setState({ activeAlphabet });
    }
  };
}
