/* eslint-disable no-param-reassign */
import React from "react";
import { Dimensions, StyleSheet, View } from "react-native";
import RecyclerListView, {
  RecyclerListViewProps,
  RecyclerListViewState
} from "recyclerlistview/dist/reactnative/core/RecyclerListView";
import DataProvider from "recyclerlistview/dist/reactnative/core/dependencies/DataProvider";
import {
  Dimension,
  LayoutProvider
} from "recyclerlistview/dist/reactnative/core/dependencies/LayoutProvider";
import StickyContainer from "recyclerlistview/dist/reactnative/core/StickyContainer";
import debounce from "lodash/debounce";

import Text from "../../text/Text";
import Icon from "../../icon/Icon";
import Theme from "../../../../utils/theme/Theme";
import AppDimensions from "../../../../utils/appDimensions/AppDimensions";
import RowLayout, { Body, Left, Right } from "../../rowLayout/RowLayout";
import Touchable from "../../touchable/Touchable";
import HorizontalRule from "../../horizontalRule/HorizontalRule";
import Settings from "../../../../utils/settings/Settings";

/**
 * ReactNative provided list options :
 * -----------------------------------
 *  1) ScrollView : https://reactnative.dev/docs/scrollview
 *  2) FlatList : https://reactnative.dev/docs/flatlist
 *        which is built on top of
 *     VirtualizedList : https://reactnative.dev/docs/virtualizedlist
 *  3) Simple list of divs
 *
 * But there are issues with all the above options :
 * -------------------------------------------------
 *  1) ScrollView renders all its react child components at once, so this has a huge performance and memory impact.
 *     Especially if there is a very long list of items, creating native views for everything all at once,
 *     most of which may not even be shown, will cause slow initial rendering (blank screen for 3-4 secs)
 *     as all elements have to be created and increases memory usage.
 *  3) Same initial slow rendering and high memory usage issue for simple list of divs.
 *
 *  So, we need a clever performant solution for large lists.
 *
 *  2) This is where VirtualizedList or FlatList comes into play.
 *     VirtualizedList/FlatList renders items lazily, when they are about to appear and removes items that
 *     scroll way off screen to save memory and processing time.
 *
 *     Comparision of ScrollView vs FlatList : https://reactnative.dev/blog/2017/03/13/better-list-views
 *
 * Issues with FlatList :
 * ----------------------
 * Even though the initial rendering and memory usage are good with FlatList, it has a serious lag issue
 * when scrolling through the list. Some references of this issue :
 *    https://github.com/facebook/react-native/issues/17555
 *    https://burakgozutok.com/why-you-shouldnt-use-react-natives-list-components/
 *
 * Even trying below perf optimizations didn't fix the slow scrolling issue :
 *    https://reactnative.dev/docs/optimizing-flatlist-configuration
 *
 * RecyclerListView :
 * ------------------
 * Fortunately, there's an open source performant list component available which uses clever cell recycling and
 * solves all the slow initial rendering, lag in scrolling and high memory usage issues. We are using this for our list.
 *    https://github.com/Flipkart/recyclerlistview
 *
 * Perf optimizations : https://github.com/Flipkart/recyclerlistview/tree/master/docs/guides/performance
 * ScrollView vs FlatList vs RecyclerListView : https://gist.github.com/vemarav/d3fbd9399bc1c3314a40ed87f8ba6877
 */

export type RecyclerListType = RecyclerListView<RecyclerListViewProps, RecyclerListViewState>;

export interface IListItem {
  key: number | string;
  isHeader?: boolean;
  title: string | JSX.Element;
  description?: string;
  leftContent?: JSX.Element;
  leftContentWidth?: number;
  rightContent?: JSX.Element;
  isSelected?: boolean;
  noSelectionCheckmark?: boolean;
  noHorzRule?: boolean;
  touchableDisabled?: boolean;
  metaData?: unknown;
  onClick?(item: IListItem): void;
}

enum LayoutType {
  HeaderLayout = "HeaderLayout",
  ListItemLayout = "ListItemLayout"
}

interface IProps {
  items: IListItem[];
  width?: number;
  setRef?(recyclerInstance: RecyclerListType): void;
  onScroll?(): void;
}

interface IState {
  dataProvider: DataProvider;
  headerIndices: number[];
}

export default class List extends React.Component<IProps, IState> {
  private readonly itemWidth = this.props.width || Dimensions.get("window").width;
  private readonly itemTotalHeight = AppDimensions.ListItemHeight + StyleSheet.hairlineWidth;
  private readonly headerTotalHeight = AppDimensions.ListHeaderHeight;

  private layoutProvider: LayoutProvider;

  public constructor(props: IProps) {
    super(props);

    const dataProvider = new DataProvider(
      // rowHasChanged()
      (row1: IListItem, row2: IListItem): boolean => {
        return row1.key !== row2.key || row1.isSelected !== row2.isSelected;
      },
      // getStableId()
      (index: number): string => {
        return this.props.items[index].key.toString();
      }
    );

    this.state = {
      dataProvider: dataProvider.cloneWithRows(this.props.items),
      headerIndices: this.getHeaderIndices(this.props.items)
    };

    this.layoutProvider = new LayoutProvider(
      // getLayoutTypeForIndex()
      (index: number): string | number => {
        return this.state.headerIndices.includes(index)
          ? LayoutType.HeaderLayout
          : LayoutType.ListItemLayout;
      },
      // setLayoutForType()
      (layoutType: string | number, dimension: Dimension): void => {
        // Having deterministic dimensions for items helps a lot in layout perf optimizations and recycling
        switch (layoutType) {
          case LayoutType.HeaderLayout:
            dimension.width = this.itemWidth;
            dimension.height = this.headerTotalHeight;
            break;
          case LayoutType.ListItemLayout:
          default:
            dimension.width = this.itemWidth;
            dimension.height = this.itemTotalHeight;
            break;
        }
      }
    );
  }

  public UNSAFE_componentWillReceiveProps(nextProps: IProps): void {
    this.setState(prevState => ({
      dataProvider: prevState.dataProvider.cloneWithRows(nextProps.items),
      headerIndices: this.getHeaderIndices(nextProps.items)
    }));
  }

  public render(): JSX.Element {
    const debouncedOnScroll = this.props.onScroll && debounce(this.props.onScroll, 100);

    // Since RecyclerListView caches layout objects for perf, whenever theme is changed in settings
    // and the list is rerendered, old theme colors are being shown in some parts.
    // So, adding a key based on theme so that whenever theme changes, the key changes and the entire
    // list component is destroyed and recreated again by React thereby showing correct new theme.
    const themeKey = Settings.get().isDarkTheme ? "darkTheme" : "lightTheme";

    return (
      /* @ts-ignore: Missing the following properties from type 'RecyclerChild': ref, props, type, key */
      <StickyContainer stickyHeaderIndices={this.state.headerIndices}>
        {/* @ts-ignore: Property 'ref' is missing in type 'Element' but required in type 'RecyclerChild' */}
        <RecyclerListView
          layoutProvider={this.layoutProvider}
          dataProvider={this.state.dataProvider}
          rowRenderer={this.renderRow}
          onScroll={debouncedOnScroll}
          ref={this.props.setRef}
          key={themeKey}
        />
      </StickyContainer>
    );
  }

  // TODO : Perf optimization - Move to a separate 'ListItem' component and use shouldComponentUpdate()
  // https://github.com/Flipkart/recyclerlistview/tree/master/docs/guides
  // /performance#4-ensure-shouldcomponentupdate-is-present
  private renderRow = (_type: string | number, data: IListItem, index: number): JSX.Element => {
    if (!data) {
      return <></>;
    }

    const item = data;
    const theme = Theme.getCurrentTheme();
    const isTitleString = typeof item.title === "string";
    const isItemBeforeHeader = this.state.headerIndices.includes(index + 1);
    const leftContentWidth = item.leftContentWidth !== undefined ? item.leftContentWidth : 50;

    return item.isHeader ? (
      <View
        style={{
          width: this.itemWidth,
          height: this.headerTotalHeight,
          backgroundColor: theme.backgroundSecondary,
          flex: 1,
          justifyContent: "center"
        }}
      >
        <RowLayout>
          <Body>
            {!isTitleString && item.title}
            {isTitleString && (
              <View style={{ marginLeft: 40 }}>
                <Text
                  type="highlight"
                  numberOfLines={1}
                  fontSize={(item.title as string).length === 1 ? 20 : 14}
                >
                  {item.title}
                </Text>
              </View>
            )}
          </Body>
          {item.rightContent && <Right>{item.rightContent}</Right>}
        </RowLayout>
      </View>
    ) : (
      <View style={{ height: this.itemTotalHeight }}>
        <Touchable
          onPress={() => this.onListItemClick(item)}
          styles={{
            width: this.itemWidth,
            height: AppDimensions.ListItemHeight
          }}
          disabled={item.touchableDisabled}
        >
          <RowLayout
            height={AppDimensions.ListItemHeight - 2 - 2}
            marginTop={2}
            marginBottom={2}
            backgroundColor={item.isSelected ? theme.backgroundTertiary : undefined}
            borderRadius={item.isSelected ? 15 : 0}
          >
            {item.leftContent && (
              <Left width={leftContentWidth}>
                {(!item.isSelected || (item.isSelected && item.noSelectionCheckmark)) &&
                  item.leftContent}
                {item.isSelected && !item.noSelectionCheckmark && (
                  <Icon
                    name="ios-checkmark-circle"
                    size={AppDimensions.PicThumbNailSize}
                    type="highlight"
                  />
                )}
              </Left>
            )}
            <Body>
              {!isTitleString && item.title}
              {isTitleString && (
                <View>
                  <Text
                    type={item.isSelected ? "highlight" : "normal"}
                    numberOfLines={1}
                    fontSize={14}
                  >
                    {item.title}
                  </Text>
                  {item.description && (
                    <Text numberOfLines={1} fontSize={12} type="lowlight">
                      {item.description}
                    </Text>
                  )}
                </View>
              )}
            </Body>
            {item.rightContent && <Right>{item.rightContent}</Right>}
          </RowLayout>
        </Touchable>
        {!item.noHorzRule && !isItemBeforeHeader && (
          <HorizontalRule noMargin={true} marginLeft={leftContentWidth} marginRight={5} />
        )}
      </View>
    );
  };

  private getHeaderIndices = (items: IListItem[]): number[] => {
    const indices: number[] = [];
    items.forEach((item, index) => {
      if (item.isHeader) {
        indices.push(index);
      }
    });

    return indices;
  };

  private onListItemClick = (item: IListItem): void => {
    if (item.onClick) {
      item.onClick(item);
    }
  };
}

export function addGroupCountInHeader(listItems: IListItem[]): void {
  const renderRightContent = (count: number): JSX.Element => (
    <View style={{ marginRight: 5 }}>
      <Text type="lowlight">{`(${count})`}</Text>
    </View>
  );

  let count = 0;
  for (let i = listItems.length - 1; i > 0; i--) {
    const listItem = listItems[i];
    if (listItem.isHeader) {
      listItem.rightContent = renderRightContent(count);
      count = 0;
    } else {
      count++;
    }
  }
  if (listItems[0].isHeader) {
    listItems[0].rightContent = renderRightContent(count);
  }
}
