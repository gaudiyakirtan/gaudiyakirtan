/* eslint-disable max-classes-per-file */
import React from "react";
import { StyleSheet, Dimensions, View } from "react-native";

interface IProps {
  children: React.ReactNode;
}

interface IRowProps extends IProps {
  height?: number | string;
  width?: number | string;
  marginTop?: number;
  marginBottom?: number;
  marginLeft?: number;
  backgroundColor?: string;
  borderRadius?: number;
}

interface ILeftProps extends IProps {
  width?: number;
  paddingLeft?: number;
}

interface IBodyProps extends IProps {
  noHorzCenter?: boolean;
  paddingLeft?: number;
  paddingRight?: number;
}

interface IRightProps extends IProps {
  flexDirection?: "row" | "column";
  alignItems?: "center" | "flex-start" | "flex-end";
  paddingRight?: number;
}

const styles = StyleSheet.create({
  row: {
    display: "flex",
    flexDirection: "row",
    maxWidth: Dimensions.get("screen").width
  },
  left: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  body: {
    flex: 1,
    flexWrap: "wrap",
    justifyContent: "center"
  },
  right: {
    marginLeft: "auto",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  }
});

export default class RowLayout extends React.Component<IRowProps> {
  public render(): JSX.Element {
    return (
      <View
        style={{
          ...styles.row,
          height: this.props.height,
          width: this.props.width,
          marginTop: this.props.marginTop,
          marginBottom: this.props.marginBottom,
          marginLeft: this.props.marginLeft,
          backgroundColor: this.props.backgroundColor,
          borderRadius: this.props.borderRadius
        }}
      >
        {this.props.children}
      </View>
    );
  }
}

export class Left extends React.Component<ILeftProps> {
  public render(): JSX.Element {
    return (
      <View
        style={{
          ...styles.left,
          width: this.props.width,
          paddingLeft: this.props.paddingLeft
        }}
      >
        {this.props.children}
      </View>
    );
  }
}

export class Body extends React.Component<IBodyProps> {
  public render(): JSX.Element {
    return (
      <View
        style={{
          ...styles.body,
          alignItems: this.props.noHorzCenter ? undefined : "center",
          paddingLeft: this.props.paddingLeft,
          paddingRight: this.props.paddingRight
        }}
      >
        {this.props.children}
      </View>
    );
  }
}

export class Right extends React.Component<IRightProps> {
  public render(): JSX.Element {
    return (
      <View
        style={{
          ...styles.right,
          flexDirection: this.props.flexDirection || "row",
          alignItems: this.props.alignItems || "center",
          paddingRight: this.props.paddingRight
        }}
      >
        {this.props.children}
      </View>
    );
  }
}
