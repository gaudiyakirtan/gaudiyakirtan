import React from "react";
import { Text as NativeBaseText } from "native-base";
import { TextProps as ReactNativeTextProps } from "react-native";

import Theme from "../../../utils/theme/Theme";
// import Color from "../../../utils/theme/Color";

export type TextType =
  | "highlight"
  | "normal"
  | "secondary"
  | "tertiary"
  | "lowlight"
  | "disabled"
  | "error"
  | "onAccent";

export interface ITextProps extends ReactNativeTextProps {
  type?: TextType;
  color?: string;
  textAlign?: "auto" | "left" | "right" | "center" | "justify";
  fontSize?: number;
  italic?: boolean;
  fontWeight?:
    | "normal"
    | "bold"
    | "100"
    | "200"
    | "300"
    | "400"
    | "500"
    | "600"
    | "700"
    | "800"
    | "900";
  onPress?: () => void;
}

export default class Text extends React.Component<ITextProps> {
  public render(): JSX.Element {
    const textType = this.props.type || "normal";
    const fontSize = this.props.fontSize || 14;
    const fontWeight = this.props.fontWeight || "normal";

    const color = this.props.color || this.getTextColor(textType);
    return (
      <NativeBaseText
        numberOfLines={this.props.numberOfLines}
        ellipsizeMode="tail"
        style={{
          color,
          fontSize,
          fontWeight,
          fontStyle: this.props.italic ? "italic" : "normal",
          textAlign: this.props.textAlign
        }}
        onPress={this.props.onPress}
      >
        {this.props.children}
      </NativeBaseText>
    );
  }

  private getTextColor(textType: TextType): string {
    const theme = Theme.getCurrentTheme();

    let color = theme.text;
    switch (textType) {
      case "highlight":
        color = theme.textHighlight;
        break;
      case "normal":
        color = theme.textNormal;
        break;
      case "secondary":
        color = theme.textSecondary;
        break;
      case "tertiary":
        color = theme.textTertiary;
        break;
      case "lowlight":
        color = theme.textLowlight;
        break;
      case "disabled":
        color = theme.textDisabled;
        break;
      case "error":
        color = theme.error;
        break;
      case "onAccent":
        color = theme.onAccent;
        break;
      default:
    }

    return color;
  }
}
