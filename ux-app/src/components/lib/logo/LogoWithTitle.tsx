import React from "react";

import LogoWithTitleDark from "../../../images/logo/logoWithTitle-dark.svg";
import LogoWithTitleLight from "../../../images/logo/logoWithTitle-light.svg";
import Settings from "../../../utils/settings/Settings";

interface IProps {
  width?: number | string;
  height?: number | string;
}

export default class LogoWithTitle extends React.Component<IProps> {
  public render(): JSX.Element {
    const isDarkTheme = Settings.get().isDarkTheme;
    const width = this.props.width || 250;
    const height = this.props.height || 250;

    return isDarkTheme ? (
      <LogoWithTitleLight width={width} height={height} />
    ) : (
      <LogoWithTitleDark width={width} height={height} />
    );
  }
}
