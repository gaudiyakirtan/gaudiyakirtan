import React from "react";

import LogoDark from "../../../images/logo/logo-dark.svg";
import LogoLight from "../../../images/logo/logo-light.svg";
import LogoLightGray from "../../../images/logo/logo-light-gray.svg";
import AppDimensions from "../../../utils/appDimensions/AppDimensions";
import Settings from "../../../utils/settings/Settings";

interface IProps {
  width?: number | string;
  height?: number | string;
  grayScale?: boolean;
}

export default class Logo extends React.Component<IProps> {
  public render(): JSX.Element {
    const isDarkTheme = Settings.get().isDarkTheme;

    const defaultSize = AppDimensions.PicThumbNailSize;
    const width = this.props.width || defaultSize;
    const height = this.props.height || defaultSize;

    let Component = LogoDark;
    if (this.props.grayScale) {
      Component = LogoLightGray;
    } else {
      Component = isDarkTheme ? LogoDark : LogoLight;
    }

    return <Component width={width} height={height} />;
  }
}
