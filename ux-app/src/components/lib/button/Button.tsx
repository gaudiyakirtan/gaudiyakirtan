import React from "react";
import { View, ViewStyle } from "react-native";

import Touchable from "../touchable/Touchable";
import Text from "../text/Text";
import Icon, { IconFamily, IconSize } from "../icon/Icon";
import Theme from "../../../utils/theme/Theme";

interface IProps {
  text: string;
  iconName?: string;
  iconFamily?: IconFamily;
  iconSize?: IconSize | number;
  iconOnRight?: boolean;
  iconAbsolutePosition?: boolean; // Useful when text position shouldn't move as icon appears/disappears
  disabled?: boolean;
  active?: boolean;
  transparent?: boolean;
  displayBorder?: boolean;
  width?: number | string;
  height?: number | string;
  defaultBackgroundColor?: string;
  touchableStyles?: ViewStyle;
  onPress: () => void;
}

/* eslint-disable no-nested-ternary */
export default class Button extends React.Component<IProps> {
  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();
    const props = this.props;
    const defaultBackgroundColor = props.defaultBackgroundColor || theme.backgroundSecondary;

    return (
      <Touchable
        onPress={props.onPress}
        flexDirectionRow={true}
        disabled={props.disabled}
        width={props.width}
        height={props.height}
        displayBorder={props.displayBorder}
        backgroundColor={
          props.transparent
            ? undefined
            : props.active
            ? theme.controlHighlightDark
            : defaultBackgroundColor
        }
        styles={props.touchableStyles}
      >
        {!props.iconOnRight && this.renderIcon()}
        <Text type={props.disabled ? "disabled" : props.active ? "onAccent" : "normal"}>
          {props.text}
        </Text>
        {props.iconOnRight && this.renderIcon()}
      </Touchable>
    );
  }

  private renderIcon(): JSX.Element {
    const props = this.props;
    if (!props.iconName) {
      return <></>;
    }

    const marginStyle: ViewStyle = props.iconOnRight ? { marginLeft: 0 } : { marginRight: 10 };
    const positionStyle: ViewStyle = props.iconAbsolutePosition
      ? {
          position: "absolute",
          left: props.iconOnRight ? undefined : 0,
          right: props.iconOnRight ? 0 : undefined
        }
      : {};

    return (
      <View style={{ ...marginStyle, ...positionStyle }}>
        <Icon
          name={props.iconName}
          family={props.iconFamily}
          type={props.disabled ? "disabled" : props.active ? "onAccent" : "highlight"}
          size={props.iconSize || IconSize.Medium}
        />
      </View>
    );
  }
}
