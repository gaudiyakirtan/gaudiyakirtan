import React from "react";
import { StyleSheet, View } from "react-native";

import Theme from "../../../utils/theme/Theme";
import Button from "../button/Button";
import { IconFamily, IconSize } from "../icon/Icon";

export type ButtonKey = string | number;

export interface IButtonOption {
  key: ButtonKey;
  text: string;
  iconName?: string;
  iconFamily?: IconFamily;
}

interface IProps {
  buttonOptions: IButtonOption[];
  selectedKey: ButtonKey;
  onButtonSelect(selectedKey: ButtonKey): void;
}

const borderRadius = 25;
const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius
  }
});

export default class MultiButtonSelector extends React.Component<IProps> {
  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();

    const buttonWidthPercent = 100 / this.props.buttonOptions.length;
    const lastIndex = this.props.buttonOptions.length - 1;

    return (
      <View style={{ ...styles.row, borderColor: theme.border }}>
        {this.props.buttonOptions.map((buttonOption, index) => {
          const isActive = buttonOption.key === this.props.selectedKey;
          return (
            <Button
              text={buttonOption.text}
              iconName={buttonOption.iconName}
              iconFamily={buttonOption.iconFamily}
              iconSize={IconSize.Small}
              iconAbsolutePosition={true}
              active={isActive}
              width={`${buttonWidthPercent}%`}
              height={45}
              defaultBackgroundColor={theme.backgroundSecondary}
              onPress={() => this.onButtonPress(buttonOption.key)}
              touchableStyles={{
                borderRightWidth: index !== lastIndex ? StyleSheet.hairlineWidth : undefined,
                borderRightColor: index !== lastIndex ? theme.border : undefined,
                borderTopLeftRadius: index === 0 ? borderRadius : 0,
                borderBottomLeftRadius: index === 0 ? borderRadius : 0,
                borderTopRightRadius: index === lastIndex ? borderRadius : 0,
                borderBottomRightRadius: index === lastIndex ? borderRadius : 0
              }}
              key={buttonOption.key}
            />
          );
        })}
      </View>
    );
  }

  private onButtonPress = (key: ButtonKey): void => {
    this.props.onButtonSelect(key);
  };
}
