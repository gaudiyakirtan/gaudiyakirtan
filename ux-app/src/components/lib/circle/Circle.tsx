import React from "react";
import { StyleSheet, View } from "react-native";

import AppDimensions from "../../../utils/appDimensions/AppDimensions";
import Theme from "../../../utils/theme/Theme";
import Text, { TextType } from "../text/Text";

interface IProps extends ITextProps, IJSXProps {
  backgroundColor?: string;
  border?: boolean;
  width?: number | string;
  height?: number | string;
}

interface ITextProps {
  text?: string;
  textType?: TextType;
  textColor?: string;
}

interface IJSXProps {
  children?: JSX.Element;
}

const styles = StyleSheet.create({
  circle: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
});

export default class Circle extends React.Component<IProps> {
  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();

    const defaultSize = AppDimensions.PicThumbNailSize;
    const width = this.props.width || defaultSize;
    const height = this.props.height || defaultSize;
    const borderWidth = this.props.border ? StyleSheet.hairlineWidth : 0;

    return (
      <View
        style={{
          width,
          height,
          borderRadius: 500,
          backgroundColor: this.props.backgroundColor || theme.backgroundSecondary,
          borderWidth,
          borderStyle: "solid",
          borderColor: theme.border,
          ...styles.circle
        }}
      >
        {this.props.text && (
          <Text
            fontSize={10}
            type={this.props.textType || "onAccent"}
            fontWeight="bold"
            color={this.props.textColor}
          >
            {this.props.text}
          </Text>
        )}
        {!this.props.text && this.props.children}
      </View>
    );
  }
}
