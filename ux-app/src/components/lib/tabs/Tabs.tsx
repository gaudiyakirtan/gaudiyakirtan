import debounce from "lodash/debounce";
import { Tab, Tabs as NativeBaseTabs } from "native-base";
import React from "react";
import { StyleSheet, View, ViewStyle } from "react-native";

import Theme from "../../../utils/theme/Theme";

export interface ITab {
  index: number;
  heading: string | JSX.Element;
  content: JSX.Element | JSX.Element[];
}

interface IProps {
  tabs: ITab[];
  activeIndex: number;
  tabBarPosition?: "top" | "bottom";
  tabContainerStyle?: ViewStyle;
  tabBarUnderlineStyle?: ViewStyle;
  scrollWithoutAnimation?: boolean;
  onTabChange(clickedTabIndex: number): void;
}

export default class Tabs extends React.Component<IProps> {
  public render(): JSX.Element {
    const props = this.props;
    const styles = this.getStyles();
    const tabContainerStyle = { ...styles.tabContainerStyle, ...props.tabContainerStyle };
    const tabBarUnderlineStyle = { ...styles.tabBarUnderlineStyle, ...props.tabBarUnderlineStyle };

    return (
      <NativeBaseTabs
        // Having below is causing an issue on ios to switch to wrong tab as onTabChange() is being called twice
        // Removed them as per : https://github.com/GeekyAnts/NativeBase/issues/3047
        // initialPage={this.props.activeIndex} // Android
        page={this.props.activeIndex} // ios
        // debouncing as onChangeTab is being called 4 times consecutively
        onChangeTab={debounce(this.onTabChange, 300)}
        tabContainerStyle={tabContainerStyle}
        tabBarUnderlineStyle={tabBarUnderlineStyle}
        tabBarPosition={this.props.tabBarPosition}
        locked={false}
        scrollWithoutAnimation={this.props.scrollWithoutAnimation}
      >
        {this.props.tabs.map(tab => this.renderTabHeading(tab))}
      </NativeBaseTabs>
    );
  }

  private renderTabHeading(tab: ITab): JSX.Element {
    const styles = this.getStyles();
    return (
      <Tab
        heading={tab.heading}
        tabStyle={styles.tabStyle}
        activeTabStyle={styles.activeTabStyle}
        textStyle={styles.textStyle}
        activeTextStyle={styles.activeTextStyle}
        key={tab.index}
      >
        {this.renderTabContent(tab.content)}
      </Tab>
    );
  }

  private renderTabContent(content: JSX.Element | JSX.Element[]): JSX.Element {
    const styles = this.getStyles();
    return <View style={styles.tabContent}>{content}</View>;
  }

  private onTabChange = (obj: { i: number; from: number }): void => {
    if (obj.i === obj.from) {
      return;
    }
    this.props.onTabChange(obj.i);
  };

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  private getStyles() {
    const theme = Theme.getCurrentTheme();

    return StyleSheet.create({
      // tabContainer
      tabContainerStyle: {
        height: "auto",
        flexWrap: "wrap",
        elevation: 0
      },
      tabBarUnderlineStyle: {
        height: 2,
        backgroundColor: theme.textHighlight
      },
      // tabHeading
      tabStyle: { backgroundColor: theme.backgroundSecondary },
      activeTabStyle: { backgroundColor: theme.backgroundSecondary },
      textStyle: { color: theme.textNormal },
      activeTextStyle: { color: theme.textHighlight },
      // tabContent
      tabContent: {
        flex: 1,
        backgroundColor: theme.backgroundPrimary
      }
    });
  }
}
