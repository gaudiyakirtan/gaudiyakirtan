import React from "react";
import { Switch as ReactNativeSwitch, Platform } from "react-native";

import Theme from "../../../utils/theme/Theme";
import Settings from "../../../utils/settings/Settings";
import ColorUtils from "../../../utils/theme/ColorUtils";

interface IProps {
  value: boolean;
  onValueChange: (newValue: boolean) => void;
}

export default class Switch extends React.Component<IProps> {
  public render(): JSX.Element {
    const colors = this.getColors();

    return (
      <ReactNativeSwitch
        value={this.props.value}
        onValueChange={this.props.onValueChange}
        thumbColor={colors.thumbColor}
        trackColor={colors.trackColor}
      />
    );
  }

  private getColors(): { thumbColor: string; trackColor: { true: string; false: string } } {
    const theme = Theme.getCurrentTheme();
    const isDarkTheme = Settings.get().isDarkTheme;
    const value = this.props.value;

    let thumbColor: string;
    let trackColor: { true: string; false: string };

    if (Platform.OS === "ios") {
      thumbColor = value
        ? ColorUtils.dim(theme.accent, isDarkTheme ? 0.8 : 1.6)
        : ColorUtils.dim(theme.accent, isDarkTheme ? 0.6 : 1.4);
      trackColor = {
        true: ColorUtils.dim(theme.accent, isDarkTheme ? 0.4 : 1.4),
        false: ColorUtils.dim(theme.accent, isDarkTheme ? 0.4 : 1.4)
      };
    } else {
      thumbColor = value ? theme.controlHighlightDark : theme.controlLowlight;
      trackColor = { true: theme.controlHighlight, false: theme.controlDisabled };
    }

    return { thumbColor, trackColor };
  }
}
