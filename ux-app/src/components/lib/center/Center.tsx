import React from "react";
import { View } from "react-native";

interface IProps {
  children?: React.ReactNode;
  hor?: boolean;
  ver?: boolean;
}

export default class Center extends React.Component<IProps> {
  public render(): JSX.Element {
    return (
      <View
        style={{
          flex: 1,
          alignItems: this.props.hor ? "center" : undefined,
          justifyContent: this.props.ver ? "center" : undefined
        }}
      >
        {this.props.children}
      </View>
    );
  }
}
