/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable max-classes-per-file */
import React from "react";
import { StyleSheet, View, ViewStyle } from "react-native";

import Text, { ITextProps } from "../text/Text";

const styles = StyleSheet.create({
  paragraph: {
    marginBottom: 10
  }
});

export const fontSize = 15;

interface IProps {
  children: React.ReactNode;
  textProps?: ITextProps;
  noTab?: boolean;
  style?: ViewStyle;
}

export default class Paragraph extends React.Component<IProps> {
  public render(): JSX.Element {
    return (
      <View style={{ ...styles.paragraph, ...this.props.style }}>
        <Text textAlign="justify" fontSize={fontSize} {...this.props.textProps}>
          {!this.props.noTab && "\t\t\t"}
          {this.props.children}
        </Text>
      </View>
    );
  }
}

// HW - HighlightWord. Named it short on purpose to be used in between paragraph text
interface IHWProps {
  children: string;
}

export class HW extends React.Component<IHWProps> {
  public render(): JSX.Element {
    return (
      <Text italic={true} type="highlight" fontSize={fontSize}>
        {this.props.children}
      </Text>
    );
  }
}

// I - Italic. Named it short on purpose to be used in between paragraph text
interface IHWProps {
  children: string;
}

export class I extends React.Component<IHWProps> {
  public render(): JSX.Element {
    return (
      <Text italic={true} fontSize={fontSize}>
        {this.props.children}
      </Text>
    );
  }
}
