import React from "react";
import ReactNativeModal, { Direction } from "react-native-modal";
import { Platform, StyleSheet, View } from "react-native";

import Theme from "../../../utils/theme/Theme";
import StatusBar from "../statusBar/StatusBar";

interface IProps {
  children: React.ReactNode;
  open: boolean;
  hasBackdrop: boolean;
  noBackdropColor?: boolean;
  mode?: "coverScreen" | "coverParent";
  swipeDirection?: Direction | Array<Direction>;
  propagateSwipe?: boolean;
  content?: {
    height?: number | "100%";
    width?: number | "100%";
    position?: { bottom?: number; top?: number; left?: number | "auto"; right?: number | "auto" };
    margin?: number;
    padding?: number;
    noBorderRadius?: boolean;
    showBottomBorderRadius?: boolean;
    backgroundColorPrimary?: boolean;
  };
  animationIn?: "zoomIn" | "fadeIn" | "slideInUp" | "fadeInRight";
  animationOut?: "zoomOut" | "fadeOut" | "slideOutDown";
  animationInTiming?: number;
  animationOutTiming?: number;
  onClose?: () => void;
}

/**
 * https://github.com/react-native-community/react-native-modal
 */
export default class Modal extends React.Component<IProps> {
  public componentWillUnmount(): void {
    if (this.props.onClose) {
      this.props.onClose();
    }
  }

  public render(): JSX.Element {
    const styles = this.getStyles();
    const theme = Theme.getCurrentTheme();
    const mode = this.props.mode || "coverParent";

    return (
      <ReactNativeModal
        isVisible={this.props.open}
        onBackdropPress={this.props.onClose}
        onBackButtonPress={this.props.onClose}
        coverScreen={mode === "coverScreen"}
        hasBackdrop={this.props.hasBackdrop}
        backdropOpacity={0.5}
        animationIn={this.props.animationIn} // Library's default is "slideInUp"
        animationOut={this.props.animationOut} // Library's default is "slideOutDown"
        animationInTiming={this.props.animationInTiming} // Set this to 1ms to not have visible animation
        animationOutTiming={this.props.animationOutTiming} // Set this to 1ms to not have visible animation
        swipeDirection={this.props.swipeDirection}
        propagateSwipe={this.props.propagateSwipe}
        onSwipeComplete={this.props.onClose}
        backdropColor={this.props.noBackdropColor ? "transparent" : theme.modalBackdrop}
        backdropTransitionOutTiming={0}
        hideModalContentWhileAnimating={true}
        style={styles.modalContainer}
      >
        <View style={styles.modalContent}>{this.props.children}</View>
      </ReactNativeModal>
    );
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private getStyles(): any {
    const theme = Theme.getCurrentTheme();

    const contentProps = this.props.content || {};
    const positionProps = contentProps.position || {};

    return StyleSheet.create({
      /**
       * Overlay :
       *    1) If coverScreen=true (i.e mode === "coverScreen"), modalContainer overlays entire screen.
       *    2) Else, it overlays only its parent.
       *    Based on this, position bottom of 'modalContent' below is relative to either entire screen or parent.
       * Backdrop Transparency :
       *    1) If hasBackdrop=true, not transparent.
       *    2) Else transparent
       *    But for "coverScreen" mode, click through does not work for the transparent area.
       *    It works for default "coverParent" mode though.
       */
      modalContainer: {
        margin: 0,
        marginTop:
          Platform.OS === "ios" && this.props.mode === "coverScreen" ? StatusBar.height() : 0
        // Use for debugging : borderColor: "red", borderWidth: 2
      },
      /**
       * Modal content can be positioned inside modalContainer using 'position: absolute' and 'bottom/top/left/right'.
       * If no 'position' prop is provided, by default modal content is displayed in the center of the modalContainer.
       * But by using the 'position' prop, modal content can be positioned at any desired vertical/horizintal position.
       */
      modalContent: {
        height: contentProps.height,
        width: contentProps.width,
        position: "absolute",
        bottom: positionProps.bottom,
        top: positionProps.top,
        left: positionProps.left || 0, //  To occupy the entire
        right: positionProps.right || 0, // available width
        margin: contentProps.margin || 0,
        padding: contentProps.padding || 0,
        borderTopLeftRadius: contentProps.noBorderRadius ? undefined : 15,
        borderTopRightRadius: contentProps.noBorderRadius ? undefined : 15,
        borderBottomLeftRadius: contentProps.showBottomBorderRadius ? 15 : 0,
        borderBottomRightRadius: contentProps.showBottomBorderRadius ? 15 : 0,
        backgroundColor: contentProps.backgroundColorPrimary
          ? theme.backgroundPrimary
          : theme.backgroundSecondary
      }
    });
  }
}
