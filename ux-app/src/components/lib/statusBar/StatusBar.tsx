import React from "react";
import { StatusBar as RNStatusBar } from "react-native";
import { getStatusBarHeight } from "react-native-status-bar-height";
import { getBottomSpace } from "react-native-iphone-x-helper";

import Theme from "../../../utils/theme/Theme";
import Settings from "../../../utils/settings/Settings";

export default class StatusBar extends React.Component {
  public static height(): number {
    return getStatusBarHeight();
  }

  public static setHidden(hidden: boolean): void {
    RNStatusBar.setHidden(hidden);
  }

  public static bottomFooterNotchSpace(): number {
    return getBottomSpace();
  }

  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();
    const isDarkTheme = Settings.get().isDarkTheme;

    return (
      <RNStatusBar
        backgroundColor={theme.backgroundPrimary}
        barStyle={isDarkTheme ? "light-content" : "dark-content"}
        translucent={true} // Otherwise some space above on android
      />
    );
  }
}
