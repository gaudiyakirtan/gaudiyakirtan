import debounce from "lodash/debounce";
import React from "react";
import { Dimensions, GestureResponderEvent, View } from "react-native";

import Theme from "../../../utils/theme/Theme";

interface IProps {
  children: React.ReactNode;
  onLeftHalfClick?(): void;
  onRightHalfClick?(): void;
  onTopHalfClick?(): void;
  onBottomHalfClick?(): void;
}

export default class ClickAreaDetector extends React.Component<IProps> {
  // Debouncing to capture double cliks as only single clicks.
  // Otherwise double clicks are causing infinite scrolling in <Tabs />
  private onLeftHalfClickDebounced?: () => void;
  private onRightHalfClickDebounced?: () => void;
  private onTopHalfClickDebounced?: () => void;
  private onBottomHalfClickDebounced?: () => void;

  public render(): JSX.Element {
    this.onLeftHalfClickDebounced =
      this.props.onLeftHalfClick && debounce(this.props.onLeftHalfClick, 300);
    this.onRightHalfClickDebounced =
      this.props.onRightHalfClick && debounce(this.props.onRightHalfClick, 300);
    this.onTopHalfClickDebounced =
      this.props.onTopHalfClick && debounce(this.props.onTopHalfClick, 300);
    this.onBottomHalfClickDebounced =
      this.props.onBottomHalfClick && debounce(this.props.onBottomHalfClick, 300);

    const theme = Theme.getCurrentTheme();
    // Without some property like bgcolor, view is not occupying height 100%
    return (
      <View
        onTouchEnd={this.onTouchStart}
        style={{ flex: 1, height: "100%", backgroundColor: theme.backgroundPrimary }}
      >
        {this.props.children}
      </View>
    );
  }

  private onTouchStart = (event: GestureResponderEvent): void => {
    const screenWidth = Dimensions.get("window").width;
    const pageX = event.nativeEvent.pageX;

    if (pageX < screenWidth / 2) {
      if (this.onLeftHalfClickDebounced) {
        this.onLeftHalfClickDebounced();
      }
    }
    if (pageX >= screenWidth / 2) {
      if (this.onRightHalfClickDebounced) {
        this.onRightHalfClickDebounced();
      }
    }

    const screenHeight = Dimensions.get("window").height;
    const pageY = event.nativeEvent.pageY;

    if (pageY < screenHeight / 2) {
      if (this.onTopHalfClickDebounced) {
        this.onTopHalfClickDebounced();
      }
    }
    if (pageY >= screenHeight / 2) {
      if (this.onBottomHalfClickDebounced) {
        this.onBottomHalfClickDebounced();
      }
    }
  };
}
