/* eslint-disable react/no-unused-state */
/* eslint-disable react/sort-comp */
import React from "react";

import Circle from "../circle/Circle";
import Text from "../text/Text";
import RadioList, { IRadioOption } from "../radioList/RadioList";
import Theme from "../../../utils/theme/Theme";
import {
  getLanguageTitle,
  UserLanguage,
  UserLanguagesInfo
} from "../../../utils/settings/UserLanguage";
import Settings from "../../../utils/settings/Settings";
import ISettings from "../../../data/models/local/ISettings";

interface IState {
  lastUpdateTime: number;
}

// Settings.get() should be the single source of truth for the latest value of settings for every component.
// Otherwise we run into race conditions with multiple copies of settings.
// So, don't store settings in local state, except for non-stored temporary state.
// Instead use a dummy time state variable to rerender.
export default class LanguageList extends React.Component<{}, IState> {
  public constructor(props: {}) {
    super(props);
    this.state = {
      lastUpdateTime: Date.now()
    };
  }

  public render(): JSX.Element {
    return (
      <RadioList
        options={this.getRadioOptions()}
        selectedKey={Settings.get().userLanguage}
        onOptionSelect={this.onOptionSelect}
      />
    );
  }

  private getRadioOptions(): IRadioOption[] {
    return Object.keys(UserLanguagesInfo).map(langCode => {
      const langInfo = UserLanguagesInfo[langCode];
      return {
        key: langCode,
        title: getLanguageTitle(langCode as UserLanguage),
        leftContent: this.renderLeftContent(langInfo.firstAlphabet)
      };
    });
  }

  private renderLeftContent(alphabet: string): JSX.Element {
    const theme = Theme.getCurrentTheme();
    return (
      <Circle backgroundColor={theme.backgroundPrimary}>
        <Text type="highlight" fontSize={18}>
          {alphabet}
        </Text>
      </Circle>
    );
  }

  private onOptionSelect = (selectedKey: number | string): void => {
    const currentSettings = Settings.get();
    this.updateSettings({ ...currentSettings, userLanguage: selectedKey as UserLanguage });
  };

  private updateSettings(newSettings: ISettings): void {
    Settings.set(newSettings);
    this.setState({ lastUpdateTime: Date.now() });
  }
}
