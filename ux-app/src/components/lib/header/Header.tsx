import React from "react";
import ReactNative, { StyleSheet, View } from "react-native";
import debounce from "lodash/debounce";

import Navigation from "../../../utils/navigation/Navigation";
import Icon from "../icon/Icon";
import Text, { TextType } from "../text/Text";
import Theme from "../../../utils/theme/Theme";
import Input from "../input/Input";
import Touchable from "../touchable/Touchable";
import RowLayout, { Left, Body, Right } from "../rowLayout/RowLayout";
import AppDimensions from "../../../utils/appDimensions/AppDimensions";
import StatusBar from "../statusBar/StatusBar";

type HeaderType = "title" | "search";

const styles = StyleSheet.create({
  container: {
    zIndex: 1000 // For sticky headers of List to not show above header when scrolling the list up
  },
  titleSubtitle: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    width: "100%"
  }
});

interface IProps {
  headerType?: HeaderType;
  title: string;
  subTitle?: string;
  textType?: TextType;
  showBackButton?: boolean;
  leftContent?: JSX.Element;
  rightContent?: JSX.Element;
  noMarginTop?: boolean;
  marginBottom?: number;
  onBackButtonPress?(): void;
  onSearchTextChange?: (newText?: string) => void;
  autoFocusSearchBox?: boolean;
  getSearchInputRef?: React.Ref<ReactNative.TextInput>;
}

export default class Header extends React.Component<IProps> {
  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();
    const props = this.props;

    const headerType = props.headerType || "title";
    const fontSize = props.subTitle ? 16 : 18;
    const textType = props.textType || "normal";
    const marginTop = props.noMarginTop ? 0 : StatusBar.height();
    const marginBottom = props.marginBottom || 0;
    const padding = props.headerType === "search" ? 3 : 10;
    const backgroundColor =
      props.headerType === "search" ? theme.backgroundPrimary : theme.backgroundSecondary;
    const onBackButtonPress = this.props.onBackButtonPress || this.onBackButtonPress;

    return (
      <View style={{ marginTop, marginBottom, ...styles.container }}>
        <RowLayout height={AppDimensions.HeaderHeight} backgroundColor={backgroundColor}>
          <Left>
            {this.props.showBackButton && (
              <Touchable onPress={onBackButtonPress}>
                <Icon name="arrow-back" />
              </Touchable>
            )}
            {this.props.leftContent}
          </Left>
          <Body paddingLeft={padding} paddingRight={padding}>
            {headerType === "search" && this.renderSearchBox()}
            {headerType === "title" && this.renderTitleSubtitle(fontSize, textType)}
          </Body>
          <Right>{this.props.rightContent}</Right>
        </RowLayout>
      </View>
    );
  }

  private renderTitleSubtitle(fontSize: number, textType: TextType): JSX.Element {
    return (
      <View style={styles.titleSubtitle}>
        <Text fontSize={fontSize} type={textType} numberOfLines={1}>
          {this.props.title}
        </Text>
        {this.props.subTitle && (
          <Text fontSize={fontSize - 2} type={textType} numberOfLines={1}>
            {this.props.subTitle}
          </Text>
        )}
      </View>
    );
  }

  private renderSearchBox(): JSX.Element {
    const debouncedonSearchTextChange =
      this.props.onSearchTextChange && debounce(this.props.onSearchTextChange, 200);
    return (
      <Input
        iconName="arrow-back"
        placeholder={this.props.title}
        onChangeText={debouncedonSearchTextChange}
        onIconPress={this.onBackButtonPress}
        autoFocus={this.props.autoFocusSearchBox}
        getRef={this.props.getSearchInputRef}
      />
    );
  }

  private onBackButtonPress = (): void => {
    Navigation.goBack();
  };
}
