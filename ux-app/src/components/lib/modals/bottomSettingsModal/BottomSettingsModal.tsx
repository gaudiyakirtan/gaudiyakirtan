import React from "react";
import { StyleSheet, View } from "react-native";

import Theme from "../../../../utils/theme/Theme";
import HorizontalRule from "../../horizontalRule/HorizontalRule";
import Modal from "../../modal/Modal";
import SlidingPanelKnob from "../../slidingPanelKnob/SlidingPanelKnob";
import Text from "../../text/Text";

const styles = StyleSheet.create({
  panelContainer: {
    width: "100%",
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 15,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15
  },
  title: { marginBottom: 5 },
  row: { flexDirection: "row" }
});

interface IProps {
  open: boolean;
  onClose(): void;
}

export default class BottomSettingsModal extends React.Component<IProps> {
  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();
    return (
      <Modal
        open={this.props.open}
        hasBackdrop={true}
        swipeDirection="down"
        content={{
          position: { bottom: 0 }
        }}
        onClose={this.props.onClose}
      >
        <View>
          <View style={{ ...styles.panelContainer, backgroundColor: theme.backgroundSecondary }}>
            <SlidingPanelKnob />
            {this.props.children}
          </View>
          <HorizontalRule noMargin={true} />
        </View>
      </Modal>
    );
  }
}

export function renderTitle(text: string, showHorRule?: boolean, margin?: number): JSX.Element {
  return (
    <View style={{ ...styles.title }}>
      {showHorRule && <HorizontalRule marginTop={margin} marginBottom={margin} />}
      <Text>{text}</Text>
    </View>
  );
}
