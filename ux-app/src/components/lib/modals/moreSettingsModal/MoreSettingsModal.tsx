/* eslint-disable no-nested-ternary */
import React from "react";
import { View } from "react-native";

import Icon, { IconFamily } from "../../icon/Icon";
import Modal from "../../modal/Modal";
import Text from "../../text/Text";
import RowLayout, { Body, Left } from "../../rowLayout/RowLayout";
import Touchable from "../../touchable/Touchable";
import AppDimensions from "../../../../utils/appDimensions/AppDimensions";

export interface IMoreSetting {
  text: string;
  iconName: string;
  iconFamily?: IconFamily;
  disabled?: boolean;
  active?: boolean;
  onPress(): void;
}

interface IProps {
  moreSettings: IMoreSetting[];
  disabled?: boolean;
}

interface IState {
  modalOpen: boolean;
}

export default class MoreSettingsModal extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      modalOpen: false
    };
  }

  public render(): JSX.Element {
    return (
      <>
        <Touchable
          onPress={this.onMoreIconPress}
          disabled={this.props.disabled}
          width={AppDimensions.MoreIconWidth}
        >
          <Icon
            name="more-vertical"
            family="Feather"
            type={this.props.disabled ? "disabled" : this.state.modalOpen ? "highlight" : "normal"}
          />
        </Touchable>
        <Modal
          open={this.state.modalOpen}
          mode="coverScreen"
          hasBackdrop={true}
          noBackdropColor={true}
          content={{
            width: 150,
            position: { top: AppDimensions.HeaderHeight, right: 0, left: "auto" },
            noBorderRadius: true
          }}
          animationIn="fadeInRight"
          animationOut="fadeOut"
          onClose={this.onModalClose}
        >
          {this.renderSettingsList()}
        </Modal>
      </>
    );
  }

  private renderSettingsList = (): JSX.Element => {
    return (
      <View style={{ marginLeft: 10 }}>
        {this.props.moreSettings.map(moreSetting => {
          const type = moreSetting.disabled
            ? "disabled"
            : moreSetting.active
            ? "highlight"
            : "normal";
          return (
            <Touchable
              onPress={() => this.onSettingPress(moreSetting)}
              disabled={moreSetting.disabled}
              key={moreSetting.text}
              width={250}
            >
              <RowLayout>
                <Left>
                  <Icon name={moreSetting.iconName} family={moreSetting.iconFamily} type={type} />
                </Left>
                <Body paddingLeft={5}>
                  <Text numberOfLines={1} type={type}>
                    {moreSetting.text}
                  </Text>
                </Body>
              </RowLayout>
            </Touchable>
          );
        })}
      </View>
    );
  };

  private onMoreIconPress = (): void => {
    this.setState(prevState => ({ modalOpen: !prevState.modalOpen }));
  };

  private onSettingPress = (moreSetting: IMoreSetting): void => {
    this.setState({ modalOpen: false }, () => {
      moreSetting.onPress();
    });
  };

  private onModalClose = (): void => {
    this.setState({ modalOpen: false });
  };
}
