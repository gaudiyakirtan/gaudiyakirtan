import React from "react";
import { View } from "react-native";

import Button from "../../button/Button";
import Modal from "../../modal/Modal";

interface IProps {
  open: boolean;
  content: JSX.Element | JSX.Element[];
  yesButtonText?: string;
  noButtonText?: string;
  onConfirmation(): void;
  onCancel(): void;
}

export default class ConfirmationModal extends React.Component<IProps> {
  public render(): JSX.Element {
    return (
      <Modal
        open={this.props.open}
        mode="coverScreen"
        hasBackdrop={true}
        onClose={this.props.onCancel}
        content={{
          noBorderRadius: true,
          margin: 30,
          padding: 15
        }}
        animationOut="fadeOut"
      >
        <>
          {this.props.content}
          <View
            style={{
              marginTop: 10,
              display: "flex",
              flexDirection: "row",
              alignSelf: "flex-end"
            }}
          >
            <View style={{ marginRight: 10 }}>
              <Button
                text={this.props.yesButtonText || "Yes"}
                active={true}
                width={75}
                height={40}
                onPress={this.props.onConfirmation}
              />
            </View>
            <Button
              text={this.props.noButtonText || "No"}
              displayBorder={true}
              width={75}
              height={40}
              onPress={this.props.onCancel}
            />
          </View>
        </>
      </Modal>
    );
  }
}
