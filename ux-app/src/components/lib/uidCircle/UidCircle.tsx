import React from "react";

import AppDimensions from "../../../utils/appDimensions/AppDimensions";
import Settings from "../../../utils/settings/Settings";
import ColorUtils from "../../../utils/theme/ColorUtils";
import Theme from "../../../utils/theme/Theme";
import Circle from "../circle/Circle";

interface IProps {
  uid: string;
}

export default class UidCircle extends React.Component<IProps> {
  public render(): JSX.Element {
    const isDarkTheme = Settings.get().isDarkTheme;
    const theme = Theme.getCurrentTheme();
    const textColor = isDarkTheme ? ColorUtils.dim(theme.accent, 2) : "#FFFFFF";
    // const textColor = isDarkTheme ? "#A0A0F0" : "#FFFFFF";

    return (
      <Circle
        text={this.props.uid}
        backgroundColor={this.colorFromUid(this.props.uid)}
        textColor={textColor}
        width={AppDimensions.UidThumbNailSize}
        height={AppDimensions.UidThumbNailSize}
      />
    );
  }

  private colorFromUid(uid: string): string {
    const isDarkTheme = Settings.get().isDarkTheme;
    const matches = uid.match(/([A-Z]{1,5})([0-9]{1,2})/);
    if (!matches) {
      return "#888888";
    }

    const letter1 = matches[1].charCodeAt(0) - 65;
    let letter2 = matches[1].charCodeAt(1) - 65;
    letter2 = Number.isNaN(letter2) ? 0 : letter2;
    const num = Number(matches[2]);

    const r = 160 + num * 2;
    const g = 40 + (letter1 / 26) * 100;
    const b = 20 + (letter2 / 26) * 50;

    return isDarkTheme
      ? ColorUtils.dim(ColorUtils.rgbToHex(b * 1.4, g * 1.5, r), 0.6)
      : ColorUtils.dim(ColorUtils.rgbToHex(r, g, b), 1.3);
  }
}
