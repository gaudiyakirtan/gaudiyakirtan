import React from "react";
import { Container as NativebaseContainer } from "native-base";

import Theme from "../../../utils/theme/Theme";
import StatusBar from "../statusBar/StatusBar";

interface IProps {
  children: React.ReactNode;
}

export default class Screen extends React.Component<IProps> {
  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();

    return (
      <NativebaseContainer style={{ backgroundColor: theme.backgroundPrimary }}>
        <StatusBar />
        {this.props.children}
      </NativebaseContainer>
    );
  }
}
