import React from "react";
import { TouchableWithoutFeedback } from "react-native";

interface IProps {
  onSingleTap?(): void;
  onDoubleTap?(): void;
  onLongPress?(): void;
  delay?: number;
}

/**
 * Basic logic from "react-native-double-tap" https://github.com/awshawka/react-native-double-tap/blob/master/index.js
 * But made some modifications to it to fit our needs, like :
 *    - TouchableWithoutFeedback instead of TouchableOpacity
 *    - onLongPress()
 */
export default class TapDetector extends React.Component<IProps> {
  private watching: boolean; // bool to check whether watching for a double tap
  private lastPressTime?: number;
  private timer?: number;
  private delayBetweenClicks: number;

  public constructor(props: IProps) {
    super(props);

    this.watching = false;
    this.lastPressTime = undefined;
    this.timer = undefined;
    this.delayBetweenClicks = this.props.delay || 300;
  }

  public componentWillUnmount(): void {
    if (this.timer) {
      clearTimeout(this.timer);
    }
  }

  public render(): React.ReactNode {
    return (
      <TouchableWithoutFeedback
        onPress={this.onPress}
        onLongPress={this.props.onLongPress}
        style={{ backgroundColor: "yellow" }}
      >
        {this.props.children}
      </TouchableWithoutFeedback>
    );
  }

  private onPress = (): void => {
    const now = new Date().getTime();

    if (!this.watching) {
      this.watching = true;
      this.lastPressTime = now;

      this.timer = window.setTimeout(() => {
        this.watching = false;
        if (this.props.onSingleTap) {
          this.props.onSingleTap();
        }
      }, this.delayBetweenClicks);
    } else if (this.watching) {
      if (this.lastPressTime && now - this.lastPressTime < this.delayBetweenClicks) {
        if (this.props.onDoubleTap) {
          this.props.onDoubleTap();
        }
        if (this.timer) {
          clearTimeout(this.timer);
        }
        this.watching = false;
      }
    }
  };
}
