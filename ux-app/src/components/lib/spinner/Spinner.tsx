import React from "react";
import { Spinner as NativeBaseSpinner } from "native-base";

interface IProps {
  // In ios size can only be 'small' or 'large' but not any other number
  size?: "small" | "large";
}

export default class Spinner extends React.Component<IProps> {
  public render(): JSX.Element {
    const size = this.props.size || "large";

    return <NativeBaseSpinner size={size} />;
  }
}
