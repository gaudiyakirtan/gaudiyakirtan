import React from "react";
import { View } from "react-native";

import Theme from "../../../utils/theme/Theme";

export default class SlidingPanelKnob extends React.Component {
  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();
    return (
      <View style={{ display: "flex", justifyContent: "center", alignItems: "center", height: 16 }}>
        <View
          style={{
            backgroundColor: theme.controlLowlight,
            width: 30,
            height: 6,
            borderRadius: 10
          }}
        />
      </View>
    );
  }
}
