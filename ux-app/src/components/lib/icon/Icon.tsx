import React from "react";
import { StyleSheet, View } from "react-native";
import { Icon as NativeBaseIcon } from "native-base";

import Theme from "../../../utils/theme/Theme";
import Color from "../../../utils/theme/Color";

export type IconType = "highlight" | "normal" | "lowlight" | "disabled" | "error" | "onAccent";

export type IconFamily =
  | "AntDesign"
  | "Entypo"
  | "EvilIcons"
  | "Feather"
  | "FontAwesome"
  | "FontAwesome5"
  | "Foundation"
  | "Ionicons"
  | "MaterialCommunityIcons"
  | "MaterialIcons"
  | "Octicons"
  | "SimpleLineIcons"
  | "Zocial";

export enum IconSize {
  Small = 16,
  Normal = 24,
  Medium = 32,
  Large = 40,
  XLarge = 48,
  XXLarge = 56,
  VeryLarge = 80
}

export interface IIconProps {
  name: string;
  family?: IconFamily;
  type?: IconType;
  color?: string;
  size?: IconSize | number;
  flipVertical?: boolean;
  width?: number | string;
  onPress?: () => void;
}

const styles = StyleSheet.create({
  container: {
    display: "flex",
    width: 40,
    alignItems: "center",
    justifyContent: "center"
  }
});

export const defaultIconWidth = 30;

export default class Icon extends React.Component<IIconProps> {
  public render(): JSX.Element {
    const iconType = this.props.type || "normal";
    const iconSize = this.props.size || IconSize.Normal;
    const iconFamily = this.props.family || "Ionicons";

    const color = this.props.color || this.getIconColor(iconType);
    const width = this.props.width || iconSize > defaultIconWidth ? iconSize : defaultIconWidth;
    const transform = this.props.flipVertical ? [{ scaleY: -1 }] : undefined;

    return (
      <View style={{ ...styles.container, width }}>
        <NativeBaseIcon
          name={this.props.name}
          type={iconFamily}
          onPress={this.props.onPress}
          style={{ fontSize: iconSize, color, transform }}
        />
      </View>
    );
  }

  private getIconColor(iconType: IconType): Color {
    const theme = Theme.getCurrentTheme();

    let color = theme.iconNormal;
    switch (iconType) {
      case "highlight":
        color = theme.iconHighlight;
        break;
      case "normal":
        color = theme.iconNormal;
        break;
      case "lowlight":
        color = theme.iconLowlight;
        break;
      case "disabled":
        color = theme.iconDisabled;
        break;
      case "error":
        color = theme.error;
        break;
      case "onAccent":
        color = theme.onAccent;
        break;
      default:
    }

    return color;
  }
}
