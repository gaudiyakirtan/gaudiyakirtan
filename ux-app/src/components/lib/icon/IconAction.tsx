import React from "react";

import Theme from "../../../utils/theme/Theme";
import Circle from "../circle/Circle";
import Touchable from "../touchable/Touchable";

import Icon, { IconFamily, IconSize } from "./Icon";

interface IProps {
  name: string;
  family?: IconFamily;
  noCircle?: boolean;
  onPress(): void;
}

export default class IconAction extends React.Component<IProps> {
  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();
    return (
      <Touchable onPress={this.props.onPress}>
        {this.props.noCircle && this.renderIcon()}
        {!this.props.noCircle && (
          <Circle width={35} height={35} backgroundColor={theme.backgroundSecondary}>
            {this.renderIcon()}
          </Circle>
        )}
      </Touchable>
    );
  }

  private renderIcon(): JSX.Element {
    return (
      <Icon
        name={this.props.name}
        family={this.props.family}
        size={this.props.noCircle ? IconSize.Normal : 20}
      />
    );
  }
}
