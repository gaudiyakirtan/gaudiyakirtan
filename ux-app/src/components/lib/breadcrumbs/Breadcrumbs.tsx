import React from "react";
import { StyleSheet, View } from "react-native";

import Touchable from "../touchable/Touchable";
import Text from "../text/Text";
import Icon, { IconSize } from "../icon/Icon";
import Theme from "../../../utils/theme/Theme";

export interface IBreadcrumb {
  text: string;
  active: boolean;
  metaData: unknown;
}

interface IProps {
  breadcrumbs: IBreadcrumb[];
  onPress(pressedBreadcrumb: IBreadcrumb): void;
}

const styles = StyleSheet.create({
  breadcrumbs: {
    zIndex: 1000, // For sticky headers of List to not show above breadcrumbs when scrolling the list up
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 5,
    marginBottom: 5,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 500
  },
  breadcrumb: {
    flexDirection: "row",
    alignItems: "center",
    height: 25
  }
});

export default class Breadcrumbs extends React.Component<IProps> {
  public render(): JSX.Element {
    if (!this.props.breadcrumbs) {
      return <></>;
    }

    const theme = Theme.getCurrentTheme();
    const lastIndex = this.props.breadcrumbs.length - 1;
    const breadcrumbsJsx = this.props.breadcrumbs.map((breadcrumb, index) => {
      return this.renderBreadcrumb(breadcrumb, index === lastIndex);
    });

    return (
      <View style={{ ...styles.breadcrumbs, backgroundColor: theme.backgroundSecondary }}>
        {breadcrumbsJsx}
      </View>
    );
  }

  private renderBreadcrumb = (breadcrumb: IBreadcrumb, isLast: boolean): JSX.Element => {
    return (
      <View style={styles.breadcrumb} key={breadcrumb.text}>
        <Touchable
          onPress={() => this.onBreadcrumbPress(breadcrumb)}
          width="auto"
          height="auto"
          styles={{ maxWidth: 120 }}
        >
          <Text type={breadcrumb.active ? "highlight" : "lowlight"} fontSize={12} numberOfLines={1}>
            {breadcrumb.text}
          </Text>
        </Touchable>
        {!isLast && <Icon name="chevron-right" family="Feather" size={IconSize.Small} />}
      </View>
    );
  };

  private onBreadcrumbPress = (pressedBreadcrumb: IBreadcrumb): void => {
    this.props.onPress(pressedBreadcrumb);
  };
}
