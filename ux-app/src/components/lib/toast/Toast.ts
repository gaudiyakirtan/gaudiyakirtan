/* eslint-disable no-nested-ternary */
import { Toast as NativeBaseToast } from "native-base";

import AppDimensions from "../../../utils/appDimensions/AppDimensions";
import Color from "../../../utils/theme/Color";
import Theme from "../../../utils/theme/Theme";

interface IOptions {
  buttonText?: string;
  position?: "top" | "bottom" | "center";
  durationSecs?: number;
}

export enum ToastType {
  Info,
  Success,
  Error
}

export default class Toast {
  public static show(toastType: ToastType, text: string, optionsParam?: IOptions): void {
    const theme = Theme.getCurrentTheme();

    const options: IOptions = optionsParam || {};
    const position = options.position || "bottom";

    NativeBaseToast.show({
      text,
      buttonText: options.buttonText,
      position,
      duration: options.durationSecs ? options.durationSecs * 1000 : 3000,
      style: {
        backgroundColor: this.getBackgroundColor(toastType),
        top: position === "top" ? AppDimensions.HeaderHeight + 10 : undefined,
        bottom: position === "bottom" ? AppDimensions.FooterHeight + 10 : undefined,
        borderRadius: 15,
        marginLeft: 10,
        marginRight: 10
      },
      textStyle: {
        marginLeft: 20,
        color: theme.textNormal
      }
    });
  }

  private static getBackgroundColor(toastType: ToastType): Color {
    const theme = Theme.getCurrentTheme();
    switch (toastType) {
      case ToastType.Info:
        return theme.backgroundSecondary;
      case ToastType.Success:
        return theme.success;
      case ToastType.Error:
        return theme.error;
      default:
        return theme.backgroundSecondary;
    }
  }
}
