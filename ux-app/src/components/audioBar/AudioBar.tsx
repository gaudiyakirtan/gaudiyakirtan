import React from "react";
import { View } from "react-native";

import ISong, { getArtistId, IAudio } from "../../data/models/server/ISong";
import HorizontalRule from "../lib/horizontalRule/HorizontalRule";
import Text from "../lib/text/Text";
import Theme from "../../utils/theme/Theme";
import Icon from "../lib/icon/Icon";
import Touchable from "../lib/touchable/Touchable";
import RowLayout, { Left, Body, Right } from "../lib/rowLayout/RowLayout";
import { LocalFileUris } from "../../utils/fileSystem/FileUris";
import LocalFileSystem from "../../utils/fileSystem/LocalFileSystem";
import AppDimensions from "../../utils/appDimensions/AppDimensions";
import { IImageDimensions } from "../lib/thumbnailPic/ThumbnailPic";
import DataTransform from "../../data/DataTransform";

import AudioDownloader from "./audioDownloader/AudioDownloader";
import AudioPlayer from "./audioPlayer/AudioPlayer";
import FullAudioPanel from "./fullAudioPanel/FullAudioPanel";
import AudioSlider from "./audioSlider/AudioSlider";
import AudioSound from "./audioPlayer/AudioSound";
import ArtistPic from "./artistPic/ArtistPic";

const touchableIconWidth = 40;

interface IProps {
  song: ISong;
  onClose(): void;
}

interface IState {
  selectedAudio?: IAudio;
  localFileExists?: boolean;
  audioSound?: AudioSound;
  autoPlay: boolean;
  fullAudioPanelOpen: boolean;
}

export default class AudioBar extends React.Component<IProps, IState> {
  private multipleArtistsExist: boolean;

  public constructor(props: IProps) {
    super(props);
    this.multipleArtistsExist = this.props.song.audio.length > 1;

    this.state = {
      selectedAudio: undefined,
      localFileExists: undefined,
      audioSound: undefined,
      autoPlay: false,
      fullAudioPanelOpen: this.multipleArtistsExist
    };
  }

  public componentDidMount(): void {
    if (!this.multipleArtistsExist) {
      this.selectAudio(this.props.song.audio[0], false);
    }
  }

  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();
    const disabled = !this.state.selectedAudio || !this.state.localFileExists;
    const artistId = this.state.selectedAudio && getArtistId(this.state.selectedAudio.uid);
    const artistName = this.state.selectedAudio && this.state.selectedAudio.meta.artist;
    const dimensions: IImageDimensions | undefined = !this.state.selectedAudio
      ? { width: 36, height: 36 }
      : undefined;

    return (
      <>
        <AudioPlayer
          audio={this.state.selectedAudio}
          disabled={disabled}
          autoPlay={this.state.autoPlay}
          onAudioSoundUpdate={this.onAudioSoundUpdate}
          key={this.state.selectedAudio && this.state.selectedAudio.fn}
        />
        <FullAudioPanel
          open={this.state.fullAudioPanelOpen}
          onClose={this.onFullAudioPanelClose}
          song={this.props.song}
          selectedAudio={this.state.selectedAudio}
          onArtistSelect={this.onArtistSelect}
          audioSound={this.state.audioSound}
          disabled={disabled}
        />
        <View style={{ backgroundColor: theme.backgroundSecondary }}>
          <HorizontalRule noMargin={true} />
          <AudioSlider audioSound={this.state.audioSound} minimized={true} disabled={disabled} />
          <RowLayout height={AppDimensions.AudioBarHeight}>
            <Left paddingLeft={5}>
              <ArtistPic
                type="thumbnail"
                artistId={artistId}
                artistName={artistName}
                dimensions={dimensions}
              />
            </Left>
            <Body paddingLeft={5}>
              <Touchable onPress={this.onAudioBarPress} width="100%" noHorzCenter={true}>
                {this.state.selectedAudio && (
                  <View>
                    <Text numberOfLines={1}>
                      {DataTransform.transformSongTitle(
                        this.props.song.title,
                        this.props.song.language
                      )}
                    </Text>
                    <Text type="lowlight" numberOfLines={1}>
                      {this.state.selectedAudio.meta.artist}
                    </Text>
                  </View>
                )}
                {!this.state.selectedAudio && <Text>Select an artist</Text>}
              </Touchable>
            </Body>
            <Right>
              {this.renderRightIcon()}
              <Touchable onPress={this.props.onClose} width={touchableIconWidth}>
                <Icon name="md-close" />
              </Touchable>
            </Right>
          </RowLayout>
          <HorizontalRule noMargin={true} />
        </View>
      </>
    );
  }

  private renderRightIcon(): JSX.Element | undefined {
    if (!this.state.selectedAudio || this.state.localFileExists === undefined) {
      return undefined;
    }
    if (!this.state.localFileExists) {
      return (
        <AudioDownloader
          audio={this.state.selectedAudio}
          iconType="highlight"
          touchableIconWidth={touchableIconWidth}
          onDownloadStart={this.onEveryDownloadAction}
          onDownloadStoppedInBetween={this.onEveryDownloadAction}
          onDownloadComplete={this.onEveryDownloadAction}
        />
      );
    } else {
      if (this.state.audioSound && this.state.audioSound.isPlaying()) {
        return (
          <Touchable onPress={this.onPausePress} width={touchableIconWidth}>
            <Icon name="md-pause" type="normal" />
          </Touchable>
        );
      } else {
        return (
          <Touchable onPress={this.onPlayPress} width={touchableIconWidth}>
            <Icon name="md-play" type="highlight" />
          </Touchable>
        );
      }
    }
  }

  private selectAudio = async (audio: IAudio, autoPlay: boolean): Promise<void> => {
    const localFileExists = await this.getLocalFileExists(audio);
    this.setState({
      selectedAudio: audio,
      localFileExists,
      autoPlay: autoPlay ? localFileExists : false
    });
  };

  private onArtistSelect = async (audio: IAudio): Promise<void> => {
    this.selectAudio(audio, true);
  };

  private onAudioSoundUpdate = (audioSound?: AudioSound): void => {
    this.setState({ audioSound });
  };

  private onEveryDownloadAction = async (): Promise<void> => {
    if (this.state.selectedAudio) {
      // eslint-disable-next-line react/no-access-state-in-setstate
      const localFileExists = await this.getLocalFileExists(this.state.selectedAudio);
      this.setState({ localFileExists });
    }
  };

  private async getLocalFileExists(audio: IAudio): Promise<boolean> {
    const localFileUri = LocalFileUris.audio(audio.fn);
    const localFileInfo = await LocalFileSystem.exists(localFileUri);
    return localFileInfo.exists;
  }

  private onAudioBarPress = (): void => {
    this.setState({ fullAudioPanelOpen: true });
  };

  private onFullAudioPanelClose = (): void => {
    this.setState({ fullAudioPanelOpen: false });
  };

  private onPlayPress = async (): Promise<void> => {
    if (this.state.audioSound) {
      await this.state.audioSound.play();
    }
  };

  private onPausePress = async (): Promise<void> => {
    if (this.state.audioSound) {
      await this.state.audioSound.pause();
    }
  };
}
