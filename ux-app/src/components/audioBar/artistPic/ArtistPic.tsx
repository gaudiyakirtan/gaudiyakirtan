import React from "react";
import { ImageURISource } from "react-native";

import { LocalFileUris } from "../../../utils/fileSystem/FileUris";
import LocalFileSystem from "../../../utils/fileSystem/LocalFileSystem";
import Data from "../../../data/Data";
import ThumbnailPic, { IImageDimensions } from "../../lib/thumbnailPic/ThumbnailPic";

interface IProps {
  artistId: string | undefined;
  artistName: string | undefined;
  type: "thumbnail" | "image";
  dimensions?: IImageDimensions;
  noLogoWhileLoading?: boolean;
}

interface IState {
  loading: boolean;
  picLocalFileUri: string | undefined;
}

export default class ArtistPic extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      loading: true,
      picLocalFileUri: undefined
    };
  }

  public async componentDidMount(): Promise<void> {
    await this.loadArtistPic(this.props);
  }

  public async UNSAFE_componentWillReceiveProps(nextProps: IProps): Promise<void> {
    if (nextProps.artistId !== this.props.artistId) {
      await this.loadArtistPic(nextProps);
    }
  }

  public render(): JSX.Element {
    const imageSource: ImageURISource | undefined = this.state.picLocalFileUri
      ? {
          uri: this.state.picLocalFileUri
        }
      : undefined;
    const noLogoWhileLoading = this.state.loading && this.props.noLogoWhileLoading;
    return (
      <ThumbnailPic
        type={this.props.type}
        imageSource={imageSource}
        dimensions={this.props.dimensions}
        modalHeight={300}
        modalTitle={this.props.artistName}
        noLogoWhenEmpty={noLogoWhileLoading}
      />
    );
  }

  private async loadArtistPic(props: IProps): Promise<void> {
    if (!props.artistId) {
      this.setState({ loading: false, picLocalFileUri: undefined });
    } else {
      this.setState({ loading: true, picLocalFileUri: undefined });

      await Data.LoadArtistPic(props.artistId);

      const localFileUri = LocalFileUris.artistPic(props.artistId);
      const localFileInfo = await LocalFileSystem.exists(localFileUri);

      const picLocalFileUri = localFileInfo.exists ? localFileUri : undefined;
      this.setState({ loading: false, picLocalFileUri });
    }
  }
}
