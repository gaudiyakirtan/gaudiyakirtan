import React from "react";
import { StyleSheet, View } from "react-native";
import Slider from "@react-native-community/slider";

import Theme from "../../../utils/theme/Theme";
import Text from "../../lib/text/Text";
import AudioSound from "../audioPlayer/AudioSound";
import AppDimensions from "../../../utils/appDimensions/AppDimensions";

const sliderHeight = 50;
const sliderPadding = 16;

const styles = StyleSheet.create({
  timestamp: {
    height: 15,
    position: "absolute",
    bottom: 0,
    width: "100%"
  },
  currentTimestamp: {
    position: "absolute",
    left: sliderPadding
  },
  durationTimestamp: {
    position: "absolute",
    right: sliderPadding
  }
});

interface IProps {
  audioSound?: AudioSound;
  minimized: boolean;
  disabled: boolean;
}

export default class AudioSlider extends React.Component<IProps> {
  private isSliding: boolean;
  private previousPosition: number | undefined;

  public constructor(props: IProps) {
    super(props);
    this.isSliding = false;
    this.previousPosition = undefined;
  }

  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();
    const sliderHeight2 = this.props.minimized ? AppDimensions.MinimizedSliderHeight : sliderHeight;

    return (
      <View
        style={{
          marginLeft: this.props.minimized ? -sliderPadding : 0,
          marginRight: this.props.minimized ? -sliderPadding : 0
        }}
      >
        <Slider
          value={this.getSliderPosition()}
          onValueChange={this.onSliderValueChange}
          onSlidingComplete={this.onSlidingComplete}
          disabled={this.props.disabled}
          minimumTrackTintColor={theme.controlHighlight}
          maximumTrackTintColor={this.props.minimized ? "transparent" : theme.controlNormal}
          thumbTintColor={this.props.minimized ? "transparent" : theme.controlHighlight} // For Android
          thumbImage={this.props.minimized ? undefined : undefined} // For IOS
          style={{ height: sliderHeight2 }}
        />
        {!this.props.minimized && (
          <View style={{ ...styles.timestamp }}>
            {this.renderCurrentTimeStamp()}
            {this.renderDurationTimeStamp()}
          </View>
        )}
      </View>
    );
  }

  private renderCurrentTimeStamp(): JSX.Element {
    return (
      <View style={styles.currentTimestamp}>
        <Text fontSize={12} type={this.props.audioSound ? "normal" : "lowlight"}>
          {this.getCurrentPositionTimestamp()}
        </Text>
      </View>
    );
  }

  private renderDurationTimeStamp(): JSX.Element | undefined {
    const durationTimeStamp = this.getTotalDurationTimestamp();
    if (durationTimeStamp) {
      return (
        <View style={styles.durationTimestamp}>
          <Text fontSize={12} type="normal">
            {durationTimeStamp}
          </Text>
        </View>
      );
    }
    return undefined;
  }

  private getSliderPosition = (): number | undefined => {
    if (this.isSliding) {
      return this.previousPosition;
    }

    if (this.props.audioSound) {
      const currentPositionMillis = this.props.audioSound.getCurrentPositionMillis();
      const totalDurationMillis = this.props.audioSound.getTotalDurationMillis();
      if (currentPositionMillis !== undefined && totalDurationMillis !== undefined) {
        const position = currentPositionMillis / totalDurationMillis;
        const positionPercent = position * 100;

        if (this.props.minimized && positionPercent < 2) {
          return 2 / 100;
        }
        this.previousPosition = position;
        return position;
      }
    }
    return 0;
  };

  private onSliderValueChange = (): void => {
    if (this.props.audioSound) {
      this.isSliding = true;
    }
  };

  private onSlidingComplete = async (value: number): Promise<void> => {
    if (this.props.audioSound) {
      const totalDurationMillis = this.props.audioSound.getTotalDurationMillis();
      if (totalDurationMillis !== undefined) {
        const seekPositionMillis = value * totalDurationMillis;
        await this.props.audioSound.playFromPosition(seekPositionMillis);
      }
      this.isSliding = false;
    }
  };

  private getCurrentPositionTimestamp = (): string => {
    if (this.props.audioSound) {
      const currentPositionMillis = this.props.audioSound.getCurrentPositionMillis();
      if (currentPositionMillis !== undefined) {
        return this.getMMSSFromMilliSeconds(currentPositionMillis);
      }
    }
    return "00:00";
  };

  private getTotalDurationTimestamp = (): string | undefined => {
    if (this.props.audioSound) {
      const totalDurationMillis = this.props.audioSound.getTotalDurationMillis();
      if (totalDurationMillis !== undefined) {
        return this.getMMSSFromMilliSeconds(totalDurationMillis);
      }
    }
    return undefined;
  };

  private getMMSSFromMilliSeconds = (milliSeconds: number): string => {
    const totalSeconds = milliSeconds / 1000;
    const seconds = Math.floor(totalSeconds % 60);
    const minutes = Math.floor(totalSeconds / 60);

    return `${this.padWithZero(minutes)}:${this.padWithZero(seconds)}`;
  };

  private padWithZero = (number: number): string => {
    const string = number.toString();
    if (number < 10) {
      return `0${string}`;
    }
    return string;
  };
}
