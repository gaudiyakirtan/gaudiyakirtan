import React from "react";

import { IAudio } from "../../../data/models/server/ISong";
import { LocalFileUris } from "../../../utils/fileSystem/FileUris";

import AudioSound from "./AudioSound";

interface IProps {
  audio?: IAudio;
  disabled: boolean;
  autoPlay: boolean;
  onAudioSoundUpdate(audioSound?: AudioSound): void;
}

interface IState {
  audioSound?: AudioSound;
}

export default class AudioPlayer extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      audioSound: undefined
    };
  }

  public async componentDidMount(): Promise<void> {
    this.init(this.props.autoPlay);
  }

  public async componentWillUnmount(): Promise<void> {
    this.destruct();
  }

  public render(): JSX.Element {
    return <></>;
  }

  private async init(autoPlay: boolean): Promise<void> {
    this.destruct();

    if (this.props.disabled || !this.props.audio) {
      this.setState({ audioSound: undefined });
    } else {
      const localFileUri = LocalFileUris.audio(this.props.audio.fn);
      const audioSound = new AudioSound(this.onSoundStatusUpdate);
      await audioSound.create(localFileUri, autoPlay);
      this.setState({ audioSound });
    }
  }

  private async destruct(): Promise<void> {
    if (this.state.audioSound) {
      await this.state.audioSound.unload();
    }
  }

  private onSoundStatusUpdate = (): void => {
    if (this.state.audioSound) {
      if (this.state.audioSound.didJustFinish()) {
        this.init(false);
      } else {
        this.props.onAudioSoundUpdate(this.state.audioSound);
      }
    }
  };
}
