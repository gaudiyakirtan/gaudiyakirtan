import { Audio } from "expo-av";
import { AVPlaybackStatus } from "expo-av/build/AV";

export default class AudioSound {
  private soundInstance?: Audio.Sound;
  private soundStatus?: AVPlaybackStatus;
  // Having a separate variable instead of using this.state.soundStatus.isPlaying because
  // it's causing a flicker in play/pause icon whenever scrubber or forward/backward buttons are presed.
  private playing: boolean;
  private onSoundStatusUpdate: () => void;

  public constructor(onSoundStatusUpdate: () => void) {
    this.soundInstance = undefined;
    this.soundStatus = undefined;
    this.playing = false;
    this.onSoundStatusUpdate = onSoundStatusUpdate;

    Audio.setAudioModeAsync({
      allowsRecordingIOS: false,
      staysActiveInBackground: true,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      playThroughEarpieceAndroid: false
    });
  }

  /**
   * soundInstance related
   */

  public async create(localFileUri: string, autoPlay: boolean): Promise<void> {
    const { sound } = await Audio.Sound.createAsync(
      { uri: localFileUri },
      { shouldPlay: autoPlay },
      this.onPlaybackStatusUpdate
    );

    this.soundInstance = sound;
    this.playing = autoPlay;
  }

  public async play(): Promise<void> {
    if (this.soundInstance) {
      this.setPlaying(true);
      await this.soundInstance.playAsync();
    }
  }

  public async playFromPosition(positionMillis: number): Promise<void> {
    if (this.soundInstance) {
      this.setPlaying(true);
      await this.soundInstance.playFromPositionAsync(positionMillis);
    }
  }

  public async pause(): Promise<void> {
    if (this.soundInstance) {
      this.setPlaying(false);
      await this.soundInstance.pauseAsync();
    }
  }

  public async unload(): Promise<void> {
    if (this.soundInstance) {
      this.setPlaying(false);
      await this.soundInstance.unloadAsync();
    }
  }

  /**
   * soundStatus related
   */

  public isPlaying(): boolean {
    return this.playing;
  }

  public didJustFinish(): boolean {
    return Boolean(this.soundStatus && this.soundStatus.isLoaded && this.soundStatus.didJustFinish);
  }

  public getCurrentPositionMillis(): number | undefined {
    if (this.soundStatus && this.soundStatus.isLoaded) {
      return this.soundStatus.positionMillis;
    }
    return undefined;
  }

  public getTotalDurationMillis(): number | undefined {
    if (this.soundStatus && this.soundStatus.isLoaded) {
      return this.soundStatus.durationMillis;
    }
    return undefined;
  }

  private onPlaybackStatusUpdate = (status: AVPlaybackStatus): void => {
    this.soundStatus = status;
    this.onSoundStatusUpdate();
  };

  private setPlaying(playing: boolean): void {
    this.playing = playing;
    this.onSoundStatusUpdate();
  }
}
