import React from "react";
import { StyleSheet, View } from "react-native";

import Modal from "../../lib/modal/Modal";
import ISong, { getArtistId, IAudio } from "../../../data/models/server/ISong";
import AudioSlider from "../audioSlider/AudioSlider";
import AudioSound from "../audioPlayer/AudioSound";
import Screen from "../../lib/screen/Screen";
import Header from "../../lib/header/Header";
import Center from "../../lib/center/Center";
import Text from "../../lib/text/Text";
import Touchable from "../../lib/touchable/Touchable";
import Icon from "../../lib/icon/Icon";
import ArtistPic from "../artistPic/ArtistPic";
import { IImageDimensions } from "../../lib/thumbnailPic/ThumbnailPic";
import DataTransform from "../../../data/DataTransform";

import ArtistList from "./artistList/ArtistList";
import AudioControls from "./audioControls/AudioControls";

const styles = StyleSheet.create({
  content: {
    flex: 1,
    marginTop: 20,
    marginBottom: 50
  },
  nonControls: {
    flex: 1
  },
  controls: {
    height: 100
  },
  title: {
    height: 50
  },
  imageContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 20,
    marginBottom: 20
  },
  artistListContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "flex-start",
    marginLeft: 10,
    marginRight: 10
  }
});

interface IProps {
  open: boolean;
  onClose(): void;
  song: ISong;
  selectedAudio?: IAudio;
  onArtistSelect(audio: IAudio): void;
  audioSound?: AudioSound;
  disabled: boolean;
}

export default class FullAudioPanel extends React.Component<IProps> {
  public render(): JSX.Element {
    const artistId = this.props.selectedAudio && getArtistId(this.props.selectedAudio.uid);
    const artistName = this.props.selectedAudio && this.props.selectedAudio.meta.artist;
    const imageDimensions = this.getImageDimensions();

    return (
      <Modal
        open={this.props.open}
        hasBackdrop={true}
        mode="coverScreen"
        content={{
          height: "100%"
        }}
        onClose={this.props.onClose}
      >
        <Screen>
          <Header
            title={this.props.song.uid}
            noMarginTop={true}
            leftContent={this.renderHeaderLeftContent()}
            rightContent={this.renderHeaderRightContent()}
          />
          <View style={styles.content}>
            <View style={styles.nonControls}>
              {this.renderSongTitle()}
              <View style={styles.imageContainer}>
                <ArtistPic
                  type="image"
                  artistId={artistId}
                  artistName={artistName}
                  dimensions={imageDimensions}
                  noLogoWhileLoading={true}
                />
              </View>
              <View style={styles.artistListContainer}>
                <ArtistList
                  song={this.props.song}
                  selectedAudio={this.props.selectedAudio}
                  onArtistSelect={this.props.onArtistSelect}
                />
              </View>
            </View>
            <View style={styles.controls}>
              <AudioSlider
                audioSound={this.props.audioSound}
                minimized={false}
                disabled={this.props.disabled}
              />
              <AudioControls audioSound={this.props.audioSound} disabled={this.props.disabled} />
            </View>
          </View>
        </Screen>
      </Modal>
    );
  }

  private renderSongTitle(): JSX.Element {
    return (
      <View style={styles.title}>
        <Center hor={true} ver={true}>
          <Text fontSize={18} textAlign="center">
            {DataTransform.transformSongTitle(this.props.song.title, this.props.song.language)}
          </Text>
        </Center>
        <Center hor={true} ver={true}>
          <Text type="lowlight" fontSize={16} textAlign="center">
            {DataTransform.transformAuthor(this.props.song.author, this.props.song.language)}
          </Text>
        </Center>
      </View>
    );
  }

  private renderHeaderLeftContent(): JSX.Element | undefined {
    return (
      <Touchable onPress={this.props.onClose}>
        <Icon name="chevron-down" family="Feather" size={28} />
      </Touchable>
    );
  }

  private renderHeaderRightContent(): JSX.Element | undefined {
    // Empty place holder to symmetrically center header title
    return <Touchable />;
  }

  private getImageDimensions(): IImageDimensions {
    // const artistCount = this.props.song.audio.length;

    // if (artistCount === 1) {
    //   return { width: 350, height: 350 };
    // } else if (artistCount === 2) {
    //   return { width: 300, height: 300 };
    // } else if (artistCount === 3) {
    //   return { width: 250, height: 250 };
    // } else {
    //   return { width: 200, height: 200 };
    // }

    return { width: 150, height: 150 };
  }
}
