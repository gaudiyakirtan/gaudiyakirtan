import React from "react";
import { StyleSheet, View } from "react-native";

import RowLayout, { Body } from "../../../lib/rowLayout/RowLayout";
import Icon, { IconSize } from "../../../lib/icon/Icon";
import Touchable from "../../../lib/touchable/Touchable";
import AudioSound from "../../audioPlayer/AudioSound";

const styles = StyleSheet.create({
  centerControls: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    width: "100%"
  }
});

interface IProps {
  audioSound?: AudioSound;
  disabled: boolean;
}

export default class AudioControls extends React.Component<IProps> {
  public render(): JSX.Element {
    return (
      <RowLayout>
        <Body>
          <View style={styles.centerControls}>
            {this.renderBackwardButton()}
            {this.renderPlayPauseButton()}
            {this.renderForwardButton()}
          </View>
        </Body>
      </RowLayout>
    );
  }

  private renderBackwardButton(): JSX.Element {
    return (
      <Touchable onPress={this.onBackwardPress} width={70} disabled={this.props.disabled}>
        <Icon
          name="replay-30"
          family="MaterialIcons"
          size={IconSize.Large}
          type={this.props.disabled ? "disabled" : "normal"}
        />
      </Touchable>
    );
  }

  private renderForwardButton(): JSX.Element {
    return (
      <Touchable onPress={this.onForwardPress} width={70} disabled={this.props.disabled}>
        <Icon
          name="forward-30"
          family="MaterialIcons"
          size={IconSize.Large}
          type={this.props.disabled ? "disabled" : "normal"}
        />
      </Touchable>
    );
  }

  private renderPlayPauseButton(): JSX.Element {
    if (this.props.audioSound && this.props.audioSound.isPlaying()) {
      return (
        <Touchable onPress={this.onPausePress} width={70} disabled={this.props.disabled}>
          <Icon
            name="pause-circle-filled"
            family="MaterialIcons"
            size={IconSize.XXLarge}
            type={this.props.disabled ? "disabled" : "normal"}
          />
        </Touchable>
      );
    } else {
      return (
        <Touchable onPress={this.onPlayPress} width={70} disabled={this.props.disabled}>
          <Icon
            name="play-circle-filled"
            family="MaterialIcons"
            size={IconSize.XXLarge}
            type={this.props.disabled ? "disabled" : "highlight"}
          />
        </Touchable>
      );
    }
  }

  private onPlayPress = async (): Promise<void> => {
    if (this.props.audioSound) {
      await this.props.audioSound.play();
    }
  };

  private onPausePress = async (): Promise<void> => {
    if (this.props.audioSound) {
      await this.props.audioSound.pause();
    }
  };

  private onBackwardPress = async (): Promise<void> => {
    if (this.props.audioSound) {
      const currentPositionMillis = this.props.audioSound.getCurrentPositionMillis();
      if (currentPositionMillis !== undefined) {
        await this.props.audioSound.playFromPosition(currentPositionMillis - 30000);
      }
    }
  };

  private onForwardPress = async (): Promise<void> => {
    if (this.props.audioSound) {
      const currentPositionMillis = this.props.audioSound.getCurrentPositionMillis();
      if (currentPositionMillis !== undefined) {
        await this.props.audioSound.playFromPosition(currentPositionMillis + 30000);
      }
    }
  };
}
