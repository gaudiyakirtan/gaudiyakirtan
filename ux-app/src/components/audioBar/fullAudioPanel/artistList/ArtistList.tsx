import React from "react";
import { View } from "react-native";

import ISong, { IAudio } from "../../../../data/models/server/ISong";
import ScrollView from "../../../lib/scrollView/ScrollView";

import Artist from "./Artist";

interface IProps {
  song: ISong;
  selectedAudio?: IAudio;
  onArtistSelect(audio: IAudio): void;
}

export default class ArtistList extends React.Component<IProps> {
  public render(): JSX.Element {
    return (
      <ScrollView persistentScrollbar={true}>
        {this.props.song.audio.map((audio, index) => (
          // eslint-disable-next-line react/no-array-index-key
          <View key={audio.uid + index}>
            <Artist
              audio={audio}
              isSelected={Boolean(
                this.props.selectedAudio && audio.uid === this.props.selectedAudio.uid
              )}
              onSelect={() => this.onArtistSelect(audio)}
            />
          </View>
        ))}
      </ScrollView>
    );
  }

  private onArtistSelect = (audio: IAudio): void => {
    this.props.onArtistSelect(audio);
  };
}
