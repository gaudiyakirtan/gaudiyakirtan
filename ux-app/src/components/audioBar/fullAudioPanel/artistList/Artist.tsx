import React from "react";

import { getArtistId, IAudio } from "../../../../data/models/server/ISong";
import Text, { TextType } from "../../../lib/text/Text";
import LocalFileSystem from "../../../../utils/fileSystem/LocalFileSystem";
import { LocalFileUris } from "../../../../utils/fileSystem/FileUris";
import Icon from "../../../lib/icon/Icon";
import AudioDownloader from "../../audioDownloader/AudioDownloader";
import RowLayout, { Left, Body, Right } from "../../../lib/rowLayout/RowLayout";
import Touchable from "../../../lib/touchable/Touchable";
import AppDimensions from "../../../../utils/appDimensions/AppDimensions";
import ArtistPic from "../../artistPic/ArtistPic";
import Theme from "../../../../utils/theme/Theme";

interface IProps {
  audio: IAudio;
  isSelected: boolean;
  onSelect(): void;
}

interface IState {
  localAudioFileExists: boolean | undefined;
  downloading: boolean;
  startDownload: () => void;
}

export default class Artist extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      localAudioFileExists: undefined,
      downloading: false,
      startDownload: () => {}
    };
  }

  public async componentDidMount(): Promise<void> {
    const localAudioFileExists = await this.getLocalAudioFileExists();
    this.setState({ localAudioFileExists });
  }

  public render(): JSX.Element {
    const theme = Theme.getCurrentTheme();

    let textType: TextType;
    if (this.state.downloading) {
      textType = "lowlight";
    } else {
      textType = this.props.isSelected ? "highlight" : "normal";
    }

    const showDownLoader = this.state.localAudioFileExists === false;
    const artistId = getArtistId(this.props.audio.uid);
    const artistName = this.props.audio.meta.artist;

    let onTextPress: (() => void) | undefined;
    if (this.state.downloading || this.props.isSelected) {
      onTextPress = undefined;
    } else if (showDownLoader) {
      onTextPress = this.state.startDownload;
    } else {
      onTextPress = this.props.onSelect;
    }

    return (
      <RowLayout
        height={AppDimensions.HeaderHeight}
        borderRadius={25}
        backgroundColor={theme.backgroundSecondary}
        marginTop={5}
        marginBottom={5}
      >
        <Left>
          <ArtistPic type="thumbnail" artistId={artistId} artistName={artistName} />
        </Left>
        <Body paddingLeft={10}>
          <Touchable
            onPress={onTextPress}
            width="100%"
            noHorzCenter={true}
            disabled={textType === "lowlight"}
          >
            <Text type={textType}>{this.props.audio.meta.artist}</Text>
          </Touchable>
        </Body>
        <Right>{this.renderRightIcon()}</Right>
      </RowLayout>
    );
  }

  private renderRightIcon(): JSX.Element {
    if (this.state.localAudioFileExists === undefined) {
      return <></>;
    }
    const showDownLoader = this.state.localAudioFileExists === false;
    if (showDownLoader) {
      return (
        <AudioDownloader
          audio={this.props.audio}
          onDownloadStart={this.onDownloadStart}
          onDownloadStoppedInBetween={this.onDownloadStoppedInBetween}
          onDownloadComplete={this.onDownloadComplete}
          setStartDownload={this.setStartDownload}
        />
      );
    }

    if (this.props.isSelected) {
      return (
        <Touchable>
          <Icon name="ios-musical-notes" type="highlight" />
        </Touchable>
      );
    } else {
      return (
        <Touchable onPress={this.props.onSelect}>
          <Icon name="md-play" type="normal" />
        </Touchable>
      );
    }
  }

  private async getLocalAudioFileExists(): Promise<boolean> {
    const localFileUri = LocalFileUris.audio(this.props.audio.fn);
    const localFileInfo = await LocalFileSystem.exists(localFileUri);
    return localFileInfo.exists;
  }

  private setStartDownload = (startDownload: () => void): void => {
    this.setState({ startDownload });
  };

  private onDownloadStart = (): void => {
    this.setState({ downloading: true });
  };

  private onDownloadStoppedInBetween = (): void => {
    this.setState({ localAudioFileExists: false, downloading: false });
  };

  private onDownloadComplete = async (): Promise<void> => {
    const localAudioFileExists = await this.getLocalAudioFileExists();
    this.setState({ localAudioFileExists, downloading: false });
  };
}
