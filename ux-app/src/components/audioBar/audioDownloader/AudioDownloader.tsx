import React from "react";
import { View } from "react-native";
import { DownloadProgressData, DownloadResumable } from "expo-file-system";
import { AnimatedCircularProgress } from "react-native-circular-progress";

import Icon, { IconSize, IconType } from "../../lib/icon/Icon";
import { IAudio } from "../../../data/models/server/ISong";
import RemoteFileSystem from "../../../utils/fileSystem/RemoteFileSystem";
import { RemoteFileUris, LocalFileUris } from "../../../utils/fileSystem/FileUris";
import Touchable from "../../lib/touchable/Touchable";
import Theme from "../../../utils/theme/Theme";
import LocalFileSystem from "../../../utils/fileSystem/LocalFileSystem";

interface IProps {
  audio: IAudio;
  touchableIconWidth?: number;
  iconType?: IconType;
  onDownloadStart(): void;
  onDownloadStoppedInBetween(): void;
  onDownloadComplete(): void;
  setStartDownload?(startDownload: () => void): void;
}

interface IState {
  downloading: boolean;
  downloadPercent: number;
  error?: string;
  showError: boolean;
}

export default class AudioDownloader extends React.Component<IProps, IState> {
  private downloadResumable?: DownloadResumable;

  public constructor(props: IProps) {
    super(props);
    this.state = {
      downloading: false,
      downloadPercent: 0,
      error: undefined,
      // eslint-disable-next-line react/no-unused-state
      showError: false
    };
  }

  public componentDidMount(): void {
    if (this.props.setStartDownload) {
      this.props.setStartDownload(this.startDownload);
    }
  }

  // eslint-disable-next-line react/sort-comp
  public startDownload = (): void => {
    this.onDownLoadPress();
  };

  public render(): JSX.Element {
    if (this.state.error) {
      return (
        <>
          <Touchable width={this.props.touchableIconWidth}>
            <Icon name="md-alert" size={IconSize.Medium} type="error" />
          </Touchable>
        </>
      );
    }

    const theme = Theme.getCurrentTheme();
    if (this.state.downloading) {
      const fill = this.state.downloadPercent < 5 ? 5 : this.state.downloadPercent;
      return (
        <Touchable onPress={this.onDownLoadStop} width={this.props.touchableIconWidth}>
          <AnimatedCircularProgress
            size={28}
            width={3}
            fill={fill}
            tintColor={theme.controlHighlight}
            backgroundColor={theme.controlLowlight}
          >
            {() => (
              <View style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
                <Icon name="controller-stop" size={16} family="Entypo" />
              </View>
            )}
          </AnimatedCircularProgress>
        </Touchable>
      );
    } else {
      return (
        <Touchable onPress={this.onDownLoadPress} width={this.props.touchableIconWidth}>
          <Icon name="md-cloud-download" type={this.props.iconType || "normal"} size={28} />
        </Touchable>
      );
    }
  }

  private onDownLoadPress = async (): Promise<void> => {
    this.props.onDownloadStart();
    this.setState({ downloading: true, downloadPercent: 0, error: undefined });

    try {
      this.downloadResumable = RemoteFileSystem.downloadResumable(
        RemoteFileUris.audio(this.props.audio.fn),
        LocalFileUris.audio(this.props.audio.fn),
        this.downloadProgressCallback
      );

      await this.downloadResumable.downloadAsync();
    } catch (exception) {
      // Ignore "Socket closed" error as it is raised when pauseAsync() is called below
      const error = exception.toString().includes("Socket closed") ? undefined : exception;

      this.setState({ downloading: false, downloadPercent: 0, error: JSON.stringify(error) });
      await this.deletePartiallyDownloadedLocalFile();
      this.props.onDownloadStoppedInBetween();
      return;
    }

    this.setState({ downloading: false, downloadPercent: 0, error: undefined });
    this.props.onDownloadComplete();
  };

  private onDownLoadStop = async (): Promise<void> => {
    this.setState({ downloading: false, downloadPercent: 0, error: undefined });

    if (this.downloadResumable) {
      try {
        await this.downloadResumable.pauseAsync();
      } catch (error) {
        this.setState({ error });
      }
    }

    await this.deletePartiallyDownloadedLocalFile();
    this.props.onDownloadStoppedInBetween();
  };

  private downloadProgressCallback = (downloadProgressData: DownloadProgressData): void => {
    const downloadPercent = this.getDownloadPercentage(downloadProgressData);
    this.setState({ downloadPercent });
  };

  private getDownloadPercentage = (downloadProgressData: DownloadProgressData): number => {
    if (
      downloadProgressData &&
      downloadProgressData.totalBytesWritten !== undefined &&
      downloadProgressData.totalBytesExpectedToWrite
    ) {
      const downloaded = downloadProgressData.totalBytesWritten;
      const total = downloadProgressData.totalBytesExpectedToWrite;
      return Math.round((downloaded / total) * 100);
    }
    return 0;
  };

  private deletePartiallyDownloadedLocalFile = async (): Promise<void> => {
    const localFileUri = LocalFileUris.audio(this.props.audio.fn);
    await LocalFileSystem.delete(localFileUri);
  };

  // private onErrorPress = (): void => {
  //   this.setState({ showError: true });
  // };

  // private hideError = (): void => {
  //   this.setState({ showError: false });
  // };
}
