import React from "react";
import { View } from "react-native";

import Data from "../../../data/Data";
import DataTransform from "../../../data/DataTransform";
import ISong from "../../../data/models/server/ISong";
import ISongList, { ISongListSong } from "../../../data/models/server/ISongList";
import ActiveScreenRefs from "../../../utils/activeScreenRefs/ActiveScreenRefs";
import RouteName from "../../../utils/navigation/RouteName";
import Theme from "../../../utils/theme/Theme";
import TransliterationUtils from "../../../utils/transliteration/TransliterationUtils";
import transliterate from "../../../utils/transliteration/transliterator";
import Center from "../../lib/center/Center";
import ErrorScreen from "../../lib/errorScreen/ErrorScreen";
import Header from "../../lib/header/Header";
import List, { IListItem } from "../../lib/lists/list/List";
import Screen from "../../lib/screen/Screen";
import Spinner from "../../lib/spinner/Spinner";
import Tabs from "../../lib/tabs/Tabs";
import Text from "../../lib/text/Text";
import UidCircle from "../../lib/uidCircle/UidCircle";
import Settings from "../../../utils/settings/Settings";
import { Mathas, MathaBlackList } from "../../../utils/settings/Matha";
import { UserLanguage } from "../../../utils/settings/UserLanguage";

import { onSongListItemClick, renderSongListItemRightContent } from "./SongList";

interface IState {
  loading: boolean;
  searching: boolean;
  searchText?: string;
  songList: ISongList;
  // Results
  uidMatchedSongs: ISongList;
  titleMatchedSongs: ISongList;
  contentMatchedSongs: ISongList;
  activeTabIndex: number;
}

export default class SongListSearch extends React.Component<{}, IState> {
  public constructor(props: {}) {
    super(props);
    this.state = {
      loading: true,
      searching: false,
      searchText: undefined,
      songList: [],
      uidMatchedSongs: [],
      titleMatchedSongs: [],
      contentMatchedSongs: [],
      activeTabIndex: 0
    };
  }

  public async componentDidMount(): Promise<void> {
    await this.loadSongList();
    ActiveScreenRefs.add(RouteName.SongListSearch, this);
  }

  public render(): JSX.Element {
    return (
      <Screen>
        <Header
          headerType="search"
          title="Search..."
          autoFocusSearchBox={true}
          onSearchTextChange={this.onSearchTextChange}
        />
        {this.renderContent()}
      </Screen>
    );
  }

  private renderContent(): JSX.Element {
    if (this.state.loading || this.state.searching) {
      return (
        <Center hor={true} ver={true}>
          {this.state.searching && <Text>Searching...</Text>}
          <Spinner />
        </Center>
      );
    }
    if (!this.state.searchText) {
      return (
        <ErrorScreen
          type="info"
          iconName="md-search"
          text="Search by kirtan uid, title, or content"
        />
      );
    }

    return (
      <Tabs
        activeIndex={this.state.activeTabIndex}
        tabs={[
          {
            index: 0,
            heading: this.renderTabHeading("Title", this.state.titleMatchedSongs.length, 0),
            content: this.renderResultsList(this.getListItems(this.state.titleMatchedSongs))
          },
          {
            index: 1,
            heading: this.renderTabHeading("Content", this.state.contentMatchedSongs.length, 1),
            content: this.renderResultsList(this.getListItems(this.state.contentMatchedSongs))
          },
          {
            index: 2,
            heading: this.renderTabHeading("UID", this.state.uidMatchedSongs.length, 2),
            content: this.renderResultsList(this.getListItems(this.state.uidMatchedSongs))
          }
        ]}
        tabContainerStyle={{ height: 45 }}
        onTabChange={this.onTabChange}
      />
    );
  }

  private renderTabHeading(heading: string, count: number, index: number): JSX.Element {
    const theme = Theme.getCurrentTheme();
    return (
      <View style={{ height: "100%", backgroundColor: theme.backgroundSecondary }}>
        <Text type={index === this.state.activeTabIndex ? "highlight" : "normal"}>{heading}</Text>
        <Text type="lowlight">{`(${count})`}</Text>
      </View>
    );
  }

  private renderResultsList(listItems: IListItem[]): JSX.Element {
    if (!listItems || listItems.length === 0) {
      return <ErrorScreen text="No results found" iconName="logo-dropbox" />;
    }
    return <List items={listItems} />;
  }

  private loadSongList = async (): Promise<void> => {
    this.setState({ loading: true, songList: [] });
    const songList = await Data.LoadSongList();
    this.setState({ loading: false, songList });
  };

  private getListItems(songList: ISongList): IListItem[] {
    // Sort by match score
    const sortedSongList = [...songList];
    sortedSongList.sort((a, b) => (a.title < b.title ? -1 : 1));

    const listItems: IListItem[] = [];
    sortedSongList.forEach(song => {
      const title = song.title.replace(/\d{4}/, "");
      listItems.push({
        key: song.uid,
        title: DataTransform.transformSongTitle(title, song.language),
        description:
          song.author[0] === "@"
            ? DataTransform.transformContent(song.author, song.language)
            : DataTransform.transformAuthor(song.author, song.language),
        leftContent: this.getListItemLeftContent(song),
        rightContent: renderSongListItemRightContent(song),
        metaData: song,
        onClick: onSongListItemClick
      });
    });

    return listItems;
  }

  private getListItemLeftContent(song: ISongListSong): JSX.Element {
    return <UidCircle uid={song.uid} />;
  }

  private onSearchTextChange = (searchText?: string): void => {
    if (!searchText) {
      this.setState({
        searching: false,
        searchText: undefined,
        uidMatchedSongs: [],
        titleMatchedSongs: [],
        contentMatchedSongs: []
      });
      return;
    }

    this.setState({ searching: true, searchText });

    const fuzzySearchText = TransliterationUtils.fuzzy(searchText.toLowerCase());
    const uidSearchText = searchText.toUpperCase();

    // Do all of the strength scoring and match type here
    const uidMatchedSongs: ISongList = [];
    const titleMatchedSongs: ISongList = [];
    const contentMatchedSongs: ISongList = [];

    const userMatha = Settings.get().userMatha;

    this.state.songList.forEach((song: ISongListSong) => {
      if (!MathaBlackList[song.uid] || Mathas[userMatha].allow.indexOf(song.uid) > -1) {
        if (song.uid.includes(uidSearchText)) {
          const newEntry = { ...song };
          newEntry.title = song.uid === uidSearchText ? `0001${song.title}` : `0002${song.title}`;
          uidMatchedSongs.push(newEntry);
        }

        if (fuzzySearchText.length > 0 && song.fuzzyTitle.includes(fuzzySearchText)) {
          const newEntry = { ...song };
          const score = TransliterationUtils.getScore(
            transliterate(song.title, song.language, UserLanguage.eng),
            searchText
          );
          if (score > 0.1) {
            const scoreString = `${1001 + Math.floor((1 - score) * 998)}`;
            newEntry.title = `${scoreString}${song.title}`;
            titleMatchedSongs.push(newEntry);
          }
        }

        if (fuzzySearchText.length > 2 && song.fuzzyContent.includes(fuzzySearchText)) {
          const newEntry = { ...song };
          const songData = Data.FetchSongData(song.uid, false); // addToRecents = false
          const matchLine = this.getMatchLine(songData, song.fuzzyContent, fuzzySearchText);
          const score = TransliterationUtils.getScore(
            transliterate(matchLine, song.language, UserLanguage.eng),
            searchText
          );
          const scoreString = `${2001 + Math.floor((1 - score) * 998)}`;
          newEntry.title = `${scoreString}${song.title}`;
          newEntry.author = `@ ${matchLine}`;
          contentMatchedSongs.push(newEntry);
        }
      }
    });

    this.setState({
      searching: false,
      searchText,
      uidMatchedSongs,
      titleMatchedSongs,
      contentMatchedSongs
    });
  };

  private getMatchLine = (s: ISong, a: string, b: string): string => {
    let p = a.indexOf(b);
    let c = a.substr(p);
    p = c.search("{");
    c = c.substr(p + 1);
    p = c.search("}");
    const xy = c.substr(0, p).split("|");

    const line = s.verses[xy[0]].lines[xy[1]];
    let content = "";
    if (!line) {
      return content;
    }
    for (let w = 0; w < line.length; w++) {
      content += line[w].w + line[w].h;
    }

    return content;
  };

  private onTabChange = (clickedTabIndex: number): void => {
    this.setState({ activeTabIndex: clickedTabIndex });
  };
}
