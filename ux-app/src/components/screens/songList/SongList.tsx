/* eslint-disable @typescript-eslint/no-use-before-define */
import React from "react";
import { View } from "react-native";

import Header from "../../lib/header/Header";
import { addGroupCountInHeader, IListItem } from "../../lib/lists/list/List";
import Spinner from "../../lib/spinner/Spinner";
import RouteName from "../../../utils/navigation/RouteName";
import ISongList, { ISongListSong } from "../../../data/models/server/ISongList";
import ErrorScreen from "../../lib/errorScreen/ErrorScreen";
import Screen from "../../lib/screen/Screen";
import Center from "../../lib/center/Center";
import Data from "../../../data/Data";
import ActiveScreenRefs from "../../../utils/activeScreenRefs/ActiveScreenRefs";
import TransliterationUtils from "../../../utils/transliteration/TransliterationUtils";
import Navigation from "../../../utils/navigation/Navigation";
import Icon from "../../lib/icon/Icon";
import AlphabeticList from "../../lib/lists/alphabeticList/AlphabeticList";
import UidCircle from "../../lib/uidCircle/UidCircle";
import Touchable from "../../lib/touchable/Touchable";
import DataTransform from "../../../data/DataTransform";
import MoreSettingsModal, {
  IMoreSetting
} from "../../lib/modals/moreSettingsModal/MoreSettingsModal";
import AppDimensions from "../../../utils/appDimensions/AppDimensions";

interface IState {
  loading: boolean;
  songList: ISongList;
}

export default class SongList extends React.Component<{}, IState> {
  public constructor(props: {}) {
    super(props);
    this.state = {
      loading: true,
      songList: []
    };
  }

  public async componentDidMount(): Promise<void> {
    await this.loadSongList();
    ActiveScreenRefs.add(RouteName.SongList, this);
  }

  public render(): JSX.Element {
    return (
      <Screen>
        <Header
          title="All Kirtans"
          leftContent={this.renderHeaderLeftContent()}
          rightContent={this.renderHeaderRightContent()}
          marginBottom={5}
        />
        {this.renderContent()}
      </Screen>
    );
  }

  private renderHeaderLeftContent(): JSX.Element {
    return (
      <>
        <Touchable />
        <Touchable width={AppDimensions.MoreIconWidth} />
      </>
    );
  }

  private renderHeaderRightContent(): JSX.Element {
    const noItemsYet = this.state.loading || !this.state.songList;
    const moreSettings: IMoreSetting[] = [
      {
        text: "Refresh",
        iconName: "ios-refresh",
        disabled: this.state.loading,
        onPress: this.onRefreshPress
      }
    ];

    return (
      <>
        <Touchable onPress={this.onSearchIconPress} disabled={noItemsYet}>
          <Icon name="md-search" type={noItemsYet ? "disabled" : "normal"} />
        </Touchable>
        <MoreSettingsModal moreSettings={moreSettings} />
      </>
    );
  }

  private renderContent(): JSX.Element {
    if (this.state.loading) {
      return (
        <Center hor={true} ver={true}>
          <Spinner />
        </Center>
      );
    }

    const listItems = this.getListItems();
    if (!listItems) {
      return (
        <ErrorScreen
          text="Oops! Kirtan list is empty"
          retry={true}
          onRetryPress={this.loadSongList}
        />
      );
    }

    return <AlphabeticList items={listItems} />;
  }

  private renderListItemLeftContent(song: ISongListSong): JSX.Element {
    return <UidCircle uid={song.uid} />;
  }

  private loadSongList = async (): Promise<void> => {
    this.setState({ loading: true, songList: [] });
    await Data.LoadBundle();
    const songList = await Data.LoadSongList();
    this.setState({ loading: false, songList });
  };

  private getListItems(): IListItem[] {
    const transliteratedSongList: ISongList = this.state.songList.map(song => ({
      ...song,
      title: DataTransform.transformSongTitle(song.title, song.language),
      author: DataTransform.transformAuthor(song.author, song.language)
    }));

    transliteratedSongList.sort((a, b) =>
      TransliterationUtils.unDiac(a.title.toLowerCase()).replace(/\(|‘/g, "") <
      TransliterationUtils.unDiac(b.title.toLowerCase()).replace(/\(|‘/g, "")
        ? -1
        : 1
    );

    const listItems: IListItem[] = [];

    // Add headers
    let prevSongFirstLetter: string | undefined;
    let index = 0;
    transliteratedSongList.forEach(song => {
      const songFirstLetter = TransliterationUtils.unDiac(
        song.title.replace(/\(|‘/g, "")[0].toLowerCase()
      ).toUpperCase();
      if (songFirstLetter !== prevSongFirstLetter) {
        listItems.push({
          key: index++,
          title: songFirstLetter,
          isHeader: true
        });
      }

      listItems.push({
        key: index++,
        title: song.title,
        description: song.author,
        leftContent: this.renderListItemLeftContent(song),
        rightContent: renderSongListItemRightContent(song),
        leftContentWidth: AppDimensions.PicThumbNailSize + 10,
        metaData: song,
        onClick: onSongListItemClick
      });

      prevSongFirstLetter = songFirstLetter;
    });

    addGroupCountInHeader(listItems);
    return listItems;
  }

  private onSearchIconPress = (): void => {
    Navigation.navigate(RouteName.SongListSearch);
  };

  private onRefreshPress = async (): Promise<void> => {
    await this.loadSongList();
  };
}

export function renderSongListItemRightContent(song: ISongListSong): JSX.Element {
  const audioExists = song.audio.length > 0;
  const multipleArtistsExist = song.audio.length > 1;

  // eslint-disable-next-line no-nested-ternary
  const name = audioExists ? (multipleArtistsExist ? "playlist-music" : "music-note") : undefined;
  const size = multipleArtistsExist ? 23 : 18;
  const width = multipleArtistsExist ? 23 : 18;

  return (
    <View style={{ marginRight: 5 }}>
      {name && (
        <Icon
          name={name}
          family="MaterialCommunityIcons"
          type="highlight"
          size={size}
          width={width}
        />
      )}
    </View>
  );
}

export function onSongListItemClick(item: IListItem): void {
  if (item.metaData) {
    const song = item.metaData as ISongListSong;
    Navigation.navigate(RouteName.Song, { songUid: song.uid, songMD5: song.md5 });
    // TODO : Replace below hack with proper navigation fix
    /* @ts-ignore */
    ActiveScreenRefs.get(RouteName.Song).loadSong();
  }
}
