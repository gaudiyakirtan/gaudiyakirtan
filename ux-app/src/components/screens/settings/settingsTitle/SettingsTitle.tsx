import React from "react";
import { View } from "react-native";

import Text, { TextType } from "../../../lib/text/Text";
import Icon, { IconFamily, IconSize } from "../../../lib/icon/Icon";
import RowLayout, { Left, Body } from "../../../lib/rowLayout/RowLayout";
import HorizontalRule from "../../../lib/horizontalRule/HorizontalRule";
import RouteName from "../../../../utils/navigation/RouteName";
import Navigation from "../../../../utils/navigation/Navigation";
import Touchable from "../../../lib/touchable/Touchable";
import Logo from "../../../lib/logo/Logo";
import Toast, { ToastType } from "../../../lib/toast/Toast";

interface IProps {
  text: string;
  textType?: TextType;
  textTooltip?: string;
  subtext?: string;
  iconName?: string;
  iconFamily?: IconFamily;
  iconSize?: IconSize;
  routeName?: RouteName;
  noHorRule?: boolean;
  displayLogo?: boolean;
  onPress?(): void;
}

export const SettingsLeftColWidth = 50;

export default class SettingsTitle extends React.Component<IProps> {
  public render(): JSX.Element {
    return (
      <View>
        <RowLayout>
          <Left width={SettingsLeftColWidth}>
            {this.props.displayLogo && <Logo width={35} height={35} />}
            {!this.props.displayLogo && this.props.iconName && (
              <Icon
                name={this.props.iconName}
                type="highlight"
                family={this.props.iconFamily}
                size={this.props.iconSize}
              />
            )}
          </Left>
          <Body noHorzCenter={true}>
            {this.props.routeName && (
              <Touchable width="100%" noHorzCenter={true} onPress={this.onPress}>
                {this.renderTitle()}
              </Touchable>
            )}
            {!this.props.routeName && this.renderTitle()}
          </Body>
        </RowLayout>
        {!this.props.noHorRule && (
          <HorizontalRule marginTop={10} marginBottom={10} marginLeft={SettingsLeftColWidth} />
        )}
      </View>
    );
  }

  private renderTitle(): JSX.Element {
    return (
      <>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Text type={this.props.textType || "normal"} fontSize={16}>
            {this.props.text}
          </Text>
          {this.props.textTooltip && (
            <Touchable height={25} width={40} onPress={this.onTextTooltipPress}>
              <Icon name="question" family="SimpleLineIcons" type="highlight" size={20} />
            </Touchable>
          )}
        </View>
        {this.props.subtext && (
          <View style={{ marginTop: 2 }}>
            <Text type="lowlight">{this.props.subtext}</Text>
          </View>
        )}
      </>
    );
  }

  private onPress = (): void => {
    if (this.props.routeName) {
      Navigation.navigate(this.props.routeName);
    } else if (this.props.onPress) {
      this.props.onPress();
    }
  };

  private onTextTooltipPress = (): void => {
    if (this.props.textTooltip) {
      Toast.show(ToastType.Info, this.props.textTooltip, { position: "top" });
    }
  };
}
