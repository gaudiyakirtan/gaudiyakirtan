import React from "react";
import { Share, TouchableWithoutFeedback, View } from "react-native";

import Data from "../../../data/Data";
import ConfirmationModal from "../../lib/modals/confirmationModal/ConfirmationModal";
import Text from "../../lib/text/Text";

interface IProps {
  children: JSX.Element;
}

interface IState {
  showDebugReport: boolean;
}

export default class DebugReport extends React.Component<IProps, IState> {
  private debugClicks = 0;

  public constructor(props: IProps) {
    super(props);
    this.state = {
      showDebugReport: false
    };
  }

  public render(): JSX.Element {
    return (
      <>
        <TouchableWithoutFeedback onPress={this.onDebugClick}>
          {/* TouchableWithoutFeedback always needs to have child View. A component composing a View isn't enough. */}
          <View>{this.props.children}</View>
        </TouchableWithoutFeedback>
        {this.state.showDebugReport && this.renderReportModal()}
      </>
    );
  }

  private renderReportModal(): JSX.Element {
    const reportJsx = Object.keys(Data.DebugReport).map(report => (
      <Text key={report} fontSize={10}>
        {`${report}: `}
        <Text textAlign="right" type="highlight" fontSize={10}>
          {Data.DebugReport[report]}
        </Text>
      </Text>
    ));

    return (
      <ConfirmationModal
        open={this.state.showDebugReport}
        content={reportJsx}
        yesButtonText="Share"
        noButtonText="Close"
        onConfirmation={this.shareDebugReport}
        onCancel={this.onModalClose}
      />
    );
  }

  private onDebugClick = (): void => {
    this.debugClicks++;
    if (this.debugClicks === 3) {
      this.debugClicks = 0;
      this.setState({ showDebugReport: true });
    }
    setTimeout(() => {
      this.debugClicks = 0;
    }, 1000);
  };

  private shareDebugReport = async (): Promise<void> => {
    this.onModalClose();
    try {
      await Share.share({
        message: JSON.stringify(Data.DebugReport, null, 2)
      });
    } catch (error) {
      // error.message
    }
  };

  private onModalClose = (): void => {
    this.setState({ showDebugReport: false });
  };
}
