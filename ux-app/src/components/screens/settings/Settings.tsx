/* eslint-disable react/no-unused-state */
import React from "react";
import { StyleSheet, View } from "react-native";

import Header from "../../lib/header/Header";
import Screen from "../../lib/screen/Screen";
import SettingsService from "../../../utils/settings/Settings";
import HorizontalRule from "../../lib/horizontalRule/HorizontalRule";
import Content from "../../lib/content/Content";
import Text from "../../lib/text/Text";
import ISettings from "../../../data/models/local/ISettings";
import RouteName from "../../../utils/navigation/RouteName";
import ActiveScreenRefs from "../../../utils/activeScreenRefs/ActiveScreenRefs";
import fetchData from "../../../data/datalz";
import fetchCollectionsData from "../../../data/collectionslz";
import ScrollView from "../../lib/scrollView/ScrollView";
import Logo from "../../lib/logo/Logo";
import Switch from "../../lib/switch/Switch";
import IconAction from "../../lib/icon/IconAction";
import { getLanguageTitle } from "../../../utils/settings/UserLanguage";
import DataTransform from "../../../data/DataTransform";

import SettingsTitle, { SettingsLeftColWidth } from "./settingsTitle/SettingsTitle";
import DebugReport from "./DebugReport";
import SettingsLanguageModal from "./sections/SettingsLanguageModal";
import SettingsControl from "./SettingsControl";
import SettingsMathaModal from "./sections/SettingsMathaModal";

const styles = StyleSheet.create({
  appVersion: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10
  },
  logo: { marginRight: 10 },
  stayTuned: { margin: 20, marginRight: 0 }
});

// SettingsService.get() should be the single source of truth for the latest value of settings for every component.
// Otherwise we run into race conditions with multiple copies of settings.
// So, don't store settings in local state, except for non-stored temporary state like 'showLanguageModal'.
// Instead use a dummy time state variable to rerender.
interface IState {
  lastUpdateTime: number;
  showLanguageModal: boolean;
  showMathaModal: boolean;
}

export default class Settings extends React.Component<{}, IState> {
  public constructor(props: {}) {
    super(props);
    this.state = {
      lastUpdateTime: Date.now(),
      showLanguageModal: false,
      showMathaModal: false
    };
  }

  public componentDidMount(): void {
    ActiveScreenRefs.add(RouteName.Settings, this);
  }

  public render(): JSX.Element {
    const s = SettingsService.get();
    return (
      <Screen>
        <Header title="Settings" />
        <ScrollView>
          <Content marginTop={15} paddingRight={15}>
            <SettingsTitle
              text="Theme"
              textType="highlight"
              iconName="ios-color-palette"
              noHorRule={true}
            />
            <SettingsControl
              text={s.isDarkTheme ? "Kṛṣṇa" : "Gaurāṅga"}
              rightContent={<Switch value={s.isDarkTheme} onValueChange={this.onThemePress} />}
              onPress={this.onThemePress}
            />
            <HorizontalRule marginTop={10} marginBottom={10} marginLeft={SettingsLeftColWidth} />

            <SettingsTitle
              text="Language"
              textType="highlight"
              iconName="language"
              iconFamily="Entypo"
              textTooltip="Set the language for verse, synonyms and translations."
              noHorRule={true}
            />
            <SettingsControl
              text={getLanguageTitle(s.userLanguage)}
              rightContent={this.renderLanguageAction()}
              onPress={this.onLanguagePress}
            />
            {this.state.showLanguageModal && (
              <SettingsLanguageModal onModalClose={this.onLanguageModalClose} />
            )}
            <HorizontalRule marginTop={10} marginBottom={10} marginLeft={SettingsLeftColWidth} />

            <SettingsTitle
              text="Sanga"
              textType="highlight"
              iconName="ios-people"
              iconFamily="Ionicons"
              textTooltip="Filter songs specific to your sanga (eg. your guru-parampara song)."
              noHorRule={true}
            />
            <SettingsControl
              text={DataTransform.transformMatha(s.userMatha)}
              rightContent={this.renderMathaAction()}
              onPress={this.onMathaPress}
            />
            {this.state.showMathaModal && (
              <SettingsMathaModal onModalClose={this.onMathaModalClose} />
            )}
            <HorizontalRule marginTop={10} marginBottom={10} marginLeft={SettingsLeftColWidth} />

            <SettingsTitle
              text="Dedication"
              subtext="At the lotus feet of..."
              iconName="flower-tulip"
              iconFamily="MaterialCommunityIcons"
              routeName={RouteName.SettingsDedication}
            />

            <SettingsTitle
              text="About"
              subtext="Gaudiya Kirtan project, team, copyrights"
              iconName="info"
              iconFamily="Entypo"
              routeName={RouteName.SettingsAbout}
            />

            <SettingsTitle
              text="Report"
              subtext="Bug, verse error, translation error"
              iconName="ios-bug"
              iconFamily="Ionicons"
              routeName={RouteName.SettingsReport}
            />

            <SettingsTitle
              text="Request"
              subtext="Feature, song, recording, translation"
              iconName="email"
              iconFamily="MaterialCommunityIcons"
              routeName={RouteName.SettingsRequest}
            />

            <SettingsTitle
              text="Tell a friend"
              subtext="Share app download link"
              iconName="satellite-uplink"
              iconFamily="MaterialCommunityIcons"
              routeName={RouteName.SettingsShare}
            />

            <SettingsTitle
              text="Gaudiya Kirtan community"
              subtext="Facebook, Youtube"
              iconName="slideshare"
              iconFamily="Entypo"
              routeName={RouteName.SettingsCommunity}
            />

            <SettingsTitle
              text="Contact"
              subtext="Email, WhatsApp, call"
              iconName="phone-in-talk"
              iconFamily="MaterialIcons"
              routeName={RouteName.SettingsContact}
            />

            {this.renderAbout()}
          </Content>
        </ScrollView>
      </Screen>
    );
  }

  private renderLanguageAction(): JSX.Element {
    return <IconAction name="launch" family="MaterialIcons" onPress={this.onLanguagePress} />;
  }

  private renderMathaAction(): JSX.Element {
    return <IconAction name="launch" family="MaterialIcons" onPress={this.onMathaPress} />;
  }

  private renderAbout(): JSX.Element {
    return (
      <DebugReport>
        <>
          <View style={styles.appVersion}>
            <View style={styles.logo}>
              <Logo />
            </View>
            <Text fontSize={16}>App version : </Text>
            <Text type="highlight" fontSize={16}>
              {`1.0.${fetchData(true)}.${fetchCollectionsData(true)}`}
            </Text>
          </View>

          <View style={styles.stayTuned}>
            <Text textAlign="justify" type="lowlight">
              Stay tuned as we continously improve and add awesome new features! Coming soon -
              Advanced search, Personal collections, Artist profiles, Notifications, and many
              more...
            </Text>
          </View>
        </>
      </DebugReport>
    );
  }

  private updateSettings(newSettings: ISettings): void {
    SettingsService.set(newSettings);
    this.setState({ lastUpdateTime: Date.now() });
  }

  private onThemePress = (): void => {
    const currentSettings = SettingsService.get();
    this.updateSettings({ ...currentSettings, isDarkTheme: !currentSettings.isDarkTheme });

    ActiveScreenRefs.get("AppFooter").forceUpdate();

    // Update other screens asynchronously without blocking Settings screen and AppFooter rerendering
    setTimeout(async () => {
      ActiveScreenRefs.get(RouteName.SongList).forceUpdate();
      ActiveScreenRefs.get(RouteName.SongListSearch).forceUpdate();
      ActiveScreenRefs.get(RouteName.Collections).forceUpdate();
      ActiveScreenRefs.get(RouteName.Song).forceUpdate();
      ActiveScreenRefs.get(RouteName.Audio).forceUpdate();
      ActiveScreenRefs.get(RouteName.AudioDownloads).forceUpdate();
    }, 0);
  };

  private onLanguagePress = (): void => {
    this.setState({ showLanguageModal: true });
  };

  private onMathaPress = (): void => {
    this.setState({ showMathaModal: true });
  };

  private onLanguageModalClose = (): void => {
    // userLanguage in settings would be updated by <LanguageList />
    this.setState({ showLanguageModal: false });
    this.updateOtherScreens();
  };

  private onMathaModalClose = (): void => {
    // userMatha in settings would be updated by <MathaList />
    this.setState({ showMathaModal: false });
    this.updateOtherScreens();
  };

  private updateOtherScreens(): void {
    // Update other screens asynchronously
    setTimeout(async () => {
      ActiveScreenRefs.get(RouteName.SongList).forceUpdate();
      ActiveScreenRefs.get(RouteName.SongListSearch).forceUpdate();
      ActiveScreenRefs.get(RouteName.Collections).forceUpdate();
      ActiveScreenRefs.get(RouteName.Song).forceUpdate();
    }, 0);
  }
}
