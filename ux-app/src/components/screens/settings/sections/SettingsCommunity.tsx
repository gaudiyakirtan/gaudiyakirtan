import React from "react";
import { View } from "react-native";

import Screen from "../../../lib/screen/Screen";
import Content from "../../../lib/content/Content";
import Header from "../../../lib/header/Header";
import Text from "../../../lib/text/Text";
import ScrollView from "../../../lib/scrollView/ScrollView";
import Touchable from "../../../lib/touchable/Touchable";
import SettingsTitle, { SettingsLeftColWidth } from "../settingsTitle/SettingsTitle";
import HorizontalRule from "../../../lib/horizontalRule/HorizontalRule";
import { IconFamily } from "../../../lib/icon/Icon";
import WebsiteAction from "../../../lib/contactActions/WebsiteAction";

export default class SettingsCommunity extends React.Component {
  private facebookLink = "https://www.facebook.com/gaudiyakirtan";
  private facebookLiveLink = "https://www.facebook.com/groups/livekirtan";
  private youtubeLink = "https://www.youtube.com/channel/UCG0qJKLHdVtThkue79CDsjQ";

  public render(): JSX.Element {
    return (
      <Screen>
        <Header
          title="Gaudiya Kirtan community"
          showBackButton={true}
          rightContent={this.renderHeaderRightContent()}
        />
        <ScrollView>
          <Content marginTop={15} paddingRight={15}>
            {this.renderSection(
              "Facebook",
              "facebook-with-circle",
              "Entypo",
              this.renderFacebookLink()
            )}
            {this.renderSection(
              "Facebook LIVE",
              "facebook-with-circle",
              "Entypo",
              this.renderFacebookLiveLink()
            )}
            {this.renderSection("YouTube", "logo-youtube", "Ionicons", this.renderYoutubeLink())}
          </Content>
        </ScrollView>
      </Screen>
    );
  }

  private renderSection(
    title: string,
    iconName: string,
    iconFamily: IconFamily,
    sectionContent: JSX.Element
  ): JSX.Element {
    return (
      <>
        <SettingsTitle
          text={title}
          textType="highlight"
          iconName={iconName}
          iconFamily={iconFamily}
          noHorRule={true}
        />
        <View style={{ marginLeft: SettingsLeftColWidth }}>{sectionContent}</View>
        <HorizontalRule marginTop={10} marginBottom={10} marginLeft={SettingsLeftColWidth} />
      </>
    );
  }

  private renderFacebookLink(): JSX.Element {
    return this.renderActionIcon(
      "facebook.com/gaudiyakirtan",
      <WebsiteAction url={this.facebookLink} />
    );
  }

  private renderFacebookLiveLink(): JSX.Element {
    return this.renderActionIcon(
      "facebook.com/groups/livekirtan",
      <WebsiteAction url={this.facebookLiveLink} />
    );
  }

  private renderYoutubeLink(): JSX.Element {
    return this.renderActionIcon(
      "Gaudiya Kirtan channel",
      <WebsiteAction url={this.youtubeLink} />
    );
  }

  private renderActionIcon(text: string, contactAction: JSX.Element): JSX.Element {
    return (
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Text fontSize={15}>{text}</Text>
        {contactAction}
      </View>
    );
  }

  private renderHeaderRightContent(): JSX.Element | undefined {
    // Empty place holder to symmetrically center header title
    return <Touchable />;
  }
}
