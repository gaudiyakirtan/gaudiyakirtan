import React from "react";
import { View } from "react-native";

import Screen from "../../../lib/screen/Screen";
import Content from "../../../lib/content/Content";
import Header from "../../../lib/header/Header";
import Text from "../../../lib/text/Text";
import ScrollView from "../../../lib/scrollView/ScrollView";
import Touchable from "../../../lib/touchable/Touchable";
import SettingsTitle, { SettingsLeftColWidth } from "../settingsTitle/SettingsTitle";
import HorizontalRule from "../../../lib/horizontalRule/HorizontalRule";
import { IconFamily } from "../../../lib/icon/Icon";
import ShareAction from "../../../lib/contactActions/ShareAction";
import EmailAction from "../../../lib/contactActions/EmailAction";
import WhatsAppAction from "../../../lib/contactActions/WhatsAppAction";

export default class SettingsShare extends React.Component {
  private shareMessage = "Download The Gaudiya Kirtan App from http://gaudiyakirtan.com";

  public render(): JSX.Element {
    return (
      <Screen>
        <Header
          title="Tell a friend"
          showBackButton={true}
          rightContent={this.renderHeaderRightContent()}
        />
        <ScrollView>
          <Content marginTop={15} paddingRight={15}>
            {this.renderSection(
              "Share",
              "satellite-uplink",
              "MaterialCommunityIcons",
              this.renderShareOptions()
            )}
          </Content>
        </ScrollView>
      </Screen>
    );
  }

  private renderSection(
    title: string,
    iconName: string,
    iconFamily: IconFamily,
    sectionContent: JSX.Element
  ): JSX.Element {
    return (
      <>
        <SettingsTitle
          text={title}
          textType="highlight"
          iconName={iconName}
          iconFamily={iconFamily}
          noHorRule={true}
        />
        <View style={{ marginLeft: SettingsLeftColWidth }}>{sectionContent}</View>
        <HorizontalRule marginTop={10} marginBottom={10} marginLeft={SettingsLeftColWidth} />
      </>
    );
  }

  private renderShareOptions(): JSX.Element {
    return this.renderActionIcon(
      "Share app download link",
      <>
        <EmailAction subject="Haribol! Download The Gaudiya Kirtan App" body={this.shareMessage} />
        <WhatsAppAction message={this.shareMessage} />
        <ShareAction message={this.shareMessage} />
      </>
    );
  }

  private renderActionIcon(text: string, contactAction: JSX.Element): JSX.Element {
    return (
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Text fontSize={15}>{text}</Text>
        {contactAction}
      </View>
    );
  }

  private renderHeaderRightContent(): JSX.Element | undefined {
    // Empty place holder to symmetrically center header title
    return <Touchable />;
  }
}
