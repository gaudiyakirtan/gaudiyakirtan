import React from "react";
import { View } from "react-native";

import MathaList from "../../../lib/mathaList/MathaList";
import Modal from "../../../lib/modal/Modal";
import Text from "../../../lib/text/Text";

interface IProps {
  onModalClose(): void;
}

export default class SettingsMathaModal extends React.Component<IProps> {
  public render(): JSX.Element {
    return (
      <Modal
        open={true}
        mode="coverScreen"
        hasBackdrop={true}
        content={{
          margin: 10,
          showBottomBorderRadius: true
        }}
        animationIn="zoomIn"
        animationOut="fadeOut"
        onClose={this.props.onModalClose}
      >
        <View>
          <View style={{ margin: 10 }}>
            <Text type="highlight" fontSize={18} textAlign="center">
              Sanga
            </Text>
          </View>
          <MathaList />
        </View>
      </Modal>
    );
  }
}
