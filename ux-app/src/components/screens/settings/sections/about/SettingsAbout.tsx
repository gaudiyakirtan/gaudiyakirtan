import React from "react";
import { View } from "react-native";

import Screen from "../../../../lib/screen/Screen";
import Content from "../../../../lib/content/Content";
import Header from "../../../../lib/header/Header";
import Text from "../../../../lib/text/Text";
import ScrollView from "../../../../lib/scrollView/ScrollView";
import Touchable from "../../../../lib/touchable/Touchable";
import SettingsTitle, { SettingsLeftColWidth } from "../../settingsTitle/SettingsTitle";
import RouteName from "../../../../../utils/navigation/RouteName";
import HorizontalRule from "../../../../lib/horizontalRule/HorizontalRule";

export default class SettingsAbout extends React.Component {
  public render(): JSX.Element {
    return (
      <Screen>
        <Header
          title="About"
          showBackButton={true}
          rightContent={this.renderHeaderRightContent()}
        />
        <ScrollView>
          <Content marginTop={15} paddingRight={15}>
            <SettingsTitle
              text="App stats"
              textType="highlight"
              iconName="linechart"
              iconFamily="AntDesign"
              noHorRule={true}
            />
            <View style={{ marginLeft: SettingsLeftColWidth, marginTop: 10, marginBottom: 5 }}>
              <Text type="tertiary" fontSize={15}>
                1000+ kirtans, 800+ recordings, 11 languages, and many more coming...
              </Text>
            </View>
            <HorizontalRule marginTop={10} marginBottom={10} marginLeft={SettingsLeftColWidth} />

            <SettingsTitle
              text="What is kirtan?"
              iconName="music"
              iconFamily="FontAwesome"
              routeName={RouteName.SettingsAboutKirtan}
            />

            <SettingsTitle
              text="About the Gaudiya Kirtan project"
              displayLogo={true}
              routeName={RouteName.SettingsAboutGKProject}
            />

            <SettingsTitle
              text="The Gaudiya Kirtan team"
              iconName="ios-people"
              iconFamily="Ionicons"
              iconSize={30}
              routeName={RouteName.SettingsAboutGKTeam}
            />

            <SettingsTitle
              text="Credits and copyright info"
              iconName="copyright"
              iconFamily="FontAwesome5"
              routeName={RouteName.SettingsAboutCopyright}
            />
          </Content>
        </ScrollView>
      </Screen>
    );
  }

  private renderHeaderRightContent(): JSX.Element | undefined {
    // Empty place holder to symmetrically center header title
    return <Touchable />;
  }
}
