/* eslint-disable react/jsx-one-expression-per-line */
import React from "react";

import Screen from "../../../../lib/screen/Screen";
import Content from "../../../../lib/content/Content";
import Header from "../../../../lib/header/Header";
import ScrollView from "../../../../lib/scrollView/ScrollView";
import Text from "../../../../lib/text/Text";
import Touchable from "../../../../lib/touchable/Touchable";
import Paragraph, { fontSize, HW, I } from "../../../../lib/paragraph/Paragraph";

export default class SettingsAboutGKProject extends React.Component {
  public render(): JSX.Element {
    return (
      <Screen>
        <Header
          title="About the Gaudiya Kirtan project"
          showBackButton={true}
          rightContent={this.renderHeaderRightContent()}
        />
        <ScrollView>
          <Content marginTop={15} paddingLeft={15} paddingRight={15} marginBottom={20}>
            <Paragraph
              noTab={true}
              textProps={{ type: "tertiary", italic: true }}
              style={{ marginLeft: 10, marginRight: 10, marginBottom: 0 }}
            >
              When the reddish sun began to rise on the eastern horizon, <HW>Śrī Gaurāṅga</HW>, the
              jewel among the twice-born, at once awoke. Taking His devotees with Him, He went into
              the towns and villages of Navadvīpa. The mṛdaṅgas resounded “Tāthai, tāthai,” and the
              karatālas played in time. Overflowing with prema, Śrī Gaurāṅga’s golden limbs swayed
              gracefully, causing the ankle bells (nūpura) on His feet to jingle. [He called out:]
              <HW>
                “Mukunda! Mādhava! Yādava! Hari! Everyone, chant! Everyone chant, filling your
                mouths with the holy names of the Lord!”
              </HW>
            </Paragraph>

            <Paragraph style={{ marginBottom: 0 }}>
              Forever enraptured in the delight of the <I>kīrtana</I> of <I>Śrī Gaurāṅga</I>, the
              pure devotees are always engaged in revealing its beauty and glory to whomever they
              meet.
            </Paragraph>

            <Paragraph>
              Thus, illustrious <I>Gauḓīya Vaiṣṇava</I> saints, such as{" "}
              <I>Śrīla Bhaktivinoda Ṭhākura</I> and <I>Śrīla Narottama dāsa Ṭhākura</I>, have
              written countless devotional songs, thereby giving us entrance into the realm of
              kīrtana.
            </Paragraph>

            <Paragraph>
              Our objective with the Gaudiya Kirtan app is to inspire one and all to enrich their
              lives by the performance of kīrtana.
            </Paragraph>

            <Paragraph>
              Our team consists of developers, translators, fidelity checkers, editors,
              proofreaders, kīrtanīyas, distributors, and contributors, working together from all
              around the globe.
            </Paragraph>

            <Paragraph>
              It is our hope that this site will be a place that stimulates confidence in those who
              are hiding, too shy to come out and sing aloud while chiming kartālas; that entices
              those who are standing at a distance, looking on with curiosity, to participate; and
              that provides companionship for those who have embraced the life of <I>kīrtana</I>.
            </Paragraph>

            <Text type="tertiary" textAlign="right" fontSize={fontSize}>
              - The Gaudiya Kirtan team
            </Text>
          </Content>
        </ScrollView>
      </Screen>
    );
  }

  private renderHeaderRightContent(): JSX.Element | undefined {
    // Empty place holder to symmetrically center header title
    return <Touchable />;
  }
}
