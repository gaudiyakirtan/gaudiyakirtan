/* eslint-disable react/jsx-one-expression-per-line */
import React from "react";

import Screen from "../../../../lib/screen/Screen";
import Content from "../../../../lib/content/Content";
import Header from "../../../../lib/header/Header";
import ScrollView from "../../../../lib/scrollView/ScrollView";
import Text from "../../../../lib/text/Text";
import Touchable from "../../../../lib/touchable/Touchable";
import Paragraph, { fontSize, HW, I } from "../../../../lib/paragraph/Paragraph";

export default class SettingsAboutKirtan extends React.Component {
  public render(): JSX.Element {
    return (
      <Screen>
        <Header
          title="What is kirtan?"
          showBackButton={true}
          rightContent={this.renderHeaderRightContent()}
        />
        <ScrollView>
          <Content marginTop={20} paddingLeft={15} paddingRight={15}>
            <Paragraph noTab={true}>
              <HW>Śravaṇam</HW> (<I>hearing</I>), <HW>kīrtana</HW> (<I>chanting</I>) and{" "}
              <HW>smaraṇam</HW> (<I>remembering</I>) are the three primary <I>aṅgas</I> (limbs) of{" "}
              <I>bhakti</I>, for all the other <I>aṅgas</I> are included within them, and of these{" "}
              three <I>aṅgas</I>, <I>kīrtana</I> is the best and most important, because{" "}
              <I>śravaṇam</I> and <I>smaraṇam</I> can be included within it.
            </Paragraph>

            <Paragraph>
              For aspirants hoping to immerse themselves in the culture of <HW>yoga</HW>, hearing is
              the first step. Otherwise, how would one come to know of <I>yoga</I> culture at
              all—its goal, the means to achieve it, or even the nature of the circumstance at which
              we are present?
            </Paragraph>

            <Paragraph>
              Yet, for hearing to occur, sound vibrations must also be there. The primordial sound,
              the divine whirlwind flute-song of <HW>Śrī Kṛṣṇa</HW>, emanates from the central arena
              of all existence. Its sweet call causes the <I>gopīs</I> (yogīnis of the highest
              order) to respond in glorious <I>kīrtana</I>.
            </Paragraph>

            <Paragraph>
              <I>Kṛṣṇa’s</I> flute sound flattens into the <I>Oṁ</I> vibration as it spreads along
              with the brilliant light shining from His Place. Like <I>Brahmā</I>, one can hear this
              seed sound in meditation and, piercing through it and the impersonal light, come to
              understand the original place of kīrtana.
            </Paragraph>

            <Paragraph>
              By Divine Grace—easily, quickly and wonderfully—that kīrtana has come to us from
              Brahmā, <HW>Lord Caitanya</HW> (500 years ago) and gurus in our own era. Taking up
              such chanting is acceptance of the greatest fortune and the best means of remembrance,
              while simultaneously making it available to all.
            </Paragraph>

            <Text type="tertiary" textAlign="right" fontSize={fontSize}>
              (written by Ṛṣabhādeva dāsa for Gaudiya Kirtan)
            </Text>
          </Content>
        </ScrollView>
      </Screen>
    );
  }

  private renderHeaderRightContent(): JSX.Element | undefined {
    // Empty place holder to symmetrically center header title
    return <Touchable />;
  }
}
