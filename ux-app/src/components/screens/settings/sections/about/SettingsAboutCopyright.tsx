/* eslint-disable react/jsx-one-expression-per-line */
import React from "react";

import Screen from "../../../../lib/screen/Screen";
import Content from "../../../../lib/content/Content";
import Header from "../../../../lib/header/Header";
import ScrollView from "../../../../lib/scrollView/ScrollView";
import Text from "../../../../lib/text/Text";
import Touchable from "../../../../lib/touchable/Touchable";
import Paragraph, { fontSize } from "../../../../lib/paragraph/Paragraph";
import Anchor from "../../../../lib/anchor/Anchor";

export default class SettingsAboutCopyright extends React.Component {
  public render(): JSX.Element {
    return (
      <Screen>
        <Header
          title="Credits and copyright info"
          showBackButton={true}
          rightContent={this.renderHeaderRightContent()}
        />
        <ScrollView>
          <Content marginTop={20} paddingLeft={15} paddingRight={15}>
            <Paragraph noTab={true}>
              <Text type="tertiary" fontSize={16}>
                © 2013 GAUDIYA VEDANTA PUBLICATIONS. SOME RIGHTS RESERVED.
              </Text>
            </Paragraph>

            <Paragraph noTab={true}>
              <Text type="tertiary" fontSize={16}>
                CC-BY-ND
              </Text>
            </Paragraph>

            <Paragraph noTab={true}>
              Except where otherwise noted, the English translations in this app are licensed under
              the creative commons attribution-no derivative works 4.0 international license.
            </Paragraph>

            <Paragraph noTab={true}>
              <>To view a copy of this license, please visit </>
              <Anchor
                href="http://creativecommons.org/licenses/by-nd/4.0/"
                textProps={{ fontSize }}
              >
                http://creativecommons.org/licenses/by-nd/4.0/
              </Anchor>
            </Paragraph>

            <Paragraph noTab={true}>
              Permissions beyond the scope of this license may be available at{" "}
              <Anchor href="http://www.purebhakti.com/pluslicense" textProps={{ fontSize }}>
                www.purebhakti.com/pluslicense
              </Anchor>{" "}
              or write to:{" "}
              <Text type="highlight" fontSize={fontSize}>
                gvp.contactus@gmail.com
              </Text>
            </Paragraph>
          </Content>
        </ScrollView>
      </Screen>
    );
  }

  private renderHeaderRightContent(): JSX.Element | undefined {
    // Empty place holder to symmetrically center header title
    return <Touchable />;
  }
}
