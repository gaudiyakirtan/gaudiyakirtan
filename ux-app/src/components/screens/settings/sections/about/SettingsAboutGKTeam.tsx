/* eslint-disable react/jsx-one-expression-per-line */
import React from "react";
import { StyleSheet, View, ViewStyle } from "react-native";

import Screen from "../../../../lib/screen/Screen";
import Content from "../../../../lib/content/Content";
import Header from "../../../../lib/header/Header";
import Text from "../../../../lib/text/Text";
import Touchable from "../../../../lib/touchable/Touchable";
import List, { IListItem } from "../../../../lib/lists/list/List";
import Icon, { IconFamily } from "../../../../lib/icon/Icon";
import Logo from "../../../../lib/logo/Logo";

const styles = StyleSheet.create({
  header: {
    marginLeft: 15,
    flexDirection: "row",
    alignItems: "center"
  },
  personsRow: {
    marginLeft: 80,
    flex: 1,
    flexDirection: "row",
    alignItems: "center"
  },
  person: {
    flexDirection: "row",
    alignItems: "center"
  },
  person1: { width: "60%" },
  person2: { width: "40%" },
  logo: { marginRight: 5 }
});

export default class SettingsAboutGKTeam extends React.Component {
  public render(): JSX.Element {
    const listItems = this.getListItems();
    return (
      <Screen>
        <Header
          title="The Gaudiya Kirtan team"
          showBackButton={true}
          rightContent={this.renderHeaderRightContent()}
        />
        <Content marginTop={10} marginBottom={20}>
          <List items={listItems} />
        </Content>
      </Screen>
    );
  }

  private renderHeaderRightContent(): JSX.Element | undefined {
    // Empty place holder to symmetrically center header title
    return <Touchable />;
  }

  private renderHeaderTitle(title: string, iconName: string, iconFamily: IconFamily): JSX.Element {
    return (
      <View style={styles.header}>
        <View style={{ marginRight: 5 }}>
          <Icon name={iconName} family={iconFamily} type="highlight" />
        </View>
        <Text type="highlight">{title}</Text>
      </View>
    );
  }

  private renderPersons(person1: string, person2?: string): JSX.Element {
    return (
      <View style={styles.personsRow}>
        {this.renderPerson(person1, styles.person1)}
        {person2 && this.renderPerson(person2, styles.person2)}
      </View>
    );
  }

  private renderPerson(name: string, style: ViewStyle): JSX.Element {
    return (
      <View style={{ ...styles.person, ...style }}>
        <View style={styles.logo}>
          <Logo width={20} height={20} grayScale={true} />
        </View>
        {/* <Icon name="dot-single" family="Entypo" width={20} /> */}
        <Text>{name}</Text>
      </View>
    );
  }

  private getListItems(): IListItem[] {
    const items: Omit<IListItem, "key">[] = [
      {
        isHeader: true,
        title: this.renderHeaderTitle("Project coordinator", "person", "Ionicons")
      },
      { title: this.renderPersons("Madhukara dāsa") },
      {
        isHeader: true,
        title: this.renderHeaderTitle("Developers", "keyboard-settings", "MaterialCommunityIcons")
      },
      { title: this.renderPersons("Jayarām (Jaya Gopāla)", "Śrīrām") },
      {
        isHeader: true,
        // eslint-disable-next-line prettier/prettier
        title: this.renderHeaderTitle("English translators", "translate", "MaterialCommunityIcons")
      },
      { title: this.renderPersons("B.V. Bhāgavata Mahārāja", "Śrīvāsa dāsa") },
      { title: this.renderPersons("Kundalatā dāsī", "Rādhikā dāsī") },
      { title: this.renderPersons("Candraśekhara dāsa") },
      {
        isHeader: true,
        title: this.renderHeaderTitle("Fidelity checkers", "youtube-searched-for", "MaterialIcons")
      },
      { title: this.renderPersons("Madhukara dāsa", "Candraśekhara dāsa") },
      { title: this.renderPersons("Kundalatā dāsī", "Śrīvāsa dāsa") },
      {
        isHeader: true,
        title: this.renderHeaderTitle("Translation consultants", "language", "FontAwesome")
      },
      { title: this.renderPersons("Mādhava-priya dāsa", "Gīta dāsī") },
      { title: this.renderPersons("Amala-kṛṣṇa dāsa", "Vijaya-kṛṣṇa däsa") },
      { title: this.renderPersons("Uttama-kṛṣṇa dāsa", "Vaṁśī-vadana dāsa") },
      { title: this.renderPersons("Gopāla-kṛṣṇa dāsa", "Rādhikā dāsī") },
      {
        isHeader: true,
        title: this.renderHeaderTitle("English editors", "pen-nib", "FontAwesome5")
      },
      { title: this.renderPersons("Vaijayantī-mālā dāsī", "Vicitri dāsī") },
      { title: this.renderPersons("Ṛṣabhadeva dāsa", "Mañjarī dāsī (Vṛndāvana)") },
      {
        isHeader: true,
        title: this.renderHeaderTitle("Bengali typing", "typewriter", "MaterialCommunityIcons")
      },
      { title: this.renderPersons("Sadānanda dāsa", "Jayadeva dāsa") },
      { title: this.renderPersons("Madhukar dāsa") },
      {
        isHeader: true,
        title: this.renderHeaderTitle("Designers", "paint-brush", "FontAwesome5")
      },
      { title: this.renderPersons("Kamalā dāsī", "Jayadeva dāsa") },
      { title: this.renderPersons("Jahnavā dāsī") }
    ];

    const listItems: IListItem[] = items.map((item, index) => ({
      ...item,
      noHorzRule: true,
      touchableDisabled: true,
      key: index
    }));
    return listItems;
  }
}
