/* eslint-disable react/jsx-one-expression-per-line */
import React from "react";
import { Image, View } from "react-native";

import Screen from "../../../lib/screen/Screen";
import Content from "../../../lib/content/Content";
import Header from "../../../lib/header/Header";
import ScrollView from "../../../lib/scrollView/ScrollView";
import Text from "../../../lib/text/Text";
import Touchable from "../../../lib/touchable/Touchable";
import Paragraph, { fontSize, HW, I } from "../../../lib/paragraph/Paragraph";

export default class SettingsDedication extends React.Component {
  public render(): JSX.Element {
    return (
      <Screen>
        <Header
          title="Dedication"
          showBackButton={true}
          rightContent={this.renderHeaderRightContent()}
        />
        <ScrollView>
          <Content marginTop={10} paddingLeft={15} paddingRight={15} marginBottom={20}>
            <View
              style={{
                width: 300,
                height: 300,
                alignSelf: "center"
              }}
            >
              <Image
                source={require("../../../../images/settings/gurudeva.png")}
                style={{ flex: 1, width: undefined, height: undefined }}
              />
            </View>

            <Paragraph noTab={true}>
              On the glorious occasion of the centennial appearance day of our beloved{" "}
              <I>śrī guru-pādapadma</I>,
            </Paragraph>

            <Paragraph
              noTab={true}
              textProps={{ type: "tertiary", italic: true }}
              style={{ marginLeft: 10, marginRight: 10, marginBottom: 0 }}
            >
              <Text type="tertiary" fontSize={15}>
                nitya-līlā-praviṣṭa oṁ viṣṇupāda aṣṭottara-śata-śrī
              </Text>
              {"\n"}
              <HW>Śrīmad Bhaktivedānta Nārāyaṇa Gosvāmī Mahārāja,</HW>
            </Paragraph>

            <Paragraph noTab={true} style={{ marginTop: 10, marginBottom: 10 }}>
              we offer him the <I>Gaudiya Kirtan app (Version 1.0)</I> with the hope that it brings
              him pleasure. He exemplified the ideal of how to perform kirtan. His tireless 60-plus
              years of dedication toward fulfilling the inner-heart’s desire of his gurudeva and
              helping one and all adopt the principles of kirtan in their lives is our sole
              inspiration and driving force.
            </Paragraph>

            <Text type="tertiary" textAlign="right" fontSize={fontSize}>
              - The Gaudiya Kirtan team
            </Text>
            <Text type="tertiary" textAlign="right" fontSize={fontSize}>
              (11 February, 2021)
            </Text>
          </Content>
        </ScrollView>
      </Screen>
    );
  }

  private renderHeaderRightContent(): JSX.Element | undefined {
    // Empty place holder to symmetrically center header title
    return <Touchable />;
  }
}
