import React from "react";
import { View } from "react-native";

import Screen from "../../../lib/screen/Screen";
import Content from "../../../lib/content/Content";
import Header from "../../../lib/header/Header";
import Text from "../../../lib/text/Text";
import ScrollView from "../../../lib/scrollView/ScrollView";
import Touchable from "../../../lib/touchable/Touchable";
import SettingsTitle, { SettingsLeftColWidth } from "../settingsTitle/SettingsTitle";
import HorizontalRule from "../../../lib/horizontalRule/HorizontalRule";
import { IconFamily } from "../../../lib/icon/Icon";
import PhoneAction from "../../../lib/contactActions/PhoneAction";
import WhatsAppAction from "../../../lib/contactActions/WhatsAppAction";
import EmailAction from "../../../lib/contactActions/EmailAction";
import WebsiteAction from "../../../lib/contactActions/WebsiteAction";

export const callPhoneNumber = "+91-789-593-9316";
export const whatsAppNumber = "+91-789-593-9316";
export const email = "gaudiyakirtan@gmail.com";

export default class SettingsContact extends React.Component {
  public render(): JSX.Element {
    return (
      <Screen>
        <Header
          title="Contact"
          showBackButton={true}
          rightContent={this.renderHeaderRightContent()}
        />
        <ScrollView>
          <Content marginTop={15} paddingRight={15}>
            {this.renderSection("Email", "email", "MaterialCommunityIcons", this.renderEmail())}
            {this.renderSection("WhatsApp", "whatsapp", "FontAwesome", this.renderWhatsApp())}
            {this.renderSection("Call", "phone", "MaterialIcons", this.renderCall())}
            {this.renderSection("Website", "web", "MaterialCommunityIcons", this.renderWebsite())}
          </Content>
        </ScrollView>
      </Screen>
    );
  }

  private renderSection(
    title: string,
    iconName: string,
    iconFamily: IconFamily,
    sectionContent: JSX.Element
  ): JSX.Element {
    return (
      <>
        <SettingsTitle
          text={title}
          textType="highlight"
          iconName={iconName}
          iconFamily={iconFamily}
          noHorRule={true}
        />
        <View style={{ marginLeft: SettingsLeftColWidth }}>{sectionContent}</View>
        <HorizontalRule marginTop={10} marginBottom={10} marginLeft={SettingsLeftColWidth} />
      </>
    );
  }

  private renderEmail(): JSX.Element {
    return this.renderActionIcon(
      email,
      <EmailAction email={email} subject="Haribol! From GK App" />
    );
  }

  private renderCall(): JSX.Element {
    return this.renderActionIcon(callPhoneNumber, <PhoneAction phoneNumber={callPhoneNumber} />);
  }

  private renderWhatsApp(): JSX.Element {
    return this.renderActionIcon(whatsAppNumber, <WhatsAppAction phoneNumber={whatsAppNumber} />);
  }

  private renderWebsite(): JSX.Element {
    return this.renderActionIcon(
      "gaudiyakirtan.com",
      <WebsiteAction url="http://www.gaudiyakirtan.com" />
    );
  }

  private renderActionIcon(text: string, contactAction: JSX.Element): JSX.Element {
    return (
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Text fontSize={15}>{text}</Text>
        {contactAction}
      </View>
    );
  }

  private renderHeaderRightContent(): JSX.Element | undefined {
    // Empty place holder to symmetrically center header title
    return <Touchable />;
  }
}
