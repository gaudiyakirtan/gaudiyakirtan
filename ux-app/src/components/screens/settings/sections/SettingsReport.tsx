/* eslint-disable react/jsx-one-expression-per-line */
import React from "react";
import { View } from "react-native";

import Screen from "../../../lib/screen/Screen";
import Content from "../../../lib/content/Content";
import Header from "../../../lib/header/Header";
import ScrollView from "../../../lib/scrollView/ScrollView";
import Text from "../../../lib/text/Text";
import Navigation from "../../../../utils/navigation/Navigation";
import RouteName from "../../../../utils/navigation/RouteName";
import Touchable from "../../../lib/touchable/Touchable";

export default class SettingsReport extends React.Component {
  public render(): JSX.Element {
    return (
      <Screen>
        <Header
          title="Report"
          showBackButton={true}
          rightContent={this.renderHeaderRightContent()}
        />
        <ScrollView>
          <Content marginTop={20} paddingLeft={15} paddingRight={15}>
            <View style={{ marginBottom: 20 }}>
              <Text fontSize={18} type="tertiary" textAlign="center">
                Coming soon...
              </Text>
            </View>
            <Text fontSize={15}>
              In the meantime, you can report a bug or error in verse/translation by email, whatsapp
              or phone :
            </Text>
            <Touchable width="100%" noHorzCenter={true}>
              <Text type="highlight" fontSize={15} onPress={this.navigateToContact}>
                Contact us
              </Text>
            </Touchable>
          </Content>
        </ScrollView>
      </Screen>
    );
  }

  private renderHeaderRightContent(): JSX.Element | undefined {
    // Empty place holder to symmetrically center header title
    return <Touchable />;
  }

  private navigateToContact = (): void => {
    Navigation.navigate(RouteName.SettingsContact);
  };
}
