import React from "react";
import { View } from "react-native";

import LanguageList from "../../../lib/languageList/LanguageList";
import Modal from "../../../lib/modal/Modal";
import Text from "../../../lib/text/Text";

interface IProps {
  onModalClose(): void;
}

export default class SettingsLanguageModal extends React.Component<IProps> {
  public render(): JSX.Element {
    return (
      <Modal
        open={true}
        mode="coverScreen"
        hasBackdrop={true}
        content={{
          margin: 20,
          showBottomBorderRadius: true
        }}
        animationIn="zoomIn"
        animationOut="fadeOut"
        onClose={this.props.onModalClose}
      >
        <View>
          <View style={{ margin: 10 }}>
            <Text type="highlight" fontSize={18} textAlign="center">
              Language
            </Text>
          </View>
          <LanguageList />
        </View>
      </Modal>
    );
  }
}
