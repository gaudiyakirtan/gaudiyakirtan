import React from "react";

import Text from "../../lib/text/Text";
import RowLayout, { Body, Right } from "../../lib/rowLayout/RowLayout";
import Touchable from "../../lib/touchable/Touchable";

import { SettingsLeftColWidth } from "./settingsTitle/SettingsTitle";

interface IProps {
  text: string;
  rightContent: JSX.Element;
  onPress(): void;
}

export default class SettingsControl extends React.Component<IProps> {
  public render(): JSX.Element {
    return (
      <RowLayout height={50} marginLeft={SettingsLeftColWidth}>
        <Body noHorzCenter={true}>
          <Touchable width="100%" noHorzCenter={true} onPress={this.props.onPress}>
            <Text fontSize={15}>{this.props.text}</Text>
          </Touchable>
        </Body>
        <Right>{this.props.rightContent}</Right>
      </RowLayout>
    );
  }
}
