import React from "react";
import { StyleSheet, View } from "react-native";

import Text from "../../lib/text/Text";

interface ITimeDiff {
  months: number;
  days: number;
  hours: number;
  minutes: number;
  seconds: number;
}

interface IState {
  timeDiff: ITimeDiff;
}

const styles = StyleSheet.create({
  row: { flexDirection: "row", alignItems: "flex-end", justifyContent: "center" },
  column: { flexDirection: "column", alignItems: "center", justifyContent: "center" }
});

export default class CountDownTimer extends React.Component<{}, IState> {
  private targetTime = new Date("Feb 11, 2021 00:00:00").getTime();
  private timerId: number;

  public constructor(props: {}) {
    super(props);
    this.state = { timeDiff: this.getTimeDifference() };
    this.timerId = window.setInterval(this.updateTime, 1000);
  }

  public componentWillUnmount(): void {
    clearInterval(this.timerId);
  }

  public render(): JSX.Element {
    const timeDiff = this.state.timeDiff;
    return (
      <View style={styles.row}>
        <View style={{ marginRight: 10 }}>
          {this.renderTimeSlice(timeDiff.months === 1 ? "Month" : "Months", timeDiff.months, 24)}
        </View>
        <View style={{ marginRight: 10 }}>
          {this.renderTimeSlice(timeDiff.days === 1 ? "Day" : "Days", timeDiff.days, 24)}
        </View>
        {this.renderTimeSlice(timeDiff.hours === 1 ? "Hour" : "Hours", timeDiff.hours, 12)}
        {this.renderSemiColon()}
        {this.renderTimeSlice(timeDiff.minutes === 1 ? "Min" : "Mins", timeDiff.minutes, 12)}
        {this.renderSemiColon()}
        {this.renderTimeSlice(timeDiff.seconds === 1 ? "Sec" : "Secs", timeDiff.seconds, 12)}
      </View>
    );
  }

  private renderTimeSlice(text: string, value: number, fontSize: number): JSX.Element {
    return (
      <View style={styles.column}>
        <Text fontSize={fontSize}>{value}</Text>
        <Text type="lowlight">{text}</Text>
      </View>
    );
  }

  private renderSemiColon(): JSX.Element {
    return (
      <View style={{ ...styles.column, alignSelf: "center" }}>
        <Text type="lowlight">:</Text>
      </View>
    );
  }

  private getTimeDifference = (): ITimeDiff => {
    const now = new Date().getTime();
    const distance = this.targetTime - now;

    // Taking the average of 30.42 days per month (365/12) for simplicity
    const months = Math.floor(distance / (1000 * 60 * 60 * 24 * 30.42));
    const days = Math.floor((distance % (1000 * 60 * 60 * 24 * 30.42)) / (1000 * 60 * 60 * 24));
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);

    return { months, days, hours, minutes, seconds };
  };

  private updateTime = (): void => {
    this.setState({ timeDiff: this.getTimeDifference() });
  };
}
