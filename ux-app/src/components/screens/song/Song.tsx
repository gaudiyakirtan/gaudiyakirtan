/* eslint-disable @typescript-eslint/no-use-before-define */
import React from "react";
import { activateKeepAwake, deactivateKeepAwake } from "expo-keep-awake";
import { Share } from "react-native";

import Header from "../../lib/header/Header";
import Navigation from "../../../utils/navigation/Navigation";
import ISong, { getArtistId } from "../../../data/models/server/ISong";
import Spinner from "../../lib/spinner/Spinner";
import ErrorScreen from "../../lib/errorScreen/ErrorScreen";
import Screen from "../../lib/screen/Screen";
import Center from "../../lib/center/Center";
import ActiveScreenRefs from "../../../utils/activeScreenRefs/ActiveScreenRefs";
import RouteName, { ISongRouteParams } from "../../../utils/navigation/RouteName";
import Icon, { IconSize } from "../../lib/icon/Icon";
import Touchable from "../../lib/touchable/Touchable";
import withAudioContext, {
  IAudioContextProps
} from "../../../context/audioContext/withAudioContext";
import MoreSettingsModal, {
  IMoreSetting
} from "../../lib/modals/moreSettingsModal/MoreSettingsModal";
import Debug from "../../../utils/debug/Debug";
import Data from "../../../data/Data";
import StatusBar from "../../lib/statusBar/StatusBar";

import Lyrics from "./lyrics/Lyrics";
import VerseModeModal from "./verseModeModal/VerseModeModal";
import VerseMode, { isVerseModeFullscreen } from "./verseModeModal/VerseMode";
import ViewSettingsModal from "./viewSettingsModal/ViewSettingsModal";
import EmptySongScreen from "./EmptySongScreen";

interface IProps extends IAudioContextProps {}

interface IState {
  loading: boolean;
  song?: ISong;
  error?: string;
  viewSettingsModalOpen: boolean;
  verseModeModalOpen: boolean;
  shareOpen: boolean;
  verseMode: VerseMode;
}

const closeAllModalsState = {
  viewSettingsModalOpen: false,
  verseModeModalOpen: false,
  shareOpen: false
};

class Song extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      loading: true,
      song: undefined,
      error: undefined,
      ...closeAllModalsState,
      verseMode: VerseMode.Normal
    };
  }

  public async componentDidMount(): Promise<void> {
    ActiveScreenRefs.add(RouteName.Song, this);
    activateKeepAwake("Song");
    await this.loadSong();
  }

  public componentWillUnmount(): void {
    deactivateKeepAwake("Song");
  }

  public render(): JSX.Element {
    const songUid = getOpenSongUid();
    return (
      <Screen>
        <Header
          showBackButton={false}
          title={songUid || ""}
          rightContent={this.renderHeaderRightContent()}
          leftContent={this.renderHeaderLeftContent()}
        />
        {this.renderContent()}
        <ViewSettingsModal
          open={this.state.viewSettingsModalOpen}
          onClose={this.onViewSettingsModalClose}
        />
        <VerseModeModal
          open={this.state.verseModeModalOpen}
          verseMode={this.state.verseMode}
          onVerseModeChange={this.onVerseModeChange}
          onClose={this.onVerseModeModalClose}
        />
      </Screen>
    );
  }

  private renderHeaderRightContent(): JSX.Element {
    const songUid = getOpenSongUid();
    if (!songUid) {
      return <></>;
    }

    const audioExists = this.state.song && this.state.song.audio.length > 0;
    const multipleArtistsExist = this.state.song && this.state.song.audio.length > 1;

    const moreSettings: IMoreSetting[] = [
      {
        text: "View Settings",
        iconName: "ios-eye",
        disabled: Boolean(this.state.loading || this.state.error || !this.state.song),
        active: this.state.viewSettingsModalOpen,
        onPress: this.onViewSettingsIconPress
      },
      {
        text: "Verse Mode",
        iconName: "select-group",
        iconFamily: "MaterialCommunityIcons",
        disabled: Boolean(this.state.loading || this.state.error || !this.state.song),
        active: this.state.verseModeModalOpen,
        onPress: this.onVerseModeIconPress
      },
      {
        text: "Share",
        iconName: "md-share",
        disabled: Boolean(this.state.loading || this.state.error || !this.state.song),
        active: this.state.shareOpen,
        onPress: this.onShare
      }
    ];

    return (
      <>
        {audioExists && (
          <Touchable onPress={this.onAudioIconPress}>
            <Icon
              name={multipleArtistsExist ? "playlist-music" : "ios-musical-notes"}
              family={multipleArtistsExist ? "MaterialCommunityIcons" : "Ionicons"}
              size={multipleArtistsExist ? 28 : IconSize.Normal}
              type={this.currentSongsAudioBarOpen() ? "highlight" : "normal"}
            />
          </Touchable>
        )}
        <MoreSettingsModal moreSettings={moreSettings} />
      </>
    );
  }

  private renderHeaderLeftContent(): JSX.Element | undefined {
    const songUid = getOpenSongUid();
    if (!songUid) {
      return <></>;
    }

    const closeButton = (
      <Touchable onPress={this.onCloseSong}>
        <Icon name="chevron-down" family="Feather" size={28} />
      </Touchable>
    );

    const audioExists = this.state.song && this.state.song.audio.length > 0;
    // Empty place holder to symmetrically center header title
    return audioExists ? (
      <>
        {closeButton}
        <Touchable />
      </>
    ) : (
      closeButton
    );
  }

  private renderContent(): JSX.Element {
    const songUid = getOpenSongUid();
    if (!songUid) {
      return <EmptySongScreen />;
    }
    if (this.state.loading) {
      return (
        <Center hor={true} ver={true}>
          <Spinner />
        </Center>
      );
    }
    if (this.state.error) {
      return (
        <ErrorScreen
          text="Oops! Unable to load song"
          textLine2="Please make sure you are connected to the internet"
          details={this.state.error}
          retry={true}
          onRetryPress={this.loadSong}
        />
      );
    }

    if (!this.state.song) {
      return (
        <ErrorScreen
          text={`'${songUid}' is waiting to update.`}
          textLine2="Tap retry to see an old version."
          retry={true}
          onRetryPress={this.loadSong}
        />
      );
    }

    return (
      <Lyrics
        song={this.state.song}
        verseMode={this.state.verseMode}
        onVerseModeChange={this.onVerseModeChange}
      />
    );
  }

  public loadSong = async (): Promise<void> => {
    const songUid = getOpenSongUid();
    if (!songUid) {
      return;
    }

    this.setState({
      loading: true,
      song: undefined,
      error: undefined,
      verseMode: VerseMode.Normal
    });

    let song: ISong | undefined;
    if (songUid) {
      song = Data.FetchSongData(songUid, true); // addToRecents = true
    }

    this.preloadArtistPics(song);
    this.setState({ loading: false, song, verseMode: VerseMode.Normal });
  };

  // Do not await and preload asynchronously in the background
  private preloadArtistPics = async (song: ISong | undefined): Promise<void> => {
    if (song) {
      song.audio.forEach(audio => {
        Data.LoadArtistPic(getArtistId(audio.uid));
      });
    }
  };

  private onAudioIconPress = (): void => {
    if (this.currentSongsAudioBarOpen()) {
      this.props.audioContext.reset();
    } else {
      if (this.state.song) {
        this.props.audioContext.recreate(this.state.song, true);
      }
    }
  };

  private onViewSettingsIconPress = (): void => {
    this.setState(prevState => ({
      ...closeAllModalsState,
      viewSettingsModalOpen: !prevState.viewSettingsModalOpen
    }));
  };

  private onViewSettingsModalClose = (): void => {
    this.setState({ viewSettingsModalOpen: false });
  };

  private onVerseModeIconPress = (): void => {
    this.setState(prevState => ({
      ...closeAllModalsState,
      verseModeModalOpen: !prevState.verseModeModalOpen
    }));
  };

  private onVerseModeChange = (newVerseMode: VerseMode): void => {
    StatusBar.setHidden(isVerseModeFullscreen(newVerseMode));
    this.setState({ verseMode: newVerseMode });
  };

  private onVerseModeModalClose = (): void => {
    this.setState({ verseModeModalOpen: false });
  };

  private onShare = async (): Promise<void> => {
    this.setState(prevState => ({
      ...closeAllModalsState,
      shareOpen: !prevState.shareOpen
    }));

    if (!this.state.song) {
      return;
    }

    const song = Data.ShareSong(this.state.song.uid);
    try {
      await Share.share({ message: song });
    } catch (e) {
      Debug.log(`Share threw an exception : ${JSON.stringify(e)}`);
    }

    this.setState({ shareOpen: false });
  };

  private currentSongsAudioBarOpen = (): boolean => {
    const audioCntxt = this.props.audioContext;
    return Boolean(
      audioCntxt.song &&
        this.state.song &&
        audioCntxt.song.uid === this.state.song.uid &&
        audioCntxt.audioBarOpen
    );
  };

  private onCloseSong = (): void => {
    // TODO : Find a better way
    Navigation.navigate(RouteName.Song, { songUid: "", songMD5: "" });
    this.setState({ song: undefined });
  };
}

export function getOpenSongUid(): string | undefined {
  const currentRouteParams = Navigation.getRouteParams(RouteName.Song);
  return currentRouteParams && (currentRouteParams as ISongRouteParams).songUid;
}

const songWithContext = withAudioContext(Song);
// eslint-disable-next-line import/prefer-default-export
export { songWithContext as Song };
