/**
 * Below 4 are the only allowed versemode scenarios.
 * Otherwise it's super confusing to users with many options.
 *
 *    FullScreen    Stacked   Landscape
 *    ---------------------------------
 * 1)     F           F          F      1) Normal
 * 2)     T           F          F      2) Fullscreen
 * 3)     T           T          F      3) Stacked (Includes Fullscreen)
 * 4)     T           T          T      4) Landscape (Includes Stacked + Fullscreen)
 */
enum VerseMode {
  Normal,
  Fullscreen,
  Stacked,
  Landscape
}

export function isVerseModeFullscreen(verseMode: VerseMode): boolean {
  return [VerseMode.Fullscreen, VerseMode.Stacked, VerseMode.Landscape].includes(verseMode);
}

export function isVerseModeStacked(verseMode: VerseMode): boolean {
  return [VerseMode.Stacked, VerseMode.Landscape].includes(verseMode);
}

export default VerseMode;
