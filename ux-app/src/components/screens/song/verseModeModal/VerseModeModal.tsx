import React from "react";
import { View } from "react-native";

import Icon, { IconFamily } from "../../../lib/icon/Icon";
import Text from "../../../lib/text/Text";
import Touchable from "../../../lib/touchable/Touchable";
import BottomSettingsModal from "../../../lib/modals/bottomSettingsModal/BottomSettingsModal";
import HorizontalRule from "../../../lib/horizontalRule/HorizontalRule";

import VerseMode from "./VerseMode";

interface IProps {
  open: boolean;
  verseMode: VerseMode;
  onVerseModeChange(newVerseMode: VerseMode): void;
  onClose(): void;
}

export default class VerseModeModal extends React.Component<IProps> {
  public render(): JSX.Element {
    return (
      <BottomSettingsModal open={this.props.open} onClose={this.props.onClose}>
        {this.renderTitle()}
        {this.renderVerseModes()}
      </BottomSettingsModal>
    );
  }

  private renderVerseModes(): JSX.Element {
    const verseMode = this.props.verseMode;

    return (
      <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
        {this.renderIcon(
          "Full Screen",
          "Single tap",
          "ios-expand",
          "Ionicons",
          verseMode === VerseMode.Fullscreen,
          this.onFullScreenPress
        )}
        {this.renderIcon(
          "Stacked",
          "Double tap",
          "copy",
          "Feather",
          verseMode === VerseMode.Stacked,
          this.onStackedPress
        )}
        {this.renderIcon(
          "Landscape",
          "Long press",
          "phone-rotate-landscape",
          "MaterialCommunityIcons",
          verseMode === VerseMode.Landscape,
          this.onLandscapePress
        )}
      </View>
    );
  }

  private renderIcon(
    text: string,
    shortcut: string,
    iconName: string,
    iconFamily: IconFamily,
    isOn: boolean,
    onPress: () => void
  ): JSX.Element {
    return (
      <View style={{ marginTop: 5 }}>
        <Touchable onPress={onPress} width="auto">
          <Icon name={iconName} family={iconFamily} type={isOn ? "highlight" : "normal"} />
          <Text type={isOn ? "highlight" : "normal"}>{text}</Text>
        </Touchable>
        <View style={{ marginTop: 5 }}>
          <Text type="lowlight">{`*(${shortcut})`}</Text>
        </View>
      </View>
    );
  }

  private renderTitle(): JSX.Element {
    return (
      <View style={{ marginBottom: 5 }}>
        <HorizontalRule marginTop={10} marginBottom={10} />
        <View style={{ flexDirection: "row" }}>
          <Text type="highlight">{`Verse Mode  `}</Text>
          <Text type="lowlight">*(Shortcut)</Text>
        </View>
      </View>
    );
  }

  private onFullScreenPress = (): void => {
    const newVerseMode =
      this.props.verseMode !== VerseMode.Fullscreen ? VerseMode.Fullscreen : VerseMode.Normal;
    this.props.onVerseModeChange(newVerseMode);
  };

  private onStackedPress = (): void => {
    const newVerseMode: VerseMode =
      this.props.verseMode !== VerseMode.Stacked ? VerseMode.Stacked : VerseMode.Normal;
    this.props.onVerseModeChange(newVerseMode);
  };

  private onLandscapePress = (): void => {
    const newVerseMode: VerseMode =
      this.props.verseMode !== VerseMode.Landscape ? VerseMode.Landscape : VerseMode.Normal;
    this.props.onVerseModeChange(newVerseMode);
  };
}
