import React from "react";
import { View } from "react-native";

import Center from "../../lib/center/Center";
import Text from "../../lib/text/Text";
import LogoWithTitle from "../../lib/logo/LogoWithTitle";

export default class EmptySongScreen extends React.Component {
  public render(): JSX.Element {
    return (
      <Center hor={true} ver={true}>
        <>
          <View style={{ marginBottom: 50 }}>
            <LogoWithTitle />
          </View>
          <Text type="highlight" fontSize={16}>
            Kīrtana is the sole fruit of kīrtana.
          </Text>
          <Text type="highlight" fontSize={16}>
            Kīrtana alone is sevā, and kīrtana is also prema.
          </Text>
          <View style={{ marginTop: 10, marginLeft: "auto", marginRight: 10 }}>
            <Text type="lowlight" fontSize={14}>
              – Śrīla Bhakti Prajñāna Keśava Gosvāmī Mahārāja
            </Text>
          </View>
        </>
      </Center>
    );
  }
}
