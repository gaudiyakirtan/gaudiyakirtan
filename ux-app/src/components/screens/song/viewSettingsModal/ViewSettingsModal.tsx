/* eslint-disable react/no-unused-state */
import React from "react";
import { View } from "react-native";

import Icon from "../../../lib/icon/Icon";
import Text from "../../../lib/text/Text";
import Settings from "../../../../utils/settings/Settings";
import ISettings from "../../../../data/models/local/ISettings";
import ActiveScreenRefs from "../../../../utils/activeScreenRefs/ActiveScreenRefs";
import RouteName from "../../../../utils/navigation/RouteName";
import Touchable from "../../../lib/touchable/Touchable";
import BottomSettingsModal, {
  renderTitle
} from "../../../lib/modals/bottomSettingsModal/BottomSettingsModal";

import FontSizeControl from "./FontSizeControl";

interface IProps {
  open: boolean;
  onClose(): void;
}

// Settings.get() should be the single source of truth for the latest value of settings for every component.
// Otherwise we run into race conditions with multiple copies of settings. So, don't store settings in local state.
// Instead use a dummy time state variable to rerender.
interface IState {
  lastUpdateTime: number;
}

export default class ViewSettingsModal extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      lastUpdateTime: Date.now()
    };
  }

  public render(): JSX.Element {
    return (
      <BottomSettingsModal open={this.props.open} onClose={this.props.onClose}>
        {renderTitle("Font Size", true)}
        {this.renderFontSizeSettings()}

        {renderTitle("Lyrics", true)}
        {this.renderLyricSettings()}
      </BottomSettingsModal>
    );
  }

  // private renderScreenSettings(): JSX.Element {
  //   return (
  //     <View style={styles.row}>
  //       {this.renderIcon("Fullscreen", "expand", true, () => {})}
  //       {this.renderIcon("Landscape", "repeat", false, () => {})}
  //       {this.renderIcon("Stacked", "copy", false, () => {})}
  //     </View>
  //   );
  // }

  private renderFontSizeSettings(): JSX.Element {
    const s = Settings.get();
    return (
      <View style={{ flexDirection: "row", marginLeft: 10 }}>
        <FontSizeControl fontSize={s.lyricsFontSize} onChange={this.onFontSizeChange} />
      </View>
    );
  }

  private renderLyricSettings(): JSX.Element {
    const onIcon = "ios-eye";
    const officon = "ios-eye-off";
    const s = Settings.get();

    return (
      <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
        {this.renderIcon(
          s.lyricsShowScript ? onIcon : officon,
          "Script",
          s.lyricsShowScript,
          this.toggleShowScript
        )}
        {this.renderIcon(
          s.lyricsShowVerse ? onIcon : officon,
          "Verse",
          s.lyricsShowVerse,
          this.toggleShowVerse
        )}
        {this.renderIcon(
          s.lyricsShowWordForWord ? onIcon : officon,
          "Synonyms",
          s.lyricsShowWordForWord,
          this.toggleShowWordForWord
        )}
        {this.renderIcon(
          s.lyricsShowTranslation ? onIcon : officon,
          "Translation",
          s.lyricsShowTranslation,
          this.toggleShowTranslation
        )}
      </View>
    );
  }

  private renderIcon(
    iconName: string,
    text: string | undefined,
    isOn: boolean,
    onPress: () => void
  ): JSX.Element {
    return (
      <Touchable onPress={onPress} width={75}>
        <Icon name={iconName} type={isOn ? "highlight" : "lowlight"} />
        {text && <Text type={isOn ? "highlight" : "lowlight"}>{text}</Text>}
      </Touchable>
    );
  }

  private updateSettings(newSettings: ISettings): void {
    Settings.set(newSettings);
    this.setState({ lastUpdateTime: Date.now() });
    ActiveScreenRefs.get(RouteName.Song).forceUpdate();
  }

  private toggleShowScript = (): void => {
    const s = Settings.get();
    this.updateSettings({ ...s, lyricsShowScript: !s.lyricsShowScript });
  };

  private toggleShowVerse = (): void => {
    const s = Settings.get();
    this.updateSettings({ ...s, lyricsShowVerse: !s.lyricsShowVerse });
  };

  private toggleShowWordForWord = (): void => {
    const s = Settings.get();
    this.updateSettings({ ...s, lyricsShowWordForWord: !s.lyricsShowWordForWord });
  };

  private toggleShowTranslation = (): void => {
    const s = Settings.get();
    this.updateSettings({ ...s, lyricsShowTranslation: !s.lyricsShowTranslation });
  };

  private onFontSizeChange = (newFontSize: number): void => {
    const s = Settings.get();
    this.updateSettings({ ...s, lyricsFontSize: newFontSize });
  };
}
