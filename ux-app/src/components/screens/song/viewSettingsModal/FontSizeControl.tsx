import React from "react";
import { StyleSheet, View } from "react-native";

import Text from "../../../lib/text/Text";
import { FontSize } from "../../../../data/models/local/ISettings";
import Touchable from "../../../lib/touchable/Touchable";

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  }
});

interface IProps {
  fontSize: number;
  onChange: (newFontSize: number) => void;
}

export default class FontSizeControl extends React.Component<IProps> {
  public render(): JSX.Element {
    const decreaseDisabled = this.props.fontSize <= FontSize.min;
    const increaseDisabled = this.props.fontSize >= FontSize.max;

    return (
      <View style={styles.container}>
        <Touchable onPress={this.decrease} disabled={decreaseDisabled}>
          <Text type={decreaseDisabled ? "disabled" : "highlight"} fontSize={20}>
            A
          </Text>
        </Touchable>

        <Touchable onPress={this.increase} disabled={increaseDisabled}>
          <Text type={increaseDisabled ? "disabled" : "highlight"} fontSize={30}>
            A
          </Text>
        </Touchable>
      </View>
    );
  }

  private decrease = (): void => {
    this.props.onChange(this.props.fontSize - 1);
  };

  private increase = (): void => {
    this.props.onChange(this.props.fontSize + 1);
  };
}
