/* eslint-disable react/no-array-index-key */
import React from "react";
import { Dimensions, StyleSheet, View } from "react-native";

import ISong from "../../../../data/models/server/ISong";
import Settings from "../../../../utils/settings/Settings";
import ErrorScreen from "../../../lib/errorScreen/ErrorScreen";
import Modal from "../../../lib/modal/Modal";
import Center from "../../../lib/center/Center";
import Text from "../../../lib/text/Text";
import TapDetector from "../../../lib/tapDetector/TapDetector";
import ScrollView from "../../../lib/scrollView/ScrollView";
import DataTransform from "../../../../data/DataTransform";
import VerseMode, { isVerseModeFullscreen, isVerseModeStacked } from "../verseModeModal/VerseMode";
import IconAction from "../../../lib/icon/IconAction";
import StatusBar from "../../../lib/statusBar/StatusBar";

import Verse from "./Verse";
import StackedVerses from "./StackedVerses";

const styles = StyleSheet.create({
  container: { flex: 1 },
  content: { flex: 1, marginLeft: 5, marginRight: 5 },
  scrollView: { flexGrow: 1 },
  lyrics: { flex: 1, marginTop: 25, marginBottom: 25 },
  songTitle: {
    paddingBottom: 25,
    flexDirection: "row",
    justifyContent: "center"
  },
  titleIconLeft: { position: "absolute", left: 0 },
  titleIconRight: { position: "absolute", right: 0 }
});

interface IProps {
  song: ISong;
  verseMode: VerseMode;
  onVerseModeChange(newVerseMode: VerseMode): void;
}

export default class Lyrics extends React.Component<IProps> {
  public render(): JSX.Element {
    if (!this.lyricsExistToShow()) {
      return (
        <ErrorScreen
          text="Oops, nothing to show!"
          textLine2="Please change your lyric filters in 'View Settings'"
          iconName="logo-dropbox"
        />
      );
    }

    const isFullScreen = isVerseModeFullscreen(this.props.verseMode);
    return (
      <View style={styles.container}>
        {isFullScreen && (
          <Modal
            open={isFullScreen}
            hasBackdrop={false}
            mode="coverScreen"
            content={{
              height: "100%",
              noBorderRadius: true,
              backgroundColorPrimary: true
            }}
            // Only below combination of animation variables is not causing a flicker
            animationIn="fadeIn"
            animationOut="fadeOut"
            animationInTiming={1}
            animationOutTiming={1}
          >
            {this.renderContent()}
          </Modal>
        )}
        {!isFullScreen && this.renderContent()}
      </View>
    );
  }

  private renderContent(): JSX.Element {
    if (this.props.verseMode === VerseMode.Landscape) {
      return this.renderLandscapeContent();
    }

    // Bcoz of ScrollView and the requirement to detect taps on empty space as well,
    // we need to have tap detection around two areas.
    return (
      <TapDetector
        onSingleTap={this.onLyricsSingleTap}
        onDoubleTap={this.onLyricsDoubleTap}
        onLongPress={this.onLyricsLongPress}
      >
        <View style={styles.content}>
          <ScrollView styles={styles.scrollView}>
            <TapDetector
              onSingleTap={this.onLyricsSingleTap}
              onDoubleTap={this.onLyricsDoubleTap}
              onLongPress={this.onLyricsLongPress}
            >
              {this.renderLyrics()}
            </TapDetector>
          </ScrollView>
        </View>
      </TapDetector>
    );
  }

  private renderLandscapeContent(): JSX.Element {
    const width =
      Dimensions.get("window").height - StatusBar.height() - StatusBar.bottomFooterNotchSpace();
    const height = Dimensions.get("window").width;
    return (
      <View style={{ justifyContent: "center", alignItems: "center", height: "100%" }}>
        <View style={{ width, height, transform: [{ rotate: "90deg" }] }}>
          {this.renderLandscapeLyrics()}
        </View>
      </View>
    );
  }

  private renderLyrics(): JSX.Element {
    return (
      <View style={styles.lyrics}>
        {this.renderTitle()}
        {!isVerseModeStacked(this.props.verseMode) && this.renderContinuousVerses()}
        {isVerseModeStacked(this.props.verseMode) && (
          <StackedVerses song={this.props.song} verseMode={this.props.verseMode} />
        )}
      </View>
    );
  }

  private renderLandscapeLyrics(): JSX.Element {
    return (
      <>
        <StackedVerses song={this.props.song} verseMode={this.props.verseMode} />
        {this.renderLandscapeIconActions()}
      </>
    );
  }

  private renderTitle(): JSX.Element {
    const song = this.props.song;
    const verseMode = this.props.verseMode;
    const fontSize = Settings.get().lyricsFontSize;
    return (
      <View style={styles.songTitle}>
        <View style={styles.titleIconLeft}>
          {isVerseModeStacked(verseMode) && (
            <IconAction
              noCircle={true}
              name="minimize"
              family="Feather"
              onPress={this.onMinimize}
            />
          )}
        </View>
        <View
          style={{ margin: isVerseModeStacked(verseMode) ? 50 : 0, marginTop: 0, marginBottom: 0 }}
        >
          <Center hor={true} ver={true}>
            <Text fontSize={fontSize + 5} type="highlight" textAlign="center">
              {DataTransform.transformSongTitle(song.title, song.language)}
            </Text>
          </Center>
          <Center hor={true} ver={true}>
            <Text fontSize={fontSize + 3} type="lowlight" textAlign="center">
              {DataTransform.transformAuthor(song.author, song.language)}
            </Text>
          </Center>
        </View>
        <View style={styles.titleIconRight}>
          {verseMode === VerseMode.Stacked && (
            <IconAction
              noCircle={true}
              name="phone-rotate-landscape"
              family="MaterialCommunityIcons"
              onPress={this.onPortraitRotate}
            />
          )}
        </View>
      </View>
    );
  }

  private renderLandscapeIconActions(): JSX.Element {
    return (
      <>
        <View style={{ position: "absolute", bottom: 0, left: 0 }}>
          <IconAction noCircle={true} name="minimize" family="Feather" onPress={this.onMinimize} />
        </View>
        <View style={{ position: "absolute", bottom: 0, right: 0 }}>
          <IconAction
            noCircle={true}
            name="phone-rotate-portrait"
            family="MaterialCommunityIcons"
            onPress={this.onLandscapeRotate}
          />
        </View>
      </>
    );
  }

  private renderContinuousVerses(): JSX.Element {
    const song = this.props.song;
    return (
      <>
        {song.verses.map((verse, index) => (
          <View key={`${song.uid}${index}`}>
            <Verse verse={verse} song={song} index={index} key={`${song.uid}${index}`} />
          </View>
        ))}
      </>
    );
  }

  private lyricsExistToShow(): boolean {
    const s = Settings.get();
    const lyricsExistToShow =
      s.lyricsShowScript ||
      s.lyricsShowVerse ||
      (s.lyricsShowWordForWord && this.props.song.has_synonyms) ||
      (s.lyricsShowTranslation && this.props.song.has_translations);

    return lyricsExistToShow;
  }

  private onLyricsSingleTap = (): void => {
    if (this.props.verseMode === VerseMode.Normal) {
      this.props.onVerseModeChange(VerseMode.Fullscreen);
    } else if (this.props.verseMode === VerseMode.Fullscreen) {
      this.props.onVerseModeChange(VerseMode.Normal);
    }
  };

  private onLyricsDoubleTap = (): void => {
    if (this.props.verseMode === VerseMode.Normal) {
      this.props.onVerseModeChange(VerseMode.Stacked);
    } else if (this.props.verseMode === VerseMode.Fullscreen) {
      this.props.onVerseModeChange(VerseMode.Normal);
    }
  };

  private onLyricsLongPress = (): void => {
    if (this.props.verseMode === VerseMode.Normal) {
      this.props.onVerseModeChange(VerseMode.Landscape);
    } else if (this.props.verseMode === VerseMode.Fullscreen) {
      this.props.onVerseModeChange(VerseMode.Normal);
    }
  };

  private onMinimize = (): void => {
    this.props.onVerseModeChange(VerseMode.Normal);
  };

  private onPortraitRotate = (): void => {
    this.props.onVerseModeChange(VerseMode.Landscape);
  };

  private onLandscapeRotate = (): void => {
    this.props.onVerseModeChange(VerseMode.Stacked);
  };
}
