/* eslint-disable react/no-array-index-key */
import React from "react";
import { Dimensions, StyleSheet, View } from "react-native";

import ISong, { IVerse } from "../../../../data/models/server/ISong";
import Tabs, { ITab } from "../../../lib/tabs/Tabs";
import Theme from "../../../../utils/theme/Theme";
import Icon from "../../../lib/icon/Icon";
import AppDimensions from "../../../../utils/appDimensions/AppDimensions";
import ClickAreaDetector from "../../../lib/clickAreaDetector/ClickAreaDetector";
import VerseMode from "../verseModeModal/VerseMode";

import Verse from "./Verse";

interface IProps {
  song: ISong;
  verseMode: VerseMode;
}

interface IState {
  activeTabIndex: number;
}

export default class StackedVerses extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      activeTabIndex: 0
    };
  }

  public render(): JSX.Element {
    const styles = this.getStyles();
    const song = this.props.song;

    const tabs: ITab[] = song.verses.map((verse, index) => ({
      index,
      heading: this.renderDotHeading(index),
      content: this.renderTabContent(verse, index)
    }));

    return (
      <Tabs
        activeIndex={this.state.activeTabIndex}
        tabs={tabs}
        tabBarPosition="bottom"
        tabContainerStyle={styles.tabContainerStyle}
        tabBarUnderlineStyle={styles.tabBarUnderlineStyle}
        onTabChange={this.onTabChange}
      />
    );
  }

  private renderTabContent(verse: IVerse, index: number): JSX.Element {
    const song = this.props.song;
    return (
      <ClickAreaDetector
        onLeftHalfClick={this.onLeftHalfClick}
        onRightHalfClick={this.onRightHalfClick}
        onTopHalfClick={this.onTopHalfClick}
        onBottomHalfClick={this.onBottomHalfClick}
      >
        <View style={{ alignItems: "center", justifyContent: "center", flex: 1 }}>
          <Verse verse={verse} song={song} index={index} key={`${song.uid}${index}`} />
        </View>
      </ClickAreaDetector>
    );
  }

  private renderDotHeading(index: number): JSX.Element {
    const styles = this.getStyles();
    return (
      <View style={styles.dotHeading}>
        <Icon
          family="FontAwesome"
          name={index === this.state.activeTabIndex ? "circle" : "circle-thin"}
          type="highlight"
          size={8}
        />
      </View>
    );
  }

  private onTabChange = (clickedTabIndex: number): void => {
    this.setState({ activeTabIndex: clickedTabIndex });
  };

  private onLeftHalfClick = (): void => {
    if (this.props.verseMode === VerseMode.Stacked) {
      this.reduceIndex();
    }
  };

  private onRightHalfClick = (): void => {
    if (this.props.verseMode === VerseMode.Stacked) {
      this.increaseIndex();
    }
  };

  private onTopHalfClick = (): void => {
    if (this.props.verseMode === VerseMode.Landscape) {
      this.reduceIndex();
    }
  };

  private onBottomHalfClick = (): void => {
    if (this.props.verseMode === VerseMode.Landscape) {
      this.increaseIndex();
    }
  };

  private reduceIndex = (): void => {
    const newTabIndex = this.state.activeTabIndex - 1;
    if (newTabIndex >= 0) {
      this.setState({ activeTabIndex: newTabIndex });
    }
  };

  private increaseIndex = (): void => {
    const newTabIndex = this.state.activeTabIndex + 1;
    const maxTabIndex = this.props.song.verses.length - 1;
    if (newTabIndex <= maxTabIndex) {
      this.setState({ activeTabIndex: newTabIndex });
    }
  };

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  private getStyles() {
    const theme = Theme.getCurrentTheme();
    const dotWidth = AppDimensions.TouchableWidth;
    const containerWidth = this.props.song.verses.length * dotWidth;

    return StyleSheet.create({
      tabContainerStyle: {
        width: containerWidth,
        maxWidth: Dimensions.get("screen").width,
        backgroundColor: theme.backgroundPrimary,
        marginLeft: "auto",
        marginRight: "auto"
      },
      tabBarUnderlineStyle: { height: 0, display: "none" },
      dotHeading: {
        width: dotWidth,
        height: dotWidth,
        minWidth: 25,
        flexShrink: 0,
        backgroundColor: theme.backgroundPrimary
      }
    });
  }
}
