import React from "react";

import Header from "../../lib/header/Header";
import Center from "../../lib/center/Center";
import Spinner from "../../lib/spinner/Spinner";
import Screen from "../../lib/screen/Screen";
import RouteName, { ICollectionsRouteParams } from "../../../utils/navigation/RouteName";
import ActiveScreenRefs from "../../../utils/activeScreenRefs/ActiveScreenRefs";
import Data from "../../../data/Data";
import Navigation from "../../../utils/navigation/Navigation";
import {
  getCollectionTitle,
  getCollectionUid,
  ICollection
} from "../../../data/models/server/ICollection";
import { IBreadcrumb } from "../../lib/breadcrumbs/Breadcrumbs";
import ErrorScreen from "../../lib/errorScreen/ErrorScreen";
import Content from "../../lib/content/Content";
import Touchable from "../../lib/touchable/Touchable";

import CollectionsList from "./CollectionsList";

interface IState {
  loading: boolean;
  allCollections: ICollection[];
}

export default class Collections extends React.Component<{}, IState> {
  private readonly uidPathSeparator = "/";
  private ready: boolean;
  private uidPath: string;

  public constructor(props: {}) {
    super(props);
    this.ready = false;
    this.uidPath = "";
    this.state = {
      loading: true,
      allCollections: []
    };
  }

  public async componentDidMount(): Promise<void> {
    ActiveScreenRefs.add(RouteName.Collections, this);
    await this.loadCollections();
  }

  public async componentDidUpdate(): Promise<void> {
    if (this.ready) {
      this.ready = false;
      await this.loadCollections();
    }
  }

  public render(): JSX.Element {
    this.uidPath = this.getUidPath();
    const uidPathExists = Boolean(this.uidPath);

    const { currentCollections, breadcrumbs } = this.getCurrentCollectionsAndBreadcrumbs();
    const lastBreadcrumb = breadcrumbs.length > 0 ? breadcrumbs[breadcrumbs.length - 1] : undefined;

    return (
      <Screen>
        <Header
          title={lastBreadcrumb ? lastBreadcrumb.text : "Collections"}
          showBackButton={uidPathExists}
          onBackButtonPress={this.onHeaderBackButtonPress}
          rightContent={this.renderHeaderRightContent()}
        />
        <Content>{this.renderContent(currentCollections, breadcrumbs)}</Content>
      </Screen>
    );
  }

  private renderHeaderRightContent(): JSX.Element {
    const uidPathExists = Boolean(this.uidPath);
    return uidPathExists ? <Touchable /> : <></>;
  }

  private renderContent(
    currentCollections: ICollection[] | undefined,
    breadcrumbs: IBreadcrumb[]
  ): JSX.Element {
    if (this.state.loading) {
      return (
        <Center hor={true} ver={true}>
          <Spinner />
        </Center>
      );
    }

    if (!currentCollections) {
      return (
        <ErrorScreen
          text={`Oops! Unable to find the collection '${this.uidPath}'`}
          textLine2="It might have been changed or deleted."
        />
      );
    }

    return (
      <CollectionsList
        imagePath={{ uri: "gaura.jpg" }}
        uidPath={this.uidPath}
        uidPathSeparator={this.uidPathSeparator}
        currentCollections={currentCollections}
        breadcrumbs={breadcrumbs}
        reRenderCollections={this.reRenderCollections}
      />
    );
  }

  private async loadCollections(): Promise<void> {
    this.setState({ loading: true });
    const collectionsData = await Data.LoadCollections();
    this.setState({ loading: false, allCollections: collectionsData.children });
    this.ready = true;
  }

  private getUidPath(): string {
    const currentParams = Navigation.getRouteParams(RouteName.Collections);
    const uidPathParam = currentParams && (currentParams as ICollectionsRouteParams).uidPath;
    return uidPathParam || "";
  }

  private getUidsFromUidPath(): string[] {
    return this.uidPath.split(this.uidPathSeparator).filter(w => w);
  }

  private getCurrentCollectionsAndBreadcrumbs(): {
    currentCollections: ICollection[] | undefined;
    breadcrumbs: IBreadcrumb[];
  } {
    const uids = this.getUidsFromUidPath();

    let currentCollections = this.state.allCollections;
    const breadcrumbs: IBreadcrumb[] = [];
    let breadcrumbUidPath = "";

    for (let i = 0; i < uids.length; i++) {
      const uid = uids[i];
      let uidMatched = false;

      for (let j = 0; j < currentCollections.length; j++) {
        const collection = currentCollections[j];

        if (uid === getCollectionUid(collection)) {
          currentCollections = collection.children || [];
          uidMatched = true;

          breadcrumbUidPath += `${this.uidPathSeparator}${uid}`;
          breadcrumbs.push({
            text: getCollectionTitle(collection),
            active: true,
            metaData: breadcrumbUidPath
          });
          break;
        }
      }
      if (!uidMatched) {
        return { currentCollections: undefined, breadcrumbs: [] };
      }
    }

    breadcrumbs.unshift({
      text: "Collections",
      active: true,
      metaData: ""
    });
    breadcrumbs[breadcrumbs.length - 1].active = false;

    return { currentCollections, breadcrumbs };
  }

  private onHeaderBackButtonPress = (): void => {
    const uids = this.getUidsFromUidPath();
    uids.pop();
    const newUidPath = uids.join(this.uidPathSeparator);

    this.reRenderCollections(newUidPath);
  };

  private reRenderCollections = (newUidPath: string): void => {
    // Navigating again to RouteName.Collections rerenders this component, but doesn't unmount and remount.
    Navigation.navigate(RouteName.Collections, { uidPath: newUidPath });
  };
}
