import React from "react";
import { ImageURISource } from "react-native";

import { LocalFileUris } from "../../../../utils/fileSystem/FileUris";
import LocalFileSystem from "../../../../utils/fileSystem/LocalFileSystem";
import Data from "../../../../data/Data";
import ThumbnailPic, { IImageDimensions } from "../../../lib/thumbnailPic/ThumbnailPic";

interface IProps {
  imagePath: string | undefined;
  type: "thumbnail" | "image";
  dimensions?: IImageDimensions;
  noLogoWhileLoading?: boolean;
  square?: boolean;
}

interface IState {
  loading: boolean;
  picLocalFileUri: string | undefined;
}

export default class CollectionPic extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      loading: true,
      picLocalFileUri: undefined
    };
  }

  public async componentDidMount(): Promise<void> {
    await this.loadCollectionPic(this.props);
  }

  public async UNSAFE_componentWillReceiveProps(nextProps: IProps): Promise<void> {
    if (nextProps.imagePath !== this.props.imagePath) {
      await this.loadCollectionPic(nextProps);
    }
  }

  public render(): JSX.Element {
    const imageSource: ImageURISource | undefined = this.state.picLocalFileUri
      ? {
          uri: this.state.picLocalFileUri
        }
      : undefined;
    const noLogoWhileLoading = this.state.loading && this.props.noLogoWhileLoading;
    return (
      <ThumbnailPic
        type={this.props.type}
        imageSource={imageSource}
        dimensions={this.props.dimensions}
        modalHeight={512}
        noLogoWhenEmpty={noLogoWhileLoading}
      />
    );
  }

  private async loadCollectionPic(props: IProps): Promise<void> {
    if (!props.imagePath) {
      this.setState({ loading: false, picLocalFileUri: undefined });
    } else {
      this.setState({ loading: true, picLocalFileUri: undefined });

      await Data.LoadCollectionPic(props.imagePath);

      const localFileUri = LocalFileUris.collectionPic(props.imagePath);
      const localFileInfo = await LocalFileSystem.exists(localFileUri);

      const picLocalFileUri = localFileInfo.exists ? localFileUri : undefined;
      this.setState({ loading: false, picLocalFileUri });
    }
  }
}
