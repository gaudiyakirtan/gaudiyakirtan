import React from "react";
import { ImageURISource, View } from "react-native";

import {
  getCollectionDescription,
  getCollectionTitle,
  getCollectionUid,
  ICollection
} from "../../../data/models/server/ICollection";
import IconColors from "../../../utils/theme/IconColors";
import Theme from "../../../utils/theme/Theme";
import Breadcrumbs, { IBreadcrumb } from "../../lib/breadcrumbs/Breadcrumbs";
import Icon, { IconFamily } from "../../lib/icon/Icon";
import List, { IListItem } from "../../lib/lists/list/List";
import UidCircle from "../../lib/uidCircle/UidCircle";
import Text from "../../lib/text/Text";
import RouteName from "../../../utils/navigation/RouteName";
import Navigation from "../../../utils/navigation/Navigation";
import ActiveScreenRefs from "../../../utils/activeScreenRefs/ActiveScreenRefs";
import AppDimensions from "../../../utils/appDimensions/AppDimensions";
import Logo from "../../lib/logo/Logo";

import CollectionPic from "./collectionPic/CollectionPic";

interface IProps {
  imagePath: ImageURISource;
  uidPath: string;
  uidPathSeparator: string;
  currentCollections: ICollection[];
  breadcrumbs: IBreadcrumb[];
  reRenderCollections(newUidPath: string): void;
}

export default class CollectionsList extends React.Component<IProps> {
  public render(): JSX.Element {
    const listItems = this.getListItems();
    return (
      <View style={{ flex: 1 }}>
        <Breadcrumbs breadcrumbs={this.props.breadcrumbs} onPress={this.onBreadcrumbPress} />
        <List items={listItems} />
      </View>
    );
  }

  // eslint-disable-next-line react/sort-comp
  private getListItems(): IListItem[] {
    const listItems: IListItem[] = [];

    this.props.currentCollections.forEach(collection => {
      const title = getCollectionTitle(collection);
      const description = getCollectionDescription(collection);
      listItems.push({
        key: collection.uid,
        isHeader: collection.type === "heading",
        title,
        description: description !== title ? description : undefined,
        leftContent: this.renderItemLeftContent(collection),
        rightContent: this.renderItemRightContent(collection),
        leftContentWidth: AppDimensions.PicThumbNailSize + 20,
        metaData: collection,
        onClick: this.onListItemClick
      });
    });
    return listItems;
  }

  private renderItemLeftContent(collection: ICollection): JSX.Element {
    if (collection.type === "collection") {
      const theme = Theme.getCurrentTheme();
      const imagePath = collection.imagePath || { uri: "gaura.jpg" };

      if (imagePath && collection.uid !== "RECENTS") {
        return (
          <CollectionPic
            type="thumbnail"
            square={true}
            dimensions={{ width: 50, height: 50 }}
            imagePath={imagePath.uri}
          />
        );
      }

      if (collection.uid === "RECENTS") {
        const iconName = collection.uid === "RECENTS" ? "history" : "book";
        const iconFamily: IconFamily =
          collection.uid === "RECENTS" ? "MaterialIcons" : "FontAwesome";
        const iconColor =
          collection.uid === "RECENTS" ? theme.iconHighlight : IconColors.getRandomColor();
        return <Icon name={iconName} family={iconFamily} color={iconColor} size={27} />;
      }

      return <Logo />;
    }
    if (collection.type === "song") {
      const songUid = getCollectionUid(collection);
      return <UidCircle uid={songUid} />;
    }
    return <></>;
  }

  private renderItemRightContent(collection: ICollection): JSX.Element {
    if (collection.type === "collection" && collection.count !== undefined) {
      return (
        <View style={{ marginRight: 10 }}>
          <Text type="lowlight">{`(${collection.count})`}</Text>
        </View>
      );
    }
    return <></>;
  }

  private onListItemClick = (item: IListItem): void => {
    if (!item.metaData) {
      return;
    }
    const collection = item.metaData as ICollection;

    if (collection.type === "collection") {
      const collectionUid = getCollectionUid(collection);
      const newUidPath = this.props.uidPath
        ? `${this.props.uidPath}${this.props.uidPathSeparator}${collectionUid}`
        : collectionUid;

      this.props.reRenderCollections(newUidPath);
    }

    if (collection.type === "song") {
      const songUid = getCollectionUid(collection);
      Navigation.navigate(RouteName.Song, { songUid, songMD5: "ignore" });
      // TODO : Replace below hack with proper navigation fix
      /* @ts-ignore */
      ActiveScreenRefs.get(RouteName.Song).loadSong();
    }
  };

  private onBreadcrumbPress = (pressedBreadcrumb: IBreadcrumb): void => {
    const breadcrumbUidPath = pressedBreadcrumb.metaData as string;
    this.props.reRenderCollections(breadcrumbUidPath);
  };
}
