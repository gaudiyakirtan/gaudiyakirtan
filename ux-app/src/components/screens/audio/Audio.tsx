import React from "react";

import Header from "../../lib/header/Header";
import Screen from "../../lib/screen/Screen";
import ActiveScreenRefs from "../../../utils/activeScreenRefs/ActiveScreenRefs";
import RouteName from "../../../utils/navigation/RouteName";
import Content from "../../lib/content/Content";
import Text from "../../lib/text/Text";
import RowLayout, { Body, Left } from "../../lib/rowLayout/RowLayout";
import Touchable from "../../lib/touchable/Touchable";
import Navigation from "../../../utils/navigation/Navigation";
import HorizontalRule from "../../lib/horizontalRule/HorizontalRule";
import Icon from "../../lib/icon/Icon";

export default class Audio extends React.Component {
  public async componentDidMount(): Promise<void> {
    ActiveScreenRefs.add(RouteName.Audio, this);
  }

  public render(): JSX.Element {
    return (
      <Screen>
        <Header title="Audio" />
        <Content paddingLeft={15} paddingRight={15}>
          {this.renderManageDownloads()}
          <HorizontalRule noMargin={true} />
        </Content>
      </Screen>
    );
  }

  public renderManageDownloads(): JSX.Element {
    return (
      <Touchable width="100%" noHorzCenter={true} onPress={this.onDownloadsPress}>
        <RowLayout height={50}>
          <Left>
            <Icon name="md-cloud-download" type="highlight" />
          </Left>
          <Body noHorzCenter={true} paddingLeft={10}>
            <Text fontSize={16}>Manage downloads</Text>
          </Body>
        </RowLayout>
      </Touchable>
    );
  }

  private onDownloadsPress = (): void => {
    Navigation.navigate(RouteName.AudioDownloads);
  };
}
