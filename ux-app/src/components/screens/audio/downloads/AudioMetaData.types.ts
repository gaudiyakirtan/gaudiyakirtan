// Putting in a separate file to break circular dependency from ISetttings.ts

export interface IAudioFile {
  songUid: string;
  songTitle: string;
  audioUid: string;
  author: string;
  artist: string;
  fileName: string;
}

export interface IDownloadedAudioFile extends IAudioFile {
  size: number;
  modificationTime?: Date;
  isSelected: boolean;
}

export type DownloadsSortBy = keyof Pick<
  IDownloadedAudioFile,
  "songTitle" | "artist" | "size" | "modificationTime"
>;
