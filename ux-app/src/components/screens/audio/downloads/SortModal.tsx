/* eslint-disable react/no-unused-state */
import React from "react";
import { View } from "react-native";

import Settings from "../../../../utils/settings/Settings";
import ISettings from "../../../../data/models/local/ISettings";
import MultiButtonSelector, {
  ButtonKey,
  IButtonOption
} from "../../../lib/multiButtonSelector/MultiButtonSelector";
import BottomSettingsModal, {
  renderTitle
} from "../../../lib/modals/bottomSettingsModal/BottomSettingsModal";

import { DownloadsSortBy } from "./AudioMetaData.types";

interface IProps {
  open: boolean;
  onSortByUpdate(): void;
  onClose(): void;
}

// Settings.get() should be the single source of truth for the latest value of settings for every component.
// Otherwise we run into race conditions with multiple copies of settings. So, don't store settings in local state.
// Instead use a dummy time state variable to rerender.
interface IState {
  lastUpdateTime: number;
}

export default class SortModal extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      lastUpdateTime: Date.now()
    };
  }

  public render(): JSX.Element {
    return (
      <BottomSettingsModal open={this.props.open} onClose={this.props.onClose}>
        {renderTitle("Sort By", false)}
        {this.renderMultiButtonSelector()}
      </BottomSettingsModal>
    );
  }

  private renderMultiButtonSelector(): JSX.Element {
    const s = Settings.get();

    const iconName = s.downloadsSortDesc ? "long-arrow-down" : "long-arrow-up";

    const isDateActive = s.downloadsSortBy === "modificationTime";
    const isSizeActive = s.downloadsSortBy === "size";
    const isTitleActive = s.downloadsSortBy === "songTitle";
    const isArtistActive = s.downloadsSortBy === "artist";

    const buttonOptions: IButtonOption[] = [
      {
        key: "modificationTime",
        text: "Date",
        iconName: isDateActive ? iconName : undefined,
        iconFamily: "FontAwesome"
      },
      {
        key: "size",
        text: "Size",
        iconName: isSizeActive ? iconName : undefined,
        iconFamily: "FontAwesome"
      },
      {
        key: "songTitle",
        text: "Title",
        iconName: isTitleActive ? iconName : undefined,
        iconFamily: "FontAwesome"
      },
      {
        key: "artist",
        text: "Artist",
        iconName: isArtistActive ? iconName : undefined,
        iconFamily: "FontAwesome"
      }
    ];

    return (
      <View style={{ marginTop: 5 }}>
        <MultiButtonSelector
          buttonOptions={buttonOptions}
          selectedKey={s.downloadsSortBy}
          onButtonSelect={this.onButtonSelect}
        />
      </View>
    );
  }

  private onButtonSelect = (selectedKey: ButtonKey): void => {
    const s = Settings.get();
    const newDownloadsSortBy = selectedKey as DownloadsSortBy;

    if (newDownloadsSortBy === s.downloadsSortBy) {
      this.updateSettings({ ...s, downloadsSortDesc: !s.downloadsSortDesc });
    } else {
      this.updateSettings({
        ...s,
        downloadsSortBy: newDownloadsSortBy,
        downloadsSortDesc:
          newDownloadsSortBy === "modificationTime" || newDownloadsSortBy === "size"
      });
    }
  };

  private updateSettings = (newSettings: ISettings): void => {
    Settings.set(newSettings);
    this.setState({ lastUpdateTime: Date.now() });
    this.props.onSortByUpdate();
  };
}
