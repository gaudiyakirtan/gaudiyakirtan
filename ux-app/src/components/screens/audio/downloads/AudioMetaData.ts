import Data from "../../../../data/Data";
import DataTransform from "../../../../data/DataTransform";

import { IAudioFile } from "./AudioMetaData.types";

export type AllAudioFiles = { [fileName: string]: IAudioFile };

// This is a helper class that goes through the list of songs data, and
// creates a reverse mapping to easily get the info of an audiofile given it's name.
export default class AudioMetaData {
  private static allAudioFiles: AllAudioFiles;

  public static async init(): Promise<void> {
    AudioMetaData.allAudioFiles = {};
    const songList = await Data.LoadSongList();

    songList.forEach(song => {
      if (song.audio) {
        song.audio.forEach(audio => {
          AudioMetaData.allAudioFiles[audio.fn] = {
            songUid: song.uid,
            songTitle: DataTransform.transformSongTitle(song.title, song.language),
            audioUid: audio.uid,
            author: song.author,
            artist: audio.meta.artist || "",
            fileName: audio.fn
          };
        });
      }
    });
  }

  public static async getAllAudioFiles(): Promise<AllAudioFiles> {
    if (!AudioMetaData.allAudioFiles) {
      await AudioMetaData.init();
    }

    return AudioMetaData.allAudioFiles;
  }
}
