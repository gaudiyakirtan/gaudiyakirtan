/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable no-param-reassign */
import React from "react";
import { View } from "react-native";
import moment from "moment";

import ActiveScreenRefs from "../../../../utils/activeScreenRefs/ActiveScreenRefs";
import RouteName from "../../../../utils/navigation/RouteName";
import LocalFileSystem from "../../../../utils/fileSystem/LocalFileSystem";
import { LocalFileUris } from "../../../../utils/fileSystem/FileUris";
import Center from "../../../lib/center/Center";
import Spinner from "../../../lib/spinner/Spinner";
import Touchable from "../../../lib/touchable/Touchable";
import Content from "../../../lib/content/Content";
import Screen from "../../../lib/screen/Screen";
import Header from "../../../lib/header/Header";
import Icon from "../../../lib/icon/Icon";
import Text from "../../../lib/text/Text";
import ErrorScreen from "../../../lib/errorScreen/ErrorScreen";
import MoreSettingsModal, {
  IMoreSetting
} from "../../../lib/modals/moreSettingsModal/MoreSettingsModal";
import ConfirmationModal from "../../../lib/modals/confirmationModal/ConfirmationModal";
import withAudioContext, {
  IAudioContextProps
} from "../../../../context/audioContext/withAudioContext";
import List, { IListItem } from "../../../lib/lists/list/List";
import AppDimensions from "../../../../utils/appDimensions/AppDimensions";
import ArtistPic from "../../../audioBar/artistPic/ArtistPic";
import { getArtistId } from "../../../../data/models/server/ISong";
import Toast, { ToastType } from "../../../lib/toast/Toast";

import { IDownloadedAudioFile } from "./AudioMetaData.types";
import SortModal from "./SortModal";
import DownloadsData from "./DownloadsData";

interface IProps extends IAudioContextProps {}

interface IState {
  loading: boolean;
  downloadedAudioFiles: IDownloadedAudioFile[];
  sortPanelOpen: boolean;
  deleteConfirmationOpen: boolean;
  deleting: boolean;
}

class Downloads extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      loading: true,
      downloadedAudioFiles: [],
      sortPanelOpen: false,
      deleteConfirmationOpen: false,
      deleting: false
    };
  }

  public async componentDidMount(): Promise<void> {
    ActiveScreenRefs.add(RouteName.AudioDownloads, this);
    await this.getDownloadedAudioFiles();
  }

  public render(): JSX.Element {
    return (
      <Screen>
        <Header
          showBackButton={true}
          title="Manage downloads"
          leftContent={this.renderHeaderLeftContent()}
          rightContent={this.renderHeaderRightContent()}
        />
        <Content marginTop={10} marginBottom={10}>
          {this.renderContent()}
        </Content>
        <SortModal
          open={this.state.sortPanelOpen}
          onSortByUpdate={this.onSortByUpdate}
          onClose={this.onSortPanelClose}
        />
        <ConfirmationModal
          open={this.state.deleteConfirmationOpen}
          content={<Text fontSize={15}>Are you sure you want to delete selected audio files?</Text>}
          onConfirmation={this.onDeleteConfirmation}
          onCancel={this.onDeleteConfirmationCancel}
        />
      </Screen>
    );
  }

  private renderContent(): JSX.Element | undefined {
    if (this.state.loading || this.state.deleting) {
      return (
        <Center hor={true} ver={true}>
          <Spinner />
        </Center>
      );
    }
    if (this.state.downloadedAudioFiles.length === 0) {
      return <ErrorScreen text="No downloaded audio files" iconName="logo-dropbox" />;
    }
    const listItems: IListItem[] = this.state.downloadedAudioFiles.map(audioFile => ({
      key: audioFile.fileName,
      title: audioFile.songTitle, // transliterate(.., 'ben', userLang)
      description: audioFile.artist,
      leftContent: this.renderListItemLeftContent(audioFile),
      rightContent: this.renderListItemRightContent(audioFile),
      isSelected: audioFile.isSelected,
      leftContentWidth: AppDimensions.PicThumbNailSize + 15,
      metaData: audioFile.fileName,
      onClick: this.onListItemPress
    }));

    return <List items={listItems} />;
  }

  private renderHeaderRightContent(): JSX.Element {
    const selectedItemsExist = this.state.downloadedAudioFiles.find(a => a.isSelected);
    const deleteDisabled = this.state.loading || !selectedItemsExist;

    const moreSettings: IMoreSetting[] = [
      {
        text: "Sort by",
        iconName: "sort",
        iconFamily: "MaterialCommunityIcons",
        onPress: this.onSortIconPress
      },
      {
        text: "Select all",
        iconName: "add-to-list",
        iconFamily: "Entypo",
        disabled:
          this.state.loading || this.state.deleting || this.state.downloadedAudioFiles.length === 0,
        onPress: this.onSelectAllPress
      },
      {
        text: "Refresh",
        iconName: "ios-refresh",
        disabled: this.state.loading || this.state.deleting,
        onPress: this.onRefreshPress
      }
    ];

    return (
      <>
        <Touchable
          onPress={this.onDeletePress}
          width={AppDimensions.MoreIconWidth}
          disabled={deleteDisabled}
        >
          <Icon
            name="trash"
            family="FontAwesome"
            type={deleteDisabled ? "disabled" : "highlight"}
          />
        </Touchable>
        <MoreSettingsModal moreSettings={moreSettings} />
      </>
    );
  }

  private renderHeaderLeftContent(): JSX.Element | undefined {
    // Empty place holder to symmetrically center header title
    return <Touchable width={AppDimensions.MoreIconWidth} />;
  }

  private renderListItemLeftContent(audioFile: IDownloadedAudioFile): JSX.Element | undefined {
    return (
      <ArtistPic
        type="thumbnail"
        artistId={getArtistId(audioFile.audioUid)}
        artistName={audioFile.artist}
      />
    );
  }

  private renderListItemRightContent(audioFile: IDownloadedAudioFile): JSX.Element | undefined {
    const date =
      audioFile.modificationTime && moment(audioFile.modificationTime).format("MMM D, YYYY");
    return (
      <View style={{ flexDirection: "column", alignItems: "flex-end", paddingRight: 5 }}>
        <Text numberOfLines={1} fontSize={12}>
          {date}
        </Text>
        <Text numberOfLines={1} fontSize={12}>{`${audioFile.size.toFixed(1)} MB`}</Text>
      </View>
    );
  }

  private getDownloadedAudioFiles = async (): Promise<void> => {
    this.setState({ loading: true, downloadedAudioFiles: [] });
    const downloadedAudioFiles = await DownloadsData.getDownloadedAudioFiles();
    this.setState({ loading: false, downloadedAudioFiles });
  };

  private deleteSelectedAudioFiles = async (): Promise<void> => {
    this.setState({ deleting: true });

    const fileDeletePromises: { [fileName: string]: Promise<void> } = {};
    this.state.downloadedAudioFiles.forEach(audioFile => {
      if (audioFile.isSelected) {
        const fileName = audioFile.fileName;
        const localFileUri = LocalFileUris.audio(fileName);
        // Do not await here inside the for loop for fast parallel processing of promises.
        // More details here : https://eslint.org/docs/rules/no-await-in-loop
        fileDeletePromises[fileName] = LocalFileSystem.delete(localFileUri);

        if (this.isSongsAudioBarOpen(audioFile.songUid)) {
          this.props.audioContext.reset();
        }
      }
    });

    // Instead await everything here
    await Promise.all(Object.values(fileDeletePromises));

    await this.getDownloadedAudioFiles();
    Toast.show(ToastType.Success, "Successfully deleted selected files !");
    this.setState({ deleting: false });
  };

  private isSongsAudioBarOpen = (songUid: string): boolean => {
    const audioCntxt = this.props.audioContext;
    return Boolean(audioCntxt.song && audioCntxt.song.uid === songUid && audioCntxt.audioBarOpen);
  };

  private onListItemPress = (item: IListItem): void => {
    if (!item.metaData) {
      return;
    }
    const pressedFileName = item.metaData as string;
    const newDownloadedAudioFiles = [...this.state.downloadedAudioFiles];
    newDownloadedAudioFiles.forEach(downloadedAudioFile => {
      if (downloadedAudioFile.fileName === pressedFileName) {
        downloadedAudioFile.isSelected = !downloadedAudioFile.isSelected;
      }
    });

    this.setState({ downloadedAudioFiles: newDownloadedAudioFiles });
  };

  private onDeletePress = async (): Promise<void> => {
    this.setState({ deleteConfirmationOpen: true });
  };

  private onDeleteConfirmation = (): void => {
    this.setState({ deleteConfirmationOpen: false }, () => {
      // Even after setting 'deleteConfirmationOpen' to false and waiting for setState callback,
      // modal is not closing before loading sign of deletion shows up.
      // So, adding tiny delay to let modal close before we start deletion.
      setTimeout(() => {
        this.deleteSelectedAudioFiles();
      }, 250);
    });
  };

  private onDeleteConfirmationCancel = (): void => {
    this.setState({ deleteConfirmationOpen: false });
  };

  private onSortIconPress = (): void => {
    this.setState(prevState => ({ sortPanelOpen: !prevState.sortPanelOpen }));
  };

  private onSortPanelClose = (): void => {
    this.setState({ sortPanelOpen: false });
  };

  private onSortByUpdate = (): void => {
    this.setState(prevState => ({
      downloadedAudioFiles: DownloadsData.sort(prevState.downloadedAudioFiles)
    }));
  };

  private onRefreshPress = async (): Promise<void> => {
    // MoreSettingsModal is not closing before loading sign of refresh shows up.
    // So, adding tiny delay to let modal to close before we start refresh.
    // This is similar race condition for a modal not closing immediately as in onDeleteConfirmation()
    setTimeout(async () => {
      await this.getDownloadedAudioFiles();
    }, 250);
  };

  private onSelectAllPress = async (): Promise<void> => {
    const newDownloadedAudioFiles = [...this.state.downloadedAudioFiles];
    newDownloadedAudioFiles.forEach(downloadedAudioFile => {
      downloadedAudioFile.isSelected = true;
    });

    this.setState({ downloadedAudioFiles: newDownloadedAudioFiles });
  };
}

const downloadsWithContext = withAudioContext(Downloads);
// eslint-disable-next-line import/prefer-default-export
export { downloadsWithContext as Downloads };
