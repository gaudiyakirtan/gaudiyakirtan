import { FileInfo } from "expo-file-system";

import { LocalDirectory, LocalFileUris } from "../../../../utils/fileSystem/FileUris";
import LocalFileSystem from "../../../../utils/fileSystem/LocalFileSystem";
import Settings from "../../../../utils/settings/Settings";
import TransliterationUtils from "../../../../utils/transliteration/TransliterationUtils";

import AudioMetaData, { AllAudioFiles } from "./AudioMetaData";
import { IDownloadedAudioFile } from "./AudioMetaData.types";

export default class DownloadsData {
  public static async getDownloadedAudioFiles(): Promise<IDownloadedAudioFile[]> {
    const allAudioFiles: AllAudioFiles = await AudioMetaData.getAllAudioFiles();
    const downloadedFileNames = await LocalFileSystem.readDirectory(LocalDirectory.audio);

    const fileInfoPromises: { [fileName: string]: Promise<FileInfo | undefined> } = {};
    downloadedFileNames.forEach(fileName => {
      const localFileUri = LocalFileUris.audio(fileName);
      // Do not await here inside the for loop for fast parallel processing of promises.
      // More details here : https://eslint.org/docs/rules/no-await-in-loop
      fileInfoPromises[fileName] = LocalFileSystem.info(localFileUri);
    });

    // Instead await everything here
    await Promise.all(Object.values(fileInfoPromises));

    const downloadedAudioFiles: IDownloadedAudioFile[] = [];

    const fileNames = Object.keys(fileInfoPromises);
    for (let i = 0; i < fileNames.length; i++) {
      const fileName = fileNames[i];

      // Below await should resolve instantly as it was already resolved above once,
      // and shouldn't cause perf issue being inside a loop.
      // eslint-disable-next-line no-await-in-loop
      const fileInfo = await fileInfoPromises[fileName];
      const audioFileInfo = allAudioFiles[fileName];

      if (audioFileInfo && fileInfo) {
        downloadedAudioFiles.push({
          ...audioFileInfo,
          size: fileInfo.size ? fileInfo.size / 1000000 : 0,
          modificationTime: fileInfo.modificationTime
            ? new Date(fileInfo.modificationTime * 1000)
            : undefined,
          isSelected: false
        });
      }
    }

    return DownloadsData.sort(downloadedAudioFiles);
  }

  public static sort(downloadedAudioFiles: IDownloadedAudioFile[]): IDownloadedAudioFile[] {
    const settings = Settings.get();

    const sortedFiles = [...downloadedAudioFiles];
    sortedFiles.sort((a, b) => {
      let result = 0;
      switch (settings.downloadsSortBy) {
        case "songTitle":
          result =
            TransliterationUtils.unDiac(a.songTitle.toLowerCase()).replace(/\(|‘/g, "") <
            TransliterationUtils.unDiac(b.songTitle.toLowerCase()).replace(/\(|‘/g, "")
              ? -1
              : 1;
          break;
        case "artist":
          result = a.artist.toLocaleLowerCase() < b.artist.toLocaleLowerCase() ? -1 : 1;
          break;
        case "size":
          result = a.size < b.size ? -1 : 1;
          break;
        case "modificationTime":
        default:
          result =
            a.modificationTime && b.modificationTime && a.modificationTime < b.modificationTime
              ? -1
              : 1;
      }
      return settings.downloadsSortDesc ? result * -1 : result;
    });

    return sortedFiles;
  }
}
