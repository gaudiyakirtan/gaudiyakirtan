/**
 * ReactNative doesn't have out of box support for SVG files.
 *
 * So, this is needed along with 'react-native-svg', 'react-native-svg-transformer' and 'metro.config.js'
 * to display svg files in ReactNative.
 *
 * More details : https://github.com/kristerkari/react-native-svg-transformer
 */

declare module "*.svg" {
  import React from "react";
  import { SvgProps } from "react-native-svg";

  const content: React.FC<SvgProps>;
  export default content;
}
