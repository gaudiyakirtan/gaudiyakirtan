/* eslint-disable no-undef */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable @typescript-eslint/no-var-requires */

/**
 * ReactNative doesn't have out of box support for SVG files.
 *
 * So, this is needed along with 'react-native-svg', ''react-native-svg-transformer' and 'svg.d.ts'
 * to display svg files in ReactNative.
 *
 * More details : https://github.com/kristerkari/react-native-svg-transformer
 */

const { getDefaultConfig } = require("metro-config");

module.exports = (async () => {
  const {
    resolver: { sourceExts, assetExts }
  } = await getDefaultConfig();
  return {
    transformer: {
      babelTransformerPath: require.resolve("react-native-svg-transformer")
    },
    resolver: {
      assetExts: assetExts.filter(ext => ext !== "svg"),
      sourceExts: [...sourceExts, "svg"]
    }
  };
})();
