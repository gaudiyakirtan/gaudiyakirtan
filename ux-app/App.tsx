// Moved App.tsx into /src so that all source code is within /src folder and only config files are outside /src
// But node_modules\expo\AppEntry.js refers to App from this location at the root of the project.
// So, this file exists to simply import and export ./src/App so that AppEntry.js can consume it.
import App from "./src/App";

export default App;
